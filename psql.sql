////////////////////////////////////////

DELIMITER $$

CREATE TRIGGER user_after_insert AFTER INSERT ON users
FOR EACH ROW 
BEGIN
	INSERT INTO role_user (role_id, user_id) VALUES (NEW.role_id, NEW.id);
END $$
DELIMITER ;

/////////////////////////////////////

DELIMITER $$

CREATE TRIGGER user_after_update AFTER UPDATE ON users
FOR EACH ROW BEGIN
	IF OLD.role_id != NEW.role_id THEN
		UPDATE role_user SET role_id = NEW.role_id WHERE user_id = NEW.id;  	
	END IF;
END $$
DELIMITER ;