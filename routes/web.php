<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Auth::routes();
/** Home **/
Route::get('/', 'HomeController@index')->name('home');
/** Reporte PDF **/
Route::post('pdf', 'PdfController@pdf')->name('pdf');

/** Ruta Logs **/
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->name('logs')
    ->middleware('permission:logs.index');

/** Reportes en pdf **/
Route::post('pdf', 'PdfController@pdf')->name('pdf');

/** Trigger **/
Route::get('triggerBuilder', 'TriggerController@index')->name('trigger.index')
    ->middleware('permission:trigger.index');

Route::post('triggerBuilder', 'TriggerController@create')->name('trigger.index')
    ->middleware('permission:trigger.index');

/** Patterns **/
Route::get('modelos', 'PatternController@index')->name('pattern.index')
    ->middleware('permission:pattern.index');

Route::get('patterns', 'PatternController@list')->name('pattern.list')
    ->middleware('permission:pattern.index');

Route::post('pattern_names', 'PatternController@names')->name('pattern.names');

Route::post('pattern', 'PatternController@store')->name('pattern.store')
    ->middleware('permission:pattern.create');

Route::get('pattern/{pattern}', 'PatternController@show')->name('pattern.show')
    ->middleware('permission:pattern.edit');

Route::put('pattern/{pattern}', 'PatternController@update')->name('pattern.update')
    ->middleware('permission:pattern.edit');

Route::post('pattern/{pattern}', 'PatternController@activate')->name('pattern.activate')
    ->middleware('permission:pattern.edit');

Route::delete('pattern/{pattern}', 'PatternController@destroy')->name('pattern.destroy')
    ->middleware('permission:pattern.destroy');

Route::get('pattern/{pattern}/{model}/times', 'PatternController@times')->name('pattern.times')
    ->middleware('permission:pattern.index');

Route::get('pattern/{pattern}/{model}/costs', 'PatternController@costs')->name('pattern.costs')
    ->middleware('permission:pattern.index');

Route::get('pattern/{pattern}/{model}/cost_time_excel', 'PatternController@role_times_excel')->name('pattern.role_times_excel')
    ->middleware('permission:pattern.index');

Route::get('pattern/{pattern}/{model}/role_times', 'PatternController@role_times')->name('pattern.role_times')
    ->middleware('permission:pattern.index');

Route::post('pattern_graphic_data', 'PatternController@graphic_data')->name('pattern.graphic_data');
//->middleware('permission:.index');

/** Pattern document types **/
Route::get('documentos_modelo', 'PatternDocumentTypeController@index')->name('patternDocumentType.index')
    ->middleware('permission:patternDocumentType.index');

Route::get('pattern_document_types', 'PatternDocumentTypeController@list')->name('patternDocumentType.list')
    ->middleware('permission:patternDocumentType.index');

Route::post('pattern_document_type', 'PatternDocumentTypeController@store')->name('patternDocumentType.store')
    ->middleware('permission:patternDocumentType.create');

Route::get('pattern_document_type/{pattern_document_type}', 'PatternDocumentTypeController@show')->name('patternDocumentType.show')
    ->middleware('permission:patternDocumentType.edit');

Route::put('pattern_document_type/{pattern_document_type}', 'PatternDocumentTypeController@update')->name('patternDocumentType.update')
    ->middleware('permission:patternDocumentType.edit');

Route::post('pattern_document_type/{pattern_document_type}', 'PatternDocumentTypeController@activate')->name('patternDocumentType.activate')
    ->middleware('permission:patternDocumentType.edit');

Route::delete('pattern_document_type/{pattern_document_type}', 'PatternDocumentTypeController@destroy')->name('patternDocumentType.destroy')
    ->middleware('permission:patternDocumentType.destroy');

/** People **/
Route::get('personas', 'PersonController@index')->name('person.index')
    ->middleware('permission:person.index');

Route::get('people', 'PersonController@list')->name('person.list')
    ->middleware('permission:person.index');

Route::get('select_people', 'PersonController@search')->name('person.search')
    ->middleware('permission:person.index');

Route::post('people', 'PersonController@search')->name('person.search')
    ->middleware('permission:person.index');

Route::post('person', 'PersonController@store')->name('person.store')
    ->middleware('permission:person.create');

Route::post('personUser', 'PersonController@user_create')->name('person.user_create')
    ->middleware('permission:person.create');

Route::get('person/{person}', 'PersonController@show')->name('person.show')
    ->middleware('permission:person.edit');

Route::put('person/{person}', 'PersonController@update')->name('person.update')
    ->middleware('permission:person.edit');

Route::post('person/{person}', 'PersonController@activate')->name('person.activate')
    ->middleware('permission:person.edit');

Route::delete('person/{person}', 'PersonController@destroy')->name('person.destroy')
    ->middleware('permission:person.destroy');

Route::get('persona/nueva', 'PersonController@create')->name('person.create')
    ->middleware('permission:person.create');

/** Roles **/
Route::get('roles_de_usuario', 'RoleController@index')->name('role.index')
    ->middleware('permission:role.index');

Route::get('roles', 'RoleController@list')->name('role.list')
    ->middleware('permission:role.index');

Route::post('role', 'RoleController@store')->name('role.store')
    ->middleware('permission:role.create');

Route::get('role/{role}', 'RoleController@show')->name('role.show')
    ->middleware('permission:role.edit');

Route::put('role/{role}', 'RoleController@update')->name('role.update')
    ->middleware('permission:role.edit');

Route::post('role/{role}', 'RoleController@activate')->name('role.activate')
    ->middleware('permission:role.edit');

Route::delete('role/{role}', 'RoleController@destroy')->name('role.destroy')
    ->middleware('permission:role.destroy');

/** User **/
Route::get('usuarios', 'UserController@index')->name('user.index')
    ->middleware('permission:user.index');

Route::get('users', 'UserController@list')->name('user.list')
    ->middleware('permission:user.index');

Route::post('user', 'UserController@store')->name('user.store')
    ->middleware('permission:user.create');

Route::get('user/{user}', 'UserController@show')->name('user.show')
    ->middleware('permission:user.edit');

Route::put('user/{user}', 'UserController@update')->name('user.update')
    ->middleware('permission:user.edit');

Route::post('user/{user}', 'UserController@activate')->name('user.activate')
    ->middleware('permission:user.edit');

Route::delete('user/{user}', 'UserController@destroy')->name('user.destroy')
    ->middleware('permission:user.destroy');

Route::post('user/change_password/{user}', 'UserController@changePassword')->name('user.change_password');

/** Company **/
Route::get('empresas', 'CompanyController@index')->name('company.index')
    ->middleware('permission:company.index');

Route::get('companies', 'CompanyController@list')->name('company.list')
    ->middleware('permission:company.index');

Route::post('companies', 'CompanyController@search')->name('company.search')
    ->middleware('permission:company.index');

Route::get('select_companies', 'CompanyController@search')->name('company.search')
    ->middleware('permission:company.index');

Route::get('empresa/{company}', 'CompanyController@view')->name('company.view')
    ->middleware('permission:company.edit');

Route::get('company/{professional}', 'CompanyController@show')->name('company.show')
    ->middleware('permission:company.edit');

Route::post('company', 'CompanyController@store')->name('company.store')
    ->middleware('permission:company.create');

Route::get('company/{company}', 'CompanyController@show')->name('company.show')
    ->middleware('permission:company.edit');

Route::put('company/{company}', 'CompanyController@update')->name('company.update')
    ->middleware('permission:company.edit');

Route::post('company/{company}', 'CompanyController@activate')->name('company.activate')
    ->middleware('permission:company.edit');

Route::delete('company/{company}', 'CompanyController@destroy')->name('company.destroy')
    ->middleware('permission:company.destroy');

/** Document **/
Route::get('documents', 'DocumentController@list')->name('document.list')
    ->middleware('permission:document.index');

Route::get('documents/{pattern_id}/{model_id}', 'DocumentController@list')->name('document.list')
    ->middleware('permission:document.index');

Route::get('document', 'DocumentController@index')->name('document.index')
    ->middleware('permission:document.index');

Route::get('document/versions/{document}', 'DocumentController@versions')->name('document.versions')
    ->middleware('permission:document.index');

Route::get('document/download/{document}', 'DocumentController@download')->name('document.download')
    ->middleware('permission:document.index');

Route::get('document/download/{document}/{version}/{name}', 'DocumentController@download')->name('document.download')
    ->middleware('permission:document.index');

Route::post('document', 'DocumentController@store')->name('document.store')
    ->middleware('permission:document.create');

Route::get('document/preSignedURL', 'DocumentController@presignedURL')->name('document.preSignedURL')
    ->middleware('permission:document.create');

Route::get('document/{document}', 'DocumentController@show')->name('document.show')
    ->middleware('permission:document.edit');

Route::put('document/{document}', 'DocumentController@update')->name('document.update')
    ->middleware('permission:document.edit');

Route::post('document/{document}', 'DocumentController@activate')->name('document.activate')
    ->middleware('permission:document.activate');

Route::delete('document/{document}', 'DocumentController@destroy')->name('document.destroy')
    ->middleware('permission:document.destroy');


/** Professional **/
Route::get('profesionales', 'ProfessionalController@index')->name('professional.index')
    ->middleware('permission:professional.index');

Route::get('professionals', 'ProfessionalController@list')->name('professional.list')
    ->middleware('permission:professional.index');

Route::post('professionals', 'ProfessionalController@search')->name('professional.search')
    ->middleware('permission:professional.index');

Route::get('select_professionals', 'ProfessionalController@search')->name('professional.search')
    ->middleware('permission:professional.index');

Route::post('professional', 'ProfessionalController@store')->name('professional.store')
    ->middleware('permission:professional.create');

Route::get('professional/{professional}', 'ProfessionalController@show')->name('professional.show')
    ->middleware('permission:professional.edit');

Route::put('professional/{professional}', 'ProfessionalController@update')->name('professional.update')
    ->middleware('permission:professional.edit');

Route::post('professional/{professional}', 'ProfessionalController@activate')->name('professional.activate')
    ->middleware('permission:professional.edit');

Route::delete('professional/{professional}', 'ProfessionalController@destroy')->name('professional.destroy')
    ->middleware('permission:professional.destroy');

/** Agreement **/
Route::get('contratos', 'AgreementController@index')->name('agreement.index')
    ->middleware('permission:agreement.index');

Route::get('contrato/{agreement}', 'AgreementController@view')->name('agreement.view')
    ->middleware('permission:agreement.index');

Route::get('agreement_search', 'AgreementController@search')->name('agreement.search')
    ->middleware('permission:agreement.index');

Route::post('agreements', 'AgreementController@list')->name('agreement.list')
    ->middleware('permission:agreement.index');

Route::get('agreements', 'AgreementController@list_agreements')->name('agreement.list_agreements')
    ->middleware('permission:agreement.index');

Route::post('agreement', 'AgreementController@store')->name('agreement.store')
    ->middleware('permission:agreement.create');

Route::get('agreement/{agreement}', 'AgreementController@show')->name('agreement.show')
    ->middleware('permission:agreement.edit');

Route::put('agreement/{agreement}', 'AgreementController@update')->name('agreement.update')
    ->middleware('permission:agreement.edit');

Route::put('agreement/{agreement}/details', 'AgreementController@details')->name('agreement.details')
    ->middleware('permission:agreement.edit');

Route::put('agreement/{agreement}/progress', 'AgreementController@progress')->name('agreement.progress')
    ->middleware('permission:agreement.edit');

Route::post('agreement/{agreement}', 'AgreementController@activate')->name('agreement.activate')
    ->middleware('permission:agreement.edit');

Route::delete('agreement/{agreement}', 'AgreementController@destroy')->name('agreement.destroy')
    ->middleware('permission:agreement.destroy');

Route::post('agreement_billing/{agreement}', 'AgreementController@conf_billing')->name('agreement.billing')
    ->middleware('permission:agreement.billing');

/** AgreementTeams **/
Route::get('agreement_team', 'AgreementTeamController@index')->name('agreement_team.index')
    ->middleware('permission:agreement_team.index');

Route::get('agreement_teams/{agreement_id}', 'AgreementTeamController@list')->name('agreement_team.list')
    ->middleware('permission:agreement_team.index');

Route::get('agreement_team/{agreement_id}', 'AgreementTeamController@search')->name('agreement_team.search')
    ->middleware('permission:agreement_team.index');

Route::get('agreement_team/{agreement_id}/{role_id}', 'AgreementTeamController@search')->name('agreement_team.search')
    ->middleware('permission:agreement_team.index');

Route::post('agreement_team', 'AgreementTeamController@store')->name('agreement_team.store')
    ->middleware('permission:agreement_team.create');

Route::delete('agreement_team/{agreement_team}', 'AgreementTeamController@destroy')->name('agreement_team.destroy')
    ->middleware('permission:agreement_team.destroy');

/** AgreementRoleCosts **/
Route::get('agreement_role_costs/{agreement_id}', 'AgreementRoleCostController@list')->name('agreement_role_costs.list')
    ->middleware('permission:agreement_role_cost.index');

Route::post('agreement_role_cost', 'AgreementRoleCostController@store')->name('agreement_role_cost.store')
    ->middleware('permission:agreement_role_cost.create');

/** Projects **/
Route::get('proyectos', 'ProjectController@index')->name('project.index')
    ->middleware('permission:project.index');

Route::get('proyecto/{project}', 'ProjectController@view')->name('project.view')
    ->middleware('permission:project.index');

Route::post('projects', 'ProjectController@list')->name('project.list')
    ->middleware('permission:project.index');

Route::get('projects/{agreement}', 'ProjectController@list_project')->name('project.listProject')
    ->middleware('permission:project.index');

Route::post('project', 'ProjectController@store')->name('project.store')
    ->middleware('permission:project.create');

Route::get('project/{project}', 'ProjectController@show')->name('project.show')
    ->middleware('permission:project.index');

Route::put('project/{project}', 'ProjectController@update')->name('project.update')
    ->middleware('permission:project.edit');

Route::put('project/{project}/progress', 'ProjectController@progress')->name('project.progress')
    ->middleware('permission:project.edit');

Route::post('project/{project}', 'ProjectController@activate')->name('project.activate')
    ->middleware('permission:project.edit');

Route::delete('project/{project}', 'ProjectController@destroy')->name('project.destroy')
    ->middleware('permission:project.destroy');

/** Products **/
Route::get('productos', 'ProductController@index')->name('product.index')
    ->middleware('permission:product.index');

Route::get('producto/{product}', 'ProductController@view')->name('product.view')
    ->middleware('permission:product.index');

Route::post('products', 'ProductController@list')->name('product.list')
    ->middleware('permission:product.index');

Route::get('products/{project}', 'ProductController@list_product')->name('product.listProduct')
    ->middleware('permission:product.index');

Route::get('product', 'ProductController@index')->name('product.index')
    ->middleware('permission:product.index');

Route::post('product', 'ProductController@store')->name('product.store')
    ->middleware('permission:product.create');

Route::get('product/{product}', 'ProductController@show')->name('product.show')
    ->middleware('permission:product.index');

Route::put('product/{product}', 'ProductController@update')->name('product.update')
    ->middleware('permission:product.edit');

Route::put('product/{product}/progress', 'ProductController@progress')->name('product.progress')
    ->middleware('permission:product.edit');

Route::post('product/{product}', 'ProductController@activate')->name('product.activate')
    ->middleware('permission:product.edit');

Route::delete('product/{product}', 'ProductController@destroy')->name('product.destroy')
    ->middleware('permission:product.destroy');

Route::get('audit_product/{type}/{product}', 'ProductController@auditory')->name('product.auditory')
    ->middleware('permission:product.index');

Route::get('flujo_caja', 'ProductController@view_cash')->name('product.view_cash')
    ->middleware('permission:product.cash');

Route::post('cash_flow', 'ProductController@cash')->name('product.cash')
    ->middleware('permission:product.cash');

/** Activities **/
Route::get('actividades', 'ActivityController@index')->name('activity.index')
    ->middleware('permission:activity.index');

Route::get('actividad/{activity}', 'ActivityController@view')->name('activity.view')
    ->middleware('permission:activity.index');

Route::get('activities/{pattern_id}/{model_id}', 'ActivityController@list')->name('activity.list')
    ->middleware('permission:activity.index');

Route::post('activities', 'ActivityController@list')->name('activity.list')
    ->middleware('permission:activity.index');

Route::post('activity', 'ActivityController@store')->name('activity.store')
    ->middleware('permission:activity.create');

Route::get('activity/{activity}', 'ActivityController@show')->name('activity.show')
    ->middleware('permission:activity.edit');

Route::get('activity/{activity}/times', 'ActivityController@times')->name('activity.times')
    ->middleware('permission:activity.index');

Route::get('activity/{activity}/costs', 'ActivityController@costs')->name('activity.costs')
    ->middleware('permission:activity.index');

Route::get('activity/{activity}/executed_times', 'ActivityController@executed_times')->name('activity.executed_times')
    ->middleware('permission:activity.index');

Route::get('activity/{activity}/executed_costs', 'ActivityController@executed_costs')->name('activity.executed_costs')
    ->middleware('permission:activity.index');

Route::put('activity/{activity}', 'ActivityController@update')->name('activity.update')
    ->middleware('permission:activity.edit');

Route::put('activity/{activity}/progress', 'ActivityController@progress')->name('activity.progress')
    ->middleware('permission:activity.edit');

Route::post('activity/{activity}', 'ActivityController@activate')->name('activity.activate')
    ->middleware('permission:activity.edit');

Route::delete('activity/{activity}', 'ActivityController@destroy')->name('activity.destroy')
    ->middleware('permission:activity.destroy');

/** ActivityEstimatedTimes **/
Route::post('activity_estimated_times', 'ActivityEstimatedTimeController@store')->name('activity_estimated_time.store')
    ->middleware('permission:activity_estimated_time.create');

/** ActivityExecutedTimes **/
Route::get('horas_ejecutadas', 'ActivityExecutedTimeController@index')->name('activity_executed_time.index')
    ->middleware('permission:activity_time.index');

Route::post('activity_executed_times', 'ActivityExecutedTimeController@list')->name('activity_executed_time.list')
    ->middleware('permission:activity_time.index');

Route::post('activity_executed_time', 'ActivityExecutedTimeController@store')->name('activity_executed_time.store')
    ->middleware('permission:activity_time.create');

Route::put('activity_executed_time/{activity_executed_time}', 'ActivityExecutedTimeController@update')->name('activity_executed_time.update')
    ->middleware('permission:activity_time.update');

Route::delete('activity_executed_time/{activity_executed_time}', 'ActivityExecutedTimeController@destroy')->name('activity_executed_time.destroy')
    ->middleware('permission:activity_time.destroy');

/** ActivityEstimatedCosts **/
Route::post('activity_estimated_costs', 'ActivityEstimatedCostController@store')->name('activity_estimated_cost.store')
    ->middleware('permission:activity_estimated_cost.create');

/** ActivityExecutedCosts **/
Route::post('activity_executed_costs', 'ActivityExecutedCostController@store')->name('activity_executed_cost.store')
    ->middleware('permission:activity_executed_cost.create');

/** ActivityTimes **/
Route::get('tiempo_actividades', 'ActivityTimeController@index')->name('activity_time.index')
    ->middleware('permission:activity_time.index');

Route::get('activity_times', 'ActivityTimeController@list')->name('activity_time.list')
    ->middleware('permission:activity_time.index');

Route::get('activity_times/{activity_id}', 'ActivityTimeController@list_activity')->name('activity_time.list_activity')
    ->middleware('permission:activity_time.index');

Route::post('activity_times', 'ActivityTimeController@search')->name('activity_time.search')
    ->middleware('permission:activity_time.index');

Route::post('activity_time', 'ActivityTimeController@store')->name('activity_time.store')
    ->middleware('permission:activity_time.create');

Route::get('activity_time/{activity_time}', 'ActivityTimeController@show')->name('activity_time.show')
    ->middleware('permission:activity_time.edit');

Route::put('activity_time/{activity_time}', 'ActivityTimeController@update')->name('activity_time.update')
    ->middleware('permission:activity_time.edit');

Route::post('activity_time/{activity_time}', 'ActivityTimeController@activate')->name('activity_time.activate')
    ->middleware('permission:activity_time.edit');

Route::delete('activity_time/{activity_time}', 'ActivityTimeController@destroy')->name('activity_time.destroy')
    ->middleware('permission:activity_time.destroy');

/** Notification **/
Route::get('notificaciones', 'NotificationController@index')->name('notification.index')
    ->middleware('permission:notification.index');

Route::get('notifications/{type}', 'NotificationController@list')->name('notification.list')
    ->middleware('permission:notification.index');

Route::post('notification', 'NotificationController@store')->name('notification.store')
    ->middleware('permission:notification.create');

Route::get('notification/{notification}/{type}', 'NotificationController@show')->name('notification.show')
    ->middleware('permission:notification.create');

Route::put('notification/{notification}', 'NotificationController@update')->name('notification.update')
    ->middleware('permission:notification.create');

Route::post('notification/{notification}', 'NotificationController@activate')->name('notification.activate')
    ->middleware('permission:notification.edit');

Route::delete('notification/{notification}', 'NotificationController@destroy')->name('notification.destroy')
    ->middleware('permission:notification.destroy');

Route::get('userNotification', 'NotificationController@user_notification')->name('notification.user');

/** Facturación **/
Route::get('facturaciones', 'BillingController@index')->name('billing.index')
    ->middleware('permission:billing.index');

Route::post('billings', 'BillingController@list')->name('billing.list')
    ->middleware('permission:billing.index');

Route::get('billings/{agreement_id}', 'BillingController@list_billing')->name('billing.agreement')
    ->middleware('permission:billing.index');

Route::get('facturacion/crear', 'BillingController@create')->name('billing.new')
    ->middleware('permission:billing.create');

Route::get('facturacion/{id}', 'BillingController@view')->name('billing.view')
    ->middleware('permission:billing.index');

Route::get('billing/{id}', 'BillingController@show')->name('billing.show')
    ->middleware('permission:billing.index');

Route::put('billing/{billing}', 'BillingController@update')->name('billing.update')
    ->middleware('permission:billing.create');

Route::delete('billing/{billing}', 'BillingController@destroy')->name('billing.destroy')
    ->middleware('permission:billing.destroy');

Route::get('billing_close/{id}', 'BillingController@close')->name('billing.close')
    ->middleware('permission:billing.close');

Route::get('product_billing/{agreement_id}', 'ProductController@list_billing')->name('billing.product')
    ->middleware('permission:billing.create');

Route::get('billing_product/{agreement_id}', 'BillingController@list_billing')->name('billing.list')
    ->middleware('permission:agreement.billing');

Route::get('billing/{billing_id}/{product_id}', 'BillingController@add_product')->name('billing.add_product')
    ->middleware('permission:billing.create');

Route::post('billing', 'BillingController@store')->name('billing.store')
    ->middleware('permission:billing.create');

Route::get('billing_detail/{id}', 'BillingController@del_billing_detail')->name('billing.del_detail')
    ->middleware('permission:billing.create');

Route::get('billing_document/{id}/{document}/{version}', 'BillingController@billing_document')->name('billing.document')
    ->middleware('permission:billing.create');

Route::get('billing_document/{id}/{document}', 'BillingController@billing_delete_document')->name('billing.document')
    ->middleware('permission:billing.create');

Route::get('factura_report/{id}', 'BillingController@report')->name('billing.report')
    ->middleware('permission:billing.report');

Route::get('factura_report/{id}/{online}', 'BillingController@report')->name('billing.report')
    ->middleware('permission:billing.report');

/** Auditoria **/
Route::get('audit/{pattern}/{model}', 'AuditController@list')->name('audit.view');

/** SystemParameter **/
Route::get('parametros_sistema', 'SystemParameterController@index')->name('system_parameter.index')
    ->middleware('permission:system_parameter.index');

Route::get('system_parameter', 'SystemParameterController@list')->name('system_parameter.list')
    ->middleware('permission:system_parameter.index');

Route::post('system_parameter', 'SystemParameterController@store')->name('system_parameter.store')
    ->middleware('permission:system_parameter.create');

Route::get('system_parameter/{system_parameter}', 'SystemParameterController@show')->name('system_parameter.show')
    ->middleware('permission:system_parameter.edit');

Route::put('system_parameter/{system_parameter}', 'SystemParameterController@update')->name('system_parameter.update')
    ->middleware('permission:system_parameter.edit');

/** Activity Cost Category **/
Route::get('categorias_costo', 'ActivityCostCategoryController@index')->name('activity_cost_category.index')
    ->middleware('permission:activity_cost_category.index');

Route::get('activity_cost_categories', 'ActivityCostCategoryController@list')->name('activity_cost_category.list')
    ->middleware('permission:activity_cost_category.index');

Route::post('activity_cost_categories', 'ActivityCostCategoryController@store')->name('activity_cost_category.store')
    ->middleware('permission:activity_cost_category.create');

Route::get('activity_cost_categories/{id}', 'ActivityCostCategoryController@show')->name('activity_cost_category.show')
    ->middleware('permission:activity_cost_category.edit');

Route::put('activity_cost_categories/{id}', 'ActivityCostCategoryController@update')->name('activity_cost_category.update')
    ->middleware('permission:activity_cost_category.edit');

Route::post('activity_cost_categories/{id}', 'ActivityCostCategoryController@activate')->name('activity_cost_category.activate')
    ->middleware('permission:activity_cost_category.edit');

Route::delete('activity_cost_categories/{id}', 'ActivityCostCategoryController@destroy')->name('activity_cost_category.destroy')
    ->middleware('permission:activity_cost_category.destroy');
//

Route::post('activity_cost_subcategories', 'ActivityCostSubCategoryController@store')->name('activity_cost_subcategories.store')
    ->middleware('permission:activity_cost_category.edit');

Route::get('activity_cost_subcategories/{id}', 'ActivityCostSubCategoryController@show')->name('activity_cost_subcategories.show')
    ->middleware('permission:activity_cost_category.edit');

Route::put('activity_cost_subcategories/{id}', 'ActivityCostSubCategoryController@update')->name('activity_cost_subcategories.update')
    ->middleware('permission:activity_cost_category.edit');

Route::post('activity_cost_subcategories/{id}', 'ActivityCostSubCategoryController@activate')->name('activity_cost_subcategories.activate')
    ->middleware('permission:activity_cost_category.edit');

Route::delete('activity_cost_subcategories/{id}', 'ActivityCostSubCategoryController@destroy')->name('activity_cost_subcategories.destroy')
    ->middleware('permission:activity_cost_category.edit');

/** ActivityEstimatedCosts **/
Route::get('parametro_notificacion', 'ParNotificationController@index')->name('par_notification.index')
    ->middleware('permission:par_notification.index');

Route::get('par_notifications', 'ParNotificationController@list')->name('par_notification.search')
    ->middleware('permission:par_notification.index');

Route::get('action_notifications', 'ParNotificationController@action')->name('par_notification.action');

//Main - Navigation
Route::get('buscador', 'SearchController@index')->name('search.index');
Route::post('search', 'SearchController@list')->name('search.list');
Route::get('calendario', 'ActivityExecutedTimeController@create')->name('activity_executed_time.create');
Route::get('activity_executed_time/calendar', 'ActivityExecutedTimeController@calendar')->name('activity_executed_time.calendar');
Route::get('calendar/productscalendar/products', 'ProductController@list_calendar');

Route::get('portafolio', function(){
    return view('agreement.brochure');
})->name('agreement.brochure');

