<?php

namespace App;

use Caffeinated\Shinobi\Traits\ShinobiTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, ShinobiTrait;
    public $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'active',
        'name',
        'email',
        'password',
        'person_id',
        'role_id',
        'user_cre_id',
        'user_mod_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


     /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    
    protected $casts = [
        "user_cre_id" => "integer",
        "user_mod_id" => "integer",
        "name" => "string|Min:4",        
        "email" => "string|Min:4",
        "role_id" => "integer",
    ];

    public static $rules = [
        'txtNomUser' => 'required',       
        'txtEmailUser' => 'required',
        'optRole' => 'required',
        'txtIdUser' => 'required',
        'txtPass' => 'required'
    ];

    public static $rules_update = [
        'txtNomUser' => 'required',       
        'txtEmailUser' => 'required',
        'optRole' => 'required'
    ];

    public static $rules_password = [
        'txtPassPrev' => 'required|min:3|current_password',
        'txtPassUserNew' => 'required|min:6',
        'txtPassUserConf' => 'required|same:txtPassUserNew'
    ];
    
    public static $fields = [
        'txtNomUser' => 'Nombre',       
        'txtEmailUser' => 'Email',
        'optRole' => 'Perfil',
        'txtIdUser' => 'Persona',
        'txtPass' => 'Contraseña',
    ];

    public static $fields_password = [
        'txtPassPrev' => 'Contraseña Actual',
        'txtPassUserNew' => 'Nueva contraseña',
        'txtPassUserConf' => 'Confirmación',
    ];

    public function roles()
    {
        return $this->belongsToMany('Caffeinated\Shinobi\Models\Role');
    }
}
