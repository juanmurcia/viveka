<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Project extends Model
{
    public $table = 'projects';

    public static $pattern_id = 2;

    public $fillable = [
        "agreement_id",
        "responsible_id",
        "name",
        "description",
        "percentage",
        "start_date",
        "end_date",
        "active",
        "pattern_state_id",
        "percentage",
        "justification",
        "value",
        "user_cre_id",
        "user_mod_id",
    ];

    public $audit_field = [
        "responsible_id"=> ['name'=>'Responsable', 'model'=>'Professional'],
        "name"=> ['name'=>'Nombre'],
        "description"=> ['name'=>'Descripción'],
        "percentage"=> ['name'=>'Porcentaje Avance'],
        "start_date"=> ['name'=>'Fecha Inicio'],
        "end_date"=> ['name'=>'Fecha Fin'],
        "active"=> ['name'=>'Activo'],
        "pattern_state_id"=> ['name'=>'Estado', 'model'=>'PatternState'],
        "justification"=> ['name'=>'Justificación']
    ];

     protected $casts = [
        "user_cre_id" => "integer",
        "user_mod_id" => "integer",
        "agreement_id" => 'integer',
        "responsible_id"=> "integer",
        "value"=> "float",
        "name"=> "string|Min:4",
        "description"=> "string|Min:4",
        "start_date"=> "date:Y-m-d",
        "end_date"=> "date:Y-m-d"
    ];

    public static $initial_rules = [
        'txtProjectAgreement' => 'required',
        'txtProjectResponsible' => 'required',
        'txtProjectName' => 'required',
        'txtProjectDescription' => 'required',
        'txtProjectStartDate' => 'required',
        'txtProjectEndDate' => 'required|after:txtProjectStartDate',
        'txtProjectValue' => 'required',

    ];

    public static $rules = [
        'txtProjectResponsible' => 'required',
        'txtProjectName' => 'required',
        'txtProjectDescription' => 'required',
        'txtProjectStartDate' => 'required',
        'txtProjectEndDate' => 'required|after:txtProjectStartDate',

    ];

    public  static $progress_rules = [
        'optProjectPatternState' => 'required',
        'txtProjectPercentage' => 'required',
        'txtProjectJustification' => 'required',
    ];
    
    public static $fields = [
        'txtProjectAgreement' => 'Contrato',       
        'txtProjectResponsible' => 'Responsable',
        'txtProjectName' => 'Nombre',
        'txtProjectDescription' => 'Descripción',
        'txtProjectStartDate' => 'Fecha Inicio',
        'txtProjectEndDate' => 'Fecha Fin',
        'optProjectPatternState' => 'Estado',
        'txtProjectPercentage' => 'Porcentage',
        'txtProjectJustification' => 'Justificación',
        'txtProjectValue' => 'Valor',
    ];
    public function filter_search($term){
        $query = DB::table($this->table)
            ->select(['projects.created_at','projects.id','projects.name AS name', 'projects.description AS detail', 'responsible.name AS responsible', 'start_date', 'end_date'])
            ->join('users AS responsible', 'responsible.id', '=', 'projects.responsible_id')
            ->whereRaw("projects.active IS TRUE AND (
                projects.name LIKE '%$term%' OR projects.justification LIKE '%$term%' OR 
                projects.description LIKE '%$term%' OR responsible.name LIKE '%$term%'  )")
            ->get();
        return $query;
    }

    public function responsible()
    {
        return $this->belongsTo('App\Models\Professional','responsible_id');
    }

    public function pattern_state()
    {
        return $this->belongsTo('App\Models\PatternState', 'pattern_state_id');
    }

    public function agreement()
    {
        return $this->belongsTo('App\Models\Agreement','agreement_id');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product','project_id');
    }

    public function user_cre()
    {
        return $this->belongsTo('App\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }
}
