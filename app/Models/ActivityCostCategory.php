<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityCostCategory extends Model
{
    public $table = 'activity_cost_categories';
    public $primaryKey = "id";

    public $fillable = [        
        "active",
        "name",
        "user_cre_id",
        "user_mod_id",
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    
    protected $casts = [
        "user_cre_id" => "integer",
        "user_mod_id" => "integer",
        "name" => "string|Min:4"
    ];

    public static $rules = [
        'txtCategoryName' => 'required'
    ];

    public static $fields = [
        'txtCategoryName' => 'Nombre'
    ];

    public function activity_cost_subcategory()
    {
        return $this->hasMany('App\Models\ActivityCostSubCategory', 'activity_cost_category_id');
    }

    public function user_cre()
    {
        return $this->belongsTo('App\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }
}
