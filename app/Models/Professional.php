<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Professional extends Model
{
    public $table = 'professionals';    

    public $fillable = [
        "active",
        "person_id",
        "birth_date",
        "nationality_id",
        "gender",
        "user_cre_id",
        "user_mod_id",
    ];

    public $audit_field = [
        "active" => ['name'=>'Activo'],
        "birth_date"=> ['name'=>'Fecha Nacimiento'],
        "nationality_id"=> ['name'=>'Nacionalidad', 'model'=>'Country'],
        "gender" => ['name'=>'Genero'],
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    
    protected $casts = [
        "user_cre_id" => "integer",
        "user_mod_id" => "integer",
        "birth_date" => "datetime:Y-m-d",
        "nationality_id" => "integer",
        "gender" => "string",
        "person_id" => "integer",
    ];

    public static $rules = [        
        'txtPerson' => 'required',
        'txtBirthDate' => 'required',
        'optNationality' => 'required',
        'txtGender' => 'required',        
    ];
    
    public static $fields = [
        'txtPerson' => 'Persona',
        'txtBirthDate' => 'Fecha Nacimiento',
        'optNationality' => 'Nacionalidad',
        'txtGender' => 'Genero',
    ];

    public function filter_search($term){
        $query = DB::table($this->table)
            ->select(DB::raw('professionals.created_at, CONCAT(people.document," - ",people.name," ",people.last_name) AS name,
            people.email AS detail, people.phone, people.address'))
            ->join('people','people.id','=','professionals.person_id')
            ->whereRaw("people.active IS TRUE AND professionals.active IS TRUE AND (
                people.name LIKE '%$term%' OR people.last_name LIKE '%$term%' OR
                people.document LIKE '%$term%' OR people.phone LIKE '%$term%' OR people.address LIKE '%$term%')")
            ->get();
        return $query;
    }

    public function person()
    {
        return $this->belongsTo('App\Models\Person','person_id');
    }

    public function nationality()
    {
        return $this->belongsTo('App\Models\Country','nationality_id');
    }

    public function user_cre()
    {
        return $this->belongsTo('App\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }

}
