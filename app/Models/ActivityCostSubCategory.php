<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityCostSubCategory extends Model
{
    public $table = 'activity_cost_subcategories';
    public $primaryKey = "id";

    public $fillable = [        
        "active",
        "name",
        "activity_cost_category_id",
        "user_cre_id",
        "user_mod_id",
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    
    protected $casts = [
        "user_cre_id" => "integer",
        "user_mod_id" => "integer",
        "name" => "string|Min:4"
    ];

    public static $rules = [
        'txtName' => 'required',
        'optActivityCostCategory' => 'required'
    ];
    /*
     *
     * 'optPatternId' => 'required',
        'txtName' => 'required'
     */
    
    public static $fields = [
        'txtName' => 'Nombre',
        'optActivityCostCategory' => 'Categoría'
    ];

    public function activity_cost_category()
    {
        return $this->belongsTo('App\Models\ActivityCostCategory', 'activity_cost_category_id');
    }

    public function user_cre()
    {
        return $this->belongsTo('App\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }
}
