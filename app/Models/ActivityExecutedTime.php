<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityExecutedTime extends Model
{
    public $table = 'activity_executed_times';

    public $fillable = [
        "active",
        "activity_id",
        "role_id",
        "person_id",
        "end_date",
        "executed",
        "description",
        "user_cre_id",
        "user_mod_id",
    ];

    protected $casts = [
        "activity_id" => "integer",
        "role_id" => "integer",
        "person_id" => "integer",
        "executed" => "integer",
        "user_cre_id" => "integer",
        "user_mod_id" => "integer"
    ];

    public static $rules = [
        'txtActivityExecutedTimeActivityId' => 'required',
        'txtActivityExecutedTimeEndDate' => 'required',
        'txtActivityExecutedTimeExecuted' => 'required',
        'txtActivityExecutedTimeDescription' => 'required',

    ];

    public static $fields = [
        'txtActivityExecutedTimeActivityId' => 'Modelo',
        'optActivityExecutedTimeRoleId' => 'Rol',
        'optActivityExecutedTimePersonId' => 'Usuario',
        'txtActivityExecutedTimeEndDate' => 'Hora de Fin',
        'txtActivityExecutedTimeExecuted' => 'Horas estimadas',
        'txtActivityExecutedTimeDescription' => 'Descripción',
    ];

    public function activity()
    {
        return $this->belongsTo('App\Models\Activity','activity_id');
    }

    public function role()
    {
        return $this->belongsTo('Caffeinated\Shinobi\Models\Role','role_id');
    }

    public function person()
    {
        return $this->belongsTo('App\Models\Person','person_id');
    }

    public function user_cre()
    {
        return $this->belongsTo('App\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }

}
