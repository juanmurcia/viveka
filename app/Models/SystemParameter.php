<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class SystemParameter extends Model
{
    public $table = 'system_parameters';
    public $primaryKey = "id";
    public static $pattern_id = 9;

    public $fillable = [        
        "name",
        "value",
        "user_cre_id",
        "user_mod_id",
    ];

    public $audit_field = [
        "name" => ['name'=>'Nombre'],
        "value" => ['name'=>'Valor'],
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    
    protected $casts = [
        "user_cre_id" => "integer",
        "user_mod_id" => "integer",
        "name" => "string|Min:4",        
        "value" => "string|Min:1",
    ];

    public static $rules = [        
        'txtNameParameter' => 'required',
        'txtValueParameter' => 'required',
    ];
    
    public static $fields = [
        'txtNameParameter' => 'Nombre',
        'txtValueParameter' => 'Valor',
    ];

    public function user_cre()
    {
        return $this->belongsTo('App\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }
}
