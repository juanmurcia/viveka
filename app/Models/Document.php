<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Document extends Model
{
    public $table = 'documents';
    public $primaryKey = "id";

    public $fillable = [
        "active",
        "document",
        "model_id",
        "pattern_document_type_id",
        "name",
        "keywords",
        "comment",
        "version",
        "user_mod_id"
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */

    protected $casts = [
        "user_mod_id" => "integer",
        "model_id" => "integer",
        "pattern_document_type_id" => "integer",
        "name" => "string|Min:4",
        "keywords" => "string",
        "comment" => "string",
        "version" => "string"
    ];

    public static $rules = [
        'txtDocumentModelId' => 'required',
        'txtDocumentPatternDocumentTypeId' => 'required',
        'txtDocumentName' => 'required',
        'txtDocumentKey' => 'required'
    ];

    public static $fields = [
        'fileDocument' => 'Documento',
        'txtDocumentName' => 'Documento',
        'txtDocumentModelId' => 'ID Modelo',
        'txtDocumentPatternDocumentTypeId' => 'Tipo de documento',
        'txtDocumentKey' => 'Llave',
        'txtDocumentKeywords' => 'Palabras Clave',
        'txtDocumentComment' => 'Comentario',
    ];

    public function filter_search($term){
        $query = DB::table($this->table)
            ->select(DB::raw('documents.created_at, documents.id, CONCAT(documents.name," | ",pattern_document_types.name) AS name,
            documents.comment AS detail, pattern_document_types.name AS type_doc, model_id'))
            ->join('pattern_document_types','pattern_document_types.id','=','documents.pattern_document_type_id')
            ->whereRaw("documents.active IS TRUE AND (
                documents.name LIKE '%$term%' OR documents.keywords LIKE '%$term%' OR
                documents.comment LIKE '%$term%' OR pattern_document_types.name LIKE '%$term%')")
            ->get();
        return $query;
    }

    public function pattern_document_type()
    {
        return $this->belongsTo('App\Models\PatternDocumentType','pattern_document_type_id');
    }

    public function user_cre()
    {
        return $this->belongsTo('App\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }

}
