<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgreementCoverage extends Model
{
    public $table = 'agreement_coverages';    

    public $fillable = [        
        "name",
        "iso",
    ];
}
