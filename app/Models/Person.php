<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Person extends Model
{
    public $table = 'people';
    public $primaryKey = "id";

    public $fillable = [        
        "active",
        "person_document_type_id",
        "taxpayer_type_id",
        "document",
        "name",
        "last_name",
        "address",
        "email",
        "phone",
        "user",
        "user_cre_id",
        "user_mod_id",
    ];

    public $audit_field = [
        "active" => ['name'=>'Activo'],
        "person_document_type_id" => ['name'=>'Tipo Documento', 'model'=>'PersonDocumentType'],
        "taxpayer_type_id" => ['name'=>'Tipo contribuyente', 'model'=>'TaxpayerType'],
        "document" => ['name'=>'Documento'],
        "name" => ['name'=>'Nombre'],
        "last_name" => ['name'=>'Apellidos'],
        "address" => ['name'=>'Dirección'],
        "email" => ['name'=>'Email'],
        "phone" => ['name'=>'Teléfonos'],
        "user" => ['name'=>'Es Usuario']
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    
    protected $casts = [
        "user_cre_id" => "integer",
        "user_mod_id" => "integer",
        "name" => "string|Min:4",        
        "document" => "string|Min:4",
        "phone" => "string|Min:4",
        "address" => "string|Min:4",        
        "email" => "string|Min:4",        
    ];

    public static $rules = [        
        'txtName' => 'required',       
        'optDocument' => 'required',
        'txtIdent' => 'required|unique:people,document',
        'txtEmail' => 'required|email|unique:people,email',
    ];
    
    public static $fields = [
        'txtName' => 'Nombre',
        'optDocument' => 'Tipo de Documento',
        'txtIdent' => 'Número de identificación',
        'txtMobil' => 'Telefono',
        'txtEmail' => 'Email',
    ];

    public function filter_search($term){
        $query = DB::table($this->table)
            ->select(DB::raw('people.created_at, CONCAT(people.document," - ",people.name," ",people.last_name) AS name,
            people.email AS detail, phone, address'))
            ->whereRaw("people.active IS TRUE AND 
            people.id NOT IN (SELECT person_id FROM professionals WHERE active IS TRUE) AND
            people.id NOT IN (SELECT person_id FROM companies WHERE active IS TRUE) AND (
                people.name LIKE '%$term%' OR people.last_name LIKE '%$term%' OR
                people.document LIKE '%$term%' OR people.phone LIKE '%$term%' OR people.address LIKE '%$term%')")
            ->get();
        return $query;
    }

    public function person_document_type()
    {
        return $this->belongsTo('App\Models\PersonDocumentType','person_document_type_id');
    }

    public function users()
    {
        return $this->hasOne('App\User','person_id');
    }

    public function user_cre()
    {
        return $this->belongsTo('App\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }
}
