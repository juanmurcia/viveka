<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaxpayerType extends Model
{
    public $table = 'taxpayer_types';    

    public $fillable = [        
        "id","name"
    ];
}
