<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use DB;

class clsBd extends Model
{        
    function make_select($arrData)
    {
        $arrSelect = [];        
        if(isset($arrData['fillable']))
        {
            $arrSelect=(isset($arrData['fillable'])&&!empty($arrData['fillable']))?$arrData['fillable']:$arrData['modelo']->fillable;
        }
        if(isset($arrData['fillableCon']))
        {
            $arrSelect = $this->select_concat($arrData['fillableCon'],$arrSelect);
        }
        if(isset($arrData['fillableMan']))
        {
            $arrSelect = $this->select_manual($arrData['fillableMan'],$arrSelect);
        }        
        return $arrSelect;
    }
    function select_concat($arrConcat, $arrSelect)
    {        
        if(is_array($arrConcat)){
            foreach ($arrConcat as $concat)
            {          
                $concat[0] = str_replace('||',"'",$concat[0]);
                $select = DB::raw('CONCAT('.$concat[0].')'.(isset($concat[1]) ? ' AS '.$concat[1] : ' '));
                array_push($arrSelect, $select);
            }
        }        
        return $arrSelect;
    }
    function select_manual($arrMan, $arrSelect)
    {        
        if(is_array($arrMan)){
            foreach ($arrMan as $concat)
            {                      
                $select = DB::raw($concat);
                array_push($arrSelect, $select);
            }
        }        
        return $arrSelect;
    }
    function consultar($arrData)
    {        
        $table=(isset($arrData['table'])&&!empty($arrData['table']))?$arrData['table']:$arrData['modelo']->table;
        $retorno = DB::table($table);
        //Si aplica funciones especiales        
        $arrFillable=$this->make_select($arrData);            
        $retorno->select($arrFillable);            
        
        //Recorre Filtros
        if(isset($arrData['filtro']) AND !empty($arrData['filtro'])){
            foreach($arrData['filtro'] as $where)
            {
                if(is_array($where))
                {
                    //Si el filtro es una sub consulta, arma el IN ()
                    if(isset($where['group']))
                    {
                        $type=(isset($where['group']['type']) ? $where['group']['type'] : 'where');
                        $retorno->$type(function ($query) use($where) { 
                            foreach($where['data'] as $where_group)
                            {                                
                                if(isset($where_group['subConsulta']))
                                {
                                    $subconsulta = $this->sub_consulta($where_group);
                                    $query->whereRaw($where_group['campo'].(isset($where_group['not']) ? ' NOT IN ' : ' IN ').' ('.$subconsulta->toSql().')',array(),(isset($where_group['type']) ? $where_group['type'] : 'AND'));
                                }else{
                                    $filtro = $this->filtro_consulta($where_group);
                                    $query->whereRaw($filtro,array(),(isset($where_group['type']) ? $where_group['type'] : 'AND'));
                                }
                            }
                        });
                    }
                    elseif(isset($where['subConsulta']))
                    {
                        $subconsulta = $this->sub_consulta($where);
                        $retorno->whereRaw($where['campo'].(isset($where['not']) ? ' NOT IN ' : ' IN ').' ('.$subconsulta->toSql().')',array(),(isset($where['type']) ? $where['type'] : 'AND'));
                        
                    }else{
                        $filtro = $this->filtro_consulta($where);
                        $retorno->whereRaw($filtro,array(),(isset($where['type']) ? $where['type'] : 'AND'));                                                
                    }
                }
                else{
                    $retorno->whereRaw($where);
                }
            }             
        }
        // Recorre Joins
        if(isset($arrData['join']) AND !empty($arrData['join'])){
            foreach($arrData['join'] as $join)
            {
                $type=(!empty($join['type'])?$join['type']:'leftJoin');
                $retorno->$type($join['table'],function($query) use($join){
                    $query->on($join['campoA'],(isset($join['operador']) ? $join['operador'] : '='),$join['campoB']);
                    //Si tiene filtro el join Array
                    if(isset($join['filtro']) AND is_array(isset($join['filtro']))){
                        foreach($join['filtro'] as $where_join)
                        {
                            if(is_int($where_join['value']))
                                $where_join['value']="'{$where_join['value']}'";
                            $query->where($where_join['campo'],(isset($where_join['operador']) ? $where_join['operador'] : ''),$where_join['value']);
                        }
                    }                
                });
            }            
        }
        //Recorre campos GroupBy
        if(isset($arrData['groupBy']) AND !empty($arrData['groupBy'])){            
            foreach($arrData['groupBy'] as $groupBy)
            {
                $retorno->groupBy($groupBy);
            }                            
        }
        //Recorre campos OrderBy
        if(isset($arrData['orderBy']) AND !empty($arrData['orderBy'])){            
            foreach($arrData['orderBy'] as $orderBy)
            {
                $campo = $this->prepara_campo($orderBy['campo']);
                $retorno->orderBy(DB::raw($campo),(isset($orderBy['type'])? $orderBy['type'] : 'ASC'));
            }                            
        }
        //Limit
        if(isset($arrData['limit']) AND !empty($arrData['limit'])){            
            $retorno->take($arrData['limit']);
        }
        //Tipo de Retorno
        if(isset($arrData['toSql'])){
            return $retorno->toSql();
        }elseif (isset($arrData['get'])){
            return $retorno->get();
        }else{
            return $retorno;
        }
    }
    function filtro_consulta($where)
    {
        if(isset($where['subConsulta']) AND $where['subConsulta']){            
            $subconsulta = $this->sub_consulta($where);
            return $where['campo'].(isset($where['not']) ? ' NOT IN ' : ' IN ').' ('.$subconsulta->toSql().')';
            
        }else{
            if(is_int($where['value']))
                $where['value']="'{$where['value']}'";
                            
            $field = $this->prepara_campo($where['campo']);        
            return $field.' '.(isset($where['operador']) ? $where['operador'] : '=').' '.$where['value'];
        }
    }
    function prepara_campo($campo)
    {
//        if((preg_match("/CONCAT/i", $campo))){
            $field = $campo;
//        }else{
//            $campo = explode('.', $campo);
//            $field = '';
//            for($i =0; $i< sizeof($campo);$i++)
//            {
//                $field .= ($i > 0 ? '.' : '').'"'.trim($campo[$i]).'"';
//            }
//        }        
        return $field;
    }
    function sub_consulta($arr_sub_consulta)
    {
        $subconsulta = DB::table($arr_sub_consulta['table'])->select($arr_sub_consulta['fillable']);
        if(isset($arr_sub_consulta['filtro']))
        {
            if(!is_array($arr_sub_consulta['filtro'])){
                $arr_conv[] = (array)$arr_sub_consulta['filtro'];
                $arr_sub_consulta['filtro'] = $arr_conv;
            }              
            foreach($arr_sub_consulta['filtro'] as $where_sub)
            {                
                $filtro = $this->filtro_consulta($where_sub);                
                $subconsulta->whereRaw($filtro,array(),(isset($where['type']) ? $where['type'] : 'AND'));                
            }                            
        }
        if(isset($arr_sub_consulta['join']))
        {
            foreach($arr_sub_consulta['join'] as $join_sub)
            {
                $type=(!empty($join_sub['type'])?$join_sub['type']:'leftJoin');
                $subconsulta->$type($join_sub['table'],function($query) use($join_sub){
                    $query->on($join_sub['campoA'],(isset($join_sub['operador']) ? $join_sub['operador'] : '='),$join_sub['campoB']);
                    //Si tiene filtro el join Array
                    if(isset($join_sub['filtro']) AND is_array(isset($join_sub['filtro']))){
                        foreach($join_sub['filtro'] as $where_join_sub)
                        {
                            $where_join_sub['value']="'{$where_join_sub['value']}'";
                            $query->where($where_join_sub['campo'],(isset($where_join_sub['operador']) ? $where_join_sub['operador'] : ''),$where_join_sub['value']);
                        }
                    }                
                });
            }                            
        }
        
        return $subconsulta;
    }
    function id_element($arrData)
    {        
        $arrEnv['get'] =true;
        $arrEnv['table'] =(isset($arrData['table'])&&!empty($arrData['table']))?$arrData['table']:$arrData['modelo']->table;
        if(isset($arrData['fillableMan'])){
            $arrEnv['fillableMan'] =(isset($arrData['id'])&&!empty($arrData['id']))? [$arrData['id'].' AS id']:[$arrData['modelo']->primaryKey.' AS id'];
        }else{
            $arrEnv['fillable'] =(isset($arrData['id'])&&!empty($arrData['id']))? [$arrData['id'].' AS id']:[$arrData['modelo']->primaryKey.' AS id'];
        }
        
        $arrEnv['limit'] = 1;
        $arrEnv['orderBy'] = (isset($arrData['orderBy'])&&!empty($arrData['orderBy']))?$arrData['orderBy']:['campo'=>$arrData['modelo']->primaryKey];
        if(isset($arrData['filtro']) AND !empty($arrData['filtro']))
            $arrEnv['filtro'] =$arrData['filtro'];
        
        $id_element = $this->consultar($arrEnv);        
        return (isset($id_element['0']->id) ? $id_element['0']->id : 0);
    }
    function get_next_id($data)
    {
        $next_id = \DB::select('select nextval(?)',(isset($data['sequece']) ? [$data['sequece']] : [$data['model']->sequence]));        
        return (isset($next_id['0']->nextval) ? $next_id['0']->nextval : 0);        
    }
    function model_transaction($arr_data)
    {
        \DB::beginTransaction();
        
        if(sizeof($arr_data) > 0)
        {
            foreach($arr_data as $data)
            {
                
                try{
                    if($data['metodo'] == 'updateRich')
                    {
                        $newQuery = $data['repository']->$data['metodo']($data['data'],$data['id']);
                    }
                    else
                    {
                        $newQuery = $data['repository']->$data['metodo']($data['data']);                    
                    }                    
                }catch (ValidationException $ex) {
                    DB::rollback();    
                    print_r($ex->getErrors());exit;
                }catch (\Exception $e) {
                    DB::rollback();                    
                    throw $e;
                }                
            }
        }
        return \DB::commit();
    }
    function query_especial($arrData, $arrEnv)
    {
        $arrEnv['get'] =true;
        $arrEnv['table'] =(isset($arrData['table'])&&!empty($arrData['table']))? $arrData['table'] : $arrData['modelo']->table;        
        if(isset($arrData['filtro']) AND !empty($arrData['filtro']))
            $arrEnv['filtro'] = $arrData['filtro'];
        if(isset($arrData['join']) AND !empty($arrData['join']))
            $arrEnv['join'] = $arrData['join'];
        if(isset($arrData['groupBy']) AND !empty($arrData['groupBy']))
            $arrEnv['groupBy'] = $arrData['groupBy'];        
        
        return $this->consultar($arrEnv);             
    }
    //bool - metodo que determina si existe o no registros con unas caracteristicas en una tabla
    function existen_registros($arrData) 
    {
        $row = $this->num_registros($arrData);        
        return (($row > 0) ? true : false);
    }
    //Metodo retorna el numero de registros de la consulta
    function num_registros($arrData) 
    {
        $arrEnv['fillable'] =[DB::raw('COUNT(*)')];     
        $row = $this->query_especial($arrData, $arrEnv);        
        return (isset($row[0]->count) ? $row[0]->count : 0);        
    }
    function max_filtro($arrData) 
    {
        $arrEnv['fillable'] =(isset($arrData['campo'])&&!empty($arrData['campo']))? [DB::raw('MAX('.$arrData['campo'].')')] : [DB::raw('MAX('.$arrData['modelo']->primaryKey.')')];
        $row = $this->query_especial($arrData, $arrEnv);
        return (isset($row[0]->max) ? $row[0]->max : 0);        
    }
    //Metodo para ejecutar consultar de procedimientos almacenados 
    function ejecuta_procedimiento($arrData)
    {
        $param ='';
        $return ='';
        
        if(isset($arrData['return']))
        {
            foreach($arrData['return'] as $returns)
            {
                $return .= (!empty($return) ? ' , '.$returns : ' AS ('.$returns);
            }            
            $return .=')';            
        }
        if(isset($arrData['param']))
        {
            foreach($arrData['param'] as $params)
            {
                $param .= (!empty($param) ? " , '$params'" : "'$params'");
            }            
        } 
        
        if(isset($arrData['toSql']))
		 {
	       $data = $this->name_procedure($arrData).' ('.$param.') '.$return;
                
		 }
		 else
		 {
	        $data = DB::select($this->name_procedure($arrData).' ('.$param.') '.$return);
		 }
		 
        return $data;
    }
    function name_procedure($arrData)
    {
        $select =(isset($arrData['return']) ? 'SELECT * FROM '.$arrData['name'] : 'SELECT '.$arrData['name']);
        return $select;
    }
}