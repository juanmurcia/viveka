<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    public $table = 'currencies';    

    public $fillable = [        
        "name",
        "iso",
        "money",
        "symbol",
    ];
}
