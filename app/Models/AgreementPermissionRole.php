<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgreementPermissionRole extends Model
{
    public $table = 'agreement_permission_roles';

    public $fillable = [
        "role_id",
        "agreement_permission_id",
        "user_cre_id",
        "user_mod_id",
    ];

    public function agreementRole()
    {
        return $this->belongsTo('Caffeinated\Shinobi\Models\Role', 'role_id');
    }
}
