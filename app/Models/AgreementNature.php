<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgreementNature extends Model
{
    public $table = 'agreement_natures';    

    public $fillable = [        
        "name",
    ];
}
