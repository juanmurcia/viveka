<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public $table = 'notifications';

    public $fillable = [
        "active",
        "seen",
        "user_id",
        "model_id",
        "model",
        "name",
        "description",
        "notification_date",
        "user_cre_id",
        "user_mod_id"
    ];

    protected $casts = [
        "user_id" => "integer",
        "user_cre_id" => "integer",
        "user_mod_id" => "integer",
        "model" => "string",
        "model_id" => "integer",
        "name" => "string|Min:4",
        "description" => "string",
    ];

    public static $rules = [
        'optUserId' => 'required',
        'txtName' => 'required',
        'txtDescription' => 'required'
    ];

    public static $fields = [
        'optUserId' => 'Usuario',
        'txtName' => 'Nombre',
        'txtDescription' => 'Descripción',
    ];

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function userCre()
    {
        return $this->belongsTo('App\User', 'user_cre_id');
    }

    public function userMod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }
}