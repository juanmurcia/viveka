<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityExecutedCost extends Model
{
    public $table = 'activity_executed_costs';
    public static $pattern_id = 13;
    public $fillable = [
        "active",
        "activity_id",
        "person_id",
        "activity_cost_subcategory_id",
        "executed",
        "end_date",
        "description",
        "user_mod_id",
    ];

    protected $casts = [
        "activity_id" => "integer",
        "person_id" => "integer",
        "activity_cost_subcategory_id" => "integer",
        "executed" => "integer",
        "end_date" => "datetime:Y-m-d",
        "user_mod_id" => "integer"
    ];

    public static $rules = [
        'txtActivityExecutedCostActivityId' => 'required',
        'txtActivityExecutedCostPersonId' => 'required',
        'optActivityExecutedCostActivityCostSubCategoryId' => 'required',
        'txtActivityExecutedCostExecuted' => 'required',
        'txtActivityExecutedCostEndDate' => 'required',
        'txtActivityExecutedCostDescription' => 'required',

    ];

    public static $fields = [
        'txtActivityExecutedCostActivityId' => 'Modelo',
        'txtActivityExecutedCostPersonId' => 'Persona',
        'optActivityExecutedCostActivityCostSubCategoryId' => 'Categoría de costo',
        'txtActivityExecutedCostExecuted' => 'Costo',
        'txtActivityExecutedCostEndDate' => 'Fecha',
        'txtActivityExecutedCostDescription' => 'Descripción',
    ];

    public function activity()
    {
        return $this->belongsTo('App\Models\Activity','activity_id');
    }

    public function person()
    {
        return $this->belongsTo('App\Models\Person','person_id');
    }

    public function activity_cost_subcategory()
    {
        return $this->belongsTo('App\Models\ActivityCostSubCategory','activity_cost_subcategory_id');
    }

    public function user_cre()
    {
        return $this->belongsTo('App\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }

}
