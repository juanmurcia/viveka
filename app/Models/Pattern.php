<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pattern extends Model
{
    public $table = 'patterns';
    public $primaryKey = "id";

    public $fillable = [        
        "active",
        "model",
        "name",
        "user_cre_id",
        "user_mod_id",
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    
    protected $casts = [
        "user_cre_id" => "integer",
        "user_mod_id" => "integer",
        "name" => "string|Min:4",
        "model" => "string|Min:4"
    ];

    public static $rules = [        
        'txtName' => 'required',       
        'txtModel' => 'required'
    ];
    
    public static $fields = [
        'txtName' => 'Nombre',       
        'txtModel' => 'Modelo',
    ];

    public function user_cre()
    {
        return $this->belongsTo('App\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }
}
