<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityEstimatedTime extends Model
{
    public $table = 'activity_estimated_times';

    public $fillable = [
        "active",
        "activity_id",
        "role_id",
        "estimated",
        "justification",
        "user_mod_id",
    ];

    protected $casts = [
        "activity_id" => "integer",
        "role_id" => "integer",
        "estimated" => "integer",
        "user_mod_id" => "integer"
    ];

    public static $rules = [
        'txtActivityEstimatedTimeActivityId' => 'required',
        'optActivityEstimatedTimeRoleId' => 'required',
        'txtActivityEstimatedTimeEstimated' => 'required',
        'txtActivityEstimatedTimeJustification' => 'required',

    ];

    public static $fields = [
        'txtActivityEstimatedTimeActivityId' => 'Actividad',
        'optActivityEstimatedTimeRoleId' => 'Rol',
        'txtActivityEstimatedTimeEstimated' => 'Horas estimadas',
        'txtActivityEstimatedTimeJustification' => 'Justificación',
    ];

    public function activity()
    {
        return $this->belongsTo('App\Models\Activity','activity_id');
    }

    public function role()
    {
        return $this->belongsTo('Caffeinated\Shinobi\Models\Role','role_id');
    }

    public function user_cre()
    {
        return $this->belongsTo('App\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }

}
