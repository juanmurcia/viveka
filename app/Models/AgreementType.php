<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgreementType extends Model
{
    public $table = 'agreement_types';    

    public $fillable = [        
        "name",
        "iso",
    ];


}
