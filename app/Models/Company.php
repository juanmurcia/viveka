<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Company extends Model
{
    public $table = 'companies';
    public static $pattern_id = 6;

    public $fillable = [
        "active",
        "person_id",
        "agent_id",
        "number_employees",
        "url",
        "logo",
        "user_cre_id",
        "user_mod_id",
    ];

    public $audit_field = [
        "active" => ['name'=>'Activo'],
        "agent_id" => ['name'=>'Representante', 'Model'=>'Person'],
        "number_employees"=> ['name'=>'# Empleados'],
        "url"=> ['name'=>'Url'],
        "logo"=> ['name'=>'Logo'],
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    
    protected $casts = [
        "user_cre_id" => "integer",
        "user_mod_id" => "integer",
        "person_id" => "integer",
        "agent_id" => "integer",
        "number_employees" => "integer",
        "url" => "string|Min:4",
        "logo" => "string|Min:4",
    ];

    public static $rules = [
        'txtPerson' => 'required',       
        'txtAgentId' => 'required',
    ];
    
    public static $fields = [
        'txtPerson' => 'Persona',       
        'txtAgentId' => 'Agente',
    ];

    public function filter_search($term){
        $query = DB::table($this->table)
            ->select(DB::raw('companies.created_at, companies.id, CONCAT(people.document," - ",people.name," ",people.last_name) AS name,
            people.email AS detail, people.phone, people.address, url, CONCAT(agent.name," ",agent.last_name) AS agent'))
            ->join('people','people.id','=','companies.person_id')
            ->join('people AS agent','agent.id','=','companies.agent_id')
            ->whereRaw("people.active IS TRUE AND companies.active IS TRUE AND (
                people.name LIKE '%$term%' OR people.last_name LIKE '%$term%' OR
                people.document LIKE '%$term%' OR people.phone LIKE '%$term%' OR people.address LIKE '%$term%' OR
                agent.name LIKE '%$term%' OR agent.last_name LIKE '%$term%' OR
                agent.document LIKE '%$term%')")
            ->get();
        return $query;
    }

    public function person()
    {
        return $this->belongsTo('App\Models\Person','person_id');
    }

    public function agent()
    {
        return $this->belongsTo('App\Models\Person','agent_id');
    }

    public function user_cre()
    {
        return $this->belongsTo('App\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }
}
