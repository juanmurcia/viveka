<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Agreement extends Model
{
 	public $table = 'agreements';

 	public static $pattern_id = 1;

    public $fillable = [        
        "id",
        "number",
        "name",
        "object",
        "employer_id",
        "contractor_id",
        "responsible_id",
        "agreement_process_type_id",
        "pattern_state_id",
        "agreement_type_id",
        "agreement_category_id",
        "agreement_coverage_id",
        "agreement_service_id",
        "agreement_nature_id",
        "currency_id",
        "department_id",
        "signing_date",
        "start_date",
        "end_date",
        "execution_term",
        "addition_term",
        "value",
        "billing_due",
        "iva_billing",
        "addition_value",
        "percentage",
        "justification",
        "single_project",
        "external_url",
        "user_cre_id",
        "user_mod_id",
    ];

    public $audit_field = [
        "number" => ['name'=>'Número'],
        "name" => ['name'=>'Nombre'],
        "object" => ['name'=>'Objeto'],
        "employer_id" => ['name'=>'Contratante', 'model'=>'Company'],
        "contractor_id" => ['name'=>'Contratista', 'model'=>'Company'],
        "responsible_id" => ['name'=>'Responsable', 'model'=>'Professional'],
        "agreement_process_type_id" => ['name'=>'Responsable', 'model'=>'AgreementProcessType'],
        "pattern_state_id" => ['name'=>'Estado', 'model'=>'PatternState'],
        "agreement_type_id" => ['name'=>'Tipo', 'model'=>'AgreementType'],
        "agreement_category_id" => ['name'=>'Categoría', 'model'=>'AgreementCategory'],
        "agreement_coverage_id" => ['name'=>'Cobertura', 'model'=>'AgreementCoverage'],
        "agreement_service_id" => ['name'=>'Servicio', 'model'=>'AgreementService'],
        "agreement_nature_id" => ['name'=>'Naturaleza', 'model'=>'AgreementNature'],
        "currency_id"=> ['name'=>'Moneda', 'model'=>'Currency'],
        "department_id"=> ['name'=>'Departamento', 'model'=>'Department'],
        "signing_date"=> ['name'=>'Fecha Firma'],
        "start_date"=> ['name'=>'Fecha Inicio'],
        "end_date"=> ['name'=>'Fecha Fin'],
        "execution_term"=> ['name'=>'Plazo'],
        "addition_term"=> ['name'=>'Adiciones'],
        "value"=> ['name'=>'Cuantia'],
        "addition_value"=> ['name'=>'Adicion'],
        "percentage" => ['name'=>'Porcentaje Avance'],
        "justification"=> ['name'=>'Justificación'],
        "external_url"=> ['name'=>'Ruta Externa'],
        "single_project" => ['name'=>'Multi Proyecto'],
    ];

    protected $casts = [
        "number" => "string",
        "name" => "string",
        "employer_id" => "integer",
        "contractor_id" => "integer",
        "responsible_id" => "integer",
        "agreement_process_type_id" => "integer",
        "pattern_state_id" => "integer",
        "agreement_type_id" => "integer",
        "agreement_category_id" => "integer",
        "agreement_coverage_id" => "integer",
        "agreement_service_id" => "integer",
        "agreement_nature_id" => "integer",
        "currency_id" => "integer",
        "department_id" => "integer",
        "object" => "string",
        "signing_date" => "date:Y-m-d",
        "start_date" => "date:Y-m-d",
        "end_date" => "date:Y-m-d",
        "execution_term" => "integer",
        "addition_term" => "integer",
        "value" => "float",
        "addition_value" => "float",
        "external_url" => "string",
        "user_cre_id" => "integer",
        "user_mod_id" => "integer",
        "percentage" => "float",
        "justification" => "string",
    ];

    public static $initial_rules = [
        'txtAgreementEmployer' => 'required',
        'txtAgreementContractor' => 'required',
        'txtAgreementNumber' => 'required',
        'txtAgreementName' => 'required',
        'txtAgreementObject' => 'required',
        'txtAgreementResponsible' => 'required',
        'txtAgreementValue' => 'required',
        'optAgreementCurrency' => 'required',
    ];

    public static $rules = [
        'optAgreementProcessType' => 'required',
        'optAgreementType' => 'required',
        'optAgreementCategory' => 'required',
        'optAgreementNature' => 'required',
        'optAgreementService' => 'required',
        'optAgreementCoverage' => 'required',
        'optAgreementDepartment' => 'required',
        'txtAgreementExecutionTerm' => 'required',
        'txtAgreementStartDate' => 'required',
        'txtAgreementEndDate' => 'required|after:txtAgreementStartDate',
        'txtAgreementAdditionValue' => 'required',
        'txtAgreementAdditionTerm' => 'required',

    ];

    public  static $progress_rules = [
        'optAgreementPatternState' => 'required',
        'txtAgreementPercentage' => 'required',
        'txtAgreementJustification' => 'required',
    ];
    
    public static $fields = [
        'txtAgreementEmployer' => 'Contratante',
        'txtAgreementContractor' => 'Contratista',
        'txtAgreementResponsible' => 'Responsable',
        'txtAgreementNumber' => 'Numero',
        'txtAgreementName' => 'Nombre',
        'txtAgreementObject' => 'Objeto',
        'optAgreementProcessType' => 'Tipo Contratación',
        'optAgreementType' => 'Tipo',
        'optAgreementCategory' => 'Categoría',
        'optAgreementNature' => 'Naturaleza',
        'optAgreementService' => 'Servicio',
        'optAgreementCoverage' => 'cobertura',
        'optAgreementCurrency' => 'Moneda',
        'optAgreementDepartment' => 'Departamento',
        'txtAgreementValue' => 'Valor del Contrato',
        'txtAgreementExecutionTerm' => 'Plazo de ejecución',
        'txtAgreementStartDate' => 'Fecha Fin',
        'txtAgreementEndDate' => 'Fecha Inicio',
        'optAgreementPatternState' => 'Estado',
        'txtAgreementPercentage' => 'Porcentage',
        'txtAgreementJustification' => 'Justificación',
        'txtAgreementAdditionValue' => 'Total adiciones',
        'txtAgreementAdditionTerm' => 'Total prorrogas',
    ];

    public function filter_search($term){
        $query = DB::table($this->table)
            ->select(
                DB::raw('agreements.id, CONCAT(agreements.number," - ",agreements.name) AS name,
                agreements.object AS detail, responsible.name AS responsible, start_date, end_date')
            )
            ->join('companies AS comp_contractor', 'comp_contractor.id', '=', 'agreements.contractor_id')
            ->join('people AS contractor', 'contractor.id', '=', 'comp_contractor.person_id')
            ->join('companies AS comp_employer', 'comp_employer.id', '=', 'agreements.employer_id')
            ->join('people AS employer', 'employer.id', '=', 'comp_employer.person_id')
            ->join('users AS responsible', 'responsible.id', '=', 'agreements.responsible_id')
            ->whereRaw("agreements.active IS TRUE AND (
                agreements.number LIKE '%$term%' OR agreements.name LIKE '%$term%' OR
                agreements.object LIKE '%$term%' OR contractor.document LIKE '%$term%' OR
                contractor.name LIKE '%$term%' OR contractor.last_name LIKE '%$term%' OR
                employer.document LIKE '%$term%' OR employer.name LIKE '%$term%' OR 
                employer.last_name LIKE '%$term%' OR responsible.name LIKE '%$term%' )")
            ->get();
        return $query;
    }

    public function employer()
    {
        return $this->belongsTo('App\Models\Company', 'employer_id');
    }

    public function contractor()
    {
        return $this->belongsTo('App\Models\Company', 'contractor_id');
    }

    public function responsible()
    {
        return $this->belongsTo('App\Models\Professional', 'responsible_id');
    }

    public function agreement_process_type()
    {
        return $this->belongsTo('App\Models\AgreementProcessType', 'agreement_process_type_id');
    }

    public function pattern_state()
    {
        return $this->belongsTo('App\Models\PatternState', 'pattern_state_id');
    }

    public function agreement_type()
    {
        return $this->belongsTo('App\Models\AgreementType', 'agreement_type_id');
    }

    public function agreement_category()
    {
        return $this->belongsTo('App\Models\AgreementCategory', 'agreement_category_id');
    }

    public function agreement_coverage()
    {
        return $this->belongsTo('App\Models\AgreementCoverage', 'agreement_coverage_id');
    }

    public function agreement_service()
    {
        return $this->belongsTo('App\Models\AgreementService', 'agreement_service_id');
    }

    public function agreement_nature()
    {
        return $this->belongsTo('App\Models\AgreementNature', 'agreement_nature_id');
    }

    public function currency()
    {
        return $this->belongsTo('App\Models\Currency', 'currency_id');
    }

    public function department()
    {
        return $this->belongsTo('App\Models\Department', 'department_id');
    }

    public function team()
    {
        return $this->hasMany('App\Models\AgreementTeam');
    }

    public function projects()
    {
        return $this->hasMany('App\Models\Project', 'agreement_id');
    }


    public function agreement_role_cost()
    {
        return $this->hasMany('App\Models\AgreementRoleCost');
    }

    public function user_cre()
    {
        return $this->belongsTo('App\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }

}
