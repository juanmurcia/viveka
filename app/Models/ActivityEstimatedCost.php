<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityEstimatedCost extends Model
{
    public $table = 'activity_estimated_costs';

    public $fillable = [
        "active",
        "activity_id",
        "activity_cost_category_id",
        "estimated",
        "justification",
        "user_mod_id",
    ];

    protected $casts = [
        "activity_id" => "integer",
        "activity_cost_type_id" => "integer",
        "estimated" => "integer",
        "user_mod_id" => "integer"
    ];

    public static $rules = [
        'txtActivityEstimatedCostActivityId' => 'required',
        'optActivityExecutedCostActivityCostCategoryId' => 'required',
        'txtActivityEstimatedCostEstimated' => 'required',
        'txtActivityEstimatedCostJustification' => 'required',

    ];

    public static $fields = [
        'txtActivityEstimatedCostActivityId' => 'Modelo',
        'optActivityExecutedCostActivityCostCategoryId' => 'Tipo de costo',
        'txtActivityEstimatedCostEstimated' => 'Costo estimado',
        'txtActivityEstimatedCostJustification' => 'Justificación',
    ];

    public function activity()
    {
        return $this->belongsTo('App\Models\Activity','activity_id');
    }

    public function activity_cost_category()
    {
        return $this->belongsTo('App\Models\ActivityCostCategory','activity_cost_category_id');
    }

    public function user_cre()
    {
        return $this->belongsTo('App\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }

}
