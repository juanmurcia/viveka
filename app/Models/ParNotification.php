<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParNotification extends Model
{
    public $table = 'par_notifications';

    public $fillable = [
        "name",
        "description",
        "time",
        "role",
        "user_cre_id",
        "user_mod_id"
    ];

    protected $casts = [
        "user_cre_id" => "integer",
        "user_mod_id" => "integer",
        "name" => "string",
        "time" => "integer",
        "description" => "string",
    ];

    public static $rules = [
        'txtTime' => 'required',
        'txtRole' => 'required',
    ];

    public function userCre()
    {
        return $this->belongsTo('App\User', 'user_cre_id');
    }

    public function userMod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }
}
