<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    public $table = 'products';
    public static $pattern_id = 3;

    public $fillable = [
        "project_id",
        "responsible_id",
        "name",
        "description",
        "percentage",
        "start_date",
        "end_date",
        "active",
        "pattern_state_id",
        "justification",
        "value",
        "contract_name",
        "contract_description",
        "user_cre_id",
        "user_mod_id",
        "updated_at",
    ];

    public $audit_field = [
        "responsible_id"=> ['name'=>'Responsable', 'model'=>'Professional'],
        "name"=> ['name'=>'Nombre'],
        "description"=> ['name'=>'Descripción'],
        "percentage"=> ['name'=>'Porcentaje Avance'],
        "start_date"=> ['name'=>'Fecha Inicio'],
        "end_date"=> ['name'=>'Fecha Fin'],
        "active"=> ['name'=>'Activo'],
        "pattern_state_id"=> ['name'=>'Estado', 'model'=>'PatternState'],
        "justification"=> ['name'=>'Justificación'],
        "contract_name"=> ['name'=>'Nombre Contractual'],
        "contract_description"=> ['name'=>'Descripción Contractual']
    ];

     protected $casts = [
        "user_cre_id" => "integer",
        "user_mod_id" => "integer",
        "project_id" => 'integer',
        "responsible_id"=> "integer",
        "value"=> "float",
        "name"=> "string|Min:4",
        "description"=> "string|Min:4",
        "start_date"=> "date:Y-m-d",
        "end_date"=> "date:Y-m-d"
    ];

    public static $initial_rules = [
        'txtProductProject' => 'required',
        'txtProductResponsible' => 'required',
        'txtProductName' => 'required',
        'txtProductDescription' => 'required',
        'txtProductStartDate' => 'required',
        'txtProductEndDate' => 'required|after:txtProductStartDate',
        'txtProductValue' => 'required',
    ];

    public static $rules = [
        'txtProductResponsible' => 'required',
        'txtProductName' => 'required',
        'txtProductDescription' => 'required',
        'txtProductStartDate' => 'required',
        'txtProductEndDate' => 'required|after:txtProductStartDate',

    ];

    public  static $progress_rules = [
        'optProductPatternState' => 'required',
        'txtProductPercentage' => 'required',
        'txtProductJustification' => 'required',
    ];
    
    public static $fields = [
        'txtProductProject' => 'Proyecto',
        'txtProductResponsible' => 'Responsable',
        'txtProductName' => 'Nombre',
        'txtProductDescription' => 'Descripción',
        'txtProductStartDate' => 'Fecha Inicio',
        'txtProductEndDate' => 'Fecha Fin',
        'optProductPatternState' => 'Estado',
        'txtProductPercentage' => 'Porcentage',
        'txtProductJustification' => 'Justificación',
        'txtProductValue' => 'Valor',
    ];

    public function filter_search($term){
        $query = DB::table($this->table)
            ->select(['products.created_at','products.id','products.name AS name','products.description AS detail', 'responsible.name AS responsible', 'start_date', 'end_date'])
            ->join('users AS responsible', 'responsible.id', '=', 'products.responsible_id')
            ->whereRaw("products.active IS TRUE AND (
                products.name LIKE '%$term%' OR products.justification LIKE '%$term%' OR 
                products.description LIKE '%$term%' OR responsible.name LIKE '%$term%'  )")
            ->get();
        return $query;
    }

    public function responsible()
    {
        return $this->belongsTo('App\Models\Professional','responsible_id');
    }

    public function project()
    {
        return $this->belongsTo('App\Models\Project','project_id');
    }

    public function pattern_state()
    {
        return $this->belongsTo('App\Models\PatternState', 'pattern_state_id');
    }

    public function user_cre()
    {
        return $this->belongsTo('App\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }
}
