<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgreementPermission extends Model
{
    public $table = 'agreement_permissions';

    public $fillable = [
        "name",
        "slug",
        "description",
    ];

    public function full_permission($agreement_id){
         $permission = AgreementTeam::where('agreement_teams.active',true)
            ->where('agreement_teams.agreement_id',$agreement_id)
            ->join('professionals','professionals.id','=','agreement_teams.professional_id')
            ->where('professionals.person_id','=',auth()->user()->person_id)
            ->count();

         $permission = (in_array(auth()->user()->role_id, [1,2]) ? 1 : $permission);

         return ($permission > 0 ? true : false);
    }
}