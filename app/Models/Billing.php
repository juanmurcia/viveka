<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    public $table = 'billings';
    public static $pattern_id = 8;
    public $primaryKey = "id";

    public $fillable = [
        "agreement_id",
        "number",
        "billing_date",
        "payment_date",
        "due_date",
        "pattern_state_id",
        "value",
        "taxes",
        "net_value",
        "comment",
        "documentation",
        "user_cre_id",
        "user_mod_id"
    ];

    public $audit_field = [
        "number"=> ['name'=>'Número'],
        "billing_date"=> ['name'=>'Fecha Facturación'],
        "payment_data"=> ['name'=>'Fecha Pago'],
        "due_date"=> ['name'=>'Fecha Vencimiento'],
        "pattern_state_id"=> ['name'=>'Estado', 'model'=>'PatternState'],
        "value"=> ['name'=>'Valor'],
        "taxes"=> ['name'=>'Retención'],
        "net_value"=> ['name'=>'Valor Neto'],
        "documentation"=> ['name'=>'Documentación'],
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */

    protected $casts = [
        "user_mod_id" => "integer",
        "user_cre_id" => "integer",
        "pattern_state_id" => "integer",
        "number" => "string",
        "billing_date" => "datetime:Y-m-d",
        "payment_data" => "datetime:Y-m-d",
        "due_date" => "datetime:Y-m-d",
        "value" => "float",
        "taxes" => "float",
        "net_value" => "float",
    ];

    public static $rules = [
        'txtBillingAgreement' => 'required',
    ];

    public static $fields = [
        'txtBillingAgreement' => 'Contrato',
    ];

    public function pattern_state()
    {
        return $this->belongsTo('App\Models\PatternState','pattern_state_id');
    }

    public function billing_detail()
    {
        return $this->hasMany('App\Models\BillingDetail','billing_id');
    }

    public function agreement()
    {
        return $this->belongsTo('App\Models\Agreement','agreement_id');
    }

    public function user_cre()
    {
        return $this->belongsTo('App\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }
}
