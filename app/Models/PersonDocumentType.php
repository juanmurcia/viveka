<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonDocumentType extends Model
{
    public $timestamps = false;

    protected $table = 'person_document_types';

    public $fillable = [
        "name",
        "slug",
    ];

}
