<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BillingDetail extends Model
{
    public $table = 'billing_details';
    public $primaryKey = "id";

    public $fillable = [
        "billing_id",
        "product_id",
        "value",
        "user_cre_id",
        "user_mod_id"
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */

    protected $casts = [
        "user_mod_id" => "integer",
        "user_cre_id" => "integer",
        "billing_id" => "integer",
        "product_id" => "integer",
        "value" => "float",
    ];

    public static $rules = [
        'txtBillingAgreement' => 'required',
    ];

    public static $fields = [
        'txtBillingAgreement' => 'Contrato',
    ];

    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_id');
    }

    public function billing()
    {
        return $this->belongsTo('App\Models\Billing','billing_id');
    }

    public function user_cre()
    {
        return $this->belongsTo('App\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }
}
