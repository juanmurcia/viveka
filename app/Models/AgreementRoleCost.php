<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgreementRoleCost extends Model
{
    public $table = 'agreement_role_costs';

    public $fillable = [
        "active",
        "agreement_id",
        "role_id",
        "value",
        "justification",
        "user_mod_id",
    ];

    protected $casts = [
        "agreement_id" => "integer",
        "role_id" => "integer",
        "value" => "integer",
        "user_mod_id" => "integer"
    ];

    public static $rules = [
        'txtAgreementRoleCostAgreementId' => 'required',
        'optAgreementRoleCostRoleId' => 'required',
        'txtAgreementRoleCostValue' => 'required',

    ];

    public static $fields = [
        'txtAgreementRoleCostAgreementId' => 'Contrato',
        'optAgreementRoleCostRoleId' => 'Rol',
        'txtAgreementRoleCostValue' => 'Costo',
        'txtAgreementRoleCostJustification' => 'Justificación',
    ];

    public function agreement()
    {
        return $this->belongsTo('App\Models\Agreement','agreement_id');
    }

    public function role()
    {
        return $this->belongsTo('Caffeinated\Shinobi\Models\Role','role_id');
    }

    public function user_cre()
    {
        return $this->belongsTo('App\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }

}
