<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use App\Models\Silog\Configuracion\AccionPagina;
use DB;

class clsUtil extends Model
{    
    protected $accion_pagina;
    protected $bd;
    
    function __construct(clsBd $bd, AccionPagina $obj_accion_pagina)
    {
        $this->accion_pagina         = $obj_accion_pagina;
        $this->bd                    = $bd;
    }
    
    /**
     * Function that removes extraneous characters
     * @param String $val
     * @param int $cant_caracteres
     * @return String
     **/
    function limpia_dato($val, $cant_caracteres = '') 
    {
        if (!empty($cant_caracteres))
            $val = addslashes(trim(substr($val, 0, $cant_caracteres)));
        else
            $val = addslashes(trim(strtoupper($val)));
								
        $val = str_replace("'", "", $val);
        $val = str_replace("\"", "", $val);
        $val = str_replace("(", "", $val);
        $val = str_replace(")", "", $val);
        $val = str_replace("&", "", $val);
        $val = str_replace("|", "", $val);
        $val = str_replace("<", "", $val);
        $val = str_replace(">", "", $val);
        $val = str_replace("--", "", $val);
        $val = str_replace("�", "�", $val);
        $val = str_replace("�", "�", $val);
        $val = str_replace("�", "a", $val);
        $val = str_replace("�", "A", $val);
        $val = str_replace("�", "a", $val);
        $val = str_replace("�", "A", $val);
        $val = str_replace("�", "e", $val);
        $val = str_replace("�", "E", $val);
        $val = str_replace("�", "i", $val);
        $val = str_replace("�", "I", $val);
        $val = str_replace("�", "o", $val);
        $val = str_replace("�", "O", $val);
        $val = str_replace("�", "u", $val);
        $val = str_replace("�", "U", $val);
        $val = str_replace('"', "", $val);
        $val = preg_replace("[\n|\r|\n\r]", " ", $val);
								
        return $val;
    }
     /**
     * Function return the active actions for one profile 
     * @param Integer $id_pagina     
     * @return Array $accion 
     **/
    function modulo_permitido() 
    {          
         $modulo = array();
        if(\Auth::check())
        {
            $arrData['name']='pl_modulo_permitido';
            $arrData['param']=[auth()->user()->id_perfil];
            $arrData['return']=[
                'count INTEGER', 
                'id_modulo_ppl INTEGER', 
                'nombre TEXT', 
                'logo_ppl TEXT', 
                'logo TEXT', 
                'icono_css TEXT', 
                'color_css TEXT', 
                'url TEXT'
            ];
            $modulo = $this->bd->ejecuta_procedimiento($arrData);   
        }
        return $modulo;
    }
    function es_accion_permitida($id_pagina) 
    {          
        $accion = array();
        if(\Auth::check() AND !empty($id_pagina))
        {
            $arrData['get']=true;
            $arrData['modelo']=$this->accion_pagina;
            $arrData['filtro']=[
                ['campo'=>'id_pagina ','value'=>$id_pagina],
                ['subConsulta'=>true,'table'=>'tb_perfil_detalle','fillable'=>['id_accion_pagina'],'campo'=>'id_accion_pagina',
                    'filtro'=>[
                        ['campo'=>'id_perfil','value'=>auth()->user()->id_perfil],
                    ]
                ], 
            ];  
            $data = $this->bd->consultar($arrData);
            if(sizeof($data) > 0)
            {
                foreach ($data as $codigo)
                {
                    $accion[$codigo->id_accion] = true;
                }
            }
        }
        return $accion;
    }
    function menu_usuario($arr_data) 
    {          
        $arr_pagina = array();  
        $arr_modulo = array();
        if(\Auth::check() AND isset($arr_data['modulo_ppl']))
        {                  
            $arrData['get']=true;
            $arrData['modelo']=$arr_data['mod_pag'];
            $arrData['filtro']=
            [
                ['campo'=>'id_modulo_ppl','value'=>$arr_data['modulo_ppl']],
                ['campo'=>'visible','value'=>'TRUE'],
                [
                    'group'=>true,
                    'data'=>[
                        ['subConsulta'=>true,'table'=>'tb_accion_pagina','fillable'=>['id_pagina'],'campo'=>'id_pagina',
                            'filtro'=>[
                                ['campo'=>'id_accion','value'=>'1'],
                                ['subConsulta'=>true,'table'=>'tb_perfil_detalle','fillable'=>['id_accion_pagina'],'campo'=>'id_accion_pagina',
                                    'filtro'=>[
                                        ['campo'=>'id_perfil','value'=>auth()->user()->id_perfil],
                                    ]
                                ], 
                            ]
                        ],                
                        ['subConsulta'=>true,'table'=>'tb_accion_pagina AS AC','fillable'=>['P.id_pagina_ppal'],'campo'=>'id_pagina',                    
                            'filtro'=>[
                                ['campo'=>'id_accion','value'=>'1'],
                                ['subConsulta'=>true,'table'=>'tb_perfil_detalle','fillable'=>['id_accion_pagina'],'campo'=>'id_accion_pagina',
                                    'filtro'=>[
                                        ['campo'=>'id_perfil','value'=>auth()->user()->id_perfil],
                                    ]
                                ], 
                            ],
                            'join'=>[
                                ['table'=>'tb_pagina AS P','campoA'=>'P.id_pagina','campoB'=>'AC.id_pagina'],
                            ],
                            'type'=>'OR'
                        ]
                    ]
                ],    
            ];                
            $arrData['join']=[                
                ['table'=>$arr_data['mod_modulo']->table,'campoA'=>$arr_data['mod_modulo']->table.'.'.$arr_data['mod_modulo']->primaryKey,'campoB'=>$arr_data['mod_pag']->table.'.'.$arr_data['mod_modulo']->primaryKey],                        
            ];
            $arrData['orderBy']=[
                ['campo'=>$arr_data['mod_pag']->table.'.'.$arr_data['mod_modulo']->primaryKey],
                ['campo'=>'submenu'],
                ['campo'=>'nom_pagina'],
            ];
            $data = $this->bd->consultar($arrData);
            
            if(sizeof($data) > 0)
            {                
                $i =0;
                foreach ($data as $pagina)
                {                        
                    $arr_modulo[$pagina->id_modulo] = [
                        'nombre'=>$pagina->nom_modulo,
                        'simbolo'=>$pagina->simbolo,
                    ];
                    if(!$pagina->submenu)
                    {
                        $arr_pagina[$pagina->id_pagina] = [
                            'nombre'=>$pagina->nom_pagina,
                            'direccion'=>(!empty($pagina->dir_pagina) ? $pagina->dir_pagina : '#'),
                            'modulo'=>$this->camel_case($pagina->nom_modulo),
                        ];
                    }                
                        
                    if($pagina->submenu)
                    {
                        $arr_pagina[$pagina->id_pagina] = [
                            'nombre'=>$pagina->nom_pagina,
                            'direccion'=>(!empty($pagina->dir_pagina) ? $pagina->dir_pagina : '#'),
                            'modulo'=>$this->camel_case($arr_pagina[$pagina->id_pagina_ppal]['nombre']),
                        ];
                    }                
                }                
            }
        }
        return array('paginas'=>$arr_pagina,'modulos'=>$arr_modulo);
    }
    function camel_case($str, array $noStrip = [])
    {
        // non-alpha and non-numeric characters become spaces
        $str = preg_replace('/[^a-z0-9' . implode("", $noStrip) . ']+/i', ' ', $str);
        $str = trim($str);
        // uppercase the first character of each word
        $str = ucwords($str);
        $str = str_replace(" ", "", $str);
        $str = lcfirst($str);

        return $str;
    }
    function perfil_sin_permiso()
    {
        return response()->json([
            'ErrorPeril' => "Esta accion no esta permitida para su perfil ",
            'TlErrorPeril' => "Whoops !! Se ha detectado un error de autenticacion ",
        ]);
    }
    function siguiente_inventario($datos)
    {
        $arrData['name']='nroactinventario';
        $arrData['param']=[$datos['id_tipo_doc_inventario'],$datos['id_sucursal']];
        $arrData['return']=[
            'num_actual numeric', 
            'prefijo varchar', 
            'num_desde numeric', 
            'id_est_doc_inventario integer', 
            'num_hasta numeric'
        ];
        $arr_inv = $this->bd->ejecuta_procedimiento($arrData);                        
        
        return $arr_inv[0];
    }
}