<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatternDocumentType extends Model
{
    public $table = 'pattern_document_types';
    public $primaryKey = "id";

    public $fillable = [        
        "active",
        "pattern_id",
        "name",
        "user_cre_id",
        "user_mod_id",
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    
    protected $casts = [
        "pattern_id" => "integer",
        "user_cre_id" => "integer",
        "user_mod_id" => "integer",
        "name" => "string|Min:4"
    ];

    public static $rules = [
        'optPatternId' => 'required',
        'txtName' => 'required'
    ];
    /*
     *
     * 'optPatternId' => 'required',
        'txtName' => 'required'
     */
    
    public static $fields = [
        'optPatternId' => 'Modelo',
        'txtName' => 'Nombre'
    ];

    public function pattern()
    {
        return $this->belongsTo('App\Models\Pattern','pattern_id');
    }

    public function user_cre()
    {
        return $this->belongsTo('App\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }
}
