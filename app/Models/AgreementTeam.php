<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgreementTeam extends Model
{
    public $table = 'agreement_teams';

    public $fillable = [
        "active",
        "professional_id",
        "agreement_id",
        "user_mod_id",
    ];

    protected $casts = [
        "professional_id" => "integer",
        "agreement_id" => "integer",
        "user_mod_id" => "integer"
    ];

    public static $rules = [
        'txtAgreementTeamAgreementId' => 'required',
        'txtAgreementTeamProfessionalId' => 'required',
    ];

    public static $fields = [
        'txtAgreementTeamAgreementId' => 'Contrato',
        'txtAgreementTeamProfessionalId' => 'Profesional',
    ];

    public function professional()
    {
        return $this->belongsTo('App\Models\Professional','professional_id');
    }

    public function agreement()
    {
        return $this->belongsTo('App\Models\Agreement','agreement_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }

}
