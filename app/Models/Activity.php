<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Activity extends Model
{
    public $table = 'activities';

    public static $pattern_id = 4;

    public $fillable = [
        "pattern_id",
        "model_id",
        "name",
        "description",
        "percentage",
        "start_date",
        "end_date",
        "active",
        "pattern_state_id",
        "percentage",
        "justification",
        "value",
        "user_cre_id",
        "user_mod_id",
    ];

    public $audit_field = [
        "responsible_id"=> ['name'=>'Responsable', 'model'=>'Professional'],
        "name"=> ['name'=>'Nombre'],
        "description"=> ['name'=>'Descripción'],
        "start_date"=> ['name'=>'Fecha Inicio'],
        "end_date"=> ['name'=>'Fecha Fin'],
        "active"=> ['name'=>'Activo'],
        "pattern_state_id"=> ['name'=>'Estado', 'model'=>'PatternState'],
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */

    protected $casts = [
        "user_cre_id" => "integer",
        "user_mod_id" => "integer",
        "pattern_id" => 'integer',
        "model_id" => 'integer',
        "value"=> "float",
        "name"=> "string|Min:4",
        "description"=> "string|Min:4",
        "start_date"=> "date:Y-m-d",
        "end_date"=> "date:Y-m-d"
    ];

    public static $initial_rules = [
        'txtActivityPatternId' => 'required',
        'txtActivityModelId' => 'required',
        'txtActivityName' => 'required',
        'txtActivityDescription' => 'required',
        'txtActivityStartDate' => 'required',
        'txtActivityEndDate' => 'required|after:txtActivityStartDate',
        'txtActivityValue' => 'required',
    ];

    public static $rules = [
        'txtActivityName' => 'required',
        'txtActivityDescription' => 'required',
        'txtActivityStartDate' => 'required',
        'txtActivityEndDate' => 'required|after:txtActivityStartDate',

    ];

    public  static $progress_rules = [
        'optActivityPatternState' => 'required',
        'txtActivityPercentage' => 'required',
        'txtActivityJustification' => 'required',
    ];

    public static $fields = [
        'txtActivityPatternId' => 'Modelo',
        'txtActivityModelId' => 'Id de Modelo',
        'txtActivityName' => 'Nombre',
        'txtActivityDescription' => 'Descripción',
        'txtActivityStartDate' => 'Fecha Inicio',
        'txtActivityEndDate' => 'Fecha Fin',
        'optActivityPatternState' => 'Estado',
        'txtActivityPercentage' => 'Porcentage',
        'txtActivityJustification' => 'Justificación',
        'txtActivityValue' => 'Valor',
    ];

    public function filter_search($term){
        $query = DB::table($this->table)
            ->select(['activities.created_at', 'activities.name AS name', 'activities.description AS detail', 'start_date', 'end_date'])
            ->whereRaw("activities.active IS TRUE AND (
                activities.name LIKE '%$term%' OR activities.description LIKE '%$term%')")
            ->get();
        return $query;
    }

    public function pattern()
    {
        return $this->belongsTo('App\Models\Pattern','pattern_id');
    }

    public function pattern_state()
    {
        return $this->belongsTo('App\Models\PatternState', 'pattern_state_id');
    }

    public function estimated_times()
    {
        return $this->hasMany('App\Models\ActivityEstimatedTime');
    }

    public function estimated_costs()
    {
        return $this->hasMany('App\Models\ActivityEstimatedCost');
    }

    public function executed_times()
    {
        return $this->hasMany('App\Models\ActivityExecutedTime');
    }

    public function executed_costs()
    {
        return $this->hasMany('App\Models\ActivityExecutedCost');
    }

    public function user_cre()
    {
        return $this->belongsTo('App\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\User', 'user_mod_id');
    }
}
