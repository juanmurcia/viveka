<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgreementCategory extends Model
{
    public $table = 'agreement_categories';    

    public $fillable = [        
        "name",
        "iso",
    ];
}
