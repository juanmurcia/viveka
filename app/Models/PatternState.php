<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatternState extends Model
{
    public $table = 'pattern_states';

    public $fillable = [        
        "pattern_id",
        "name",
    ];

    public function pattern()
    {
        return $this->belongsTo('App\Models\Pattern','pattern_id');
    }
}
