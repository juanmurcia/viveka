<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgreementProcessType extends Model
{
    public $table = 'agreement_process_types';    

    public $fillable = [        
        "name",
    ];
}
