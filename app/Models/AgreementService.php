<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgreementService extends Model
{
    public $table = 'agreement_services';    

    public $fillable = [        
        "name",
    ];
}
