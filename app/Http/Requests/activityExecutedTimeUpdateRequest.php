<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\ActivityExecutedTime;

class activityExecutedTimeUpdateRequest extends FormRequest
{
    /**
     * Determine if the Activity is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return ActivityExecutedTime::$rules;
    }

    /** Get alias from data form 
     * @return array
     */
    
    public function attributes()
    {
       return ActivityExecutedTime::$fields;
    }
    
    /** 
     * Get id from url  
     * @return integer
     */
    private function getSegmentFromEnd($position_from_end = 1) 
    {
        $segments =$this->segments();
        return $segments[sizeof($segments) - $position_from_end];
    }
}
