<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Document;

class documentUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Document $rules)
    {
        return Document::$rules;
    }

    /** Get alias from data form
     * @return array
     */

    public function attributes()
    {
        return Document::$fields;
    }

    /**
     * Get id from url
     * @return integer
     */

    private function getSegmentFromEnd($position_from_end = 1)
    {
        $segments =$this->segments();
        return $segments[sizeof($segments) - $position_from_end];
    }
}
