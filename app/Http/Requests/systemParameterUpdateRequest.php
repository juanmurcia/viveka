<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\SystemParameter;

class systemParameterUpdateRequest extends FormRequest
{
    /**
     * Determine if the Person is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(SystemParameter $rules)
    {
        return SystemParameter::$rules;
    }

    /** Get alias from data form 
     * @return array
     */
    
    public function attributes()
    {
       return SystemParameter::$fields;
    }
    
    /** 
     * Get id from url  
     * @return integer
     */
    private function getSegmentFromEnd($position_from_end = 1) 
    {
        $segments =$this->segments();
        return $segments[sizeof($segments) - $position_from_end];
    }
}
