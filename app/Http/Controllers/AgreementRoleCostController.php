<?php

namespace App\Http\Controllers;

use App\Http\Requests\agreementRoleCostCreateRequest;
use App\Models\AgreementRoleCost;
use Caffeinated\Shinobi\Models\Role;

class AgreementRoleCostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function list($id)
    {
        $role = Role::find(auth()->user()->role_id);
        $destroy = $role->can('agreement_role_cost.destroy');

        $agreement_role_cost = AgreementRoleCost::with('role')->where('agreement_id',$id)->get();

        $data = [];
        foreach ($agreement_role_cost as $agreement_role_cost_item) {
            if($agreement_role_cost_item['active']==false){
                continue;
            }
            $item=[];
            $item['id'] = $agreement_role_cost_item['id'];
            $item['role'] = $agreement_role_cost_item['role']['name'];
            $item['value'] = $agreement_role_cost_item['value'];

            if($destroy){
                $item['buttons'] = '<a class="btn btn-round btn-danger btn-xs" onclick="deleteAgreementRoleCost('.$item['id'].')"><i class="fa fa-close"></i></a>';
            }

            array_push($data,$item);
        }

        return response()->json(['data' => $data]);
    }

    public function store(agreementRoleCostCreateRequest $request)
    {
        if ($request->ajax()) {
            $agreement_role_cost = AgreementRoleCost::firstOrCreate(
                [
                    'agreement_id' => $request['txtAgreementRoleCostAgreementId'],
                    'role_id' => $request['optAgreementRoleCostRoleId'],
                ],
                [
                    'value' => 0,
                    'user_mod_id' => auth()->user()->id,
                ]
            );

            $agreement_role_cost->update(
                [
                    'value' => $request['txtAgreementRoleCostValue'],
                    'user_mod_id' => auth()->user()->id,
                ]
            );

            return response()->json(['mensaje' => 'Guardado']);
        }
    }

    public function show($id)
    {
        $agreement_role_cost = AgreementRoleCost::find($id)->with(['agreement','role'])->get();

        return response()->json(['data' => $agreement_role_cost]);
    }
}