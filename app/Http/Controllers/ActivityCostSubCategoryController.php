<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ActivityCostSubCategory;

use Caffeinated\Shinobi\Models\Role;

class ActivityCostSubCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        if ($request->ajax())
        {
            $data =[
                'name' => $request['txtCategoryName'],
                'activity_cost_category_id' => $request['txtCategoryId'],
                'active' => true,
                'user_cre_id' => auth()->user()->id,
            ];
            $activity_cost_subcategory = new ActivityCostSubCategory($data);
            $activity_cost_subcategory->save();

            return response()->json(['mensaje' => 'creado']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $activity_cost_subcategory = ActivityCostSubCategory::with(['activity_cost_category'])->where('id',$id)->get();
        return response()->json($activity_cost_subcategory[0]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $data =[
            'active' => true,
            'user_mod_id' => auth()->user()->id,
        ];
        ActivityCostSubCategory::find($id)->update($data);

        return response()->json(['mensaje' => 'Activado']);
    }


    public function update(Request $request, $id)
    {
        if ($request->ajax()) 
        {
            $data =[
                'name' => $request['txtCategoryName'],
                'user_mod_id' => auth()->user()->id,
            ];      
            ActivityCostSubCategory::find($id)->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }


    public function destroy($id)
    {
        $data =[
            'active' => false,
            'user_mod_id' => auth()->user()->id,
        ];
        ActivityCostSubCategory::find($id)->update($data);

        return response()->json(['mensaje' => 'Inactivo']);
    }
}