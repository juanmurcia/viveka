<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Professional;
use App\Models\Country;
use App\Models\PersonDocumentType;
use App\Models\TaxpayerType;
use Caffeinated\Shinobi\Models\Role;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use DataTables;

use App\Http\Requests\professionalCreateRequest;
use App\Http\Requests\professionalUpdateRequest;

class ProfessionalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role= Role::all();
        $person_document_type = PersonDocumentType::all();
        $taxpayer_type = TaxpayerType::all();
        $country = Country::all();

        return view('professional.table', [
            'role'=>$role,
            'country'=>$country,
            'taxpayer_type'=>$taxpayer_type,
            'person_document_type'=>$person_document_type
        ]);
    }

    /**
     * Show the dataTable.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        if ($request->ajax()) {
            $role = Role::find(auth()->user()->role_id);
            $professionals = Professional::with(['person','person.person_document_type','nationality'])->select('professionals.*');

            return Datatables::eloquent($professionals)
                ->editColumn('gender', function($professional){
                    return ($professional->gender == 'M' ? 'Masculino' : 'Femenino');
                })
                ->addColumn('age', function ($professional){
                    $birth = explode('-',substr($professional->birth_date,0,10));
                    return Carbon::createFromDate($birth[0],$birth[1],$birth[2])->age;
                })
                ->addColumn('state', function ($professional){
                    return ($professional->active ? 'ACTIVO' : 'INACTIVO');
                })
                ->addColumn('buttons', function ($professional) use ($role) {
                    $button = '<div class="row">';

                    if($role->can('professional.edit')){
                        $button .= '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round" onclick="editProfessional('.$professional->id.');" title="Editar Profesional" rol="tooltip" ><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                    }

                    if($role->can('user.create')){
                        $button .= '<a href="javascript:void(0)" class="btn btn-sm btn-warning btn-icon btn-outline btn-round" onclick="userPerson('.$professional->person_id.');"  title="Agregar Usuario" rol="tooltip" ><i class="fa fa-user" aria-hidden="true"></i></a>';
                    }

                    if($role->can('professional.audit')){
                        $button .= '<a onclick="audit(7,'.$professional->id.');" class="btn btn-sm btn-warning btn-icon btn-outline btn-round" title="Ver Auditoria" rol="tooltip" ><i class="fa fa-history" aria-hidden="true"></i></a>';
                    }

                    if($role->can('professional.destroy')) {
                        if ($professional->active) {
                            $button .= '<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-icon btn-outline btn-round" onclick="destroyProfessional('.$professional->id.')" title="Inctivar Profesional" rol="tooltip" ><i class="fa fa-close" aria-hidden="true"></i></a>';
                        }else{
                            $button .= '<a href="javascript:void(0)" class="btn btn-sm btn-success btn-icon btn-outline btn-round" onclick="activateProfessional('.$professional->id.')" title="Activar Profesional" rol="tooltip" ><i class="fa fa-check" aria-hidden="true"></i></a>';
                        }
                    }

                    $button .= '</div>';
                    return $button;
                })
                ->rawColumns(['buttons'])
                ->setRowId('id')
                ->make(true);
        }
    }


    public function create()
    {
        
    }

    public function store(professionalCreateRequest $request)
    {
        if ($request->ajax()) 
        {
            $data =[
                'person_id' => $request['txtPerson'],
                'birth_date' => $request['txtBirthDate'],
                'gender' => $request['txtGender'],
                'nationality_id' => $request['optNationality'],
                'user_cre_id' => auth()->user()->id,
            ];
            $professional = new professional($data);
            $professional->save();

            return response()->json(['mensaje' => 'creado']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $professional = Professional::find($id);
        $professional->person;

        return response()->json($professional);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $data =[
            'active' => true,
            'user_mod_id' => auth()->user()->id,
        ];
        $professional = Professional::find($id)
                    ->update($data);

        return response()->json(['mensaje' => 'Activado']);
    }


    public function update(professionalUpdateRequest $request, $id)
    {
        if ($request->ajax()) 
        {
            $data =[
                'person_id' => $request['txtPerson'],
                'birth_date' => $request['txtBirthDate'],
                'gender' => $request['txtGender'],
                'nationality_id' => $request['optNationality'],
                'user_mod_id' => auth()->user()->id,
            ];      
            $professional = Professional::find($id)
                        ->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }


    public function destroy($id)
    {
        $data =[
            'active' => false,
            'user_mod_id' => auth()->user()->id,
        ];
        $professional = Professional::find($id)
                    ->update($data);

        return response()->json(['mensaje' => 'Inactivo']);
    }

    public function search(Request $request)
    {
        $data = [];
        $professionals = DB::table('professionals')
            ->join('people', function ($query) {
                $query->on('people.id', '=', 'professionals.person_id');
            })
            ->join('users', function ($query) {
                $query->on('people.id', '=', 'users.person_id');
            })
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->where('people.active', '=', true)
            ->where('users.active', '=', true)
            ->where('professionals.active', '=', true);

        if($request->has('role_slugs')) {
            $role_slugs = explode(',', $request['role_slugs']);
            $professionals = $professionals->whereIn('roles.slug', $role_slugs);
        }

        if($request->has('q')) {
            $professionals = $professionals->whereRaw("CONCAT_WS('',people.document,' - ',people.name,' ',people.last_name) LIKE ?", ['%' . $request['q'] . '%']);
        }

        $professionals = $professionals
            ->select('people.document','people.name','people.last_name','professionals.id AS professional_id')
            ->orderBy('people.name','asc')
            ->get();


        foreach ($professionals as $result) {
            $value = $result->document.' - '.$result->name.' '.$result->last_name;
            $data[] = [ 'id' => $result->professional_id, 'text' => $value];
        }

        if(count($data)==0){
            return response()->json(['results' => [ 'id' => '', 'text' => 'Sin resultados']]);
        }

        return response()->json(['results' => $data]);
    }
}