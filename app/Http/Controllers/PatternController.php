<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Pattern as pattern;
use Caffeinated\Shinobi\Models\Role;
use App\Models\Activity;
use App\Models\Agreement;
use App\Models\Project;
use App\Models\Product;
use App\Models\AgreementRoleCost;
use App\Models\ActivityEstimatedTime;
use App\Models\ActivityExecutedTime;
use App\Models\ActivityEstimatedCost;
use App\Models\ActivityExecutedCost;
use App\Models\ActivityCostCategory;

use App\Http\Requests\patternCreateRequest;
use App\Http\Requests\patternUpdateRequest;

class PatternController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $role = Role::all();

        return view('pattern.table');
    }

    /**
     * Show the dataTable.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $role = Role::find(auth()->user()->role_id);
        $edit = $role->can('pattern.edit');
        $destroy = $role->can('pattern.destroy');

        $patterns = pattern::all();

        $data = [];
        foreach ($patterns as $pattern) {
            $id = $pattern['id'];

            $pattern['state'] = ($pattern['active'] ? 'ACTIVO' : 'INACTIVO');

            $pattern['buttons'] = '<div>';

            if($edit){
                $pattern['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round" onclick="editPattern('.$id.')" title="Editar Modelo" rol="tooltip" ><i class="fa fa-pencil" aria-hidden="true"></i></a>';
            }

            if($destroy) {
                if ($pattern['active']) {
                    $pattern['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-icon btn-outline btn-round" onclick="destroyPattern('.$id.')" title="Inctivar Modelo" rol="tooltip" ><i class="fa fa-close" aria-hidden="true"></i></a>';
                }else{
                    $pattern['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-success btn-icon btn-outline btn-round" onclick="activatePattern('.$id.')" title="Activar Modelo" rol="tooltip" ><i class="fa fa-check" aria-hidden="true"></i></a>';
                }
            }
            

            $pattern['buttons'] .= '</div>';
            array_push($data,$pattern);
        }

        return response()->json(['data' => $data]);
    }


    public function create()
    {
        
    }

    public function store(patternCreateRequest $request)
    {
        if ($request->ajax()) 
        {
            $data =[
                'model' => $request['txtModel'],
                'name' => $request['txtName'],
                'active' => true,
                'user_cre_id' => auth()->user()->id,
            ];
            $pattern = new pattern($data);
            $pattern->save();

            return response()->json(['mensaje' => 'creado']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pattern = pattern::find($id);

        return response()->json($pattern);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $data =[
            'active' => true,
            'user_mod_id' => auth()->user()->id,
        ];
        $pattern = pattern::find($id)
                    ->update($data);

        return response()->json(['mensaje' => 'Activado']);
    }


    public function update(patternUpdateRequest $request, $id)
    {
        if ($request->ajax()) 
        {
            $data =[
                'model' => $request['txtModel'],
                'name' => $request['txtName'],
                'user_mod_id' => auth()->user()->id,
            ];      
            $pattern = pattern::find($id)
                        ->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }


    public function destroy($id)
    {
        $data =[
            'active' => false,
            'user_mod_id' => auth()->user()->id,
        ];
        $pattern = pattern::find($id)
                    ->update($data);

        return response()->json(['mensaje' => 'Inactivo']);
    }

    public static function activities($pattern_id, $model_id){
        $models=[];

        //self
        $models[] = ['pattern_id' => $pattern_id, 'model_id' => $model_id];

        foreach ($models as $model){
            $agreements=[];
            if($model['pattern_id']==1){
                $agreements[]=$model['model_id'];
            }
            $projects = DB::table('projects')->whereIn('agreement_id',$agreements)->select('id')->get();
            foreach ($projects as $project){
                $models[] = ['pattern_id' => 2, 'model_id' => $project->id];
            }
        }

        foreach ($models as $model){
            $projects=[];
            if($model['pattern_id']==2){
                $projects[]=$model['model_id'];
            }
            $products = DB::table('products')->whereIn('project_id',$projects)->select('id')->get();
            foreach ($products as $product){
                $models[] = ['pattern_id' => 3, 'model_id' => $product->id];
            }
        }

        $activities = DB::table('activities')
            ->where('active',true)
            ->where(function ($query) use ($models){
                foreach ($models as $model){
                    $query->orWhere(function ($query) use ($model){
                        $query->where('pattern_id',$model['pattern_id']);
                        $query->where('model_id',$model['model_id']);
                    });
                }
            });


        $activities = $activities->select('id')->get();

        $activity_ids=[];
        foreach ($activities as $activity){
            $activity_ids[]=$activity->id;
        }

        return $activity_ids;
    }

    public function names(Request $request)
    {
        $data=[];

        $pattern_names = DB::table('pattern_names')->join('patterns','patterns.id','=','pattern_names.pattern_id');

        /*if($request->has('id')){
            $people=$people->whereRaw('people.id IN ('.$request['id'].')');
        }*/

        $results=$pattern_names->whereRaw("CONCAT_WS('',patterns.name,' - ',pattern_names.name) LIKE ?",['%'.$request['search'].'%'])
            ->select(
                'pattern_names.model_id AS model_id',
                'pattern_names.pattern_id AS pattern_id',
                'patterns.name AS pattern',
                'pattern_names.name AS pattern_name')
            ->orderBy('patterns.id','asc')
            ->orderBy('pattern_names.name','asc')
            ->get();

        foreach ($results as $result) {
            $id = $result->pattern_id.','.$result->model_id;
            $value = $result->pattern.' - '.$result->pattern_name;
            $data[] = [ 'id' => $id, 'value' => $value];
        }

        if(count($data)==0){
            $data[]= [ 'id' => '', 'value' => 'No se encontró'];
        }

        return response()->json($data);
    }

    public function times($pattern_id, $model_id){
        $roles = Role::all();
        $activity_ids = $this->activities($pattern_id,$model_id);

        $activity_estimated_times = ActivityEstimatedTime::where('active',true)
            ->whereIn('activity_id',$activity_ids)
            ->groupBy('role_id')
            ->selectRaw('role_id, SUM(estimated) as estimated')
            ->get();

        $activity_executed_times = ActivityExecutedTime::where('active',true)
            ->whereIn('activity_id',$activity_ids)
            ->groupBy('role_id')
            ->selectRaw('role_id, SUM(executed) as executed')
            ->get();

        $data=[];
        foreach ($roles as $role){
            $data[$role['id']]=[
                'role'=>$role['name'],
                'role_id'=>$role['id'],
                'estimated'=>0,
                'executed'=>0
            ];
        }

        foreach ($activity_estimated_times as $activity_estimated_time){
            $data[$activity_estimated_time['role_id']]['estimated']=$activity_estimated_time['estimated'];
        }

        foreach ($activity_executed_times as $activity_executed_time){
            $data[$activity_executed_time['role_id']]['executed']=$activity_executed_time['executed'];
        }

        return response()->json(['data' => $data]);
    }


    public function costs($pattern_id, $model_id){
        $activity_cost_categories = ActivityCostCategory::all();
        $activity_ids = $this->activities($pattern_id,$model_id);

        $activity_estimated_costs = ActivityEstimatedCost::where('active',true)
            ->whereIn('activity_id',$activity_ids)
            ->groupBy('activity_cost_category_id')
            ->selectRaw('activity_cost_category_id, SUM(estimated) as estimated')
            ->get();

        $activity_executed_costs = ActivityExecutedCost::where('activity_executed_costs.active',true)
            ->whereIn('activity_executed_costs.activity_id',$activity_ids)
            ->join('activity_cost_subcategories','activity_cost_subcategories.id','=','activity_executed_costs.activity_cost_subcategory_id')
            ->join('activity_cost_categories','activity_cost_categories.id','=','activity_cost_subcategories.activity_cost_category_id')
            ->groupBy('activity_cost_categories.id')
            ->selectRaw('activity_cost_categories.id AS activity_cost_category_id, SUM(executed) as executed')
            ->get();

        $data=[];
        foreach ($activity_cost_categories as $activity_cost_category){
            $data[$activity_cost_category['id']]=[
                'activity_cost_category'=>$activity_cost_category['name'],
                'activity_cost_category_id'=>$activity_cost_category['id'],
                'estimated'=>0,
                'executed'=>0
            ];
        }

        foreach ($activity_estimated_costs as $activity_estimated_cost){
            $data[$activity_estimated_cost['activity_cost_category_id']]['estimated']=$activity_estimated_cost['estimated'];
        }

        foreach ($activity_executed_costs as $activity_executed_cost){
            $data[$activity_executed_cost['activity_cost_category_id']]['executed']=$activity_executed_cost['executed'];
        }

        return response()->json(['data' => $data]);
    }

    public function role_times($pattern_id,$model_id)
    {
        $complete_table_data = []; /** final table to be returned **/
        $date_limits = [];
        $role_costs = [];
        $roles = Role::all();
        $activity_cost_categories = ActivityCostCategory::all();

        /** Get model instance, audit table and cost-hour of each role **/
        switch ($pattern_id) {
            case 1:
                $model = Agreement::with(['agreement_role_cost','projects','projects.products'])->find($model_id);
                $audit = 'audit_agreements';
                $agreement_role_costs = $model->agreement_role_cost;
                $project = [];
                $product = [];
                foreach ($model->projects as $data_project){
                    array_push( $project, $data_project->id);
                    foreach ($data_project->products AS $data_product){
                        array_push( $product, $data_product->id);
                    }
                }

                $filter_activity = DB::select("SELECT GROUP_CONCAT(id SEPARATOR ',') AS ids FROM activities WHERE (pattern_id = 1 AND model_id = $model_id) 
                                    OR (pattern_id = 2 AND model_id IN (".implode(',',$project).")) 
                                    OR (pattern_id = 3 AND model_id IN (".implode(',',$product)."))") ;
                $filter_activity = (isset($filter_activity[0]) ? $filter_activity[0]->ids: 0);
                break;
            case 2:
                $model = Project::with(['agreement.agreement_role_cost','products'])->find($model_id);
                $audit = 'audit_projects';
                $agreement_role_costs = $model->agreement->agreement_role_cost;

                $product = [];
                foreach ($model->products AS $data_product){
                    array_push( $product, $data_product->id);
                }

                $filter_activity = DB::select("SELECT GROUP_CONCAT(id SEPARATOR ',') AS ids FROM activities WHERE (pattern_id = 2 AND model_id = $model_id)                                    
                                    OR (pattern_id = 3 AND model_id IN (".implode(',',$product)."))") ;
                $filter_activity = (isset($filter_activity[0]) ? $filter_activity[0]->ids: 0);

                break;
            case 3:
                $model = Product::with(['project.agreement.agreement_role_cost'])->find($model_id);
                $audit = 'audit_products';
                $agreement_role_costs = $model->project->agreement->agreement_role_cost;
                $filter_activity = DB::select("SELECT GROUP_CONCAT(id SEPARATOR ',') AS ids FROM activities WHERE (pattern_id = 3 AND model_id = $model_id)") ;
                $filter_activity = (isset($filter_activity[0]) ? $filter_activity[0]->ids: 0);
                break;
            default:
                return response()->json(['error', 'pattern_id '.$pattern_id.'not supported']);
                break;
        }

        /** save cost-hour as an array[id]=value to search by key after **/
        foreach ($agreement_role_costs as $agreement_role_cost){
            $role_costs[$agreement_role_cost->role_id]=$agreement_role_cost->value;
        }

        /** get all activities from current model and their children **/
        $activity_ids = $this->activities($pattern_id,$model_id);

        /** get all executed hours using list of activities as filter **/
        $executed = ActivityExecutedTime::where('active',true)
            ->whereIn('activity_id',$activity_ids)
            ->orderBy('end_date','asc')
            ->get();

        //return response()->json($activity_ids);
        $activity_ids[]=0;

        /** get last estimated time of each day **/
        $estimated = DB::select("SELECT
                      ATS.activity_id, ATS.role_id, AA.old_value,AA.new_value, CAST(AA.created_at AS DATE) AS audit_date
                FROM `audit_activity_estimated_times` AS AA
                LEFT JOIN `audit_activity_estimated_times` AS OO ON AA.id_parent = OO.id_parent AND AA.field = OO.field AND CAST(AA.created_at AS DATE) = CAST(OO.created_at AS DATE) AND AA.created_at < OO.created_at
                INNER JOIN activity_estimated_times AS ATS ON ATS.id = AA.id_parent
                WHERE ATS.activity_id IN (".implode(',',$activity_ids).") AND AA.field IN ('estimated') AND OO.audit_id IS NULL
                ORDER BY AA.created_at ASC "); 
        //return response()->json($estimated);

        /** get last progress report of each day **/
        $real_progress = DB::select("SELECT
                      AA.id_parent, AA.old_value,AA.new_value, CAST(AA.created_at AS DATE) AS audit_date
                FROM `" . $audit . "` AS AA
                LEFT JOIN `" . $audit . "` AS OO ON AA.id_parent = OO.id_parent AND AA.field = OO.field AND CAST(AA.created_at AS DATE) = CAST(OO.created_at AS DATE) AND AA.created_at < OO.created_at
                WHERE AA.id_parent = :id AND AA.field IN ('percentage') AND OO.audit_id IS NULL
                ORDER BY AA.created_at ASC ", [ 'id' => $model_id ]);

        /** get last progress report of each day **/
        $activity_executed_costs = DB::select("SELECT
                      CAST(end_date AS DATE) AS end_date, activity_cost_categories.id AS activity_cost_category_id, SUM(executed) as executed
                FROM activity_executed_costs
                LEFT JOIN activity_cost_subcategories ON activity_cost_subcategories.id = activity_executed_costs.activity_cost_subcategory_id
                LEFT JOIN activity_cost_categories ON activity_cost_categories.id = activity_cost_subcategories.activity_cost_category_id
                WHERE activity_executed_costs.active IS TRUE AND activity_executed_costs.activity_id IN (".$filter_activity.") 
                GROUP BY 1,2
                ORDER BY 1,2 ASC ");

        /** get and save limit dates **/
        if (sizeof($executed) > 0) {
            $date_limits[] = $executed[0]->end_date;
        }

        if (sizeof($estimated) > 0) {
            $date_limits[] = $estimated[0]->audit_date;
        }

        if (sizeof($real_progress) > 0) {
            $date_limits[] = $real_progress[0]->audit_date;
        }

        if (sizeof($activity_executed_costs) > 0) {
            $date_limits[] = $activity_executed_costs[0]->end_date;
        }

        if (!empty($model->start_date)) {
            $date_limits[] = $model->start_date;
        }

        if(count($date_limits)==0){
            return $complete_table_data;
            //$date_limits[] = date('Y-m-d');
        }

        /** clears any invalid date **/
        foreach ($date_limits as &$date_limit) {
            if (empty($date_limit)) {
                unset($date_limit);
            }
        }
        unset($date_limit);

        $min_date = min($date_limits);
        $first_date = date_create($min_date);
        
        $current_activity_estimated=[];

        /** fill table based on $min_date value
         *       table structure:
         *       index of date
         *          date                    Date formatted
         *
         *          role_1_estimated
         *          role_1_executed
         *          role_1_executed_cost    Multiply Executed by Cost hour
         *          role_2_estimated
         *          role_2_executed
         *          role_2_executed_cost
         *          ...
         *          role_x_estimated
         *          role_x_executed
         *          role_x_executed_cost
         *
         *          real_progress           Progress reported by person
         *          estimated_progress      Ramp from start date to end date
         *
         *          executed_hours          Sum of executed
         *          executed_hours_cost     Sum of executed cost
         *          estimated_hours         Sum of estimated
         *          progress_hours          Divide executed_hours in estimated_hours
         **/

        /** group executed hours by date and role using indexed date **/
        foreach ($executed as $row) {
            $indexed_date = intval($first_date->diff(date_create($row->end_date))->format('%a'));
            /**  if wasn't set, initializes with 0 **/
            if(!isset($complete_table_data[$indexed_date]['role_' . $row->role_id . '_executed'])){
                $complete_table_data[$indexed_date]['role_' . $row->role_id . '_executed']=0;
            }
            $complete_table_data[$indexed_date]['role_' . $row->role_id . '_executed'] += $row->executed;
        }

        /** group estimated hours by date and role using indexed date */
        foreach ($estimated as $row) {
            $indexed_date = intval($first_date->diff(date_create($row->audit_date))->format('%a'));
            $current_activity_estimated[$row->role_id][$row->activity_id] = $row->new_value;

            $role_total_estimated=array_sum($current_activity_estimated[$row->role_id]);

            $complete_table_data[$indexed_date]['role_' . $row->role_id . '_estimated'] = $role_total_estimated;
        }

        /** set real progress using indexed date */
        foreach ($real_progress as $row) {
            $indexed_date = intval($first_date->diff(date_create($row->audit_date))->format('%a'));
            $complete_table_data[$indexed_date]['real_progress'] = $row->new_value;
        }

        /** Cost Executed */
        foreach ($activity_executed_costs as $row) {
            $indexed_date = intval($first_date->diff(date_create($row->end_date))->format('%a'));
            $complete_table_data[$indexed_date]['category_' . $row->activity_cost_category_id . '_executed_cost'] = $row->executed;
        }

        /** if there is not data to show its not required to calculate other columns  */
        if(count($complete_table_data)==0){
            return $complete_table_data;
        }

        /** calculate percentages and fill missing data **/

        /** initialize current values which will contain all row information*/
        $current_values = [];
        $current_values['real_progress'] = 0.0;
        $current_values['estimated_progress'] = 0.0;

        foreach ($roles as $role) {
            $current_values['role_' . $role->id . '_executed'] = 0.0;
            $current_values['role_' . $role->id . '_executed_cost'] = 0.0;
            $current_values['role_' . $role->id . '_estimated'] = 0.0;
        }

        foreach ($activity_cost_categories as $category) {
            $current_values['category_' . $category->id . '_executed_cost'] = 0.0;
        }

        /** loops through all dates and search values using index*/
        $max_date_index = max(array_keys($complete_table_data));
        for ($i = 0; $i <= $max_date_index; $i++) {

            $current_date = clone $first_date;
            $current_values['date']=$current_date->add(new \DateInterval('P'.$i.'D'))->format('Y-m-d');

            /** each time it finds a new progress value, its replaced */
            if (isset($complete_table_data[$i]['real_progress'])) {
                $current_values['real_progress'] = $complete_table_data[$i]['real_progress'];
            }

            /** initialize totals per role with 0 */
            $total_executed = 0.0;
            $total_executed_cost = 0.0;
            $total_estimated = 0.0;
            $total_additional_cost = 0.00;

            /** each role has its own total
             *  executed and estimated hours are added
             *  executed cost are recalculated
             */
            foreach ($roles as $role) {

                $role_cost=isset($role_costs[$role->id])?$role_costs[$role->id]:0;

                if (isset($complete_table_data[$i]['role_' . $role->id . '_executed'])) {
                    $current_values['role_' . $role->id . '_executed'] += $complete_table_data[$i]['role_' . $role->id . '_executed'];
                    $current_values['role_' . $role->id . '_executed_cost'] = $current_values['role_' . $role->id . '_executed']*$role_cost;
                }
                $total_executed += $current_values['role_' . $role->id . '_executed'];
                $total_executed_cost += $current_values['role_' . $role->id . '_executed_cost'];

                if (isset($complete_table_data[$i]['role_' . $role->id . '_estimated'])) {
                    $current_values['role_' . $role->id . '_estimated'] = $complete_table_data[$i]['role_' . $role->id . '_estimated'];
                }
                $total_estimated += $current_values['role_' . $role->id . '_estimated'];
            }

            foreach ($activity_cost_categories as $activity_cost_category) {

                if (isset($complete_table_data[$i]['category_' . $activity_cost_category->id . '_executed_cost'])) {
                    $current_values['category_' . $activity_cost_category->id . '_executed_cost'] = $complete_table_data[$i]['category_' . $activity_cost_category->id . '_executed_cost'];
                }

                $total_additional_cost += $current_values['category_' . $activity_cost_category->id . '_executed_cost'];
            }

            $current_values['executed_hours'] = $total_executed;
            $current_values['executed_hours_cost'] = $total_executed_cost;
            $current_values['executed_additional_cost'] = $total_executed_cost + $total_additional_cost;
            $current_values['estimated_hours'] = $total_estimated;
            $current_values['progress_hours'] = $total_estimated > 0 ? round (100 * $total_executed / $total_estimated , 2) : 0.0;
            $current_values['progress_cost'] = $model->value > 0 ? round (100 * $current_values['executed_additional_cost'] / $model->value , 2) : 0.0;

            if (!empty($model->start_date) && !empty($model->end_date)) {
                $start_date_index = intval($first_date->diff(date_create($model->start_date))->format('%a'));
                $end_date_index = intval($first_date->diff(date_create($model->end_date))->format('%a'));
                $date_range = $end_date_index - $start_date_index;

                if ($i > $end_date_index || $date_range == 0) {
                    $current_values['estimated_progress'] = 100.0;
                } else {
                    $current_values['estimated_progress'] = round(100 * ($i - $start_date_index) / $date_range ,2);
                }
            }

            $complete_table_data[$i] = $current_values;
        }

        /** forces table to be ordered by date index */
        ksort($complete_table_data);

        //return response()->json($complete_table_data);
        return $complete_table_data;
    }

    public function role_times_excel($pattern_id, $model_id){
        $roles = Role::all();
        $activity_cost_categories = ActivityCostCategory::all();

        $complete_table_data=$this->role_times($pattern_id,$model_id);

        $xml = '<?xml version="1.0"?>
                <ss:Workbook xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">
                    <ss:Styles> 
                        <ss:Style ss:ID="1">
                            <ss:Font ss:Bold="1"/>
                        </ss:Style>
                    </ss:Styles>
                    <ss:Worksheet ss:Name="Sheet1">
                        <ss:Table>
                            <ss:Column ss:Width="80"/>
                            <ss:Column ss:Width="80"/>
                            <ss:Column ss:Width="80"/>
                            <ss:Row ss:StyleID="1">
                                <ss:Cell>
                                    <ss:Data ss:Type="String">Fecha</ss:Data>
                                </ss:Cell>
                                <ss:Cell>
                                    <ss:Data ss:Type="String">Real</ss:Data>
                                </ss:Cell>
                                <ss:Cell>
                                    <ss:Data ss:Type="String">Esperado</ss:Data>
                                </ss:Cell>
                                <ss:Cell>
                                    <ss:Data ss:Type="String">Horas Ejecutadas</ss:Data>
                                </ss:Cell>
                                <ss:Cell>
                                    <ss:Data ss:Type="String">Costo Horas</ss:Data>
                                </ss:Cell>
                                ';
        foreach ($roles as $role) {
            $xml.= '
                                <ss:Cell>
                                    <ss:Data ss:Type="String">'.$role->name.' - Presupuestadas</ss:Data>
                                </ss:Cell>
                                <ss:Cell>
                                    <ss:Data ss:Type="String">'.$role->name.' - Ejecutadas</ss:Data>
                                </ss:Cell>
                                <ss:Cell>
                                    <ss:Data ss:Type="String">'.$role->name.' - Costo</ss:Data>
                                </ss:Cell>   
                                    ';
        }

        foreach ($activity_cost_categories as $activity_cost_category) {
            $xml.= '
                                <ss:Cell>
                                    <ss:Data ss:Type="String">'.$activity_cost_category->name.' - Ejecutado</ss:Data>
                                </ss:Cell>                       
                                    ';
        }
        $xml .='
                                <ss:Cell>
                                    <ss:Data ss:Type="String">Total Costos - Ejecutado</ss:Data>
                                </ss:Cell> 
                                <ss:Cell>
                                    <ss:Data ss:Type="String">% Costos </ss:Data>
                                </ss:Cell>   
                            </ss:Row>
                            ';

        foreach ($complete_table_data as $row){
            $xml .='
                            <ss:Row>
                                <ss:Cell>
                                    <ss:Data ss:Type="String">'.$row['date'].'</ss:Data>
                                </ss:Cell>
                                <ss:Cell>
                                    <ss:Data ss:Type="Number">'.$row['real_progress'].'</ss:Data>
                                </ss:Cell>
                                <ss:Cell>
                                    <ss:Data ss:Type="Number">'.$row['estimated_progress'].'</ss:Data>
                                </ss:Cell>
                                <ss:Cell>
                                    <ss:Data ss:Type="Number">'.$row['progress_hours'].'</ss:Data>
                                </ss:Cell>
                                <ss:Cell>
                                    <ss:Data ss:Type="Number">'.$row['executed_hours_cost'].'</ss:Data>
                                </ss:Cell>
                                        ';
            foreach ($roles as $role) {
                $xml.= '
                                <ss:Cell>
                                    <ss:Data ss:Type="Number">'.$row['role_' . $role->id . '_estimated'].'</ss:Data>
                                </ss:Cell>
                                <ss:Cell>
                                    <ss:Data ss:Type="Number">'.$row['role_' . $role->id . '_executed'].'</ss:Data>
                                </ss:Cell>
                                <ss:Cell>
                                    <ss:Data ss:Type="Number">'.$row['role_' . $role->id . '_executed_cost'].'</ss:Data>
                                </ss:Cell>
                                ';
            }

            foreach ($activity_cost_categories as $activity_cost_category) {
                $xml.= '
                                <ss:Cell>
                                    <ss:Data ss:Type="Number">'.$row['category_' . $activity_cost_category->id . '_executed_cost'].'</ss:Data>
                                </ss:Cell>
                                ';
            }
            $xml .='
                                <ss:Cell>
                                    <ss:Data ss:Type="Number">'.$row['executed_additional_cost'].'</ss:Data>
                                </ss:Cell>
                                <ss:Cell>
                                    <ss:Data ss:Type="Number">'.$row['progress_cost'].'</ss:Data>
                                </ss:Cell>
                            </ss:Row>
                        ';
        }
        $xml .='
                        </ss:Table>
                    </ss:Worksheet>
                </ss:Workbook>';


        header("Content-Type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=datos.xls");

        return $xml;
    }

    public function graphic_data(Request $request)
    {

        $pattern_id=$request['txtRoleTimePattern'];
        $model_id=$request['txtRoleTimeModel'];
        $from=$request['txtRoleTimeDateFrom'];
        $to=$request['txtRoleTimeDateTo'];
        $roles = Role::all();
        $complete_table_data=$this->role_times($pattern_id,$model_id);

        if(count($complete_table_data)==0){
            return response()->json(['message'=>'No hay datos para graficar']);
        }
        //return response()->json($complete_table_data);
        $dates=array_column($complete_table_data,'date');
        $from_index=array_search($from,$dates);
        $to_index=array_search($to,$dates);
        if($from_index===false){
            $from_index=0;
        }
        if($to_index===false){
            $to_index=count($dates);
        }
        $length=$to_index-$from_index;

        $complete_table_data=array_slice($complete_table_data,$from_index,$length);

        $series_role_graph = [];

        //roles
        foreach ($roles as $role) {
            $series_role_graph[] = [
                'name' => $role->name,
                'type' => 'line',
                'smooth' => true,
                'itemStyle' => [
                    'normal' => [
                        'areaStyle' => [
                            'type' => 'default'
                        ]
                    ]
                ],
                'data' => array_column($complete_table_data,'role_' . $role->id . '_executed')
            ];
        }

        $series_progress_graph=[];

        $series_progress_graph[0] = [
            'name' => 'Horas',
            'type' => 'line',
            'smooth' => true,
            'data' => array_column($complete_table_data,'progress_hours')
        ];

        $series_progress_graph[1] = [
            'name' => 'Real',
            'type' => 'line',
            'smooth' => true,
            'data' => array_column($complete_table_data,'real_progress')
        ];

        $series_progress_graph[2] = [
            'name' => 'Esperado',
            'type' => 'line',
            'smooth' => true,
            'data' => array_column($complete_table_data,'estimated_progress'),
            'color' => '#F39100',
        ];

        $series_progress_graph[3] = [
            'name' => 'Costos',
            'type' => 'line',
            'smooth' => true,
            'data' => array_column($complete_table_data,'progress_cost'),
        ];

        $labels = array_column($complete_table_data,'date');
        $data_progress_graph= array_column($series_progress_graph,'name');
        $data_role_graph= array_column($series_role_graph,'name');

        return response()->json([
            'labels' => $labels,
            'series_progress_graph' => $series_progress_graph,
            'data_progress_graph' => $data_progress_graph,
            'series_role_graph' => $series_role_graph,
            'data_role_graph' => $data_role_graph
        ]);
    }

/*
    public function time_estimated($pattern_id = '', $model_id = '', $role_id = '')
    {
        $activity_estimated_times = DB::table('activity_estimated_times')->where('active',true);

        if ($pattern_id != '') {
            $activity_estimated_times = $activity_estimated_times->where('pattern_id', $pattern_id);
        }
        if ($model_id != '') {
            $activity_estimated_times = $activity_estimated_times->where('model_id', $model_id);
        }
        if ($role_id != '') {
            $activity_estimated_times = $activity_estimated_times->where('role_id', $role_id);
        }

        $total = $activity_estimated_times->sum('estimated');

        return response()->json(['total' => $total]);
    }


    public function audit_times_estimated($pattern_id = '', $model_id = '', $role_id = '')
    {
        $activity_estimated_times = DB::table('activity_estimated_times')->where('active',true);

        if ($pattern_id != '') {
            $activity_estimated_times = $activity_estimated_times->where('activity_estimated_times.pattern_id', $pattern_id);
        }
        if ($model_id != '') {
            $activity_estimated_times = $activity_estimated_times->where('activity_estimated_times.model_id', $model_id);
        }
        if ($role_id != '') {
            $activity_estimated_times = $activity_estimated_times->where('activity_estimated_times.role_id', $role_id);
        }

        $data = $activity_estimated_times
            ->select(
                'activity_estimated_times.pattern_id',
                'activity_estimated_times.model_id',
                'activity_estimated_times.estimated',
                'activity_estimated_times.role_id',
                )
            ->orderBy('roles.id', 'asc')
            ->get();

        return response()->json(['data' => $data]);
    }
*/
}