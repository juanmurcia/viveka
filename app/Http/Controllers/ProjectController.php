<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use Caffeinated\Shinobi\Models\Role;
use App\Models\Agreement;
use App\Models\PatternState;
use App\Models\Activity;
use App\Models\PatternDocumentType;
use DataTables;

use App\Http\Requests\projectCreateRequest;
use App\Http\Requests\projectUpdateRequest;
use App\Http\Requests\projectProgressRequest;
use App\Models\AgreementPermission;

class ProjectController extends Controller
{
    public function __construct(AgreementPermission $agreementPermission)
    {
        $this->middleware('auth');
        $this->permission = $agreementPermission;
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $project_pattern_state = PatternState::where('pattern_id',2)->get();
        $agreement = Agreement::where('active',1)->get();

        return view('project.table',['agreement'=>$agreement, 'project_pattern_state'=>$project_pattern_state]);
    }

    /**
     * Show the dataTable.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        if ($request->ajax()) {
            $role = Role::find(auth()->user()->role_id);
            $projects = Project::with(['responsible','agreement','pattern_state'])->select('projects.*')->where('projects.active',1);

            return Datatables::eloquent($projects)
                ->filter(function ($query) use ($request){

                    if ($request->has('optProjectPatternState')) {
                        $query->where('projects.pattern_state_id',$request->get('optProjectPatternState'));
                    }

                    if ($request->has('txtProjectName')) {
                        $query->where('projects.name','like', "%{$request->get('txtProjectName')}%");
                    }

                    if ($request->has('optAgreement')) {
                        $query->where('projects.agreement_id',$request->get('optAgreement'));
                    }

                    if ($request->has('txtProjectStartDateFrom')) {
                        $query->where('projects.start_date','>=',$request->get('txtProjectStartDateFrom'));
                    }

                    if ($request->has('txtProjectStartDateTo')) {
                        $query->where('projects.start_date','<=',$request->get('txtProjectStartDateTo').' 23:59:59');
                    }

                    if ($request->has('txtProjectEndDateFrom')) {
                        $query->where('projects.end_date','>=', $request->get('txtProjectEndDateFrom').' 00:00:00');
                    }

                    if ($request->has('txtProjectEndDateTo')) {
                        $query->where('projects.end_date','<=',$request->get('txtProjectEndDateTo').' 23:59:59');
                    }

                })
                ->addColumn('buttons', function ($project) use ($role) {
                    $button = '<div class="row">';
                    if($role->can('project.view')) {
                        $button.= '<a href="/proyecto/' . $project->id . '" class="btn btn-sm btn-primary btn-icon btn-outline btn-round" title="Ver Proyecto" rol="tooltip" ><i class="fa fa-eye" aria-hidden="true"></i></a>';
                    }

                    if($role->can('project.audit')){
                        $button.= '<a onclick="audit(2,'.$project->id.');" class="btn btn-sm btn-warning btn-icon btn-outline btn-round" title="Ver Auditoria" rol="tooltip" ><i class="fa fa-history" aria-hidden="true"></i></a>';
                    }

                    $button .= '</div>';
                    return $button;
                })
                ->rawColumns(['buttons'])
                ->setRowId('id')
                ->make(true);
        }

    }

    /**
     * Show the dataTable.
     *
     * @return \Illuminate\Http\Response
     */
    public function list_project($agreement)
    {
        $projects = Project::with(['responsible.person','agreement','pattern_state'])->where('agreement_id',$agreement)
            ->where('active','1')
            ->get();

        $data = [];
        foreach ($projects as $project) {
            $id = $project['id'];

            array_push($data,$project);
        }

        return response()->json(['data' => $data]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response 
     */
    public function create()
    {

    }

    public function store(projectCreateRequest $request)
    {
        if ($request->ajax()) 
        {
            $permission = $this->permission->full_permission($request['txtProjectAgreement']);
            if(!$permission){
                return response()->view('errors.403', ['error' => 'No pertenece al grupo de trabajo del contrato']);
            }

            $data =[
                'agreement_id' => $request['txtProjectAgreement'],
                'responsible_id' => $request['txtProjectResponsible'],
                'name' => $request['txtProjectName'],
                'description' => $request['txtProjectDescription'],
                'start_date' => $request['txtProjectStartDate'],
                'end_date' => $request['txtProjectEndDate'],
                'value' => $request['txtProjectValue'],
                'user_cre_id' => auth()->user()->id,
            ];
            $project = new Project($data);
            $project->save();

            return response()->json(['mensaje' => 'creado']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::with(['responsible.person','agreement'])->find($id);

        return response()->json($project);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $project = Project::find($id);

        if($project===NULL){
            return redirect('proyectos');
        }

        $permission = $this->permission->full_permission($project->agreement_id);
        if(!$permission){
            return response()->view('errors.403', ['error' => 'No pertenece al grupo de trabajo del contrato']);
        }

        $activity_pattern_state = PatternState::where('pattern_id',Activity::$pattern_id)->get();
        $project_pattern_state = PatternState::where('pattern_id',Project::$pattern_id)->get();
        $pattern_document_type = PatternDocumentType::where('pattern_id',Project::$pattern_id)->get();

        return view('project.view', [
            'pattern_id'=>Project::$pattern_id,
            'id'=>$id,
            'agreement_id'=>$project->agreement_id,
            'activity_pattern_state'=>$activity_pattern_state,
            'project_pattern_state'=>$project_pattern_state,
            'pattern_document_type'=>$pattern_document_type
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $data =[
            'active' => true,
            'user_mod_id' => auth()->user()->id,
        ];
        $project = Project::find($id)
                    ->update($data);

        return response()->json(['mensaje' => 'Activado']);
    }


    public function update(projectUpdateRequest $request, $id)
    {
        if ($request->ajax()) 
        {
            $data =[
                'responsible_id' => $request['txtProjectResponsible'],
                'name' => $request['txtProjectName'],
                'description' => $request['txtProjectDescription'],
                'start_date' => $request['txtProjectStartDate'],
                'end_date' => $request['txtProjectEndDate'],
                'value' => $request['txtProjectValue'],
                'user_mod_id' => auth()->user()->id,
            ];      
            $project = Project::find($id)
                        ->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }


    public function progress(projectProgressRequest $request, $id)
    {
        if ($request->ajax())
        {
            $data =[
                "percentage" => $request['txtProjectPercentage'],
                "pattern_state_id" => $request['optProjectPatternState'],
                "justification" => $request['txtProjectJustification'],
                'user_mod_id' => auth()->user()->id,
            ];
            $project = Project::find($id);

            $project->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }


    public function destroy($id)
    {
        $data =[
            'active' => false,
            'user_mod_id' => auth()->user()->id,
        ];
        $project = Project::find($id)
                    ->update($data);

        return response()->json(['mensaje' => 'Inactivo']);
    }
}