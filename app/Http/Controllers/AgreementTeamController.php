<?php

namespace App\Http\Controllers;

use App\Http\Requests\agreementTeamCreateRequest;
use App\Models\AgreementTeam;
use Illuminate\Http\Request;
use Caffeinated\Shinobi\Models\Role;
use Illuminate\Support\Facades\DB;


class AgreementTeamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        

    }

    /**
     * Show the dataTable.
     *
     * @return \Illuminate\Http\Response
     */
    public function list($id)
    {


        $role = Role::find(auth()->user()->role_id);
        $destroy = $role->can('agreement_team.destroy');

        $agreement_team = AgreementTeam::with('professional.person')->where('agreement_id',$id)->get();

        $data = [];
        foreach ($agreement_team as $agreement_team_item) {
            if($agreement_team_item['active']==false){
                continue;
            }
            $item=[];
            $item['id'] = $agreement_team_item['id'];
            $item['document'] = $agreement_team_item['professional']['person']['document'];
            $item['name'] = $agreement_team_item['professional']['person']['name'].' '.$agreement_team_item['professional']['person']['last_name'];

            if($destroy){
                $item['buttons'] = '<a class="btn btn-round btn-danger btn-xs" onclick="deleteAgreementTeam('.$item['id'].')"><i class="fa fa-close"></i></a>';
            }

            array_push($data,$item);
        }

        return response()->json(['data' => $data]);
    }


    public function create()
    {
        
    }

    public function store(agreementTeamCreateRequest $request)
    {
        if ($request->ajax()) 
        {
            AgreementTeam::updateOrCreate(
                [
                    'agreement_id' => $request['txtAgreementTeamAgreementId'],
                    'professional_id' => $request['txtAgreementTeamProfessionalId'],
                ],
                [
                    'active' => true,
                    'user_mod_id' => auth()->user()->id,
                ]
            );

            return response()->json(['mensaje' => 'creado']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agreement_team = AgreementTeam::find($id)->with('person');

        return response()->json($agreement_team);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $data =[
            'active' => true,
            'user_mod_id' => auth()->user()->id,
        ];
        $agreement_team = AgreementTeam::find($id)
                    ->update($data);

        return response()->json(['mensaje' => 'Activado']);
    }


    public function update(agreementTeamCreateRequest $request, $id)
    {

    }


    public function destroy($id)
    {

        $data =[
            'active' => false,
            'user_mod_id' => auth()->user()->id,
        ];
        $agreement_team = AgreementTeam::find($id)
                    ->update($data);

        return response()->json(['mensaje' => 'Inactivo']);
    }

    public function search(Request $request,$agreement_id,$role_id='')
    {
        $data=[];

        $agreement_team=
            DB::table('agreement_teams')
                ->select('people.document','people.name','people.last_name','professionals.id AS professional_id','people.id AS person_id')
                ->orderBy('people.name','asc')
                ->join('professionals','agreement_teams.professional_id','=','professionals.id')
                ->join('people','people.id','=','professionals.person_id')
                ->join('users','people.id','=','users.person_id')
                ->join('role_user','role_user.user_id','=','users.id')
                ->where('agreement_teams.active',true)
                ->where('people.active',true)
                ->where('professionals.active',true);

        if($agreement_id != 0){
            $agreement_team=$agreement_team->where('agreement_teams.agreement_id',$agreement_id);
        }

        if($role_id!=''){
            $agreement_team=$agreement_team->where('role_user.role_id',$role_id);
        }

        if($request->has('q')) {
            $results=$agreement_team->whereRaw("CONCAT_WS('',people.document,' ',people.name,' ',people.last_name) LIKE ?",['%'.$request['q'].'%'])
                ->groupBy('people.document','people.name','people.last_name','professionals.id','people.id')
                ->get();
        }else{
            $results = $agreement_team->groupBy('people.document','people.name','people.last_name','professionals.id','people.id')
                ->get();
        }

        foreach ($results as $result) {
            $value = $result->document.' - '.$result->name.' '.$result->last_name;
            $data[] = [ 'person_id' => $result->person_id, 'id' => $result->professional_id, 'text' => $value];
        }

        if(count($data)==0){
            return response()->json(['results' => [ 'id' => '', 'text' => 'Sin resultados']]);
        }

        return response()->json(['results' => $data]);
    }
}