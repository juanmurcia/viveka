<?php

namespace App\Http\Controllers;

use \Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use App\Models\Notification;
use App\User;

use App\Http\Requests\notificationCreateRequest;
use App\Http\Requests\notificationUpdateRequest;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('active',1)->get();
        return view('notification.table', ['users'=> $users]);
    }

    public function user_notification(){
        $notifications = Notification::orderBy('created_at', 'DESC')->where('active',1)->where('seen','0')->where('user_id', auth()->user()->id)->get();

        $data = [];
        foreach ($notifications as $notification){
            $user = User::find($notification->user_cre_id);
            $date = Carbon::parse($notification->notification_date);

            array_push($data,[
                'sender' => $user->name,
                'time' => $date->diffForHumans(),
                'message' => ($notification->notification ? $notification->description : $notification->name),
                'link' => '/notificaciones/#'.$notification->id,
            ]);
        }
        return response()->json(['count'=>sizeof($data),'data'=>$data]);
    }

    /**
     * Show the dataTable.
     *
     * @return \Illuminate\Http\Response
     */
    public function list($type)
    {
        $field = ($type == 'received' ? 'user_id' : 'user_cre_id');
        $notifications = Notification::orderBy('created_at', 'DESC')->where($field,auth()->user()->id)->get();

        $data = [];
        foreach ($notifications as $notification) {
            $date = Carbon::parse($notification['notification_date']);

            $notification['class'] = ($notification['seen'] ? 'fa-circle-o' : 'fa-circle');
            $notification['message'] = ($notification['notification'] ? $notification['description'] : $notification['name']);
            $notification['type'] = ($notification['notification'] ? 'fa-bell-o' : 'fa-envelope-o');
            $notification['sender'] = $notification['userCre']->name;
            $notification['addressee'] = $notification['User']->name;
            $notification['time'] = $date->diffForHumans();
            $notification['action'] = "show_message({$notification['id']});";

            array_push($data,$notification);
        }

        return response()->json(['data' => $data]);
    }


    public function create()
    {

    }

    public function store(notificationCreateRequest $request)
    {
        if ($request->ajax())
        {
            $data =[
                'user_id' => $request['optUserId'],
                'name' => $request['txtName'],
                'description' => $request['txtDescription'],
                'user_cre_id' => auth()->user()->id,
            ];

            $notification = new Notification($data);
            $notification->save();
            $notification->User;
            $notification->userCre;

            if($request['chkMail']){
                if(Mail::to($notification->User->email)->send(new \App\Mail\Notification($notification))){
                    return response()->json(['mensaje' => 'Email enviado']);
                }else{
                    return response()->json(['errorBd' => 'Error envío email']);
                }
            }


            return response()->json(['mensaje' => 'Creado']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $type)
    {
        $notification = Notification::find($id);
        $date = Carbon::parse($notification->notification_date);
        $notification->date = $date->format('l jS \\of F Y H:i:s');
        $notification->User;
        $notification->userCre;

        if(!$notification->seen AND $type == 'received'){
            Notification::find($id)->update(['seen'=>true, 'user_mod_id'=>auth()->user()->id]);
        }

        return response()->json($notification);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $data =[
            'active' => true,
            'user_mod_id' => auth()->user()->id,
        ];
        Notification::find($id)->update($data);

        return response()->json(['mensaje' => 'Activado']);
    }


    public function update(notificationUpdateRequest $request, $id)
    {
        if ($request->ajax())
        {
            $data =[
                'user_id' => $request['optUserId'],
                'name' => $request['txtName'],
                'description' => $request['txtDescription'],
                'notification_date' => $request['txtDateNotification'],
                'seen' => false,
                'user_mod_id' => auth()->user()->id,
            ];
            Notification::find($id)->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }


    public function destroy($id)
    {
        $data =[
            'active' => false,
            'user_mod_id' => auth()->user()->id,
        ];
        Notification::find($id)->update($data);

        return response()->json(['mensaje' => 'Inactivo']);
    }
}