<?php

namespace App\Http\Controllers;

use App\Models\SystemParameter;
use Caffeinated\Shinobi\Models\Role;

use App\Http\Requests\systemParameterCreateRequest;
use App\Http\Requests\systemParameterUpdateRequest;

class SystemParameterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('system_parameter.table');
    }

    /**
     * Show the dataTable rows.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $role = Role::find(auth()->user()->role_id);
        $edit = $role->can('system_parameter.edit');
        $system_parameters = SystemParameter::with(['user_cre'])->get();

        $data = [];
        foreach ($system_parameters as $system_parameter) {
            $system_parameter['buttons'] = '<div>';
            if($edit){
                $system_parameter['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round" onclick="editSystemParameter('.$system_parameter['id'].')" title="Editar Parámetro" rol="tooltip"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
            }

            if($role->can('system_parameter.audit')){
                $system_parameter['buttons'] .= '<a onclick="audit(SystemParameter::$pattern_id,'.$system_parameter->id.');" class="btn btn-sm btn-warning btn-icon btn-outline btn-round" title="Ver Auditoria" rol="tooltip" ><i class="fa fa-history" aria-hidden="true"></i></a>';
            }

            $system_parameter['buttons'] .= '</div>';
            array_push($data,$system_parameter);
        }

        return response()->json(['data' => $data]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $system_parameter = SystemParameter::find($id);
        return response()->json($system_parameter);
    }     

    public function store(systemParameterCreateRequest $request)
    {
        if ($request->ajax()) 
        {
            $data =[
                'name' => $request['txtNameParameter'],
                'value' => $request['txtValueParameter'],
                'user_cre_id' => auth()->user()->id,
            ];
            $system_parameter = new SystemParameter($data);
            $system_parameter->save();

            return response()->json(['mensaje' => 'Creado','id' => $system_parameter['id']]);
        }
    }


    public function update(systemParameterUpdateRequest $request, $id)
    {
        if ($request->ajax())
        {
            $data =[
                'name' => $request['txtNameParameter'],
                'value' => $request['txtValueParameter'],
                'user_mod_id' => auth()->user()->id,
            ];
            SystemParameter::find($id)->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }
}