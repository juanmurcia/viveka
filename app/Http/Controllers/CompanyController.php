<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use Caffeinated\Shinobi\Models\Role;
use App\Models\PersonDocumentType;
use App\Models\TaxpayerType;
use App\Models\PatternDocumentType;
use Illuminate\Support\Facades\DB;
use DataTables;

use App\Http\Requests\companyCreateRequest;
use App\Http\Requests\companyUpdateRequest;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $person_document_type = PersonDocumentType::all();
        $taxpayer_type = TaxpayerType::all();
        return view('company.table', [
            'taxpayer_type'=>$taxpayer_type,
            'person_document_type'=>$person_document_type
        ]);

    }

    /**
     * Show the dataTable.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        if ($request->ajax()) {
            $role = Role::find(auth()->user()->role_id);
            $companies = Company::with(['person','person.person_document_type','agent'])->select('companies.*');

            return Datatables::eloquent($companies)
                ->addColumn('state', function($company) {
                    return ($company->active ? 'ACTIVO' : 'INACTIVO');
                })
                ->addColumn('buttons', function ($company) use ($role) {
                    $button = '<div class="row">';

                    if($role->can('company.view')){
                        $button .= '<a href="/empresa/' . $company->id . '" class="btn btn-sm btn-primary btn-icon btn-outline btn-round" title="Ver Empresa" rol="tooltip" ><i class="fa fa-eye" aria-hidden="true"></i></a>';
                    }

                    if($role->can('company.audit')){
                        $button .= '<a onclick="audit('.Company::$pattern_id.','.$company->id.');" class="btn btn-sm btn-warning btn-icon btn-outline btn-round" title="Ver Auditoria" rol="tooltip" ><i class="fa fa-history" aria-hidden="true"></i></a>';
                    }

                    if($role->can('company.destroy')) {
                        if ($company->active) {
                            $button .= '<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-icon btn-outline btn-round" onclick="destroyCompany('.$company->id.')" title="Inctivar Empresa" rol="tooltip" ><i class="fa fa-close" aria-hidden="true"></i></a>';
                        }else{
                            $button .= '<a href="javascript:void(0)" class="btn btn-sm btn-success btn-icon btn-outline btn-round" onclick="activateCompany('.$company->id.')" title="Activar Empresa" rol="tooltip" ><i class="fa fa-check" aria-hidden="true"></i></a>';
                        }
                    }

                    $button .= '</div>';
                    return $button;
                })
                ->rawColumns(['buttons'])
                ->setRowId('id')
                ->make(true);
        }
    }

    public function store(companyCreateRequest $request)
    {
        if ($request->ajax()) 
        {
            $data =[
                'person_id' => $request['txtPerson'],
                'agent_id' => $request['txtAgentId'],
                'number_employees' => $request['txtEmployees'],
                'url' => $request['txtUrl'],
                'user_cre_id' => auth()->user()->id,
            ];
            $company = new company($data);
            $company->save();

            return response()->json(['mensaje' => 'creado']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function view($id)
    {
        $pattern_document_type = PatternDocumentType::where('pattern_id',Company::$pattern_id)->get();
        $person_document_type = PersonDocumentType::all();
        $taxpayer_type = TaxpayerType::all();

        return view('company.view', [
            'id'=>$id,
            'pattern_id'=>6,
            'pattern_document_type'=>$pattern_document_type,
            'taxpayer_type'=>$taxpayer_type,
            'person_document_type'=>$person_document_type
        ]);
    }

    public function show($id)
    {
        $company = Company::where('id',$id)->with(['person','agent'])->get();
        return response()->json($company[0]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $data =[
            'active' => true,
            'user_mod_id' => auth()->user()->id,
        ];
        Company::find($id)->update($data);

        return response()->json(['mensaje' => 'Activado']);
    }


    public function update(companyUpdateRequest $request, $id)
    {
        if ($request->ajax()) 
        {
            $data =[
                'person_id' => $request['txtPerson'],
                'agent_id' => $request['txtAgentId'],
                'number_employees' => $request['txtEmployees'],
                'url' => $request['txtUrl'],
                'user_mod_id' => auth()->user()->id,
            ];      
            Company::find($id)->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }


    public function destroy($id)
    {
        $data =[
            'active' => false,
            'user_mod_id' => auth()->user()->id,
        ];
        Company::find($id)->update($data);

        return response()->json(['mensaje' => 'Inactivo']);
    }

    public function search(Request $request)
    {
        $data=[];

        $companies=
            DB::table('companies')
                ->join('people','people.id','=','companies.person_id')
                ->where('people.active',true)
                ->where('companies.active',true);

        if($request->has('id')){
            $companies = $companies->whereRaw('people.id IN ('.$request['id'].')');
        }

        if($request->has('q')) {
            $companies = $companies->whereRaw("CONCAT_WS('',people.document,' - ',people.name,' ',people.last_name) LIKE ?", ['%' . $request['q'] . '%']);
        }

        $companies = $companies
            ->select('people.document', 'people.name', 'people.last_name', 'companies.id AS id_label')
            ->orderBy('people.name', 'asc')
            ->get();

        foreach ($companies as $result) {
            $value = $result->document.' - ';
            $value .= $result->name;
            $value .= $result->last_name!=''?' '.$result->last_name:'';
            $data[] = [ 'id' => $result->id_label, 'text' => $value];
        }

        if(count($data)==0){
            return response()->json(['results' => [ 'id' => '', 'text' => 'Sin resultados']]);
        }

        return response()->json(['results' => $data]);
    }
}