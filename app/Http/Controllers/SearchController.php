<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Models\Pattern;

class SearchController extends Controller
{
    function index(){
        return view('search.table');
    }

    function list(Request $request){
        if($request->ajax()){
            $data = [];
            if($request['filterPattern']){
                foreach($request['filterPattern'] as $pattern_id){
                    $pattern = Pattern::find($pattern_id);
                    $pattern = str_replace(' ','','App\Models\ '.$pattern->model);
                    $model = $pattern::find(1);
                    $query = $model->filter_search($request['txtSearch']);

                    $data[$pattern_id] = $query;
                }
            }

            return response()->json($data);
        }
    }
}
