<?php

namespace App\Http\Controllers;

use PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Billing;
use App\Models\BillingDetail;
use App\Models\PatternState;
use App\Models\Agreement;
use App\Models\Product;
use App\Models\Document;
use Caffeinated\Shinobi\Models\Role;

use App\Http\Requests\billingCreateRequest;

class BillingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $billing_pattern_state = PatternState::where('pattern_id',Billing::$pattern_id)->get();
        return view('billing.table', ['user' => auth()->user()->name, 'billing_pattern_state'=>$billing_pattern_state]);
    }

    /**
     * Show the dataTable rows.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        if($request->ajax())
        {
            $billings = Billing::with(['agreement', 'pattern_state']);

            if ($request['txtBillingNumber']!='') {
                $billings=$billings->where('number','like','%'.$request['txtBillingNumber'].'%');
            }

            if ($request['txtAgreementName']!='') {
                $billings=$billings->where('name','like','%'.$request['txtAgreementName'].'%');
            }

            if ($request['optBillingPatternState']!='') {
                $billings=$billings->where('pattern_state_id',$request['optBillingPatternState']);
            }

            if ($request['txtBillingStartDateFrom']!='') {
                $billings=$billings->where('billing_date','>=',$request['txtBillingStartDateFrom']);
            }

            if ($request['txtAgreementStartDateTo']!='') {
                $billings=$billings->where('billing_date','<=',$request['txtAgreementStartDateTo'].' 23:59:59');
            }

            if ($request['txtBillingEndDateFrom']!='') {
                $billings=$billings->where('due_date','>=',$request['txtBillingEndDateFrom']);
            }

            if ($request['txtBillingEndDateTo']!='') {
                $billings=$billings->where('due_date','<=',$request['txtBillingEndDateTo'].' 23:59:59');
            }

            $billings=$billings->orderBy('number','desc')->get();

            $data=[];
            $role = Role::find(auth()->user()->role_id);

            foreach ($billings as $billing) {

                $billing['buttons'] = '<div>';
                if($role->can('billing.view')){
                    $billing['buttons'] .= '<a href="/facturacion/'.$billing->id.'" class="btn btn-sm btn-primary btn-icon btn-outline btn-round" title="Ver Contrato" rol="tooltip" ><i class="fa fa-eye" aria-hidden="true"></i></a>';
                }

                if($role->can('billing.audit')){
                    $billing['buttons'] .= '<a onclick="audit(8,'.$billing->id.');" class="btn btn-sm btn-warning btn-icon btn-outline btn-round" title="Ver Auditoria" rol="tooltip" ><i class="fa fa-history" aria-hidden="true"></i></a>';
                }
                $billing['buttons'] .= '</div>';
                array_push($data,$billing);
            }

            return response()->json(['data' => $data]);
        }
    }

    /**
     * Show the dataTable rows.
     *
     * @return \Illuminate\Http\Response
     */
    public function list_billing($agreement_id)
    {
        $billings = Billing::with(['agreement', 'pattern_state'])
            ->where('agreement_id',$agreement_id)
            ->whereIn('pattern_state_id',[44])
            ->orderBy('number','desc')->get();

        $data=[];
        $role = Role::find(auth()->user()->role_id);

        foreach ($billings as $billing) {
            $billing['buttons'] = '<div>';
            if($role->can('billing.view')){
                $billing['buttons'] .= '<a href="/facturacion/'.$billing->id.'" class="btn btn-sm btn-primary btn-icon btn-outline btn-round" title="Ver Contrato" rol="tooltip" ><i class="fa fa-eye" aria-hidden="true"></i></a>';
            }

            $billing['buttons'] .= '</div>';
            array_push($data,$billing);
        }

        return response()->json(['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('billing.new', ['user' => auth()->user()->name]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(billingCreateRequest $request)
    {
        if ($request->ajax())
        {
            $agreement = Agreement::find($request['txtBillingAgreement']);
            $due_date = Carbon::parse("+{$agreement->billing_due} days")->toDateString();

            $data =[
                "agreement_id" => $request['txtBillingAgreement'],
                "pattern_state_id" => 44,
                "billing_date" => date('Y-m-d'),
                "due_date" => $due_date,
                "documentation" => '[{}]',
                'user_cre_id' => auth()->user()->id,
            ];

            $billing = new Billing($data);
            $billing->save();

            return response()->json(['mensaje' => 'Creado satisfactoriamente', 'id' => $billing['id']]);
        }
    }

    function add_product($billing_id, $product_id){
        $product = Product::find($product_id);
        $data =[
            'product_id' => $product_id,
            'billing_id' => $billing_id,
            'value' => $product->value,
            'user_cre_id' => auth()->user()->id,
        ];
        $billing_detail = new BillingDetail($data);
        $billing_detail->save();

        return response()->json(['mensaje' => 'creado']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        return view('billing.view', ['user' => auth()->user()->name, 'id'=> $id]);
    }

    public function show($id)
    {
        $billing = Billing::where('id',$id)
            ->with(['agreement','agreement.contractor.person','agreement.employer.person','billing_detail.product'])
            ->get();

        return response()->json($billing[0]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->ajax())
        {
            $billing = Billing::find($id);
            $agreement = Agreement::find($billing->agreement_id);
            $due_billing = Carbon::parse($request['txtBillingDate'])->addDays($agreement->billing_due)->toDateString();

            $data =[
                "billing_date" => $request['txtBillingDate'],
                "number" => $request['numberI'],
                "comment" => $request['txtDetailBilling'],
                "due_date" => $due_billing,
                'user_mod_id' => auth()->user()->id,
            ];
            $billing->update($data);

            return response()->json(['mensaje' => 'Creado satisfactoriamente']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function close($id)
    {
        $billing = Billing::find($id);

        $data =[
            "pattern_state_id" => 45,
            'user_mod_id' => auth()->user()->id,
        ];
        $billing->update($data);

        return response()->json(['mensaje' => 'Creado satisfactoriamente']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $billing = Billing::find($id);

        $data =[
            "pattern_state_id" => 46,
            'user_mod_id' => auth()->user()->id,
        ];

        $billing->update($data);

        return response()->json(['mensaje' => 'Creado satisfactoriamente']);
    }

    function del_billing_detail($id)
    {
        $billing_detail = BillingDetail::find($id);
        $billing = Billing::find($billing_detail->billing_id);

        if($billing->pattern_state_id == 45){
            return response()->json(['errorBD' => 'La facturación ya fue cerrada, no puede modificar su información']);
        }

        $billing_detail->update(['user_mod_id' => auth()->user()->id]);
        $billing_detail->delete();

        return response()->json(['mensaje' => 'Eliminado satisfactoriamente']);
    }

    function billing_document($id, $document_id, $version)
    {
        $billing = Billing::find($id);
        $document = Document::find($document_id);
        $documentation = json_decode($billing->documentation);

        array_push($documentation,[
            'document_id' => $document_id,
            'version' => $version,
            'name' => $document->name,
            'type' => $document->pattern_document_type->name,
            'date' => Carbon::now()->toDateTimeString()
        ]);

        $data =[
            "documentation" => json_encode($documentation),
            'user_mod_id' => auth()->user()->id,
        ];

        $billing->update($data);

        return response()->json(['mensaje' => 'Creado satisfactoriamente']);
    }

    function billing_delete_document($id, $document_id)
    {
        $billing = Billing::find($id);
        $documentation = json_decode($billing->documentation);

        for($i = 0; $i<sizeof($documentation); $i++){
            if(isset($documentation[$i]->document_id) AND $documentation[$i]->document_id == $document_id){
                unset($documentation[$i]);
            }
        }

        $data =[
            "documentation" => json_encode($documentation),
            'user_mod_id' => auth()->user()->id,
        ];

        $billing->update($data);

        return response()->json(['mensaje' => 'Creado satisfactoriamente']);
    }

    function report($id, $online = 'true'){
        $billing = Billing::find($id);
        $info_billing = Billing::where('id',$id)
            ->with(['agreement','agreement.department','agreement.contractor.person','agreement.employer.person','billing_detail.product'])
            ->get();

        PDF::changeFormat('LETTER');
        PDF::setMargins(0,0,0,0);
        PDF::reset();
        PDF::SetAutoPageBreak(false, 0);
        PDF::SetTitle('FACT-'.$billing->number);
        PDF::AddPage();
        if($online == 'true'){
            PDF::Image(public_path().'\images\factura.jpg',0,0,PDF::GetPageWidth());
        }


        //
        PDF::SetXY(167, 20);
        PDF::Cell(35, 8, $info_billing[0]->number,0,1,'C',false, '', 1);
        PDF::SetXY(28, 37);
        PDF::Cell(91, 8, $info_billing[0]->agreement->contractor->person->name.' '.$info_billing[0]->agreement->contractor->person->last_name,0,1,'L',false, '', 1);
        PDF::SetXY(125, 37);
        PDF::Cell(40, 8, $info_billing[0]->agreement->contractor->person->document,0,1,'L',false, '', 1);
        PDF::SetXY(32, 46);
        PDF::Cell(85, 8, $info_billing[0]->agreement->contractor->person->address,0,1,'L',false, '', 1);
        PDF::SetXY(125, 46);
        PDF::Cell(40, 8, $info_billing[0]->agreement->contractor->person->phone,0,1,'L',false, '', 1);
        PDF::SetXY(27, 55);
        PDF::Cell(85, 8, $info_billing[0]->agreement->department->name,0,1,'L',false, '', 1);

        //
        PDF::SetXY(169, 42);
        PDF::Cell(10, 8, substr($info_billing[0]->billing_date,8,2),0,1,'C',false, '', 1);
        PDF::SetXY(181, 42);
        PDF::Cell(10, 8, substr($info_billing[0]->billing_date,5,2),0,1,'C',false, '', 1);
        PDF::SetXY(194, 42);
        PDF::Cell(10, 8, substr($info_billing[0]->billing_date,0,4),0,1,'C',false, '', 1);
        PDF::SetXY(169, 55);
        PDF::Cell(10, 8, substr($info_billing[0]->due_date,8,2),0,1,'C',false, '', 1);
        PDF::SetXY(181, 55);
        PDF::Cell(10, 8, substr($info_billing[0]->due_date,5,2),0,1,'C',false, '', 1);
        PDF::SetXY(194, 55);
        PDF::Cell(10, 8, substr($info_billing[0]->due_date,0,4),0,1,'C',false, '', 1);

        //
        PDF::SetXY(40, 208);
        PDF::Cell(106, 8, $info_billing[0]->comment,0,1,'L',false, '', 1);
        PDF::SetXY(177, 208);
        PDF::Cell(28, 8, '$ '.number_format($info_billing[0]->value),0,1,'R',false, '', 1);
        PDF::SetXY(140, 216);
        PDF::Cell(28, 8, number_format($info_billing[0]->agreement->iva_billing),0,1,'R',false, '', 1);
        PDF::SetXY(177, 216);
        PDF::Cell(28, 8, '$ '.number_format($info_billing[0]->taxes),0,1,'R',false, '', 1);
        PDF::SetXY(177, 224);
        PDF::Cell(28, 8, '$ '.number_format($info_billing[0]->net_value),0,1,'R',false, '', 1);

        //
        $html = '<table width="100%">';
        foreach ($info_billing[0]['billing_detail'] as $detail){
            $html .= '<tr>
                            <td style="width: 79%">('. $detail->product->id.') '.$detail->product->name.', '.$detail->product->description.'</td>
                            <td style="width: 1%"><br></td>                            
                            <td style="width: 20%;text-align: right;">$ '. number_format($detail->value).'</td>
                        </tr>';
        }
        $html .= '</table>';

        PDF::writeHTMLCell(184, '', 20, 73, $html, 0, 0, 0, true, 'J', true);

        return PDF::Output('FACT-'.$billing->number.'.pdf');
    }
}
