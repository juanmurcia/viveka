<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Person as person;
use App\Models\PersonDocumentType;
use App\Models\TaxpayerType;
use Caffeinated\Shinobi\Models\Role;

use App\Http\Requests\personCreateRequest;
use App\Http\Requests\personUpdateRequest;
use DataTables;

class PersonController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $person = person::where('id',$id)->with(['person_document_type','user_cre','user_mod','users'])->get();
        return response()->json($person[0]);
    }

    public function store(personCreateRequest $request)
    {
        if ($request->ajax()) 
        {
            $data =[
                'person_document_type_id' => $request['optDocument'],
                'taxpayer_type_id' => $request['optTaxpayer'],
                'document' => $request['txtIdent'],
                'name' => $request['txtName'],
                'last_name' => $request['txtLastName'],
                'address' => $request['txtAddress'],                
                'email' => $request['txtEmail'],                
                'phone' => $request['txtMobil'],
                'active' => true,
                'user_cre_id' => auth()->user()->id,
            ];
            $person = new person($data);
            $person->save();

            return response()->json(['mensaje' => 'Creado','id' => $person['id']]);
        }
    }


    public function update(personUpdateRequest $request, $id)
    {
        if ($request->ajax())
        {
            $data =[
                'person_document_type_id' => $request['optDocument'],
                'taxpayer_type_id' => $request['optTaxpayer'],
                'document' => $request['txtIdent'],
                'name' => $request['txtName'],
                'last_name' => $request['txtLastName'],
                'address' => $request['txtAddress'],
                'email' => $request['txtEmail'],
                'phone' => $request['txtMobil'],
                'user_mod_id' => auth()->user()->id,
            ];
            $person = person::find($id)
                ->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }
    /**
     * Activates the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function activate($id)
    {
        $data =[
            'active' => true,
            'user_mod_id' => auth()->user()->id,
        ];
        $person = person::find($id)
                    ->update($data);

        return response()->json(['mensaje' => 'Activado']);
    }


    public function destroy($id)
    {
        $data =[
            'active' => false,
            'user_mod_id' => auth()->user()->id,
        ];
        $person = person::find($id)
                    ->update($data);

        return response()->json(['mensaje' => 'Inactivo']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role= Role::all();
        $person_document_type = PersonDocumentType::all();
        $taxpayer_type = TaxpayerType::all();

        return view('person.table',[
            'taxpayer_type'=>$taxpayer_type,
            'person_document_type'=>$person_document_type,
            'role'=>$role
        ]);
    }

    /**
     * Show the dataTable rows.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        if ($request->ajax()) {
            $role = Role::find(auth()->user()->role_id);

            $people = person::with(['person_document_type'=>function($query){
                $query->select('id','slug');
            }])->select('people.*');

            return Datatables::eloquent($people)
                ->addColumn('buttons', function ($person) use ($role) {
                    $button = '<div class="row">';

                    if ($role->can('user.create')) {
                        $button .= '<a href="javascript:void(0)" class="col-md-3 btn btn-sm btn-warning btn-icon btn-outline btn-round" onclick="userPerson(' . $person->id . ')" title="Agregar Usuario" rol="tooltip" ><i class="fa fa-user" aria-hidden="true"></i></a>';
                    }

                    if ($role->can('person.edit')) {
                        $button .= '<a href="javascript:void(0)" class="col-md-3 btn btn-sm btn-default btn-icon btn-outline btn-round" onclick="editPerson(' . $person->id . ')" title="Editar Persona" rol="tooltip" data-toggle="modal" data-target="#modPerson"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                    }

                    if ($role->can('person.audit')) {
                        $button .= '<a onclick="audit(5,' . $person->id . ');" class="col-md-3 btn btn-sm btn-warning btn-icon btn-outline btn-round" title="Ver Auditoria" rol="tooltip" ><i class="fa fa-history" aria-hidden="true"></i></a>';
                    }

                    if ($role->can('person.destroy')) {
                        if ($person->active) {
                            $button .= '<a href="javascript:void(0)" class="col-md-3 btn btn-sm btn-danger btn-icon btn-outline btn-round" onclick="destroyPerson(' . $person->id . ')" title="Inctivar Persona" rol="tooltip" ><i class="fa fa-close" aria-hidden="true"></i></a>';
                        } else {
                            $button .= '<a href="javascript:void(0)" class="col-md-3 btn btn-sm btn-success btn-icon btn-outline btn-round" onclick="activatePerson(' . $person->id . ')" title="Activar Persona" rol="tooltip" ><i class="fa fa-check" aria-hidden="true"></i></a>';
                        }
                    }

                    $button .= '</div>';
                    return $button;
                })
                ->rawColumns(['buttons'])
                ->setRowId('id')
                ->make(true);
        }
    }

    public function search(Request $request)
    {
        $data=[];
        $people= DB::table('people')->where('active',true);

        if($request->has('id')){
            $people=$people->whereRaw('people.id IN ('.$request['id'].')');
        }

        if($request->has('q')) {
            $people = $people->whereRaw("CONCAT_WS('',document,' - ',name,' ',last_name) LIKE ?", ['%' . $request['q'] . '%']);
        }

        if($request->has('search')) {
            $people = $people->whereRaw("CONCAT_WS('',document,' - ',name,' ',last_name) LIKE ?", ['%' . $request['search'] . '%']);
        }

        $people = $people->select('document','name','last_name','id AS id_label')
            ->orderBy('name','asc')
            ->get();

        $field = ($request->isMethod('post')) ?  'value' : 'text';
        foreach ($people as $result) {
            $value = $result->document.' - '.$result->name;
            $value.= $result->last_name!='' ? ' '.$result->last_name:'';
            $data[] = [ 'id' => $result->id_label, $field => $value];
        }

        if(count($data)==0){
            $data = ['id' => '', $field => 'No se encontró'];
            return ($request->isMethod('post')) ? response()->json([$data]) : response()->json(['results' => $data]);
        }

        return ($request->isMethod('post')) ? response()->json($data) : response()->json(['results' => $data]);
    }

}