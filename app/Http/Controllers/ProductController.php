<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
use Caffeinated\Shinobi\Models\Role;
use App\Models\PatternState;
use App\Models\Activity;
use App\Models\PatternDocumentType;
use App\Models\Agreement;
use App\Models\Project;
use DataTables;

use App\Http\Requests\productCreateRequest;
use App\Http\Requests\productUpdateRequest;
use App\Http\Requests\productProgressRequest;
use App\Models\AgreementPermission;

class ProductController extends Controller
{
    public function __construct(AgreementPermission $agreementPermission)
    {
        $this->middleware('auth');
        $this->permission = $agreementPermission;
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product_pattern_state = PatternState::where('pattern_id',3)->get();
        $project = Project::where('active',1)->get();

        return view('product.table',['project'=>$project, 'product_pattern_state'=>$product_pattern_state]);
    }

    /**
     * Show the dataTable.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        if ($request->ajax()) {
            $role = Role::find(auth()->user()->role_id);
            $products = Product::with(['responsible','project','pattern_state'])->select('products.*')->where('products.active',1);

            return Datatables::eloquent($products)
                ->filter(function ($query) use ($request){

                    if ($request->has('optProductPatternState')) {
                        $query->where('products.pattern_state_id',$request->get('optProductPatternState'));
                    }

                    if ($request->has('txtProductName')) {
                        $query->where('products.name','like', "%{$request->get('txtProductName')}%");
                    }

                    if ($request->has('optProject')) {
                        $query->where('products.project_id',$request->get('optProject'));
                    }

                    if ($request->has('txtProductStartDateFrom')) {
                        $query->where('products.start_date','>=',$request->get('txtProductStartDateFrom'). ' 00:00:00');
                    }

                    if ($request->has('txtProductStartDateTo')) {
                        $query->where('products.start_date','<=',$request->get('txtProductStartDateTo').' 23:59:59');
                    }

                    if ($request->has('txtProductEndDateFrom')) {
                        $query->where('products.end_date','>=', $request->get('txtProductEndDateFrom').' 00:00:00');
                    }

                    if ($request->has('txtProductEndDateTo')) {
                        $query->where('products.end_date','<=',$request->get('txtProductEndDateTo').' 23:59:59');
                    }

                })
                ->addColumn('buttons', function ($product) use ($role) {
                    $button = '<div class="row">';
                    if($role->can('product.view')) {
                        $button .= '<a href="/producto/' . $product->id . '" class="btn btn-sm btn-primary btn-icon btn-outline btn-round" title="Ver Proyecto" rol="tooltip" ><i class="fa fa-eye" aria-hidden="true"></i></a>';
                    }

                    if($role->can('product.audit')){
                        $button .= '<a onclick="audit(3,'.$product->id.');" class="btn btn-sm btn-warning btn-icon btn-outline btn-round" title="Ver Auditoria" rol="tooltip" ><i class="fa fa-history" aria-hidden="true"></i></a>';
                    }

                    $button .= '</div>';
                    return $button;
                })
                ->rawColumns(['buttons'])
                ->setRowId('id')
                ->make(true);
        }
    }

    /**
     * Show the dataTable.
     *
     * @return \Illuminate\Http\Response
     */
    public function list_product($project)
    {
        $products = Product::with(['responsible.person','project','pattern_state'])->where('project_id',$project)
            ->where('active','1')
            ->orderBy('end_date','asc')
            ->get();

        $data = [];
        foreach ($products as $product) {
            array_push($data,$product);
        }

        return response()->json(['data' => $data]);
    }

    /**
     * Show the dataTable.
     *
     * @return \Illuminate\Http\Response
     */
    public function list_billing($agreement)
    {
        $products = Product::with(['responsible.person','project','pattern_state'])
            ->whereRaw('project_id IN (SELECT id FROM projects WHERE agreement_id = ?)',$agreement)
            ->where('pattern_state_id','33')
            ->orderBy('end_date','asc')
            ->get();

        return response()->json(['data' => $products]);
    }

    public function store(productCreateRequest $request)
    {
        if ($request->ajax()) 
        {
            $project = Project::find($request['txtProductProject']);
            $permission = $this->permission->full_permission($project->agreement_id);
            if(!$permission){
                return response()->view('errors.403', ['error' => 'No pertenece al grupo de trabajo del contrato']);
            }

            $data =[
                'project_id' => $request['txtProductProject'],
                'responsible_id' => $request['txtProductResponsible'],
                'name' => $request['txtProductName'],
                'description' => $request['txtProductDescription'],
                'start_date' => $request['txtProductStartDate'],
                'end_date' => $request['txtProductEndDate'],
                'value' => $request['txtProductValue'],
                'user_cre_id' => auth()->user()->id,
            ];
            $product = new Product($data);
            $product->save();

            return response()->json(['mensaje' => 'creado']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::with(['responsible.person','project.agreement'])->find($id);

        return response()->json($product);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $product = Product::find($id);
        if($product===NULL){
            return redirect('productos');
        }
        $permission = $this->permission->full_permission($product->project->agreement_id);
        if(!$permission){
            return response()->view('errors.403', ['error' => 'No pertenece al grupo de trabajo del contrato']);
        }

        $product->responsible;
        $activity_pattern_state = PatternState::where('pattern_id',Activity::$pattern_id)->get();
        $product_pattern_state = PatternState::where('pattern_id',Product::$pattern_id)->get();
        $pattern_document_type = PatternDocumentType::where('pattern_id',Product::$pattern_id)->get();

        return view('product.view', [
            'pattern_id'=>Product::$pattern_id,
            'id'=>$id,
            'project_id'=>$product->project_id,
            'agreement_id'=>$product->project->agreement_id,
            'activity_pattern_state'=>$activity_pattern_state,
            'product_pattern_state'=>$product_pattern_state,
            'pattern_document_type'=>$pattern_document_type
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $data =[
            'active' => true,
            'user_mod_id' => auth()->user()->id,
        ];
        Product::find($id)
                    ->update($data);

        return response()->json(['mensaje' => 'Activado']);
    }


    public function update(productUpdateRequest $request, $id)
    {
        if ($request->ajax()) 
        {
            $data =[
                'responsible_id' => $request['txtProductResponsible'],
                'name' => $request['txtProductName'],
                'description' => $request['txtProductDescription'],
                'start_date' => $request['txtProductStartDate'],
                'end_date' => $request['txtProductEndDate'],
                'value' => $request['txtProductValue'],
                'contract_name' => $request['txtProductNameContract'],
                'contract_description' => $request['txtProductDescriptionContract'],
                'user_mod_id' => auth()->user()->id,
            ];      
            Product::find($id)
                        ->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }


    public function progress(productProgressRequest $request, $id)
    {
        if ($request->ajax())
        {
            $data =[
                "percentage" => $request['txtProductPercentage'],
                "pattern_state_id" => $request['optProductPatternState'],
                "justification" => $request['txtProductJustification'],
                "updated_at" => $request['txtProductFecMod'],
                'user_mod_id' => auth()->user()->id,
            ];
            $product = Product::find($id);

            $product->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }


    public function destroy($id)
    {
        $data =[
            'active' => false,
            'user_mod_id' => auth()->user()->id,
        ];
        $product = Product::find($id)
                    ->update($data);

        return response()->json(['mensaje' => 'Inactivo']);
    }

    public function list_calendar(Request $request){
        if($request->ajax()){
            $products = Product::select("products.id","products.name", "products.end_date", "products.percentage", "products.description", "products.pattern_state_id")
                ->join("professionals","professionals.id","=","products.responsible_id")
                ->where("professionals.person_id",auth()->user()->person_id)
                ->whereIn("pattern_state_id",[31,32,33,34])
                ->get();

            return response()->json($products);
        }
    }

    public function auditory($type, $id){
        /* 1. : State, 2. : Percentage */
        $field = ($type == 1 ? 'pattern_state_id' : 'percentage');

        $audit_products= DB::table('audit_products')
            ->where('id_parent',$id)
            ->where([
                ['field','=','justification'],
                ['field','=',$field, 'OR']
            ])
            ->select('field','old_value','new_value','user_cre_id','created_at')
            ->orderBy('created_at','asc')
            ->get();

        $data = [];
        foreach ($audit_products as $result) {
            if($result->field != 'justification'){
                $pattern_state = PatternState::find($result->new_value);
                $field = ($type == 1 ? $pattern_state->name : $result->new_value);
                $user = User::find($result->user_cre_id);
                $data[] = [
                    'field' => $field,
                    'user' => $user->name,
                    'created_at' => $result->created_at,
                    'justification' => ''
                ];
            }
        }

        foreach ($audit_products as $result) {
            if($result->field == 'justification'){
                $key = array_search($result->created_at, array_column($data, 'created_at'));
                if($key){
                    $data[$key]['justification'] =$result->new_value;
                }
            }
        }

        return response()->json(['data' => $data]);
    }
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function view_cash()
    {
        $agreements = Agreement::whereIn('pattern_state_id',[11,12,13])->get();
        $projects = Project::whereIn('pattern_state_id',[21,22])->get();
        $products = Product::whereIn('pattern_state_id',[31,32,33])->get();
        $pattern_states = PatternState::whereIn('pattern_id',[3])->get();

        return view('product.cash-flow', [
            'agreements' => $agreements,
            'projects' => $projects,
            'products' => $products,
            'pattern_states' => $pattern_states,
        ]);
    }
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function cash(Request $request)
    {
        if($request->ajax()) {
            $filter_request = $request->all();
            $pattern_states = PatternState::where("active", true)->where("pattern_id",3);
            if($filter_request['optStateFlow'] > 0){
                $pattern_states = $pattern_states->where("id",$filter_request['optStateFlow']);
            }
            $pattern_states = $pattern_states->get();

            $data = [];

            foreach ($pattern_states as $pattern_state){
                $products = Product::where('products.active', true)->where("products.pattern_state_id",$pattern_state->id);
                $products = $products->join("projects","products.project_id","=","projects.id");

                if($filter_request['optAgreementFlow'] > 0){
                    $products = $products->where("projects.agreement_id",$filter_request['optAgreementFlow']);
                }
                if($filter_request['optProjectFlow'] > 0){
                    $products = $products->where("projects.id",$filter_request['optProjectFlow']);
                }
                if($filter_request['optProductFlow'] > 0){
                    $products = $products->where("products.id",$filter_request['optProductFlow']);
                }

                if(in_array($pattern_state->id, [31,32,33,34])){
                    $products = $products->whereRaw("SUBSTRING(products.end_date,1,7) = ?",$request->fecFlow);
                    $products = $products->select('products.name', 'products.value', 'products.start_date', 'products.end_date', 'products.pattern_state_id');
                }elseif ($pattern_state->id == 35){
                    $products = $products->join("billing_details","billing_details.product_id","=","products.id");
                    $products = $products->join("billings","billings.id","=","billing_details.billing_id");

                    $products = $products->whereIn("billings.pattern_state_id",[41,42,43,44,45]);
                    $products = $products->whereRaw("SUBSTRING(billings.due_date,1,7) = ?",$request->fecFlow);
                    $products = $products->select('products.name', 'billing_details.value', 'products.start_date', 'billings.due_date AS end_date', 'products.pattern_state_id');
                }elseif ($pattern_state->id == 36){
                    $products = $products->join("billing_details","billing_details.product_id","=","products.id");
                    $products = $products->join("billings","billings.id","=","billing_details.billing_id");

                    $products = $products->whereIn("billings.pattern_state_id",[47]);
                    $products = $products->whereRaw("SUBSTRING(billings.payment_date,1,7) = ?",$request->fecFlow);
                    $products = $products->select('products.name', 'billings.net_value AS value', 'products.start_date', 'billings.payment_date AS end_date', 'products.pattern_state_id');
                }

                $products = $products->get();

                foreach($products as $product){
                    $data[] = $product;
                }
            }

            return response()->json(['data' => $data]);
        }
    }
}