<?php 
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use PDF;

class PdfController extends Controller
{
    function __construct()
    {
    }
        
    public function pdf() 
    {
        $datos=Input::all();
        $json_data = $datos['data'];
        $json_data=base64_decode($json_data);
        $data= json_decode($json_data);
        $data = (array) $data;
        
        if(isset($data['vista']))
        {
            $nombre_reporte=(isset($data['nom_reporte'])&&!empty($data['nom_reporte']))?$data['nom_reporte']:'Reporte';
            $descargar=(isset($data['descargar'])&&$data['descargar']==true)?true:false;
            if(!isset($data['prueba']))
            {
                //$html =  \View::make($data['vista'], compact('data'))->render();
				
                if(isset($data['conversion']))
				{
					    $pdf = \App::make('snappy.pdf.wrapper');
						$pdf->loadHTML($html);
						
						
						
					if($descargar)//si quiere que se descargue
						return $pdf->download($nombre_reporte);
				   else //para visualizar desde el navegador
						return $pdf->inline();
				}
				else
				{



                    /*
					    $pdf = \App::make('dompdf.wrapper');
						$pdf->loadHTML($html);
						
						if($descargar)//si quiere que se descargue
							return $pdf->download($nombre_reporte);
					   else//para visualizar desde el navegador
						 return $pdf->stream($nombre_reporte);
                    */
				} 
            }
            else//para ver el reporte en  html
                return view($data['vista'],compact('data'));
        }
        else
        {
            printf('No Hay una vista asignada al reporte');
            return;
        }
    }
}
