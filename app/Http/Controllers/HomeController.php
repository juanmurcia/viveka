<?php

namespace App\Http\Controllers;

use App\Models\Agreement;
use App\Models\Product;
use App\Models\Project;
use App\Models\Activity;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agreements = Agreement::whereIn('pattern_state_id',[11,12,13]);
        $projects = Project::whereIn('pattern_state_id',[21,22]);
        $products = Product::whereIn('pattern_state_id',[31,32,33]);
        $activities = Activity::whereIn('pattern_state_id',[41,42,43]);

        if(auth()->user()->role_id == 1){
            $agreements = $agreements->get();
            $projects = $projects->get();
            $products = $products->get();
            $activities = $activities->get();
        }else{
            $agreements = $agreements->whereHas('team.professional', function($q){
                $q->where('person_id', '=', auth()->user()->person_id );
            })->get();

            $agreements_id =  array_column($agreements, 'id');
            $projects = $projects->whereIn('agreement_id',$agreements_id)->get();

            $projects_id =  array_column($projects, 'id');
            $products = $products->whereIn('products_id',$projects_id)->get();

            $products_id =  array_column($products, 'id');
            $activities = $activities->whereIn('product_id',$products_id)->get();
        }

        return view('main-menu.main-menu', [
            'agreements' => $agreements,
            'projects' => $projects,
            'products' => $products,
            'activities' => $activities,
            'user' => auth()->user()->name]);
    }
}
