<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Models\Pattern;
use Caffeinated\Shinobi\Models\Role;

class AuditController extends Controller
{
    public $arr_name;

    function list($pattern_id, $model_id){
        $pattern = Pattern::find($pattern_id);
        $role = Role::find(auth()->user()->role_id);
        $permission = $pattern->model.'.audit';

        if(!$role->can($permission)){
            return response()->json(['ErrorBd' => 'No posee permisos de auditoría de '.$pattern->name]);
        }

        if(sizeof($pattern) == 0){
            return response()->json(['ErrorBd' => 'Modelo no encontrado o configurado']);
        }

        $pattern = str_replace(' ','','App\Models\ '.$pattern->model);
        $model = $pattern::where('id',$model_id)->first();
        $this->arr_name = $model->audit_field;

        if(sizeof($model) == 0){
            return response()->json(['ErrorBd' => 'Registro no encontrado']);
        }

        $arr_audit =
            DB::table('audit_'.$model->table.' AS A')
                ->select('A.field','A.old_value','A.new_value','users.name AS user','A.created_at')
                ->join('users','A.user_cre_id','=','users.id')
                ->where('A.id_parent',$model_id)
                ->orderBy('A.created_at','desc')
                ->get();

        $audit = [];

        foreach ($arr_audit as $item) {
            $obj = $this->validate_field($item);
            $obj['created_at'] = $item->created_at;
            $obj['user'] = $item->user;
            array_push($audit, $obj);
        }
        return response()->json(['data' => $audit]);
    }

    function validate_field($item){
        $field = $this->field_name($item->field);
        $value = $this->field_value($item);

        return ['field' => $field, 'old_value' => $value[0], 'new_value' => $value[1]];
    }

    function field_name($field){
        $field = (isset($this->arr_name[$field]) ? $this->arr_name[$field]['name'] : $field);
        return $field;
    }

    function field_value($item){
        $new = $item->new_value;
        $old = $item->old_value;

        if(isset($this->arr_name[$item->field]['model'])){
            $pattern = str_replace(' ', '', 'App\Models\ '.$this->arr_name[$item->field]['model']);
            $model = $pattern::where('id',$item->old_value)->first();
            $old = (in_array($this->arr_name[$item->field]['model'],['Company','Professional']) ? $model->person->name.' '.$model->person->last_name : $model->name);

            $model = $pattern::where('id',$item->new_value)->first();
            $new = (in_array($this->arr_name[$item->field]['model'],['Company','Professional']) ? $model->person->name.' '.$model->person->last_name : $model->name);
        }

        return [$old,$new];
    }
}
