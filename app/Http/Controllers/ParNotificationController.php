<?php

namespace App\Http\Controllers;

use App\Models\ParNotification;
use Illuminate\Http\Request;
use Caffeinated\Shinobi\Models\Role;

class ParNotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        return view('notification.parameter', compact('roles'));
    }

    /**
     * Show the dataTable.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        if($request->ajax()){
            $par_notifications = ParNotification::all();
            $role = Role::find(auth()->user()->role_id);
            $edit = $role->can('par_notification.edit');

            $data = [];
            foreach ($par_notifications as $par_notification) {


                $par_notification->buttons = '<div>';
                if($edit){
                    $par_notification->buttons .= '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round" onclick="editParNotification('.$par_notification->id.')" title="Editar Documento" rol="tooltip" ><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                }
                $par_notification->buttons .= '</div>';
                
                array_push($data,$par_notification);
            }

            return response()->json(['data' => $data]);
        }
    }

    function action(){
        $par_notifications = ParNotification::all();
        foreach ($par_notifications as $par_notification) {
            if(in_array($par_notification->id, [1,2,3,7,8,9])){

            }
        }
    }
}
