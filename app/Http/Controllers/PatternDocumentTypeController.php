<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PatternDocumentType as patternDocumentType;
use App\Models\Pattern;
use Caffeinated\Shinobi\Models\Role;

use App\Http\Requests\patternDocumentTypeCreateRequest;
use App\Http\Requests\patternDocumentTypeUpdateRequest;

class PatternDocumentTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $role = Role::all();
        $pattern = Pattern::all();

        return view('pattern_document_type.table',[
            'pattern' => $pattern
        ]);
    }

    /**
     * Show the dataTable.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $role = Role::find(auth()->user()->role_id);
        $edit = $role->can('patternDocumentType.edit');
        $destroy = $role->can('patternDocumentType.destroy');

        $pattern_document_types = patternDocumentType::all();

        $data = [];
        foreach ($pattern_document_types as $pattern_document_type) {
            $id = $pattern_document_type['id'];

            $pattern_document_type['pattern_name'] = $pattern_document_type['pattern']['name'];

            $pattern_document_type['state'] = ($pattern_document_type['active'] ? 'ACTIVO' : 'INACTIVO');

            $pattern_document_type['buttons'] = '<div>';

            if($edit){
                $pattern_document_type['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round" onclick="editPatternDocumentType('.$id.')" title="Editar Documento Modelo" rol="tooltip" ><i class="fa fa-pencil" aria-hidden="true"></i></a>';
            }

            if($destroy) {
                if ($pattern_document_type['active']) {
                    $pattern_document_type['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-icon btn-outline btn-round" onclick="destroyPatternDocumentType('.$id.')" title="Inctivar Documento Modelo" rol="tooltip" ><i class="fa fa-close" aria-hidden="true"></i></a>';
                }else{
                    $pattern_document_type['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-success btn-icon btn-outline btn-round" onclick="activatePatternDocumentType('.$id.')" title="Activar Documento Modelo" rol="tooltip" ><i class="fa fa-check" aria-hidden="true"></i></a>';
                }
            }
            

            $pattern_document_type['buttons'] .= '</div>';
            array_push($data,$pattern_document_type);
        }

        return response()->json(['data' => $data]);
    }


    public function create()
    {
        
    }

    public function store(patternDocumentTypeCreateRequest $request)
    {
        if ($request->ajax())
        {
            $data =[
                'pattern_id' => $request['optPatternId'],
                'name' => $request['txtName'],
                'active' => true,
                'user_cre_id' => auth()->user()->id,
            ];
            $pattern_document_type = new patternDocumentType($data);
            $pattern_document_type->save();

            return response()->json(['mensaje' => 'creado']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pattern_document_type = patternDocumentType::find($id);

        return response()->json($pattern_document_type);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $data =[
            'active' => true,
            'user_mod_id' => auth()->user()->id,
        ];
        $pattern_document_type = patternDocumentType::find($id)
                    ->update($data);

        return response()->json(['mensaje' => 'Activado']);
    }


    public function update(patternDocumentTypeUpdateRequest $request, $id)
    {
        if ($request->ajax()) 
        {
            $data =[
                'pattern_id' => $request['optPatternId'],
                'name' => $request['txtName'],
                'user_mod_id' => auth()->user()->id,
            ];      
            $pattern_document_type = patternDocumentType::find($id)
                        ->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }


    public function destroy($id)
    {
        $data =[
            'active' => false,
            'user_mod_id' => auth()->user()->id,
        ];
        $pattern_document_type = patternDocumentType::find($id)
                    ->update($data);

        return response()->json(['mensaje' => 'Inactivo']);
    }
}