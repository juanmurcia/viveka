<?php

namespace App\Http\Controllers;

use App\Models\Agreement;
use App\Models\Product;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Models\ActivityTime;
use App\Models\Activity;
use Caffeinated\Shinobi\Models\Role;

use App\Http\Requests\activityTimeCreateRequest;
use App\Http\Requests\activityTimeUpdateRequest;

class ActivityTimeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        return view('activity_time.table');
    }

    /**
     * Show the dataTable.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $role = Role::find(auth()->user()->role_id);
        $edit = $role->can('activity_time.edit');
        $destroy = $role->can('activity_time.destroy');

        $activity_times=ActivityTime::join('activities','activities.id','=','activity_times.activity_id')
            ->join('patterns','patterns.id','=','activities.pattern_id')
            ->join('people','people.id','=','activity_times.person_id')
            ->join('pattern_names',function($join) {
                $join->on('activities.pattern_id', '=', 'pattern_names.pattern_id')
                    ->on('activities.model_id', '=', 'pattern_names.model_id');
            });

        if ($request['optActivityTimePattern']!='') {
            $activity_times=$activity_times->where('patterns.id',$request['optActivityTimePattern']);
        }

        if ($request['txtActivityTimeEndDateFrom']!='') {
            $activity_times=$activity_times->where('start_date','>=',$request['txtActivityTimeEndDateFrom'].' 00:00:00');
        }

        if ($request['txtActivityTimeEndDateTo']!='') {
            $activity_times=$activity_times->where('end_date','<=',$request['txtActivityTimeEndDateFrom'].' 23:59:59');
        }

        $activity_times=$activity_times->select(
            'activity_times.*',
            'activities.name AS activity_name',
            'people.name AS person_name',
            'people.last_name AS person_last_name',
            'patterns.name AS pattern',
            'pattern_names.name AS pattern_name'
        );
        $activity_times=$activity_times->get();

        $data = [];
        foreach ($activity_times as $activity_time) {
            $id = $activity_time['id'];
            $activity_time['person_name']=$activity_time['person_name'].' '.$activity_time['person_last_name'];
            $activity_time['buttons'] = '<div>';

            if($edit){
                $activity_time['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round" onclick="editActivityTime('.$id.')" title="Editar Actividad" rol="tooltip" ><i class="fa fa-pencil" aria-hidden="true"></i></a>';
            }

            if($destroy) {
                if ($activity_time['active']) {
                    $activity_time['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-icon btn-outline btn-round" onclick="destroyActivityTime('.$id.')" title="Inctivar Actividad" rol="tooltip" ><i class="fa fa-close" aria-hidden="true"></i></a>';
                }else{
                    $activity_time['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-success btn-icon btn-outline btn-round" onclick="activateActivityTime('.$id.')" title="Activar Actividad" rol="tooltip" ><i class="fa fa-check" aria-hidden="true"></i></a>';
                }
            }

            $activity_time['buttons'] .= '</div>';
            array_push($data,$activity_time);
        }

        return response()->json(['data' => $data]);
    }

    public function list_activity($activity_id)
    {
        $activity_times = ActivityTime::where('activity_id',$activity_id)->where('active',true)->with('person')->get();
        return response()->json(['data' => $activity_times]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $agreements = Agreement::whereIn('pattern_state_id',[11,12,13]);
        $projects = Project::whereIn('pattern_state_id',[21,22]);
        $products = Product::whereIn('pattern_state_id',[31,32,33]);
        $activities = Activity::whereIn('pattern_state_id',[41,42,43]);

        if(auth()->user()->role_id == 1){
            $agreements = $agreements->get();
            $projects = $projects->get();
            $products = $products->get();
            $activities = $activities->get();
        }else{
            $agreements = $agreements->whereHas('team.professional', function($q){
                $q->where('person_id', '=', auth()->user()->person_id );
            })->get();

            $agreements_id =  array_column($agreements, 'id');
            $projects = $projects->whereIn('agreement_id',$agreements_id)->get();

            $projects_id =  array_column($projects, 'id');
            $products = $products->whereIn('products_id',$projects_id)->get();

            $products_id =  array_column($products, 'id');
            $activities = $activities->whereIn('product_id',$products_id)->get();
        }

        return view('activity_time.calendar', [
            'agreements' => $agreements,
            'projects' => $projects,
            'products' => $products,
            'activities' => $activities
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(activityTimeCreateRequest $request)
    {
        if ($request->ajax()) 
        {
            $data =[
                "activity_id" => $request['txtActivityTimeActivityId'],
                "person_id" => $request['txtActivityTimePersonId'],
                "end_date" => $request['txtActivityTimeEndDate'],
                "executed" => $request['txtActivityTimeExecuted'],
                "name" => $request['txtActivityTimeName'],
                "description" => $request['txtActivityTimeDescription'],
                'user_cre_id' => auth()->user()->id,
            ];
            $activity_time = new ActivityTime($data);
            $activity_time->save();

            return response()->json(['mensaje' => 'creado']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $activity_time = ActivityTime::find($id);
        $activity_time['activity'];
        $activity_time['person'];


        return response()->json($activity_time);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $data =[
            'active' => true,
            'user_mod_id' => auth()->user()->id,
        ];
        $activity_time = ActivityTime::find($id)
                    ->update($data);

        return response()->json(['mensaje' => 'Activado']);
    }


    public function update(activityTimeUpdateRequest $request, $id)
    {
        if ($request->ajax()) 
        {
            $data =[
                "activity_id" => $request['txtActivityTimeActivityId'],
                "person_id" => $request['txtActivityTimePersonId'],
                "end_date" => $request['txtActivityTimeEndDate'],
                "executed" => $request['txtActivityTimeExecuted'],
                "name" => $request['txtActivityTimeName'],
                "description" => $request['txtActivityTimeDescription'],
                'user_mod_id' => auth()->user()->id,
            ];      
            $activity_time = ActivityTime::find($id);
            $activity_time->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }


    public function destroy($id)
    {
        $data =[
            'active' => false,
            'user_mod_id' => auth()->user()->id,
        ];
        $activity_time = ActivityTime::find($id)
                    ->update($data);

        return response()->json(['mensaje' => 'Inactivo']);
    }
}