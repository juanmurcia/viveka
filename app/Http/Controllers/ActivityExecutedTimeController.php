<?php

namespace App\Http\Controllers;

use App\Models\Agreement;
use App\Models\Product;
use App\Models\Project;
use App\Models\Activity;
use App\Http\Requests\activityExecutedTimeCreateRequest;
use App\Http\Requests\activityExecutedTimeUpdateRequest;
use App\Models\ActivityExecutedTime;
use Illuminate\Http\Request;
use Caffeinated\Shinobi\Models\Role;

class ActivityExecutedTimeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('activity_executed_time.table');
    }

    public function create()
    {
        $agreements = Agreement::whereIn('pattern_state_id',[11,12,13]);
        $projects = Project::whereIn('pattern_state_id',[21,22]);
        $products = Product::whereIn('pattern_state_id',[31,32,33]);
        $activities = Activity::whereIn('pattern_state_id',[41,42,43]);

        if(auth()->user()->role_id == 1){
            $agreements = $agreements->get();
            $projects = $projects->get();
            $products = $products->get();
            $activities = $activities->get();
        }else{
            $agreements = $agreements->whereHas('team.professional', function($q){
                $q->where('person_id', '=', auth()->user()->person_id );
            })->get();

            $agreements_id =  array_column($agreements, 'id');
            $projects = $projects->whereIn('agreement_id',$agreements_id)->get();

            $projects_id =  array_column($projects, 'id');
            $products = $products->whereIn('products_id',$projects_id)->get();

            $products_id =  array_column($products, 'id');
            $activities = $activities->whereIn('product_id',$products_id)->get();
        }

        return view('activity_time.calendar', [
            'agreements' => $agreements,
            'projects' => $projects,
            'products' => $products,
            'activities' => $activities
        ]);
    }

    public function store(activityExecutedTimeCreateRequest $request)
    {
        if ($request->ajax())
        {

            $data =[
                "activity_id" => $request['txtActivityExecutedTimeActivityId'],
                "person_id" => Auth()->user()->person_id,
                "role_id" => Auth()->user()->roles()->orderBy('roles.id','asc')->first()->id,
                "end_date" => $request['txtActivityExecutedTimeEndDate'],
                "executed" => $request['txtActivityExecutedTimeExecuted'],
                "description" => $request['txtActivityExecutedTimeDescription'],
                'user_cre_id' => Auth()->user()->id,
            ];
            $activity_executed_time = new ActivityExecutedTime($data);
            $activity_executed_time->save();

            return response()->json(['mensaje' => 'creado', 'id' =>$activity_executed_time->id]);
        }
    }

    public function update(activityExecutedTimeUpdateRequest $request, $id)
    {
        if ($request->ajax())
        {
            $data =[
                "end_date" => $request['txtActivityExecutedTimeEndDate'],
                "executed" => $request['txtActivityExecutedTimeExecuted'],
                "description" => $request['txtActivityExecutedTimeDescription'],
                'user_mod_id' => Auth()->user()->id,
            ];
            $activity_executed_time = ActivityExecutedTime::find($id);
            $activity_executed_time->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }

    public function activate($id)
    {
        $data =[
            'active' => true,
            'user_mod_id' => auth()->user()->id,
        ];
        ActivityExecutedTime::find($id)
            ->update($data);

        return response()->json(['mensaje' => 'Activado']);
    }

    public function destroy($id)
    {
        $data =[
            'active' => false,
            'user_mod_id' => auth()->user()->id,
        ];
        ActivityExecutedTime::find($id)
            ->update($data);

        return response()->json(['success' => true]);
    }

    public function show($id)
    {
        $activity_executed_time = ActivityExecutedTime::find($id)->with(['activity','role','person'])->get();
        return response()->json(['data' => $activity_executed_time]);
    }

    public function calendar()
    {
        $activity_executed_time = ActivityExecutedTime::where('user_cre_id',Auth()->user()->id)->where('active',1)->with(['activity', 'person'])->get();
        return response()->json(['data' => $activity_executed_time]);
    }

    public function list(Request $request)
    {
        $role = Role::find(auth()->user()->role_id);
        $edit = $role->can('activity_executed_time.edit');
        $destroy = $role->can('activity_executed_time.destroy');

        $activity_executed_times=ActivityExecutedTime::join('activities','activities.id','=','activity_executed_times.activity_id')
            ->join('patterns','patterns.id','=','activities.pattern_id')
            ->join('people',function ($join){
                $join->on('people.id','activity_executed_times.person_id');
                $join->where('people.id','=',auth()->user()->person_id);
            })
            ->join('pattern_names',function($join) {
                $join->on('activities.pattern_id', '=', 'pattern_names.pattern_id')
                    ->on('activities.model_id', '=', 'pattern_names.model_id');
            });

        if ($request['optActivityExecutedTimePattern']!='') {
            $activity_executed_times=$activity_executed_times->where('patterns.id',$request['optActivityExecutedTimePattern']);
        }

        if ($request['txtActivityExecutedTimeEndDateFrom']!='') {
            $activity_executed_times=$activity_executed_times->where('start_date','>=',$request['txtActivityExecutedTimeEndDateFrom'].' 00:00:00');
        }

        if ($request['txtActivityExecutedTimeEndDateTo']!='') {
            $activity_executed_times=$activity_executed_times->where('end_date','<=',$request['txtActivityExecutedTimeEndDateFrom'].' 23:59:59');
        }

        $activity_executed_times=$activity_executed_times->select(
            'activity_executed_times.*',
            'activities.name AS activity_name',
            'people.name AS person_name',
            'people.last_name AS person_last_name',
            'patterns.name AS pattern',
            'pattern_names.name AS pattern_name'
        );
        $activity_executed_times=$activity_executed_times->get();

        $data = [];
        foreach ($activity_executed_times as $activity_executed_time) {
            $id = $activity_executed_time['id'];
            $activity_executed_time['person_name']=$activity_executed_time['person_name'].' '.$activity_executed_time['person_last_name'];
            $activity_executed_time['buttons'] = '<div>';

            if($edit){
                $activity_executed_time['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round" onclick="editActivityExecutedTime('.$id.')" title="Editar Actividad" rol="tooltip" ><i class="fa fa-pencil" aria-hidden="true"></i></a>';
            }

            if($destroy) {
                if ($activity_executed_time['active']) {
                    $activity_executed_time['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-icon btn-outline btn-round" onclick="destroyActivityExecutedTime('.$id.')" title="Inctivar Actividad" rol="tooltip" ><i class="fa fa-close" aria-hidden="true"></i></a>';
                }else{
                    $activity_executed_time['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-success btn-icon btn-outline btn-round" onclick="activateActivityExecutedTime('.$id.')" title="Activar Actividad" rol="tooltip" ><i class="fa fa-check" aria-hidden="true"></i></a>';
                }
            }

            $activity_executed_time['buttons'] .= '</div>';
            array_push($data,$activity_executed_time);
        }

        return response()->json(['data' => $data]);
    }

}