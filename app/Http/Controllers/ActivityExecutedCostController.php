<?php

namespace App\Http\Controllers;

use App\Http\Requests\activityExecutedCostCreateRequest;
use App\Models\ActivityExecutedCost;

class ActivityExecutedCostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(activityExecutedCostCreateRequest $request)
    {
        if ($request->ajax())
        {
            $data =[
                "activity_id" => $request['txtActivityExecutedCostActivityId'],
                "person_id" => $request['txtActivityExecutedCostPersonId'],
                "activity_cost_subcategory_id" => $request['optActivityExecutedCostActivityCostSubCategoryId'],
                "end_date" => $request['txtActivityExecutedCostEndDate'],
                "executed" => $request['txtActivityExecutedCostExecuted'],
                "description" => $request['txtActivityExecutedCostDescription'],
                'user_cre_id' => auth()->user()->id,
            ];
            $activity_executed_cost = new ActivityExecutedCost($data);
            $activity_executed_cost->save();

            return response()->json(['mensaje' => 'creado', 'id' => $activity_executed_cost['id']]);
        }
    }

    public function update(activityExecutedCostUpdateRequest $request, $id)
    {
        if ($request->ajax())
        {
            $data =[
                "person_id" => $request['txtActivityExecutedCostPersonId'],
                "activity_cost_subcategory_id" => $request['optActivityExecutedCostActivityCostSubCategoryId'],
                "end_date" => $request['txtActivityExecutedCostEndDate'],
                "executed" => $request['txtActivityExecutedCostExecuted'],
                "description" => $request['txtActivityExecutedCostDescription'],
                'user_mod_id' => auth()->user()->id,
            ];
            $activity_executed_cost = ActivityExecutedCost::find($id);
            $activity_executed_cost->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }

    public function activate($id)
    {
        $data =[
            'active' => true,
            'user_mod_id' => auth()->user()->id,
        ];
        ActivityExecutedCost::find($id)
            ->update($data);

        return response()->json(['mensaje' => 'Activado']);
    }

    public function destroy($id)
    {
        $data =[
            'active' => false,
            'user_mod_id' => auth()->user()->id,
        ];
        ActivityExecutedCost::find($id)->update($data);

        return response()->json(['mensaje' => 'Inactivo']);
    }

    public function show($id)
    {
        $activity_executed_cost = ActivityExecutedCost::find($id)->with(['activity','activity_cost_subcategory','person'])->get();

        return response()->json(['data' => $activity_executed_cost]);
    }
}