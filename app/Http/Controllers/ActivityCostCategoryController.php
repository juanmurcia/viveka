<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ActivityCostCategory;
use App\Models\Pattern;
use Caffeinated\Shinobi\Models\Role;

use App\Http\Requests\activityCostCategoryCreateRequest;
use App\Http\Requests\activityCostCategoryUpdateRequest;

class ActivityCostCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('activity_cost_category.table');
    }

    /**
     * Show thphpe dataTable.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $role = Role::find(auth()->user()->role_id);
        $edit = $role->can('activityCostCategory.edit');
        $destroy = $role->can('activityCostCategory.destroy');

        $activity_cost_categorys = ActivityCostCategory::with(['user_cre'])->orderBy('active')->get();

        $data = [];
        foreach ($activity_cost_categorys as $activity_cost_category) {
            $id = $activity_cost_category['id'];

            $activity_cost_category['pattern_name'] = $activity_cost_category['pattern']['name'];
            $activity_cost_category['state'] = ($activity_cost_category['active'] ? 'ACTIVO' : 'INACTIVO');
            $activity_cost_category['buttons'] = '<div>';

            if($edit){
                $activity_cost_category['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round" onclick="editActivityCostCategory('.$id.')" title="Editar Categoría de Costo" rol="tooltip" ><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                $activity_cost_category['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round" onclick="viewActivityCostSubCategory('.$id.')" title="Editar Sub Categoría de Costo" rol="tooltip" ><i class="fa fa-code-fork" aria-hidden="true"></i></a>';
            }

            if($destroy) {
                if ($activity_cost_category['active']) {
                    $activity_cost_category['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-icon btn-outline btn-round" onclick="destroyActivityCostCategory('.$id.')" title="Inctivar Documento Modelo" rol="tooltip" ><i class="fa fa-close" aria-hidden="true"></i></a>';
                }else{
                    $activity_cost_category['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-success btn-icon btn-outline btn-round" onclick="activateActivityCostCategory('.$id.')" title="Activar Documento Modelo" rol="tooltip" ><i class="fa fa-check" aria-hidden="true"></i></a>';
                }
            }
            

            $activity_cost_category['buttons'] .= '</div>';
            array_push($data,$activity_cost_category);
        }

        return response()->json(['data' => $data]);
    }

    public function store(activityCostCategoryCreateRequest $request)
    {
        if ($request->ajax())
        {
            $data =[
                'name' => $request['txtCategoryName'],
                'active' => true,
                'user_cre_id' => auth()->user()->id,
            ];
            $activity_cost_category = new ActivityCostCategory($data);
            $activity_cost_category->save();

            return response()->json(['mensaje' => 'creado']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $activity_cost_category = ActivityCostCategory::with(['activity_cost_subcategory','activity_cost_subcategory.user_cre'])->where('id',$id)->get();
        return response()->json($activity_cost_category[0]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $data =[
            'active' => true,
            'user_mod_id' => auth()->user()->id,
        ];
        ActivityCostCategory::find($id)->update($data);

        return response()->json(['mensaje' => 'Activado']);
    }


    public function update(activityCostCategoryUpdateRequest $request, $id)
    {
        if ($request->ajax()) 
        {
            $data =[
                'name' => $request['txtCategoryName'],
                'user_mod_id' => auth()->user()->id,
            ];      
            ActivityCostCategory::find($id)->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }


    public function destroy($id)
    {
        $data =[
            'active' => false,
            'user_mod_id' => auth()->user()->id,
        ];
        ActivityCostCategory::find($id)->update($data);

        return response()->json(['mensaje' => 'Inactivo']);
    }
}