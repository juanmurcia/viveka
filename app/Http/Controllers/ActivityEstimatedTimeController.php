<?php

namespace App\Http\Controllers;

use App\Http\Requests\activityEstimatedTimeCreateRequest;
use App\Models\ActivityEstimatedTime;

class ActivityEstimatedTimeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(activityEstimatedTimeCreateRequest $request)
    {
        if ($request->ajax()) {
            $activity_estimated_time = ActivityEstimatedTime::firstOrCreate(
                [
                    'activity_id' => $request['txtActivityEstimatedTimeActivityId'],
                    'role_id' => $request['optActivityEstimatedTimeRoleId'],
                ],
                [
                    'estimated' => 0,
                    'justification' => 'Inicializado',
                    'user_mod_id' => auth()->user()->id,
                ]
            );

            $activity_estimated_time->update(
                [
                    'estimated' => $request['txtActivityEstimatedTimeEstimated'],
                    'justification' => $request['txtActivityEstimatedTimeJustification'],
                    'user_mod_id' => auth()->user()->id,
                ]
            );

            return response()->json(['mensaje' => 'Guardado']);
        }
    }

    public function show($id)
    {
        $activity_estimated_time = ActivityEstimatedTime::find($id)->with(['activity','role'])->get();

        return response()->json(['data' => $activity_estimated_time]);
    }
}