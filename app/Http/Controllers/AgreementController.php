<?php

namespace App\Http\Controllers;

use App\Http\Requests\agreementDetailsRequest;
use App\Http\Requests\agreementProgressRequest;
use App\Models\Activity;
use App\Models\AgreementTeam;
use App\Models\PatternDocumentType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Agreement;
use App\Models\Project;
use Caffeinated\Shinobi\Models\Role;
use App\Models\AgreementType;
use App\Models\AgreementProcessType;
use App\Models\AgreementCategory;
use App\Models\AgreementNature;
use App\Models\AgreementService;
use App\Models\AgreementCoverage;
use App\Models\Department;
use App\Models\Currency;
use App\Models\PatternState;
use App\Models\SystemParameter;
use App\Http\Requests\agreementCreateRequest;
use App\Http\Requests\agreementUpdateRequest;
use App\Models\AgreementPermission;
use DataTables;

class AgreementController extends Controller
{
    public $permission;

    public function __construct(AgreementPermission $agreementPermission)
    {
        $this->middleware('auth');
        $this->permission = $agreementPermission;
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $roles = Role::all();
        $agreement_pattern_state = PatternState::where('pattern_id',Agreement::$pattern_id)->get();
        $agreement_type = AgreementType::all();
        $agreement_process_type = AgreementProcessType::all();
        $agreement_category = AgreementCategory::all();
        $agreement_nature = AgreementNature::all();
        $agreement_service = AgreementService::all();
        $agreement_coverage = AgreementCoverage::all();
        $currency = Currency::all();
        $department = Department::all();

        return view('agreement.table', [
        	'currency'=>$currency,
            'roles'=>$roles,
        	'department'=>$department,
        	'agreement_pattern_state'=>$agreement_pattern_state,
            'agreement_type'=>$agreement_type,
            'agreement_category'=>$agreement_category,
            'agreement_nature'=>$agreement_nature,
            'agreement_service'=>$agreement_service,
            'agreement_coverage'=>$agreement_coverage,        	
        	'agreement_process_type'=>$agreement_process_type,
        	'user' => auth()->user()->name]);
    }

    /**
     * Show the dataTable rows.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        if ($request->ajax()) {
            $role = Role::find(auth()->user()->role_id);
            $agreements = Agreement::with(['pattern_state','agreement_type','agreement_process_type','agreement_category',
                'agreement_nature','agreement_service','agreement_coverage','currency','department','responsible.person'])
                ->where('agreements.active',true)->select('*');

            return Datatables::eloquent($agreements)
                ->filter(function ($query) use ($request){

                    if ($request->has('txtAgreementNumber')) {
                        $query->where('agreements.number', 'like', "%{$request->get('txtAgreementNumber')}%");
                    }

                    if ($request->has('txtAgreementName')) {
                        $query->where('agreements.name', 'like', "%{$request->get('txtAgreementName')}%");
                    }

                    if ($request->has('optAgreementPatternState')) {
                        $query->where('agreements.pattern_state_id',$request->get('optAgreementPatternState'));
                    }

                    if ($request->has('optAgreementProcessType')) {
                        $query->where('agreements.agreement_process_type_id',$request->get('optAgreementProcessType'));
                    }

                    if ($request->has('optAgreementType')) {
                        $query->where('agreements.agreement_type_id',$request->get('optAgreementType'));
                    }

                    if ($request->has('optAgreementCategory')) {
                        $query->where('agreements.agreement_category_id',$request->get('optAgreementCategory'));
                    }

                    if ($request->has('optAgreementNature')) {
                        $query->where('agreements.agreement_nature_id',$request->get('optAgreementNature'));
                    }

                    if ($request->has('optAgreementService')) {
                        $query->where('agreements.agreement_service_id',$request->get('optAgreementService'));
                    }

                    if ($request->has('optAgreementCoverage')) {
                        $query->where('agreements.agreement_coverage_id',$request->get('optAgreementCoverage'));
                    }

                    if ($request->has('optAgreementCurrency')) {
                        $query->where('agreements.currency_id',$request->get('optAgreementCurrency'));
                    }

                    if ($request->has('optAgreementDepartment')) {
                        $query->where('agreements.department_id',$request->get('optAgreementDepartment'));
                    }

                    if ($request->has('txtAgreementStartDateFrom')) {
                        $query->where('agreements.start_date','>=',$request->get('txtAgreementStartDateFrom').' 00:00:00');
                    }

                    if ($request->has('txtAgreementStartDateTo')) {
                        $query->where('agreements.start_date','<=',$request->get('txtAgreementStartDateTo').' 23:59:59');
                    }

                    if ($request->has('txtAgreementEndDateFrom')) {
                        $query->where('agreements.end_date','>=',$request->get('txtAgreementEndDateFrom').' 00:00:00');
                    }

                    if ($request->has('txtAgreementEndDateTo')) {
                        $query->where('agreements.end_date','<=',$request->get('txtAgreementEndDateTo').' 23:59:59');
                    }
                })
                ->addColumn('buttons', function ($agreement) use ($role) {
                    $button = '<div class="row">';

                    $agreement['buttons'] = '<div>';
                    if($role->can('agreement.view')){
                        $button .= '<a href="/contrato/'.$agreement->id.'" class="btn btn-sm btn-primary btn-icon btn-outline btn-round" title="Ver Contrato" rol="tooltip" ><i class="fa fa-eye" aria-hidden="true"></i></a>';
                    }

                    if($role->can('agreement.audit')){
                        $button .= '<a onclick="audit(1,'.$agreement->id.');" class="btn btn-sm btn-warning btn-icon btn-outline btn-round" title="Ver Auditoria" rol="tooltip" ><i class="fa fa-history" aria-hidden="true"></i></a>';
                    }

                    $button .= '</div>';
                    return $button;
                })
                ->rawColumns(['buttons'])
                ->setRowId('id')
                ->make(true);
        }
    }

    public function list_agreements()
    {
        $agreements = Agreement::with(['responsible.person','currency','employer.person','projects', 'contractor.person']);

        if(auth()->user()->roles()->whereIn('roles.slug',['manager','admin'])->doesntExist()){
            $agreements = $agreements->whereHas('team.professional', function($q){
                $q->where('person_id', '=', auth()->user()->person_id );
            });
        }

        $agreements= $agreements->get();

        $data = [];
        foreach ($agreements as $agreement) {
            $agreement['string_value'] = $agreement['currency']['symbol'].number_format($agreement->value,0).' '.$agreement['currency']['iso'];

            array_push($data,$agreements);
        }

        return response()->json(['data' => $agreements]);
    }


    public function create()
    {
        
    }

    public function store(agreementCreateRequest $request)
    {
        if ($request->ajax())
        {
            $iva = SystemParameter::find(1); // Parámetro valor de iva
            $data =[
                "iva_billing" => $iva->value,
                "number" => $request['txtAgreementNumber'],
                "name" => $request['txtAgreementName'],
                "object" => $request['txtAgreementObject'],
                "employer_id" => $request['txtAgreementEmployer'],
                "contractor_id" => $request['txtAgreementContractor'],
                "responsible_id" => $request['txtAgreementResponsible'],
                "value" => $request['txtAgreementValue'],
                "currency_id" => $request['optAgreementCurrency'],
                "single_project" => !$request['txtSingleProject'],
                'user_cre_id' => auth()->user()->id,
            ];

            $agreement = new Agreement($data);
            $agreement->save();

            //initialize agreement team
            $data=[
                "active" => true,
                "agreement_id" => $agreement['id'],
                "professional_id" => $agreement['responsible_id'],
                'user_mod_id' => auth()->user()->id,
            ];

            $agreement_team = new AgreementTeam($data);
            $agreement_team->save();

            //create first project
            if(!$request['txtSingleProject']){
                $data =[
                    'agreement_id' => $agreement['id'],
                    'responsible_id' => $agreement['responsible_id'],
                    'name' => $agreement['name'],
                    'description' => $agreement['object'],
                    'start_date' => $request['start_date'],
                    'end_date' => $request['end_date'],
                    'value' => $agreement['value'],
                    'user_cre_id' => auth()->user()->id,
                ];
                $project = new Project($data);
                $project->save();
            }

            return response()->json(['mensaje' => 'Creado satisfactoriamente', 'id' => $agreement['id']]);
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agreement = Agreement::with([
            'employer.person',
            'contractor.person',
            'responsible.person',
            'pattern_state',
            'agreement_process_type',
            'agreement_type',
            'agreement_category',
            'agreement_coverage',
            'agreement_service',
            'agreement_nature',
            'currency',
            'department',
            'projects'
        ])->find($id);

        return response()->json($agreement);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $permission = $this->permission->full_permission($id);
        if(!$permission){
            return response()->view('errors.403', ['error' => 'No pertenece al grupo de trabajo del contrato']);
        }

        $agreement= Agreement::find($id);
        if($agreement===NULL){
            return redirect('contratos');
        }

        $roles = Role::all();
        $activity_pattern_state = PatternState::where('pattern_id',Activity::$pattern_id)->get();
        $agreement_type = AgreementType::all();
        $agreement_process_type = AgreementProcessType::all();
        $agreement_category = AgreementCategory::all();
        $agreement_nature = AgreementNature::all();
        $agreement_service = AgreementService::all();
        $agreement_coverage = AgreementCoverage::all();
        $currency = Currency::all();
        $department = Department::all();
        $agreement_pattern_state = PatternState::where('pattern_id',Agreement::$pattern_id)->get();
        $pattern_document_type = PatternDocumentType::where('pattern_id',Agreement::$pattern_id)->get();

        return view('agreement.view', [
            'pattern_id'=>Agreement::$pattern_id,
            'id'=>$id,
            'currency'=>$currency,
            'roles'=>$roles,
            'department'=>$department,
            'agreement_pattern_state'=>$agreement_pattern_state,
            'agreement_type'=>$agreement_type,
            'agreement_category'=>$agreement_category,
            'agreement_nature'=>$agreement_nature,
            'agreement_service'=>$agreement_service,
            'agreement_coverage'=>$agreement_coverage,
            'agreement_process_type'=>$agreement_process_type,
            'activity_pattern_state'=>$activity_pattern_state,
            'pattern_document_type'=>$pattern_document_type
        ]);
    }

    /*
    public function show($id)
    {
        $agreement = Agreement::find($id);
        $agreement['employers'];
        $agreement['contractors'];

        $agreement_team = AgreementTeam::where('agreement_id',$id)->get();
        for ($i=0; $i < sizeof($agreement_team); $i++) {
            $agreement_team[$i]->people;
        }
        $agreement['team']=$agreement_team;

        $agreement = json_encode([
            'agreement' => $agreement,
            'agreement_team' => $agreement_team
        ]);

        $roles = Role::all();
        $pattern_state = PatternState::all();
        $agreement_type = AgreementType::all();
        $agreement_process_type = AgreementProcessType::all();
        $agreement_category = AgreementCategory::all();
        $agreement_nature = AgreementNature::all();
        $agreement_service = AgreementService::all();
        $agreement_coverage = AgreementCoverage::all();
        $currency = Currency::all();
        $department = Department::all();

        return view('agreement.view', [
            'id'=>$id,
            'page'=>$number,
            'currency'=>$currency,
            'agreement'=>$agreement,
            'roles'=>$roles,
            'department'=>$department,
            'pattern_state'=>$pattern_state,
            'agreement_type'=>$agreement_type,
            'agreement_category'=>$agreement_category,
            'agreement_nature'=>$agreement_nature,
            'agreement_service'=>$agreement_service,
            'agreement_coverage'=>$agreement_coverage,
            'agreement_process_type'=>$agreement_process_type,
            'user' => auth()->user()->name]);

    }*/

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $data =[
            'active' => true,
            'user_mod_id' => auth()->user()->id,
        ];
        $agreement = Agreement::find($id)
                    ->update($data);

        return response()->json(['mensaje' => 'Activate']);
    }


    public function update(agreementUpdateRequest $request, $id)
    {
        if ($request->ajax()) 
        {
            $data =[
                "number" => $request['txtAgreementNumber'],
                "name" => $request['txtAgreementName'],
                "object" => $request['txtAgreementObject'],
                "employer_id" => $request['txtAgreementEmployer'],
                "contractor_id" => $request['txtAgreementContractor'],
                "responsible_id" => $request['txtAgreementResponsible'],
                "value" => $request['txtAgreementValue'],
                "currency_id" => $request['optAgreementCurrency'],
                "single_project" => $request['txtSingleProject'],
                'user_mod_id' => auth()->user()->id,
            ];      
            $agreement = Agreement::find($id);
            $agreement->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }


    public function details(agreementDetailsRequest $request, $id)
    {
        if ($request->ajax())
        {
            $data =[
                "agreement_process_type_id" => $request['optAgreementProcessType'],
                "agreement_type_id" => $request['optAgreementType'],
                "agreement_category_id" => $request['optAgreementCategory'],
                "agreement_coverage_id" => $request['optAgreementCoverage'],
                "agreement_service_id" => $request['optAgreementService'],
                "agreement_nature_id" => $request['optAgreementNature'],
                "department_id" => $request['optAgreementDepartment'],
                "start_date" => $request['txtAgreementStartDate'],
                "end_date" => $request['txtAgreementEndDate'],
                "execution_term" => $request['txtAgreementExecutionTerm'],
                "addition_term" => $request['txtAgreementAdditionTerm'],
                "addition_value" => $request['txtAgreementAdditionValue'],
                "external_url" => $request['txtAgreementExternalUrl'],
                'user_mod_id' => auth()->user()->id,
            ];
            $agreement = Agreement::find($id);
            $agreement->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }


    public function progress(agreementProgressRequest $request, $id)
    {
        if ($request->ajax())
        {
            $data =[
                "percentage" => $request['txtAgreementPercentage'],
                "pattern_state_id" => $request['optAgreementPatternState'],
                "justification" => $request['txtAgreementJustification'],
                "signing_date" => $request['txtAgreementSigningDate'],
                'user_mod_id' => auth()->user()->id,
            ];
            $agreement = Agreement::find($id);

            $agreement->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }


    public function destroy($id)
    {
        $data =[
            'active' => false,
            'user_mod_mod' => auth()->user()->id,
        ];
        $agreement = Agreement::find($id)
                    ->update($data);

        return response()->json(['mensaje' => 'Inactivo']);
    }

    public function times($agreement_id=null,$role_id=null)
    {
        $activity_estimated_times= DB::table('activity_estimated_times');

        if($agreement_id){
            $activity_estimated_times=$activity_estimated_times->where('activity_estimated_times.agreement_id',$agreement_id);
        }
        if($role_id){
            $activity_estimated_times=$activity_estimated_times->where('activity_estimated_times.role_id',$role_id);
        }

        $data=$activity_estimated_times
            ->join('roles', 'roles.id', '=', 'activity_estimated_times.role_id')
            ->select('activity_estimated_times.*', 'roles.name AS role')
            ->orderBy('roles.id','asc')
            ->get();

        return response()->json(['data' => $data]);
    }

    public function search(Request $request)
    {
        $data=[];
        $agreements = DB::table('agreements');

        if($request->has('id')){
            $agreements=$agreements->whereRaw('agreements.pattern_state_id IN ('.$request['id'].')');
        }

        if($request->has('q')) {
            $agreements = $agreements->whereRaw("CONCAT_WS('',agreements.number,' ',agreements.name,' ',agreements.object) LIKE ?",['%'.$request['q'].'%']);
        }

        $agreements = $agreements
            ->select('agreements.number','agreements.name','agreements.id AS id_label')
            ->orderBy('agreements.end_date','asc')
            ->get();

        foreach ($agreements as $result) {
            $value = $result->number.' - '.$result->name;
            $data[] = [ 'id' => $result->id_label, 'text' => $value];
        }

        if(count($data)==0){
            return response()->json(['results' => [ 'id' => '', 'text' => 'Sin resultados']]);
        }

        return response()->json(['results' => $data]);
    }

    public function conf_billing(Request $request, $id)
    {
        if ($request->ajax())
        {
            $data =[
                "iva_billing" => $request['txtIvaBilling'],
                "billing_due" => $request['txtAgrementBillingDue'],
                'user_mod_id' => auth()->user()->id,
            ];

            Agreement::find($id)->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }
}