<?php

namespace App\Http\Controllers;

use App\Models\Document;
use FontLib\Table\Type\name;
use Illuminate\Http\Request;
use Caffeinated\Shinobi\Models\Role;
use Illuminate\Support\Facades\DB;

use App\Http\Requests\documentCreateRequest;
use App\Http\Requests\documentUpdateRequest;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

class DocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('document.table');
    }

    /**
     * Show the dataTable.
     *
     * @return \Illuminate\Http\Response
     */
    public function list($pattern_id='',$model_id='')
    {
        $role = Role::find(auth()->user()->role_id);
        $edit = $role->can('document.edit');
        $destroy = $role->can('document.destroy');

        $documents = DB::table('documents');
        $documents = $documents->join('pattern_document_types','pattern_document_types.id','=','documents.pattern_document_type_id');
        $documents = $documents->join('patterns','pattern_document_types.pattern_id','=','patterns.id');
        $documents = $documents->join('pattern_names',function($join) {
            $join->on('pattern_document_types.pattern_id', '=', 'pattern_names.pattern_id')
                ->on('documents.model_id', '=', 'pattern_names.model_id');
        });
        $documents = $documents->join('users','users.id','=','documents.user_mod_id');

        if($pattern_id!='' && $model_id!=''){
            $documents = $documents
                ->where('pattern_document_types.pattern_id',$pattern_id)
                ->where('documents.model_id',$model_id);
        }

        $documents=$documents->select(
            'documents.*',
            'patterns.name AS pattern',
            'pattern_document_types.name AS pattern_document_type_name',
            'pattern_names.name AS pattern_name',
            'users.name AS user_mod_name'
        );

        $documents=$documents->get();

        $data = [];
        foreach ($documents as $document) {
            $id = $document->id;

            $document->origin = $document->pattern.' - '.$document->pattern_name;

            $document->buttons = '<div>';

            $document->buttons .= '<a download href="/document/download/'.$id.'" target="_blank" class="btn btn-sm btn-default btn-icon btn-outline btn-round" title="Descargar Documento" rol="tooltip" ><i class="fa fa-download" aria-hidden="true"></i></a>';
            $document->buttons .= '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round" onclick="versionDocument('.$id.')" title="Versiones del Documento" rol="tooltip"><i class="fa fa-clock-o" aria-hidden="true"></i></a>';

            if($edit){
                $document->buttons .= '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round" onclick="editDocument('.$id.')" title="Editar Documento" rol="tooltip" data-toggle="modal" data-target="#modDocument"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
            }

            if($destroy) {
                if ($document->active) {
                    $document->buttons .= '<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-icon btn-outline btn-round" onclick="destroyDocument('.$id.')" title="Inctivar Documento" rol="tooltip" ><i class="fa fa-close" aria-hidden="true"></i></a>';
                }else{
                    $document->buttons .= '<a href="javascript:void(0)" class="btn btn-sm btn-success btn-icon btn-outline btn-round" onclick="activateDocument('.$id.')" title="Activar Documento" rol="tooltip" ><i class="fa fa-check" aria-hidden="true"></i></a>';
                }
            }

            $document->buttons .= '</div>';
            array_push($data,$document);
        }

        return response()->json(['data' => $data]);
    }


    public function create()
    {

    }

    public function store(documentCreateRequest $request)
    {
        if ($request->ajax())
        {
            $data =[
                'model_id' => $request['txtDocumentModelId'],
                'pattern_document_type_id' => $request['txtDocumentPatternDocumentTypeId'],
                'name' => $request['txtDocumentName'],
                'keywords' => $request['txtDocumentKeywords'],
                'comment' => $request['txtDocumentComment'],
                'version' => '',
                'active' => true,
                'user_mod_id' => auth()->user()->id,
            ];

            $document = new document($data);
            $document->save();

            $new_key='document/'.$document['id'];
            $current_key=$request['txtDocumentKey'];

            //return response()->json([$new_key,$current_key]);

            $copy_response=$this->copyFile($current_key,$new_key);

            if(isset($copy_response['version'])){
                $document->update(['version'=>$copy_response['version']]);
            }else{
                $document->delete();
                return response()->json(['error' => $copy_response['error']]);
            }

            return response()->json(['mensaje' => 'Documento Cargado']);
        }
    }

    public function copyFile($from,$to){
        $s3Client = new S3Client([
            'scheme' => 'http',
            'version' => 'latest',
            'region'  => env('AWS_REGION'),
            'key'  => env('AWS_ACCESS_KEY_ID'),
            'secret'  => env('AWS_SECRET_ACCESS_KEY')
        ]);

        try {
            $result = $s3Client->copyObject([
                'Bucket'     => env('AWS_BUCKET'),
                'Key'        => $to,
                'CopySource' => env('AWS_BUCKET').'/'.$from,
            ]);
        } catch (S3Exception $e) {
            return ['error' => $e->getMessage()];
        }

        return ['version'=>$result['VersionId']];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $document = Document::find($id);
        $document->user;

        return response()->json($document);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $data =[
            'active' => true,
            'user_mod_id' => auth()->user()->id,
        ];
        $document = Document::find($id)
            ->update($data);

        return response()->json(['mensaje' => 'Activado']);
    }


    public function update(documentUpdateRequest $request, $id)
    {
        if ($request->ajax())
        {
            $document = Document::find($id);

            $data =[
                'model_id' => $request['txtDocumentModelId'],
                'pattern_document_type_id' => $request['txtDocumentPatternDocumentTypeId'],
                'name' => $request['txtDocumentName'],
                'keywords' => $request['txtDocumentKeywords'],
                'comment' => $request['txtDocumentComment'],
                'active' => true,
                'user_mod_id' => auth()->user()->id,
            ];

            $new_key='document/'.$document['id'];
            $current_key=$request['txtDocumentKey'];

            //return response()->json([$new_key,$current_key]);

            if($current_key!=$new_key) {

                $copy_response = $this->copyFile($current_key, $new_key);

                if (isset($copy_response['version'])) {
                    $data['version'] = $copy_response['version'];
                } else {
                    return response()->json(['error' => $copy_response['error']]);
                }

            }

            $document->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }


    public function download($id,$version='',$name='')
    {
        $document = Document::find($id);
        if($version==''){
            $version=$document->version;
        }
        if($name==''){
            $name=$document->name;
        }

        //return response()->json(['version'=>$version,'name'=>$name]);
        ///dw.7g6pQcVvjAFf2pGMsEIAD5EZ957Th/Contrato_V2.doc

        $s3Client = new S3Client([
            'version' => 'latest',
            'region'  => env('AWS_REGION'),
            'key'  => env('AWS_ACCESS_KEY_ID'),
            'secret'  => env('AWS_SECRET_ACCESS_KEY'),
            'http' => [ 'verify' => false ]
        ]);

        $cmd = $s3Client->getCommand('GetObject', [
            'Bucket'  => env('AWS_BUCKET'),
            'Key'     => 'document/'.$document['id'],
            'Version' => $version,
            'ResponseContentDisposition' => 'attachment; filename='.$name,
        ]);

        $request = $s3Client->createPresignedRequest($cmd, '+30 minutes');

        $preSignedUrl = (string) $request->getUri();

        header("Location: ".$preSignedUrl);
        exit;
        /*try {
            $result = $s3Client->getObject([
                'Bucket'     => env('AWS_BUCKET'),
                'Key'        => 'document/'.$document['id'],
                'Version'    => $document['version']
            ]);

            header("Content-Type: {$result['ContentType']}");
            header("Content-Disposition: attachment; filename=\"{$document['name']}\"");
            echo $result['Body'];
        } catch (S3Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }*/
    }

    public function destroy($id)
    {
        $data = [
            'active' => false,
            'user_mod_mod' => auth()->user()->id,
        ];
        $document = Document::find($id)
            ->update($data);

        return response()->json(['mensaje' => 'Inactivo']);
    }

    public function versions($id)
    {
        $document = Document::find($id);
        $current_name = $document->name;
        $current_keywords = $document->keywords;

        $document_changes = DB::table('audit_documents')
            ->join('users','users.id','=','audit_documents.user_cre_id')
            ->where('audit_documents.id_parent',$id)
            ->orderBy('audit_documents.created_at','desc')
            ->orderBy('audit_documents.field','desc')
            ->select('audit_documents.*','users.name as user_name')
            ->get();
        ;

        $data=[];
        foreach ( $document_changes as $document_change ){
            if($document_change->field=='name'){
                $current_name = $document_change->old_value;
            }

            if($document_change->field=='keywords'){
                $current_keywords = $document_change->old_value;
            }

            if($document_change->field=='version'){
                $current_version = $document_change->new_value;
                $data[]=[
                    'name' => $current_name,
                    'keywords' => $current_keywords,
                    'version' => $current_version,
                    'user_name' => $document_change->user_name,
                    'date' => $document_change->created_at
                ];
            }
        }

        return response()->json(['data' => $data]);
    }

    public function preSignedURL(){
        $s3Client = new S3Client([
            'version' => 'latest',
            'region'  => env('AWS_REGION'),
            'key'  => env('AWS_ACCESS_KEY_ID'),
            'secret'  => env('AWS_SECRET_ACCESS_KEY'),
            'http' => [ 'verify' => false ]
        ]);

        $document_key='tmp/'.uniqid("",true);

        try {
            $cmd = $s3Client->getCommand('PutObject', [
                'Bucket'  => env('AWS_BUCKET'),
                'Key'    => $document_key
            ]);

            $request = $s3Client->createPresignedRequest($cmd, '+30 minutes');

            $preSignedUrl = (string) $request->getUri();
        } catch (S3Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        return response()->json(['url' => $preSignedUrl, 'key' => $document_key]);
    }
}
