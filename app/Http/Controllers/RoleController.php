<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pattern;
use \Caffeinated\Shinobi\Models\Role as Role;
use Caffeinated\Shinobi\Models\Permission;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$permissions = Permission::all();

    	$arr_permission = [];
    	foreach($permissions as $permission){
    	    $model = explode('.',$permission->slug);
            $model = explode('_',$model[0]);
            $pattern = Pattern::where('model',$model[0])->get();
            $model = (isset($pattern[0]->name) ? $pattern[0]->name : $model[0]);

    	    if(!isset($arr_permission[$model])){
    	        $arr_permission[$model] = [];
            }

            array_push($arr_permission[$model] , $permission);
        }

        return view('role.table', [
            'permissions' => $arr_permission
        ]);
    }

    /**
     * Show the dataTable.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $roles = Role::all();
        $role = \Caffeinated\Shinobi\Models\Role::find(auth()->user()->role_id);
        $edit = $role->can('role.edit');
        $destroy = $role->can('role.destroy');

        $data = [];
        foreach ($roles as $role) {
            $id = $role['id'];

            $role['state'] = ($role['active'] ? 'ACTIVO' : 'INACTIVO');

            $role['buttons'] = '<div>';

            if($edit){
                $role['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round" onclick="editRole('.$id.')" title="Editar Rol" rol="tooltip" ><i class="fa fa-cogs" aria-hidden="true"></i></a>';
            }

            if($destroy) {
                if ($role['active']) {
                    $role['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-icon btn-outline btn-round" onclick="destroyRole('.$id.')" title="Inctivar Rol" rol="tooltip" ><i class="fa fa-close" aria-hidden="true"></i></a>';
                }else{
                    $role['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-success btn-icon btn-outline btn-round" onclick="activateRole('.$id.')" title="Activar Rol" rol="tooltip" ><i class="fa fa-check" aria-hidden="true"></i></a>';
                }
            }
            
            
            array_push($data,$role);
        }

        return response()->json(['data' => $data]);
    }
    public function store(conceptCreateRequest $request)
    {
        if ($request->ajax()) 
        {
            $data =[
                'nombre' => $request['txtNombre'],
                'description' => $request['txtDescripcion'],
                'event_type_id' => $request['optEventType'],
                'service' => (isset($request['chkService']) ? true : false),
                'active' => true,
                'user_cre_id' => auth()->user()->id,
            ];
            $concept = new concept($data);
            $concept->save();

            return response()->json(['mensaje' => 'creado']);
        }
    }

    public function show($id)
    {
        $role = Role::find($id);
        $role->getPermissions();
        return response()->json($role);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $data =[
            'active' => true,
            'user_mod_id' => auth()->user()->id,
        ];
        $role = $role::find($id)
            ->update($data);

        return response()->json(['mensaje' => 'Activado']);
    }

    public function update(Request $request, $id)
    {
        if ($request->ajax()) 
        {
        	$role = Role::find($id);        	        	
    		$role->revokeAllPermissions();
    		$role->save();
        	
        	if($request['chkPermission']){
	        	$role = Role::find($id);
	        	foreach ($request['chkPermission'] as $value) {
					$role->assignPermission($value);
	        	}
	        	$role->save();
        	}
            
            return response()->json(['mensaje' => 'Modificado']);
        }
    }
}
