<?php

namespace App\Http\Controllers;

use App\Http\Requests\activityEstimatedCostCreateRequest;
use App\Models\ActivityEstimatedCost;

class ActivityEstimatedCostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(activityEstimatedCostCreateRequest $request)
    {
        if ($request->ajax()) {
            ActivityEstimatedCost::updateOrCreate(
                [
                    'activity_id' => $request['txtActivityEstimatedCostActivityId'],
                    'activity_cost_category_id' => $request['optActivityExecutedCostActivityCostCategoryId'],
                ],
                [
                    'estimated' => $request['txtActivityEstimatedCostEstimated'],
                    'justification' => $request['txtActivityEstimatedCostJustification'],
                    'user_mod_id' => auth()->user()->id,
                ]
            );

            return response()->json(['mensaje' => 'Creado']);
        }
    }

    public function show($id)
    {
        $activity_estimated_cost = ActivityEstimatedCost::find($id)->with(['activity','activity_cost_type'])->get();

        return response()->json(['data' => $activity_estimated_cost]);
    }
}