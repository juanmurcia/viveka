<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User as User;
use Caffeinated\Shinobi\Models\Role;
use App\Http\Requests\userCreateRequest;
use App\Http\Requests\userUpdateRequest;
use App\Http\Requests\userPasswordRequest;
use DataTables;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Role::all();
        return view('user.table', ['roles' => $role]);//, 'user' => auth()->user()->name
    }

    /**
     * Show the dataTable.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        if ($request->ajax()) {
            $role = Role::find(auth()->user()->role_id);
            $users = User::with(['roles'])->select('users.*');

            return Datatables::eloquent($users)
                ->editColumn('role_id', function ($user){
                    return $user->roles[0]->name;
                })
                ->addColumn('state', function ($user){
                    return  ($user->active ? 'ACTIVO' : 'INACTIVO');
                })
                ->addColumn('buttons', function ($user) use ($role) {
                    $button = '<div class="row">';

                    if ($role->can('user.edit')) {
                        $button .= '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round" onclick="editUser('.$user->id.')" title="Editar Usuario" rol="tooltip" ><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                    }

                    if ($role->can('user.destroy')) {
                        if ($user->active) {
                            $button.= '<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-icon btn-outline btn-round" onclick="destroyUser(' . $user->id. ')" title="Inctivar Usuario" rol="tooltip" ><i class="fa fa-close" aria-hidden="true"></i></a>';
                        } else {
                            $button.= '<a href="javascript:void(0)" class="btn btn-sm btn-success btn-icon btn-outline btn-round" onclick="activateUser(' . $user->id. ')" title="Activar Usuario" rol="tooltip" ><i class="fa fa-check" aria-hidden="true"></i></a>';
                        }
                    }

                    $button .= '</div>';
                    return $button;
                })
                ->rawColumns(['buttons'])
                ->setRowId('id')
                ->make(true);
        }

    }


    public function create()
    {
        //
    }

    public function store(userCreateRequest $request)
    {
        if ($request->ajax()) {
            $data = [
                'name' => $request['txtNomUser'],
                'password' => bcrypt($request['txtPass']),
                'email' => $request['txtEmailUser'],
                'person_id' => $request['txtIdUser'],
                'role_id' => $request['optRole'],
                'active' => true,
                'user_cre_id' => auth()->user()->id,
            ];
            $user = new user($data);
            $user->save();

            return response()->json(['mensaje' => 'creado']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = user::find($id);
        return response()->json($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $data = [
            'active' => true,
            'password' => bcrypt('viveka2018'),
            'user_mod_id' => auth()->user()->id,
        ];
        $user = user::find($id)
            ->update($data);

        return response()->json(['mensaje' => 'Activado']);
    }

    public function update(userUpdateRequest $request, $id)
    {
        if ($request->ajax()) {
            $data = [
                'role_id' => $request['optRole'],
                'user_mod_id' => auth()->user()->id,
            ];
            user::find($id)->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }

    public function changePassword(userPasswordRequest $request)
    {
        if ($request->ajax()) {
            $data = [
                'password' => bcrypt($request['txtPassUserConf']),
                'user_mod_id' => auth()->user()->id,
            ];
            user::find(auth()->user()->id)->update($data);

            return response()->json(['mensaje' => 'Modificado', 'success' => true]);
        }
    }


    public function destroy($id)
    {
        $data = [
            'active' => false,
            'password' => "1",
            'user_mod_id' => auth()->user()->id,
        ];
        user::find($id)->update($data);

        return response()->json(['mensaje' => 'Inactivo']);
    }
}