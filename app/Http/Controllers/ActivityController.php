<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Agreement;
use App\Models\PatternState;
use App\Models\Pattern;
use App\Models\PatternDocumentType;
use Caffeinated\Shinobi\Models\Role;
use Illuminate\Http\Request;
use App\Models\ActivityEstimatedTime;
use App\Models\ActivityExecutedTime;
use App\Models\ActivityEstimatedCost;
use App\Models\ActivityExecutedCost;
use App\Models\ActivityCostCategory;
use Illuminate\Support\Facades\DB;


use App\Http\Requests\activityCreateRequest;
use App\Http\Requests\activityUpdateRequest;
use App\Http\Requests\activityProgressRequest;

class ActivityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $activity_pattern_state = PatternState::where('pattern_id',4)->get();
        $patterns = Pattern::all();
        $agreements = Agreement::where('active',1)->get();

        return view('activity.table',[
            'agreements'=>$agreements,
            'activity_pattern_state'=>$activity_pattern_state,
            'patterns'=>$patterns
        ]);
    }

    /**
     * Show the dataTable.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $pattern_id='',$model_id='')
    {

        if($pattern_id!='' && $model_id!=''){
            $activities = Activity::where('activities.pattern_id',$pattern_id)
                                    ->where('activities.model_id',$model_id)
                                    ->where('activities.active',true)
                                    ->with(['pattern','pattern_state']);
        }else {
            $activities = Activity::with(['pattern','pattern_state']);
        }

        if ($request['optActivityPatternState']!='') {
            $activities=$activities->where('activities.pattern_state_id',$request['optActivityPatternState']);
        }

        if ($request['txtActivityName']!='') {
            $activities=$activities->where('activities.name','like','%'.$request['txtActivityName'].'%');
        }

        if ($request['optActivityPattern']!='') {
            $activities=$activities->where('activities.pattern_id',$request['optActivityPattern']);
        }

        if ($request['txtActivityModel']!='') {
            $activity_model=explode(',',$request['txtActivityModel']);
            $model_activities = PatternController::activities($activity_model[0],$activity_model[1]);
            $activities=$activities->whereIn('activities.id',$model_activities);
        }

        if ($request['txtActivityStartDateFrom']!='') {
            $activities=$activities->where('activities.start_date','>=',$request['txtActivityStartDateFrom'].' 00:00:00');
        }

        if ($request['txtActivityStartDateTo']!='') {
            $activities=$activities->where('activities.start_date','<=',$request['txtActivityStartDateTo'].' 23:59:59');
        }

        if ($request['txtActivityEndDateFrom']!='') {
            $activities=$activities->where('activities.end_date','>=',$request['txtActivityEndDateFrom'].' 00:00:00');
        }

        if ($request['txtActivityEndDateTo']!='') {
            $activities=$activities->where('activities.end_date','<=',$request['txtActivityEndDateTo'].' 23:59:59');
        }

        $activities = $activities->get();

        $data = [];

        foreach ($activities as $activity) {
            $id = $activity['id'];

            $activity['name'] = '<a href="/actividad/' . $id . '" title="Ver Actividad" rol="tooltip" >'.$activity['name'].'</a>';

            $activity['buttons'] = '<div>';
            $activity['buttons'] .= '<a onclick="createActivityExecutedTime('.$id.');" class="btn btn-sm btn-default btn-icon btn-outline btn-round" title="Cargar Horas" rol="tooltip" ><i class="fa fa-clock-o" aria-hidden="true"></i></a>';
            $activity['buttons'] .= '<a onclick="createActivityExecutedCost('.$id.');" class="btn btn-sm btn-default btn-icon btn-outline btn-round" title="Cargar Costo" rol="tooltip" ><i class="fa fa-usd" aria-hidden="true"></i></a>';
            $activity['buttons'] .= '</div>';
            array_push($data,$activity);
        }

        return response()->json(['data' => $data]);
    }

    public function create()
    {
        
    }

    public function store(activityCreateRequest $request)
    {
        if ($request->ajax()) 
        {
            $data =[
                "pattern_id" => $request['txtActivityPatternId'],
                "model_id" => $request['txtActivityModelId'],
                "start_date" => $request['txtActivityStartDate'],
                "end_date" => $request['txtActivityEndDate'],
                "name" => $request['txtActivityName'],
                "pattern_state_id" => 41,
                "description" => $request['txtActivityDescription'],
                'user_cre_id' => auth()->user()->id,
            ];
            $activity = new activity($data);
            $activity->save();

            return response()->json(['mensaje' => 'creado']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $activity = Activity::with('pattern')->find($id);
        $activity->parent_url = route($activity->pattern->model.'.view',[$activity->model_id]);
        $activity->parent_name = DB::table(str_plural($activity->pattern->model))->where('id',$activity->model_id)->pluck('name')[0];

        return response()->json($activity);
    }

    public function view($id)
    {
        $activity = Activity::find($id);
        if($activity===NULL){
            return redirect('actividades');
        }

        $activity_pattern_state = PatternState::where('pattern_id',Activity::$pattern_id)->get();
        $pattern_document_type = PatternDocumentType::where('pattern_id',Activity::$pattern_id)
            ->orWhere('pattern_id',ActivityExecutedCost::$pattern_id)
            ->get();


        return view('activity.view', [
            'pattern_id'=>Activity::$pattern_id,
            'id'=>$id,
            'activity_pattern_id'=>$activity->pattern_id,
            'activity_model_id'=>$activity->model_id,
            'activity_pattern_state'=>$activity_pattern_state,
            'pattern_document_type'=>$pattern_document_type,
        ]);
    }
    
    
    public function activate($id)
    {
        $data =[
            'active' => true,
            'user_mod_id' => auth()->user()->id,
        ];
        Activity::find($id)
                    ->update($data);

        return response()->json(['mensaje' => 'Activado']);
    }


    public function update(activityUpdateRequest $request, $id)
    {
        if ($request->ajax()) 
        {
            $data =[
                "start_date" => $request['txtActivityStartDate'],
                "end_date" => $request['txtActivityEndDate'],
                "name" => $request['txtActivityName'],
                "description" => $request['txtActivityDescription'],
                'user_mod_id' => auth()->user()->id,
            ];      
            $activity = Activity::find($id);
            $activity->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }

    public function progress(activityProgressRequest $request, $id)
    {
        if ($request->ajax())
        {
            $data =[
                "percentage" => $request['txtActivityPercentage'],
                "pattern_state_id" => $request['optActivityPatternState'],
                "justification" => $request['txtActivityJustification'],
                'user_mod_id' => auth()->user()->id,
            ];
            $activity = Activity::find($id);

            $activity->update($data);

            return response()->json(['mensaje' => 'Modificada']);
        }
    }


    public function destroy($id)
    {
        $data =[
            'active' => false,
            'user_mod_id' => auth()->user()->id,
        ];
        Activity::find($id)->update($data);

        return response()->json(['mensaje' => 'Inactivo']);
    }

    public function search(Request $request)
    {
        $data=[];

        $activities=DB::table('activities')
            ->join('patterns','patterns.id','=','activities.pattern_id')
            ->join('pattern_names',function($join) {
                $join->on('activities.pattern_id', '=', 'pattern_names.pattern_id')
                    ->on('activities.model_id', '=', 'pattern_names.model_id');
            })
            ->whereIn('activities.pattern_state_id',[41,42])
        ;

        $results=$activities->whereRaw("CONCAT_WS('',activities.name,' - ',patterns.name,' - ',pattern_names.name) LIKE ?",['%'.$request['search'].'%'])
            ->select(
                'activities.id AS activity_id',
                'activities.name AS activity_name',
                'patterns.name AS pattern',
                'pattern_names.name AS pattern_name')
            ->orderBy('activities.end_date','asc')
            ->get();

        foreach ($results as $result) {
            $value = $result->activity_name." - ".$result->pattern.' - '.$result->pattern_name;
            $data[] = [ 'id' => $result->activity_id, 'value' => $value];
        }

        if(count($data)==0){
            $data[]= [ 'id' => '', 'value' => 'No se encontró'];
        }

        return response()->json($data);
    }

    public function times($id){
        $roles = Role::all();
        $activity_estimated_times = ActivityEstimatedTime::where('active',true)
            ->where('activity_id',$id)
            ->groupBy('role_id')
            ->selectRaw('role_id, SUM(estimated) as estimated')
            ->get();

        $activity_executed_times = ActivityExecutedTime::where('active',true)
            ->where('activity_id',$id)
            ->groupBy('role_id')
            ->selectRaw('role_id, SUM(executed) as executed')
            ->get();

        $data=[];
        foreach ($roles as $role){
            $data[$role['id']]=[
                'role'=>$role['name'],
                'role_id'=>$role['id'],
                'estimated'=>0,
                'executed'=>0
            ];
        }

        foreach ($activity_estimated_times as $activity_estimated_time){
            $data[$activity_estimated_time['role_id']]['estimated']=$activity_estimated_time['estimated'];
        }

        foreach ($activity_executed_times as $activity_executed_time){
            $data[$activity_executed_time['role_id']]['executed']=$activity_executed_time['executed'];
        }

        return response()->json(['data' => $data]);
    }

    public function costs($id){
        $activity_cost_categories = ActivityCostCategory::where('active', true)->get();
        $activity_estimated_costs = ActivityEstimatedCost::where('active',true)
            ->where('activity_id',$id)
            ->groupBy('activity_cost_category_id')
            ->selectRaw('activity_cost_category_id, SUM(estimated) as estimated')
            ->get();

        $activity_executed_costs = ActivityExecutedCost::where('activity_executed_costs.active',true)
            ->where('activity_executed_costs.activity_id',$id)
            ->join('activity_cost_subcategories','activity_cost_subcategories.id','=','activity_executed_costs.activity_cost_subcategory_id')
            ->join('activity_cost_categories','activity_cost_categories.id','=','activity_cost_subcategories.activity_cost_category_id')
            ->groupBy('activity_cost_categories.id')
            ->selectRaw('activity_cost_categories.id AS activity_cost_category_id, SUM(executed) as executed')
            ->get();

        $data=[];
        foreach ($activity_cost_categories as $activity_cost_category){
            $data[$activity_cost_category['id']]=[
                'activity_cost_category'=>$activity_cost_category['name'],
                'activity_cost_category_id'=>$activity_cost_category['id'],
                'estimated'=>0,
                'executed'=>0
            ];
        }

        foreach ($activity_estimated_costs as $activity_estimated_cost){
            $data[$activity_estimated_cost['activity_cost_category_id']]['estimated']=$activity_estimated_cost['estimated'];
        }

        foreach ($activity_executed_costs as $activity_executed_cost){
            $data[$activity_executed_cost['activity_cost_category_id']]['executed']=$activity_executed_cost['executed'];
        }

        return response()->json(['data' => $data]);
    }

    public function executed_times($id)
    {
        $activity_executed_times = ActivityExecutedTime::where('activity_id',$id)->with(['person','role'])->get();

        $role = Role::find(auth()->user()->role_id);
        $edit = $role->can('activity_executed_time.edit');
        $destroy = $role->can('activity_executed_time.destroy');
        $data = [];

        foreach ($activity_executed_times as $activity_executed_time) {
            $activity_executed_time['person_name']=$activity_executed_time['person']['name'].' '.$activity_executed_time['person']['last_name'];
            $activity_executed_time['buttons'] = '<div>';

            if($edit){
                $activity_executed_time['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round" onclick="editActivityExecutedTime('.$activity_executed_time['id'].')" title="Editar Horas Ejecutadas" rol="tooltip" data-toggle="modal" data-target="#modActivityExecutedTime"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
            }

            if($destroy) {
                if ($activity_executed_time['active']) {
                    $activity_executed_time['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-icon btn-outline btn-round" onclick="destroyActivityExecutedTime('.$activity_executed_time['id'].')" title="Inctivar Horas Ejecutadas" rol="tooltip" ><i class="fa fa-close" aria-hidden="true"></i></a>';
                }else{
                    $activity_executed_time['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-success btn-icon btn-outline btn-round" onclick="activateActivityExecutedTime('.$activity_executed_time['id'].')" title="Activar Horas Ejecutadas" rol="tooltip" ><i class="fa fa-check" aria-hidden="true"></i></a>';
                }
            }

            $activity_executed_time['buttons'] .= '</div>';
            array_push($data,$activity_executed_time);
        }

        return response()->json(['data' => $data]);
    }

    public function executed_costs($id)
    {
        $activity_executed_costs = ActivityExecutedCost::where('activity_id',$id)->with(['person','activity_cost_subcategory','activity_cost_subcategory.activity_cost_category'])->get();

        $role = Role::find(auth()->user()->role_id);
        $edit = $role->can('activity_executed_cost.edit');
        $destroy = $role->can('activity_executed_cost.destroy');
        $data = [];

        foreach ($activity_executed_costs as $activity_executed_cost) {
            $activity_executed_cost['person_name']=$activity_executed_cost['person']['name'].' '.$activity_executed_cost['person']['last_name'];
            $activity_executed_cost['buttons'] = '<div>';

            if($edit){
                $activity_executed_cost['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round" onclick="editActivityExecutedCost('.$activity_executed_cost['id'].')" title="Editar Horas Ejecutadas" rol="tooltip" data-toggle="modal" data-target="#modActivityExecutedCost"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                $activity_executed_cost['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-warning btn-icon btn-outline btn-round" onclick="documentActivityExecutedCost('.$activity_executed_cost['id'].')" title="Documentos Horas Ejecutadas" rol="tooltip" data-toggle="modal" data-target="#modDocActivityExecutedCost"><i class="fa fa-files-o" aria-hidden="true"></i></a>';
            }

            if($destroy) {
                if ($activity_executed_cost['active']) {
                    $activity_executed_cost['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-icon btn-outline btn-round" onclick="destroyActivityExecutedCost('.$activity_executed_cost['id'].')" title="Inctivar Horas Ejecutadas" rol="tooltip" ><i class="fa fa-close" aria-hidden="true"></i></a>';
                }else{
                    $activity_executed_cost['buttons'] .= '<a href="javascript:void(0)" class="btn btn-sm btn-success btn-icon btn-outline btn-round" onclick="activateActivityExecutedCost('.$activity_executed_cost['id'].')" title="Activar Horas Ejecutadas" rol="tooltip" ><i class="fa fa-check" aria-hidden="true"></i></a>';
                }
            }

            $activity_executed_cost['buttons'] .= '</div>';
            array_push($data,$activity_executed_cost);
        }

        return response()->json(['data' => $data]);
    }


    public function role_times($pattern_id,$model_id)
    {
        $data_result = [
            'pattern_id' => $pattern_id,
            'model_id' => $model_id
        ];

        $data_real_progress = [
            'id' => $model_id
        ];

        $date_limits = [];

        switch ($pattern_id) {
            case 1:
                $model = Agreement::find($model_id);
                $audit = 'audit_agreements';
                break;
            case 2:
                $model = Project::find($model_id);
                $audit = 'audit_projects';
                break;
            case 3:
                $model = Product::find($model_id);
                $audit = 'audit_products';
                break;
            default:
                return response()->json(['error', 'pattern_id '.$pattern_id.'not supported']);
                break;
        }

        $roles = Role::all();

        /*
                $executed = DB::select("SELECT
                      ATS.role_id, AA.old_value,AA.new_value, CAST(AA.created_at AS DATE) AS audit_date
                FROM `audit_activity_estimated_times` AS AA
                LEFT JOIN `audit_activity_estimated_times` AS OO ON AA.id_parent = OO.id_parent AND AA.field = OO.field AND CAST(AA.created_at AS DATE) = CAST(OO.created_at AS DATE) AND AA.created_at < OO.created_at
                INNER JOIN activity_estimated_times AS ATS ON ATS.id = AA.id_parent
                WHERE ATS.pattern_id = :pattern_id AND ATS.model_id = :model_id AND AA.field IN ('executed') AND OO.audit_id IS NULL
                ORDER BY AA.created_at ASC ", $data_result);

                $estimated = DB::select("SELECT
                      ATS.role_id, AA.old_value,AA.new_value, CAST(AA.created_at AS DATE) AS audit_date
                FROM `audit_activity_estimated_times` AS AA
                LEFT JOIN `audit_activity_estimated_times` AS OO ON AA.id_parent = OO.id_parent AND AA.field = OO.field AND CAST(AA.created_at AS DATE) = CAST(OO.created_at AS DATE) AND AA.created_at < OO.created_at
                INNER JOIN activity_estimated_times AS ATS ON ATS.id = AA.id_parent
                WHERE ATS.pattern_id = :pattern_id AND ATS.model_id = :model_id AND AA.field IN ('estimated') AND OO.audit_id IS NULL
                ORDER BY AA.created_at ASC ", $data_result);

                $real_progress = DB::select("SELECT
                      AA.id_parent, AA.old_value,AA.new_value, CAST(AA.created_at AS DATE) AS audit_date
                FROM `" . $audit . "` AS AA
                LEFT JOIN `" . $audit . "` AS OO ON AA.id_parent = OO.id_parent AND AA.field = OO.field AND CAST(AA.created_at AS DATE) = CAST(OO.created_at AS DATE) AND AA.created_at < OO.created_at
                WHERE AA.id_parent = :id AND AA.field IN ('percentage') AND OO.audit_id IS NULL
                ORDER BY AA.created_at ASC ", $data_real_progress);
        */
        $executed = [];

        $estimated = [];

        $real_progress = [];


        //get and save limit dates
        if (sizeof($executed) > 0) {
            $date_limits[] = $executed[0]->audit_date;
        }

        if (sizeof($estimated) > 0) {
            $date_limits[] = $estimated[0]->audit_date;
        }

        if (sizeof($real_progress) > 0) {
            $date_limits[] = $real_progress[0]->audit_date;
        }

        if (!empty($model->start_date)) {
            $date_limits[] = $model->start_date;
        }

        //clears any invalid date
        foreach ($date_limits as &$date_limit) {
            if (empty($date_limit)) {
                unset($date_limit);
            }
        }
        unset($date_limit);

        $min_date = min($date_limits);
        $first_date = date_create($min_date);

        $complete_table_data = [];

        //fill table based on $min_date value;
        foreach ($executed as $row) {
            $indexed_date = intval($first_date->diff(date_create($row->audit_date))->format('%a'));
            $complete_table_data[$indexed_date]['role_' . $row->role_id . '_executed'] = $row->new_value;
        }

        foreach ($estimated as $row) {
            $indexed_date = intval($first_date->diff(date_create($row->audit_date))->format('%a'));
            $complete_table_data[$indexed_date]['role_' . $row->role_id . '_estimated'] = $row->new_value;
        }

        foreach ($real_progress as $row) {
            $indexed_date = intval($first_date->diff(date_create($row->audit_date))->format('%a'));
            $complete_table_data[$indexed_date]['real_progress'] = $row->new_value;
        }

        if(count($complete_table_data)==0){
            return $complete_table_data;
        }
        //calculate percentages and fill missing data
        $max_date_index = max(array_keys($complete_table_data));
        $current_values = [];
        $current_values['real_progress'] = 0.0;
        $current_values['estimated_progress'] = 0.0;
        foreach ($roles as $role) {
            $current_values['role_' . $role->id . '_executed'] = 0.0;
            $current_values['role_' . $role->id . '_estimated'] = 0.0;
        }

        for ($i = 0; $i <= $max_date_index; $i++) {
            $current_date = clone $first_date;
            $current_values['date']=$current_date->add(new \DateInterval('P'.$i.'D'))->format('Y-m-d');
            if (isset($complete_table_data[$i]['real_progress'])) {
                $current_values['real_progress'] = $complete_table_data[$i]['real_progress'];
            }

            $total_executed = 0.0;
            $total_estimated = 0.0;
            foreach ($roles as $role) {
                if (isset($complete_table_data[$i]['role_' . $role->id . '_executed'])) {
                    $current_values['role_' . $role->id . '_executed'] = $complete_table_data[$i]['role_' . $role->id . '_executed'];
                }
                $total_executed += $current_values['role_' . $role->id . '_executed'];

                if (isset($complete_table_data[$i]['role_' . $role->id . '_estimated'])) {
                    $current_values['role_' . $role->id . '_estimated'] = $complete_table_data[$i]['role_' . $role->id . '_estimated'];
                }
                $total_estimated += $current_values['role_' . $role->id . '_estimated'];
            }

            $current_values['executed_hours'] = $total_executed;
            $current_values['estimated_hours'] = $total_estimated;
            $current_values['progress_hours'] = $total_estimated > 0 ? round (100 * $total_executed / $total_estimated , 2) : 0.0;

            if (!empty($model->start_date) && !empty($model->end_date)) {
                $start_date_index = intval($first_date->diff(date_create($model->start_date))->format('%a'));
                $end_date_index = intval($first_date->diff(date_create($model->end_date))->format('%a'));
                $date_range = $end_date_index - $start_date_index;

                if ($i > $end_date_index || $date_range == 0) {
                    $current_values['estimated_progress'] = 100.0;
                } else {
                    $current_values['estimated_progress'] = round(100 * ($i - $start_date_index) / $date_range ,2);
                }
            }

            $complete_table_data[$i] = $current_values;
        }
        ksort($complete_table_data);

        //return response()->json($complete_table_data);
        return $complete_table_data;
    }

    public function role_times_excel($pattern_id, $model_id){
        $roles = Role::all();
        $complete_table_data=$this->role_times($pattern_id,$model_id);
        $xml = '<?xml version="1.0"?>
                <ss:Workbook xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">
                    <ss:Styles>
                        <ss:Style ss:ID="1">
                            <ss:Font ss:Bold="1"/>
                        </ss:Style>
                    </ss:Styles>
                    <ss:Worksheet ss:Name="Sheet1">
                        <ss:Table>
                            <ss:Column ss:Width="80"/>
                            <ss:Column ss:Width="80"/>
                            <ss:Column ss:Width="80"/>
                            <ss:Row ss:StyleID="1">
                                <ss:Cell ss:MergeDown="1">
                                    <ss:Data ss:Type="String">Fecha</ss:Data>
                                </ss:Cell>
                                <ss:Cell ss:MergeDown="1">
                                    <ss:Data ss:Type="String">Real</ss:Data>
                                </ss:Cell>
                                <ss:Cell ss:MergeDown="1">
                                    <ss:Data ss:Type="String">Esperado</ss:Data>
                                </ss:Cell>
                                <ss:Cell ss:MergeDown="1">
                                    <ss:Data ss:Type="String">Horas Ejecutadas</ss:Data>
                                </ss:Cell>
                                ';
        foreach ($roles as $role) {
            $xml.= '
                                    <ss:Cell ss:MergeAcross="1">
                                        <ss:Data ss:Type="String">'.$role->name.'</ss:Data>
                                    </ss:Cell>
                                    ';
        }
        $xml .='
                            </ss:Row>
                            <ss:Row ss:StyleID="1">
                                ';
        $offset=4;
        foreach ($roles as $role) {
            $offset++;
            $xml.= '
                                    <ss:Cell ss:Index="'.$offset++.'">
                                        <ss:Data ss:Type="String">Presupuestadas</ss:Data>
                                    </ss:Cell>
                                    <ss:Cell ss:Index="'.$offset.'">
                                        <ss:Data ss:Type="String">Ejecutadas</ss:Data>
                                    </ss:Cell>   
                                    ';
        }
        $xml .='
                            </ss:Row>
                            ';

        foreach ($complete_table_data as $row){
            $xml .='
                                    <ss:Row>
                                        <ss:Cell>
                                            <ss:Data ss:Type="String">'.$row['date'].'</ss:Data>
                                        </ss:Cell>
                                        <ss:Cell>
                                            <ss:Data ss:Type="Number">'.$row['real_progress'].'</ss:Data>
                                        </ss:Cell>
                                        <ss:Cell>
                                            <ss:Data ss:Type="Number">'.$row['estimated_progress'].'</ss:Data>
                                        </ss:Cell>
                                        <ss:Cell>
                                            <ss:Data ss:Type="Number">'.$row['progress_hours'].'</ss:Data>
                                        </ss:Cell>
                                        ';
            foreach ($roles as $role) {
                $xml.= '
                                            <ss:Cell>
                                                <ss:Data ss:Type="Number">'.$row['role_' . $role->id . '_estimated'].'</ss:Data>
                                            </ss:Cell>
                                            <ss:Cell>
                                                <ss:Data ss:Type="Number">'.$row['role_' . $role->id . '_executed'].'</ss:Data>
                                            </ss:Cell>
                                            ';
            }
            $xml .='
                                    </ss:Row>
                                ';
        }
        $xml .='
                        </ss:Table>
                    </ss:Worksheet>
                </ss:Workbook>';


        header("Content-Type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=datos.xls");

        return $xml;
    }

    public function graphic_data(Request $request)
    {

        $pattern_id=$request['txtRoleTimePattern'];
        $model_id=$request['txtRoleTimeModel'];
        $from=$request['txtRoleTimeDateFrom'];
        $to=$request['txtRoleTimeDateTo'];
        $roles = Role::all();
        $complete_table_data=$this->role_times($pattern_id,$model_id);

        if(count($complete_table_data)==0){
            return response()->json(['message'=>'No hay datos para graficar'],419);
        }
        //return response()->json($complete_table_data);
        $dates=array_column($complete_table_data,'date');
        $from_index=array_search($from,$dates);
        $to_index=array_search($to,$dates);
        if($from_index===false){
            $from_index=0;
        }
        if($to_index===false){
            $to_index=count($dates);
        }
        $length=$to_index-$from_index;

        $complete_table_data=array_slice($complete_table_data,$from_index,$length);

        $series_role_graph = [];

        //roles
        foreach ($roles as $role) {
            $series_role_graph[] = [
                'name' => $role->name,
                'type' => 'line',
                'smooth' => true,
                'itemStyle' => [
                    'normal' => [
                        'areaStyle' => [
                            'type' => 'default'
                        ]
                    ]
                ],
                'data' => array_column($complete_table_data,'role_' . $role->id . '_executed')
            ];
        }

        $series_progress_graph=[];

        $series_progress_graph[0] = [
            'name' => 'Horas',
            'type' => 'line',
            'smooth' => true,
            'data' => array_column($complete_table_data,'progress_hours')
        ];

        $series_progress_graph[1] = [
            'name' => 'Real',
            'type' => 'line',
            'smooth' => true,
            'data' => array_column($complete_table_data,'real_progress')
        ];

        $series_progress_graph[2] = [
            'name' => 'Esperado',
            'type' => 'line',
            'smooth' => true,
            'data' => array_column($complete_table_data,'estimated_progress'),
            'color' => '#F39100',
        ];

        $labels = array_column($complete_table_data,'date');
        $data_progress_graph= array_column($series_progress_graph,'name');
        $data_role_graph= array_column($series_role_graph,'name');

        return response()->json([
            'labels' => $labels,
            'series_progress_graph' => $series_progress_graph,
            'data_progress_graph' => $data_progress_graph,
            'series_role_graph' => $series_role_graph,
            'data_role_graph' => $data_role_graph
        ]);
    }
}