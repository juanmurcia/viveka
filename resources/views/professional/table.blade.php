@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div>
            <div class="page-title clearfix">
                <div style="float: left">
                    <h1>Profesionales</h1>
                </div>
                @can('person.create')
                <div class="btn-group" style="float: right">
                    <button type="button" class="btn btn-lg btn-round btn-warning"  onclick="createProfessional()">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                </div>
                @endcan()
            </div>
            <div class="x_panel">
                <button type="button" style="float: right" class="btn btn-default dropdown-toggle" data-toggle="modal" data-target="#modSelectColumn">
                    <i class="fa fa-sliders" aria-hidden="true"></i> Columnas
                </button>

                <table id="tbProfessional" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>T.Doc</th>
                        <th>Doc</th>
                        <th>Nombre</th>
                        <th>Nacionalidad</th>
                        <th>Genero</th>
                        <th>Edad</th>
                        <th>Acción</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    @include('user.new')

    <div class="modal fade" id="modProfessional" role="dialog">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title" id="titleModal"></h3>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-body">
                    <!-- Smart Wizard -->
                    <div id="wizard" class="form_wizard wizard_horizontal">
                        <ul class="wizard_steps">
                            <li>
                                <a href="javascript:void(0)" id="step1" onclick="nextStep(this);" data-step="1">
                                    <span class="step_no">1</span>
                                    <span class="step_descr">Paso 1<br/>
                                        <small>Documento Identidad</small>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" id="step2" onclick="nextStep(this);" data-step="2">
                                    <span class="step_no">2</span>
                                    <span class="step_descr">Paso 2<br/>
                                        <small>Datos Generales</small>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" id="step3" onclick="nextStep(this);" data-step="3">
                                    <span class="step_no">3</span>
                                    <span class="step_descr">Paso 3<br/>
                                       <small>Info Adicional</small>
                                    </span>
                                </a>
                            </li>
                        </ul>
                        <div id="step-1" class="step" width="100%">
                            <form action="#" class="form-horizontal">
                                <div class="row">
                                    <div class="form-group col-xs-12">
                                        <label class="control-label col-sm-3 col-xs-12">Documento <span
                                                    class="required">*</span></label>
                                        <div class="col-sm-8 col-xs-12">
                                            <input type="text" id="TBUStxtPersonId" name="TBUStxtPersonId" class="form-control col-xs-12 has-feedback-left"/>
                                            <span class="fa fa-search form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div id="step-2" class="step" width="100%">
                            <form action="#" class="form-horizontal" id="frmPerson" name="frmPerson" onsubmit="return false;">
                                @include('person.form')
                                {{ csrf_field() }}
                            </form>
                        </div>
                        <div id="step-3" class="step" width="100%">
                            <form action="#" class="form-horizontal form-label-left" id="frmProfessional"
                                  name="frmProfessional" onsubmit="return false;">
                                <div class="row">
                                    <input type="hidden" name="txtPerson" id="txtPerson">
                                    <input type="hidden" name="txtProfessionalId" id="txtProfessionalId">
                                    <div class="form-group col-xs-12">
                                        <label class="control-label col-sm-3 col-xs-12">Nacionalidad <span class="required">*</span></label>
                                        <div class="col-sm-8 col-xs-12">
                                            <select id="optNationality" name="optNationality"
                                                    class="select2_single form-control col-xs-12"  required="required">
                                                <option value="">..Seleccione uno ..</option>
                                                @foreach ($country as $key => $val)
                                                    <option value="{{ $val->id }}">{{ $val->iso }}
                                                        - {{ $val->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group col-xs-12">
                                        <label class="control-label col-sm-3 col-xs-12">Fec Nacimiento </label>
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="input-group date myDatePicker2">
                                                <input type='text' id="txtBirthDate" name="txtBirthDate" class="form-control col-xs-12" placeholder="aaaa-mm-dd"/>
                                                <span class="input-group-addon">
                                                   <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-xs-12">
                                        <label class="control-label col-sm-3 col-xs-12">Genero</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <div id="txtGender" class="btn-group radio-button" data-toggle="buttons">
                                                <label class="btn btn-default">
                                                    <input type="radio" name="txtGender" id="txtGenderM" value="M">
                                                    Masculino
                                                </label>
                                                <label class="btn btn-default">
                                                    <input type="radio" name="txtGender" id="txtGenderF" value="F"> Femenino
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    {{ csrf_field() }}
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- End SmartWizard Content -->
                </div>

                <div class="modal-footer">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2 col-xs-12">
                            <div class="row">
                                <button id="pre" class="btn btn-primary" type="button"
                                        onclick="nextStep(this);" data-step="1">
                                    <i class="fa fa-long-arrow-left" aria-hidden="true"></i> Atrás
                                </button>
                                <button type="button" id="actionProfessional" data-role="add"
                                        class="btn btn-primary btn-success">
                                    <i class="fa fa-check" aria-hidden="true"></i> Finalizar
                                </button>
                                <button id="next" class="btn btn-primary btn-primary" type="button"
                                        onclick="nextStep(this);" data-step="2">
                                    Siguiente <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop

@section('script')
    @parent
    <script src="{{asset('js/person/form.js')}}"></script>
    <script src="{{asset('js/professional/table.js')}}"></script>
@stop