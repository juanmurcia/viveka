<div class="modal fade" id="modSearchUpload" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" id="titleModalUser">Registrar Costo/Documentos Actividad</h3>
                <div class="clearfix"></div>
            </div>

            <div class="modal-body">
                <form action="#" data-parsley-validate class="form-horizontal" onsubmit="return false;">
                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="control-label col-md-4 col-sm-3 col-xs-12">Contrato :</label>
                            <div class="col-sm-8 col-xs-12">
                                <select id="optAgreementCalendar" name="optAgreementCalendar" onchange="make_project(this.value);make_activity(1,this.value)" class="select2 form-control col-md-12">
                                    <option value="">.. Seleccione uno ..</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label class="control-label col-md-4 col-sm-3 col-xs-12">Proyecto :</label>
                            <div class="col-sm-8 col-md-8 col-xs-12">
                                <select id="optProjectCalendar" name="optProjectCalendar" onchange="make_product(this.value);make_activity(2,this.value);" class="select2 form-control col-md-12">
                                    <option value="">.. Seleccione uno ..</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="control-label col-md-4 col-sm-3 col-xs-12">Producto :</label>
                            <div class="col-sm-8 col-xs-12">
                                <select id="optProductCalendar" name="optProductCalendar" onchange="make_activity(3,this.value)"
                                        class="select2 form-control col-md-12">
                                    <option value="">.. Seleccione uno ..</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label class="control-label col-md-4 col-sm-3 col-xs-12">Actividad :</label>
                            <div class="col-sm-8 col-md-8 col-xs-12">
                                <select id="optActivityCalendar" required name="optActivityCalendar" class="select2 form-control col-md-12">
                                    <option value="">.. Seleccione uno ..</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="control-label col-md-4 col-sm-3 col-xs-12">Tipo Cargue :</label>
                            <div class="col-sm-8 col-xs-12">
                                <p>
                                    Costo:
                                    <input type="radio" checked class="flat" name="type_search" id="type_searchC" value="C" checked="" required /> Documento:
                                    <input type="radio" class="flat" name="type_search" id="type_searchD" value="D" />
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-center">
                            <button type="submit" id="actionSearchUpload" data-role="add"
                                    class="btn btn-lg btn-round btn-success">
                                <i class="fa fa-check" aria-hidden="true"></i> Siguiente
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="util" style="display: none">
    <input type="text" id="agreements" value="{{ json_encode($agreements) }}">
    <input type="text" id="projects" value="{{ json_encode($projects) }}">
    <input type="text" id="products" value="{{ json_encode($products) }}">
    <input type="text" id="activities" value="{{ json_encode($activities) }}">
</div>

@include('activity.new-cost')

<div class="modal fade" id="modDocument" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" id="titleModalDocument">Nuevo Documento</h3>
                <div class="clearfix"></div>
            </div>

            <div class="modal-body">
                @include('document.new-tab')
            </div>
        </div>
    </div>
</div>

@section('script')
    @parent
    <script src="{{asset('js/main-menu/search-upload.js')}}"></script>
@stop