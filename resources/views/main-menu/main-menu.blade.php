@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div >
            <h1 class="text-center">Navegación</h1>
            <div class="nav-menu text-center">
                <a href="{{ route('agreement.brochure') }}" class="item-menu hm">
                    <i class="menu-icon icon fa fa-folder-open" ></i>
                    <span>Portafolio</span>
                </a>
                <a href="{{ route('agreement.brochure') }}" class="item-menu fb">
                    <i class="menu-icon icon fa fa-clipboard" ></i>
                    <span>Sub - Contratos</span>
                </a>
                <a href="{{ route('activity_executed_time.create') }}" class="item-menu gp">
                    <i class="menu-icon icon fa fa-calendar" ></i>
                    <span>Calendario</span>
                </a>
                <a data-toggle="modal" data-target="#modSearchUpload" class="item-menu tw">
                    <i class="menu-icon icon fa fa-tasks" ></i>
                    <span>Cargue Info</span>
                </a>
                @can('billing.create')
                    <a data-toggle="modal"
                       data-target="#modAddBilling" class="item-menu cl">
                        <i class="menu-icon icon fa fa-money" ></i>
                        <span>Facturar</span>
                    </a>
                @endcan
                <a href="{{ route('search.index') }}" class="item-menu search">
                    <i class="menu-icon icon fa fa-search" ></i>
                    <span>Buscador</span>
                </a>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="{{ route('notification.index') }}">
                    <span><h3><i class="fa fa-exchange"></i> Ir a menú de administrador</h3></span>
                </a>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modAddBilling" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title" id="titleModalAddBilling">Crear Facturación</h3>
                    <div class="clearfix"></div>
                </div>

                <div class="modal-body">
                    <form action="#" data-parsley-validate class="form-horizontal" id="frmNewBilling" name="frmNewBilling" onsubmit="return false;">
                        @include('billing.new')
                        <div class="form-group">
                            <div class="col-md-12 col-xs-12 text-center">
                                <button type="submit" id="newBilling" class="btn btn-lg btn-round btn-success">
                                    <i class="fa fa-check" aria-hidden="true"></i> Generar
                                </button>
                            </div>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('main-menu.search-upload')
@stop

@section('style')
    @parent
    <link rel="stylesheet" href="{{asset('build/css/main-menu.css')}}">
@stop