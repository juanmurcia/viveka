@slot('header')
    @component('mail::header', ['url' => config('app.name')])
        {{ config('app.name') }}
    @endcomponent
@endslot
@component('mail::message')
    # Hola {{ $notification->user->name }}, Bienvenido(a) a {{ config('app.name') }}<br>
    <strong>Nos Vamos a divertir !!! </strong>

    Se ha creado tu usuario de acceso. <br>

    Ingresa, actualiza tu contraseña y prepárate para jugar. <br>
    @component('mail::button', ['url' => config('app.name')])
        Ingresar
    @endcomponent

    Thanks,<br>
    {{ config('app.name') }}
@endcomponent
