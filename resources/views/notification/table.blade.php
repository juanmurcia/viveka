@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div>
            <div class="page-title clearfix">
                <div style="float: left">
                    <h1>Buzón
                        <small>mensajes usuario</small>
                    </h1>
                </div>
            </div>
            <div class="x_panel">
                <div class="row">
                    <div class="col-md-2">
                        <button id="compose" class="btn btn-sm btn-round btn-warning btn-block" type="button">
                            <i class="fa fa-plus" aria-hidden="true"></i> Redactar
                        </button>
                        <br>
                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                            <ul id="myTab1" class="nav nav-tabs tabs-right">
                                <li class="active">
                                    <a href="#received-r" id="received" onclick="consultar('received');"
                                       data-toggle="tab"><i class="fa fa-inbox" aria-hidden="true"></i> Recibidos</a>
                                </li>
                                <li>
                                    <a href="#sended-r" id="sended" onclick="consultar('sended');" data-toggle="tab"><i
                                                class="fa fa-send-o" aria-hidden="true"></i> Enviados</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-3 mail_list_column">
                        <div id="preMessage"></div>
                    </div>
                    <!-- /MAIL LIST -->

                    <!-- CONTENT MAIL -->
                    <div class="col-sm-7 mail_view">
                        <div class="inbox-body" style="display: none;">
                            <div class="mail_heading row">
                                <div class="col-md-8">
                                    <div class="btn-group" style="display: none;">
                                        <button class="btn btn-sm btn-primary" type="button"><i class="fa fa-reply"></i>
                                            Reply
                                        </button>
                                        <button class="btn btn-sm btn-default" type="button" data-placement="top"
                                                data-toggle="tooltip" data-original-title="Forward"><i
                                                    class="fa fa-share"></i></button>
                                        <button class="btn btn-sm btn-default" type="button" data-placement="top"
                                                data-toggle="tooltip" data-original-title="Print"><i
                                                    class="fa fa-print"></i></button>
                                        <button class="btn btn-sm btn-default" type="button" data-placement="top"
                                                data-toggle="tooltip" data-original-title="Trash"><i
                                                    class="fa fa-trash-o"></i></button>
                                    </div>
                                </div>
                                <div class="col-md-4 text-right">
                                    <p class="date" id="dateNotification"></p>
                                </div>
                                <div class="col-md-12">
                                    <h4 id="issueNotification"></h4>
                                </div>
                            </div>
                            <div class="sender-info">
                                <div class="row">
                                    <div class="col-md-12">
                                        <strong id="senderNotification"></strong>
                                        <span id="mailNotification"></span> para
                                        <strong id="addressee"></strong>
                                        <a class="sender-dropdown"><i class="fa fa-chevron-down"></i></a>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="view-mail"><p id="messageNotification"></p></div>
                        </div>
                    </div>
                    <!-- /CONTENT MAIL -->
                </div>
            </div>
        </div>
    </div>

    <div class="compose col-md-6 col-xs-12" id="composeMesasage">
        <div class="compose-header bg-orange">
            Nuevo Mensaje
            <button type="button" class="close compose-close"><span>×</span></button>
        </div>

        <div class="compose-body">
            <br>
            <form action="#" id="frmNotification" name="frmNotification" onsubmit="return false;">
                <div class="form-group col-xs-12">
                    <label class="control-label col-md-12 col-sm-12 col-xs-12 text-left">Destinatario <span
                                class="required">*</span></label>
                    <select id="optUserId" name="optUserId" class="form-control col-md-12 col-sm-12 col-xs-12"
                            required="required">
                        <option value="">..Seleccione uno ..</option>
                        @foreach ($users as $key => $val)
                            <option value="{{ $val->id }}">{{ $val->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-xs-12">
                    <label class="control-label col-sm-10 col-xs-10 text-left">Asunto <span
                                class="required">*</span></label>
                    <input type="text" class="form-control col-md-12 col-sm-12 col-xs-12" id="txtName" name="txtName">
                </div>

                <div class="form-group col-xs-12">
                    <label class="control-label col-sm-10 col-xs-10 text-left">Mensaje<span
                                class="required">*</span></label>
                </div>

                <div class="form-group col-xs-12">
                    <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor-one">
                        <div class="btn-group">
                            <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i
                                        class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a data-edit="fontSize 5">
                                        <p style="font-size:17px">Huge</p>
                                    </a>
                                </li>
                                <li>
                                    <a data-edit="fontSize 3">
                                        <p style="font-size:14px">Normal</p>
                                    </a>
                                </li>
                                <li>
                                    <a data-edit="fontSize 1">
                                        <p style="font-size:11px">Small</p>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="btn-group">
                            <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                            <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
                            <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
                            <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
                        </div>

                        <div class="btn-group">
                            <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                            <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
                            <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
                            <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                        </div>

                        <div class="btn-group">
                            <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
                            <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
                            <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
                            <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
                        </div>
                        <!--
                        <div class="btn-group">
                            <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
                            <div class="dropdown-menu input-append">
                                <input class="span2" placeholder="URL" type="text" data-edit="createLink" />
                                <button class="btn" type="button">Add</button>
                            </div>
                            <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>
                        </div>

                        <div class="btn-group">
                            <a class="btn" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="fa fa-picture-o"></i></a>
                            <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
                        </div>
                        -->
                        <div class="btn-group">
                            <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                            <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
                        </div>
                    </div>
                    <div id="editor-one" class="editor-wrapper"></div>
                    <input type="hidden" name="txtDescription" id="txtDescription" style="display:none;">
                </div>
        </div>

        <div class="compose-footer col-md-6 col-md-offset-3">
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="chkMail" name="chkMail"> Enviar copia a email
                </label>
            </div>
            <button id="actionNotification" class="btn btn-round btn-block btn-warning" type="button">Enviar</button>
        </div>
        {{ csrf_field() }}
        </form>
    </div>
@stop

@section('script')
    <script src="{{asset('/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js')}}"></script>
    <script src="{{asset('/vendors/jquery.hotkeys/jquery.hotkeys.js')}}"></script>
    <script src="{{asset('/vendors/google-code-prettify/src/prettify.js')}}"></script>
    <script src="{{asset('js/notification/table.js')}}"></script>
@stop