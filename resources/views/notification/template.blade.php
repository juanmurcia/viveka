<div id="t_notification" style="display: none;">
    <li>
        <a href="|link|">
            <span class="image"><img src="{{ asset('/images/viveka_logo.svg' )}}" ></span>
            <span>
                <span>|sender|</span>
                <span class="time">|time|</span>
            </span>
            <span class="message">|message|</span>
        </a>
    </li>
</div>

<div id="t_message" style="display: none;">
    <div  onclick="|action|" style="cursor: pointer;">
        <div class="mail_list">
            <div class="row">
                <div class="col-md-11">
                    <div class="left"><i class="fa |fa-class|"></i></div>
                    <div class="right">
                        <div class="row">
                            <div class="col-sm-8"><h3>|sender|</h3></div>
                            <div class="col-sm-4"><h3><small>|time|</small></h3></div>
                        </div>
                        <div class="row">
                            <div class="left"><i class="fa |fa-type|"></i></div>
                            <div class="right"><p>|message|</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="message-selected col-md-8 col-md-offset-2" id="list|id|" style="height: 100%"></div>
            </div>
        </div>
    </div>
</div>

@section('script')
    @parent
    <!-- Notification-->
    <script src="{{asset('js/notification/user_notification.js')}}"></script>
@stop

