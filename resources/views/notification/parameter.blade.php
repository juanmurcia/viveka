@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div class="page-title clearfix">
            <div style="float: left">
                <h1>Parametros Notificaciones</h1>
            </div>
            @can('par_notification.create')
                <div class="btn-group" style="float: right">
                    <button type="button" id="btnParNotification" class="btn btn-lg btn-round btn-warning"
                            onclick="createParNotification()" data-toggle="modal" data-target="#modParNotification">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                </div>
            @endcan()
        </div>

        <div class="x_panel">
            <button type="button" style="float: right" class="btn btn-default dropdown-toggle" data-toggle="modal" data-target="#modSelectColumn">
                <i class="fa fa-sliders" aria-hidden="true"></i> Columnas
            </button>

            <table id="tbParNotification" class="table table-striped table-hover col-12">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Tiempo</th>
                    <th>Roles</th>
                    <th>Acción</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

@stop

@section('script')
    @parent
    <script src="{{asset('js/notification/parameter.js')}}"></script>
@stop