@extends('layouts.empty')

@section('content')
    <div class="page">
        <div class="page-login" >
            <div class="row" >
                <div class="box-login col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4" >
                    <div class="col-xs-10 col-xs-offset-1">
                        <form method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <div class="page-brand-info center">
                                <img src="{{ asset('/images/viveka_logo_texto.svg') }}" style="margin:2em 0; width: 100%">
                            </div>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger alert-dismissible">
                                    <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                                    <i class="icon fa fa-ban"></i><strong>Whoops!</strong> Hay algunos problemas en la autenticacion.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="form-group">
                                <input id="email" type="email" class="input_login{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input id="password" placeholder="Password" type="password" class="input_login{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <button type="submit" style="color: white;" class="btn btn-block btn-warning btn-lg">Ingresa</button>
                            </div>
                            <div class="social-auth-links text-center">
                                <a href="#"><p>¿Olvido su contraseña?</p></a>
                            </div>
                            <br>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="{{asset('css/login.css')}}" rel="stylesheet">
@endsection