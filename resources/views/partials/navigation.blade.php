<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{ route('home') }}" class="site_title text-center">
                <span>
                    <img src="{{ asset('/images/viveka_logo_solo_texto.svg') }}" alt="Viveka"style="height: 80%">
                </span>
                <img src="{{ asset('/images/viveka_logo.svg') }}" alt="..." style="height: 80%">
            </a>
        </div>

        <div class="clearfix"></div>
        <br/>

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <ul class="nav side-menu">
                    <li id="menu_dashboard"><a href="{{  route('home')  }}"><i class="fa fa-home"></i>Inicio</a></li>
                    @can('proposal.index')
                        <li id="menu_proposal"><a href="#"><i class="fa fa-briefcase"></i>Propuestas</a></li>
                    @endcan
                    @can('agreement.index')
                        <li id="menu_agreement"><a href="{{ route('agreement.index') }}"><i class="fa fa-gavel"></i>Contratos</a></li>
                    @endcan
                    @can('project.index')
                        <li id="menu_project"><a href="{{ route('project.index') }}"><i class="fa fa-sitemap"></i>Proyectos</a></li>
                    @endcan
                    @can('product.index')
                        <li id="menu_product"><a href="{{ route('product.index') }}"><i class="fa fa-cubes"></i>Productos</a></li>
                    @endcan
                    @can('activity.index')
                        <li id="menu_dashboard"><a href="{{ route('activity.index') }}"><i class="fa fa-calendar"></i>Actividades</a></li>
                    @endcan

                    <!--<li id="menu_activity_time"><a href="{{ route('activity_executed_time.index') }}"><i class="fa fa-clock-o"></i>Horas</a></li>-->
                    @can('proposal.index')
                        <li id="menu_invoices"><a href="{{ route('billing.index') }}"><i class="fa fa-money"></i>Facturación</a></li>
                    @endcan
                    @can('document.index')
                        <li id="menu_documents"><a href="{{ route('document.index') }}"><i class="fa fa-archive"></i>Documentos</a></li>
                    @endcan

                    <li><a><i class="fa fa-users"></i>Recursos<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            @can('people.index')
                            <li><a href="{{ route('person.index') }}"> Personas</a></li>
                            @endcan
                            @can('company.index')
                                <li><a href="{{ route('company.index') }}"> Empresas</a></li>
                            @endcan
                            @can('professional.index')
                                <li><a href="{{ route('professional.index') }}"> Profesionales</a></li>
                            @endcan
                            @can('user.index')
                                <li><a href="{{ route('user.index') }}">Usuarios</a></li>
                            @endcan
                        </ul>
                    </li>

                    <li><a><i class="fa fa-gears"></i>Configuración<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            @can('pattern.index')
                                <li><a href="{{ route('pattern.index') }}">Modelos </a></li>
                            @endcan
                            @can('patternDocumentType.index')
                                <li><a href="{{ route('patternDocumentType.index') }}">Tipos de Documentos</a></li>
                            @endcan
                            @can('activity_cost_category.index')
                                <li><a href="{{ route('activity_cost_category.index') }}">Categorias Costo</a></li>
                            @endcan
                            @can('role.index')
                                <li><a href="{{ route('role.index') }}">Roles de Usuario</a></li>
                            @endcan
                            @can('system_parameter.index')
                                <li><a href="{{ route('system_parameter.index') }}">Par Empresa </a></li>
                            @endcan
                            @can('par_notification.index')
                                <li><a href="{{ route('par_notification.index') }}">Par Notificaciones</a></li>
                            @endcan
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->
    </div>
</div>

<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
            <input type="hidden" id="auth_person_id" value="{{ Auth::user()->person_id }}">
            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                       aria-expanded="false">
                        <img src="{{ asset('/images/viveka_logo.svg' )}}" alt=""> {{ Auth::user()->name }}
                        <i class=" fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li>
                            <a href="javascript:;" onclick="user_password();">
                                <i class="fa fa-user pull-right"></i> Perfil
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out pull-right"></i> Log Out
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                  style="display: none;">{{ csrf_field() }}</form>
                        </li>
                    </ul>
                </li>

                <li role="presentation" class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle info-number" onclick="user_notification();"
                       data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-envelope-o"></i>
                        <span class="badge bg-orange span-notification" style="display: none"></span>
                    </a>
                    <ul class="dropdown-menu list-unstyled msg_list" role="menu">
                        <div id="menu1"></div>
                        <li>
                            <div class="text-center">
                                <a href="/notificaciones"><strong>Ver todas</strong><i class="fa fa-angle-right"></i></a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>

<div class="modal fade" id="modSelectColumn" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Seleccione Columnas</h3>
                <div class="clearfix"></div>
            </div>
            <div class="modal-body">
                <ul class="to_do"></ul>
            </div>
        </div>
    </div>
</div>

@include('notification.template')
@include('audit.table')