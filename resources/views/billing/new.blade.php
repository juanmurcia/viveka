<div class="form-group">
    <label class="control-label text-right col-md-4 col-sm-3 col-xs-12">Contrato <span class="required">*</span></label>
    <div class="col-md-8 col-sm-8 col-xs-12">
        <select requirred id="txtBillingAgreement" name="txtBillingAgreement"></select>
    </div>
</div>


@section('script')
    <script src="{{ asset('js/billing/new.js') }}"></script>
    @parent
@stop