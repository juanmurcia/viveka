@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Facturación</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row" id="divBilling">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Detalle Facturación <small></small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <form action="#" class="form-horizontal" id="frmUpBilling" name="frmUpBilling" onsubmit="return false;">
                                <section class="content invoice">
                                    <!-- title row -->
                                    <div class="row">
                                        <div class="col-xs-12 invoice-header">
                                            <h1>
                                                <i class="fa fa-globe"></i>
                                                <small class="pull-right">
                                                    Fecha Facturación: <span id="created"></span>
                                                    <div class="input-group date myDatePicker2 col-md-2 col-sm-3 col-xs-6" id="divBillingDate">
                                                        <input type='text' id="txtBillingDate" name="txtBillingDate"
                                                               class="form-control" placeholder="aaaa-mm-dd"/>
                                                        <span class="input-group-addon">
                                                           <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                </small>
                                            </h1>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- info row -->
                                    <div class="row invoice-info">
                                        <div class="col-sm-4 invoice-col">
                                            Facturado por:
                                            <div id="from"></div>
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-sm-4 invoice-col">
                                            Facturado a:
                                            <div id="to"></div>
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-sm-4 invoice-col">
                                            <b>Factura # <h3 id="number"></h3><input type="text" id="numberI" name="numberI"></b>
                                            <br>
                                            <br>
                                            <b>Contrato ID:</b> <span id="agreement"></span>
                                            <br>
                                            <b>Fec. Vencimiento:</b> <span id="due_date"></span>
                                            <br>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->

                                    <!-- Table row -->
                                    <div class="row">
                                        <div class="col-xs-12 table">
                                            <table class="table table-striped" id="tbDetBilling" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Product</th>
                                                        <th>Fec Entrega</th>
                                                        <th style="width: 50%">Description</th>
                                                        <th>Subtotal</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                            </table>

                                            <div class="btn-group" style="float: right">
                                                <button type="button" id="btnProductBilling" class="btn btn-lg btn-round btn-warning" onclick="productBilling();">
                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>

                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->

                                    <div class="row">
                                        <!-- accepted payments column -->
                                        <div class="col-xs-6">
                                            <label class="control-label text-left">Observaciones:</label>
                                            <textarea id="txtDetailBilling" name="txtDetailBilling" required class="form-control" rows="3"></textarea>
                                            <p id="divDetailBilling"></p>
                                            <!--
                                            <p class="lead">Payment Methods:</p>
                                            <img src="images/visa.png" alt="Visa">
                                            <img src="images/mastercard.png" alt="Mastercard">
                                            <img src="images/american-express.png" alt="American Express">
                                            <img src="images/paypal.png" alt="Paypal">
                                            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                                Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                                            </p>
                                            -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-xs-6">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <tbody>
                                                    <tr>
                                                        <th class="text-right">Subtotal:</th>
                                                        <th class="text-center">$</th>
                                                        <td class="text-right" id="value"></td>
                                                    </tr>
                                                    <tr>
                                                        <th class="text-right">Iva:</th>
                                                        <th class="text-center">$</th>
                                                        <td class="text-right" id="tax"></td>
                                                    </tr>
                                                    <tr>
                                                        <th class="text-right">Total:</th>
                                                        <th class="text-center">$</th>
                                                        <td class="text-right" id="net_value"></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->

                                    <!-- this row will not appear when printing -->
                                    <div class="row no-print" id="btnGeneratedBilling">
                                        <div class="col-xs-12">
                                            <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                                            <button class="btn btn-danger pull-right" onclick="delBilling();" style="margin-right: 5px;"><i class="fa fa-close"></i> Anular</button>
                                            <button class="btn btn-success pull-right" onclick="saveBilling();"><i class="fa fa-floppy-o"></i> Actualziar</button>
                                            <button class="btn btn-primary pull-right" onclick="closeBilling();" style="margin-right: 5px;"><i class="fa fa-calculator"></i> Cerrar</button>
                                        </div>
                                    </div>
                                    <div class="row no-print" id="btnCloseBilling">
                                        <div class="col-md-12 col-xs-12 text-right">
                                            <button class="btn btn-success " onclick="sendBilling();"><i class="fa fa-send"></i> Enviar Factura</button>
                                            <button class="btn btn-primary " onclick="payBilling();"><i class="fa fa-money"></i> Pagar</button>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <button type="button" class="btn btn-danger"><i class="fa fa-print"></i> Ver PDF</button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="/factura_report/{{$id}}" target="_blank" >Ver Factura</a></li>
                                                    <li><a href="/factura_report/{{$id}}/false" target="_blank" >Impresión Factura</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" value="{{ $id }}" id="billingId" name="billingId">
                                </section>
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>

                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Documentos Facturación <small></small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="text-center">
                                <button type="button" id="btnBillingDocument" onclick="searchDocument();" data-document="" class="btn btn-lg btn-round btn-warning" title="Agregar documento"
                                        data-toggle="modal" data-target="#modBillingDocument">
                                    <i class="fa fa-plus" aria-hidden="true" style="margin-right: 16px"></i>Agregar Documento
                                </button>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <div class="x_content">
                                        <table id="tbDocumentsAddded" width="100%" class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Documento</th>
                                                    <th>Nombre</th>
                                                    <th>Fecha Cargue</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modDocumentVersion" role="dialog">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">Versiones Documento</h3>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-body">
                    <!-- Smart Wizard -->
                    <table id="tbDocumentsVersion" width="100%" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Palabras Clave</th>
                            <th>Creador</th>
                            <th>Fec Cre</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                    <!-- End SmartWizard Content -->
                </div>

                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modBillingDocument" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">Documentos Disponibles</h3>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-body">
                    <!-- Smart Wizard -->
                    <table id="tbBillingDocuments" width="100%" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Documento</th>
                                <th>Nombre</th>
                                <th>Creador</th>
                                <th>Fec Cre</th>
                                <th>Fec Mod</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                    <!-- End SmartWizard Content -->
                </div>

                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modProductBilling" role="dialog">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">Productos Contrato</h3>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-body">
                    <!-- Smart Wizard -->
                    <table class="table table-striped" id="tbProduct" width="100%">
                        <thead>
                        <tr>
                            <th>Product</th>
                            <th>Fec Fin</th>
                            <th>Responsable</th>
                            <th>Valor</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                    <!-- End SmartWizard Content -->
                </div>

                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <div style="display: none">
        <div id="address">
            <address>
                <strong id="name"></strong>
                <br ><span id="address"></span>
                <br ><span id="phone"></span>
                <br ><span id="email"></span>
            </address>
        </div>
    </div>
@stop

@section('script')
    <script src="{{asset('js/jquery.redirect.js')}}"></script>
    <script src="{{ asset('js/billing/view.js') }}"></script>
    @parent
@stop