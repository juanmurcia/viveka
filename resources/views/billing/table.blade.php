@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h1>Facturaciones</h1>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Buscar</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li class="dropdown" style="visibility: hidden;">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                </li>
                                <li style="visibility: hidden;"><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <form action="#" class="form-horizontal form-label-left" id="frmBilling"
                                  name="frmBilling" onsubmit="return false;">
                                <div class="row">
                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Número</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <input type="text" id="txtBillingNumber" name="txtBillingNumber"
                                                   class="form-control col-md-7 col-xs-12"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Contrato</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <input type="text" id="txtBillingName" name="txtBillingName"
                                                   class="form-control col-md-7 col-xs-12"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Estado</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <select id="optBillingPatternState" name="optBillingPatternState"
                                                    class="select2_single form-control col-xs-12" >
                                                <option value="">..Seleccione uno ..</option>
                                                @foreach ($billing_pattern_state as $key => $val)
                                                    <option value="{{ $val->id }}">{{ $val->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fec Facturación
                                            Desde</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="input-group date myDatePicker2">
                                                <input type='text' id="txtBillingStartDateFrom" name="txtBillingStartDateFrom"
                                                       class="form-control col-xs-12" placeholder="aaaa-mm-dd"/>
                                                <span class="input-group-addon">
												   <span class="glyphicon glyphicon-calendar"></span>
												</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fec Facturación
                                            Hasta</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="input-group date myDatePicker2">
                                                <input type='text' id="txtBillingStartDateTo" name="txtBillingStartDateTo"
                                                       class="form-control col-xs-12" placeholder="aaaa-mm-dd"/>
                                                <span class="input-group-addon">
												   <span class="glyphicon glyphicon-calendar"></span>
												</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fec Vencimiento Desde</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="input-group date myDatePicker2">
                                                <input type='text' id="txtBillingEndDateFrom" name="txtBillingEndDateFrom"
                                                       class="form-control col-xs-12" placeholder="aaaa-mm-dd"/>
                                                <span class="input-group-addon">
												   <span class="glyphicon glyphicon-calendar"></span>
												</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fec Vencimiento Hasta</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="input-group date myDatePicker2">
                                                <input type='text' id="txtBillingEndDateTo" name="txtBillingEndDateTo"
                                                       class="form-control col-xs-12" placeholder="aaaa-mm-dd"/>
                                                <span class="input-group-addon">
												   <span class="glyphicon glyphicon-calendar"></span>
												</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="ln_solid"></div>
                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                        <button type="submit" id="actionBilling" data-role="add"
                                                class="btn btn-lg btn-round btn-primary">
                                            <i class="fa fa-check" aria-hidden="true"> </i> Consultar
                                        </button>
                                        <button class="btn btn-lg btn-round btn-default" type="reset">
                                            <i class="fa fa-eraser" aria-hidden="true"> </i> Limpiar
                                        </button>
                                        @can('billing.create')
                                            <button type="button" id="btnAddBilling" data-role="add"
                                                    class="btn btn-lg btn-round btn-success" data-toggle="modal"
                                                    data-target="#modAddBilling">
                                                <i class="fa fa-plus" aria-hidden="true"> </i> Nuevo
                                            </button>
                                        @endcan()
                                    </div>
                                </div>
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="x_panel">
                        <table id="tbBilling" class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Contrato</th>
                                <th>Estado</th>
                                <th>Fec Faturación</th>
                                <th>Fec Vencimiento</th>
                                <th>Valor</th>
                                <th>Impto</th>
                                <th>V. Neto</th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modAddBilling" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title" id="titleModalAddBilling">Crear Facturación</h3>
                        <div class="clearfix"></div>
                    </div>

                    <div class="modal-body">
                        <form action="#" data-parsley-validate class="form-horizontal" id="frmNewBilling" name="frmNewBilling" onsubmit="return false;">
                            @include('billing.new')

                            <div class="form-group col-xs-12">
                                <div class="col-md-7 col-xs-12 text-right">
                                    <button type="submit" id="newBilling" class="btn btn-lg btn-round btn-success">
                                        <i class="fa fa-check" aria-hidden="true"></i> Generar
                                    </button>
                                </div>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    @parent
    <script src="{{ asset('js/billing/table.js') }}"></script>
@stop