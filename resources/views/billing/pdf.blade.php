<!doctype html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style >
        @page { margin: 5px; }
        html {
            font-family: sans-serif;
        }
        h1 {
            margin: .67em 0;
            font-size: 2em;
        }
        small {
            font-size: 80%;
        }
        img {
            border: 0;
        }
        table {
            border-spacing: 0;
            border-collapse: collapse;
        }
        td,
        th {
            padding: 0;
        }
        html {
            font-size: 10px;
        }
        body {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 14px;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
            margin: 0;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        .h1,
        .h2,
        .h3,
        .h4,
        .h5,
        .h6 {
            font-weight: 500;
            line-height: 1.1;
            color: inherit;
        }
        h1 small,
        h2 small,
        h3 small,
        h4 small,
        h5 small,
        h6 small,
        .h1 small,
        .h2 small,
        .h3 small,
        .h4 small,
        .h5 small,
        .h6 small,
        h1 .small,
        h2 .small,
        h3 .small,
        h4 .small,
        h5 .small,
        h6 .small,
        .h1 .small,
        .h2 .small,
        .h3 .small,
        .h4 .small,
        .h5 .small,
        .h6 .small {
            font-weight: normal;
            line-height: 1;
            color: #777;
        }
        h1,
        .h1,
        h2,
        .h2,
        h3,
        .h3 {
            margin-top: 20px;
            margin-bottom: 10px;
        }
        h1 small,
        .h1 small,
        h2 small,
        .h2 small,
        h3 small,
        .h3 small,
        h1 .small,
        .h1 .small,
        h2 .small,
        .h2 .small,
        h3 .small,
        .h3 .small {
            font-size: 65%;
        }
        h4,
        .h4,
        h5,
        .h5,
        h6,
        .h6 {
            margin-top: 10px;
            margin-bottom: 10px;
        }
        h4 small,
        .h4 small,
        h5 small,
        .h5 small,
        h6 small,
        .h6 small,
        h4 .small,
        .h4 .small,
        h5 .small,
        .h5 .small,
        h6 .small,
        .h6 .small {
            font-size: 75%;
        }
        h1,
        .h1 {
            font-size: 36px;
        }
        h2,
        .h2 {
            font-size: 30px;
        }
        h3,
        .h3 {
            font-size: 24px;
        }
        h4,
        .h4 {
            font-size: 18px;
        }
        h5,
        .h5 {
            font-size: 10px;
        }
        h6,
        .h6 {
            font-size: 8px;
        }
        small,
        .small {
            font-size: 85%;
        }
        .text-left {
            text-align: left;
        }
        .text-right {
            text-align: right;
        }
        .text-center {
            text-align: center;
        }
        .text-justify {
            text-align: justify;
        }
        .text-nowrap {
            white-space: nowrap;
        }
        .text-lowercase {
            text-transform: lowercase;
        }
        .text-uppercase {
            text-transform: uppercase;
        }
        .text-capitalize {
            text-transform: capitalize;
        }
        .text-muted {
            color: #777;
        }
        .text-primary {
            color: #337ab7;
        }
        .page-header {
            padding-bottom: 9px;
            margin: 40px 0 20px;
            border-bottom: 1px solid #eee;
        }
        .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
            position: relative;
            min-height: 1px;
            padding-right: 15px;
            padding-left: 15px;
        }
        .col-xs-1, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9, .col-xs-10, .col-xs-11, .col-xs-12 {
            float: left;
        }
        .col-xs-12 {
            width: 100%;
        }
        .col-xs-11 {
            width: 91.66666667%;
        }
        .col-xs-10 {
            width: 83.33333333%;
        }
        .col-xs-9 {
            width: 75%;
        }
        .col-xs-8 {
            width: 66.66666667%;
        }
        .col-xs-7 {
            width: 58.33333333%;
        }
        .col-xs-6 {
            width: 50%;
        }
        .col-xs-5 {
            width: 41.66666667%;
        }
        .col-xs-4 {
            width: 33.33333333%;
        }
        .col-xs-3 {
            width: 25%;
        }
        .col-xs-2 {
            width: 16.66666667%;
        }
        .col-xs-1 {
            width: 8.33333333%;
        }
        .col-xs-pull-12 {
            right: 100%;
        }
        .col-xs-pull-11 {
            right: 91.66666667%;
        }
        .col-xs-pull-10 {
            right: 83.33333333%;
        }
        .col-xs-pull-9 {
            right: 75%;
        }
        .col-xs-pull-8 {
            right: 66.66666667%;
        }
        .col-xs-pull-7 {
            right: 58.33333333%;
        }
        .col-xs-pull-6 {
            right: 50%;
        }
        .col-xs-pull-5 {
            right: 41.66666667%;
        }
        .col-xs-pull-4 {
            right: 33.33333333%;
        }
        .col-xs-pull-3 {
            right: 25%;
        }
        .col-xs-pull-2 {
            right: 16.66666667%;
        }
        .col-xs-pull-1 {
            right: 8.33333333%;
        }
        .col-xs-pull-0 {
            right: auto;
        }
        .col-xs-push-12 {
            left: 100%;
        }
        .col-xs-push-11 {
            left: 91.66666667%;
        }
        .col-xs-push-10 {
            left: 83.33333333%;
        }
        .col-xs-push-9 {
            left: 75%;
        }
        .col-xs-push-8 {
            left: 66.66666667%;
        }
        .col-xs-push-7 {
            left: 58.33333333%;
        }
        .col-xs-push-6 {
            left: 50%;
        }
        .col-xs-push-5 {
            left: 41.66666667%;
        }
        .col-xs-push-4 {
            left: 33.33333333%;
        }
        .col-xs-push-3 {
            left: 25%;
        }
        .col-xs-push-2 {
            left: 16.66666667%;
        }
        .col-xs-push-1 {
            left: 8.33333333%;
        }
        .col-xs-push-0 {
            left: auto;
        }
        .col-xs-offset-12 {
            margin-left: 100%;
        }
        .col-xs-offset-11 {
            margin-left: 91.66666667%;
        }
        .col-xs-offset-10 {
            margin-left: 83.33333333%;
        }
        .col-xs-offset-9 {
            margin-left: 75%;
        }
        .col-xs-offset-8 {
            margin-left: 66.66666667%;
        }
        .col-xs-offset-7 {
            margin-left: 58.33333333%;
        }
        .col-xs-offset-6 {
            margin-left: 50%;
        }
        .col-xs-offset-5 {
            margin-left: 41.66666667%;
        }
        .col-xs-offset-4 {
            margin-left: 33.33333333%;
        }
        .col-xs-offset-3 {
            margin-left: 25%;
        }
        .col-xs-offset-2 {
            margin-left: 16.66666667%;
        }
        .col-xs-offset-1 {
            margin-left: 8.33333333%;
        }
        .col-xs-offset-0 {
            margin-left: 0;
        }
        table {
            background-color: transparent;
        }
        caption {
            padding-top: 8px;
            padding-bottom: 8px;
            color: #777;
            text-align: left;
        }
        th {
            text-align: left;
        }
        .table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 10px;
        }
        .table > thead > tr > th,
        .table > tbody > tr > th,
        .table > tfoot > tr > th,
        .table > thead > tr > td,
        .table > tbody > tr > td,
        .table > tfoot > tr > td {
            padding: 0px;
            vertical-align: top;

        }
        .table > thead > tr > th {
            vertical-align: bottom;

        }
        .table > tbody + tbody {
            border-top: 0px solid #ddd;
        }
        .table .table {
            background-color: #fff;
        }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #ddd !important;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
            <br>
            <table class="table" width="100%" border="0" cellspacing="1" cellpadding="1">
                <tr>
                    <td class="text-center" width="20%"><img src="{{ public_path().'/images/viveka_logo_texto_gris.jpg' }}"></td>
                    <th class="text-center" width="50%" rowspan="2">
                        VIVEKA S.A.S <br>
                        <h6>
                            DOCUMENTO OFICIAL DE AUTORIZACIÓN DE NUMERACIÓN <br>
                            DE FACTURACIÓN No. 18762008654427 DE 2018/06/12 <br>
                            NUM. AUTORIZADA DEL 375 AL 449, VIGENCIA 18 MESES <br>
                            NO SOMOS GRANDES CONTRIBUYENTES <br>
                            ICA - ACT. ECO. CIIU 7420 - TARIFA 302 6.9 X 1.000
                        </h6>
                    </th>
                    <th class="text-center" width="28%">FACTURA DE VENTA</th>
                </tr>
                <tr>
                    <td class="text-left"><h6>NIT. 900.461.668-1 <br> IVA REGIMEN COMÚN</h6></td>
                    <td class="text-center" width="28%">
                        <div style="background-color: #FDEBD0;height: 30px;border-style: solid;border-width: 1px;border-radius: 10px;">{{ $data['datos']->number }}</div>
                    </td>
                </tr>
            </table>
            <br>
            <table class="table" width="100%" border="0" cellspacing="2" cellpadding="2">
                <tr>
                    <td class="text-left" width="70%">
                        <div style="background-color: #FDEBD0; border-style: solid;border-width: 1px;border-radius: 10px;">
                            <table style="background-color: #FDEBD0;border-radius: 10px;" width="100%" border="0">
                                <tr>
                                    <td class="text-left" width="60%"><strong>Cliente: </strong> {{ $data['datos']->agreement->contractor->person->name }}</td>
                                    <td class="text-left" width="40%"><strong>NIT: </strong> {{ $data['datos']->agreement->contractor->person->document }}</td>
                                </tr>
                    ont         <tr>
                                    <td class="text-left" width="60%"><strong>Dirección: </strong> {{ substr($data['datos']->agreement->contractor->person->address,0,30) }}</td>
                                    <td class="text-left" width="40%"><strong>Teléfono: </strong> {{ substr($data['datos']->agreement->contractor->person->phone, 0, 15) }}</td>
                                </tr>
                                <tr>
                                    <th class="text-left" width="60%">Ciudad:</th>
                                    <th class="text-left" width="40%"> Forma Pago:</th>
                                </tr>
                                <tr><th class="text-left" colspan="2"><br></th></tr>
                            </table>
                        </div>
                    </td>
                    <td width="1%"><br></td>
                    <td class="text-right" width="29%">
                        <div style="border-style: solid;border-width: 1px;border-radius: 10px;">
                            <table style="border-radius: 10px;" width="100%" border="0">
                                <tr>
                                    <td colspan="3" class="text-center" >
                                        <div style="background-color: #FDEBD0;border-radius: 10px;">
                                            FECHA DE FACTURA
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">Día : {{ substr($data['datos']->billing_date, 8, 2) }}</td>
                                    <td class="text-center"><div style="border-left: 1px solid;border-right: 1px solid;">Mes : {{ substr($data['datos']->billing_date, 5, 2) }}</div></td>
                                    <td class="text-center">Año : {{ substr($data['datos']->billing_date, 0, 4) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="text-center">
                                        <div style="background-color: #FDEBD0;">
                                            FECHA DE VENCIMIENTO
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">Día : {{ substr($data['datos']->due_date, 8, 2) }}</td>
                                    <td class="text-center"><div style="border-left: 1px solid;border-right: 1px solid;">Mes : {{ substr($data['datos']->due_date, 5, 2) }}</div></td>
                                    <td class="text-center">Año : {{ substr($data['datos']->due_date, 0, 4) }}</td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <div style="border-style: solid;border-width: 1px;border-radius: 10px;">
                <table class="table" width="100%" style="margin-bottom: 0mm" border="0">
                    <tr>
                        <th class="text-center" colspan="2" style="border-bottom: 1px solid;">DESCRIPCION</th>
                        <th class="text-center" style="border-left: 1px solid;border-bottom: 1px solid;">VALOR TOTAL</th>
                    </tr>

                    <tbody>
                        @foreach ($data['datos']->billing_detail as $detail)
                            <tr style="height: 1px">
                                <td colspan="2">{{ $detail->product->id}} {{ $detail->product->name}}, {{ $detail->product->description}}</td>
                                <td class="text-right" style="border-left: 1px solid;">$ {{ number_format($detail->value)}}</td>
                            </tr>
                        @endforeach

                        <tr><th colspan="2"><div style="position: fixed; top: 150mm"><br></div></th><th style="border-left: 1px solid;"><br></th></tr>
                    <tbody>

                    <tr>
                        <td class="text-left" width="70%" rowspan="3" style="border-top: 1px solid;"><strong>Observación: </strong> {{ $data['datos']->comment }}</td>
                        <th class="text-center" style="background-color: #FDEBD0;border-left: 1px solid;border-top: 1px solid;">SUB-TOTAL</th>
                        <td class="text-right" style="background-color: #FDEBD0;border-left: 1px solid;border-top: 1px solid;">$ {{ number_format($data['datos']->value) }}</td>
                    </tr>
                    <tr>
                        <th class="text-center" style="background-color: #FDEBD0;border-left: 1px solid;border-top: 1px solid;">I.V.A %</th>
                        <td class="text-right" style="background-color: #FDEBD0;border-left: 1px solid;border-top: 1px solid;">$ {{ number_format($data['datos']->taxes) }}</td>
                    </tr>
                    <tr>
                        <th class="text-center" style="background-color: #FDEBD0;border-left: 1px solid;border-top: 1px solid;">VALOR TOTAL</th>
                        <td class="text-right" style="background-color: #FDEBD0;border-left: 1px solid;border-top: 1px solid;">$ {{ number_format($data['datos']->net_value) }}</td>
                    </tr>

                </table>
            </div>
            <h5 class="text-center">Esta factura de venta, se asimila para todos los efectos a un Titulo Valor conforme a lo dispuesto
                en la Ley 1231 de Julio 17 de 2008, <br> y causara intereses de mora a la tasa máxima legal permitida por la Ley (Art. 1517 Código Civil)
            </h5>
            <table class="table" width="100%" border="0" cellspacing="1" cellpadding="1">
                <tr>
                    <td width="45%"  class="text-center">
                        <div style="background-color: #FDEBD0;height:105px;border-style: solid;border-width: 1px;border-radius: 10px;">
                            <div class="text-center" style="height: 68px"><strong>VIVEKA S.A.S</strong></div>
                            <div>
                                <hr>
                                <h5 class="text-center">FIRMA Y SELLO AUTORIZADOS</h5>
                            </div>
                        </div>
                    </td>
                    <td width="5%"><br></td>
                    <td width="45%" class="text-center">
                        <div style="background-color: #FDEBD0;height:105px;border-style: solid;border-width: 1px;border-radius: 10px;">
                            <div class="text-left" style="height: 38px"><h5>ACEPTADA, FIRMADA Y SELLO</h5></div>
                            <hr>
                            <div><h6 class="text-left">&nbsp; CC. o NIT.</h6><h6 class="text-left">&nbsp; FECHA DE RECIBIDO: </h6></div>
                        </div>
                    </td>
                </tr>
                <tr><td><br></td></tr>
                <tr>
                    <td colspan="3">
                        <div style="border-style: solid;border-width: 1px;border-radius: 5px;">
                            Cra. 11 No. 93 A - 53, Of. 501 - Tel: (57-1) 742 40 59 Ext. 110 - E-mail: info@viveka.com.co - Bogotá D.C. - Colombia
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
</body>
</html>