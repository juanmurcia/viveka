<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <form action="#" class="form-horizontal" id="frmNewBilling" name="frmNewBilling" onsubmit="return false;">
                <input type="hidden" id="txtBillingAgreement" name="txtBillingAgreement" value="{{ $id }}"/>
                <div class="col-lg-4 col-sm-6 col-xs-12 BillingCreate">
                    @can('billing.new')
                        <button type="button" id="newBilling" class="btn btn-lg btn-round btn-warning">
                            <i class="fa fa-plus" aria-hidden="true" style="margin-right: 16px"></i>Nueva Facturación
                        </button>
                    @endcan
                </div>
                {{ csrf_field() }}
            </form>
        </div>
        <div class="row" id="listBilling">
            <div class="col-sm-12">
                <div class="x_panel">
                    <table id="tbBillingAgreement" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Contrato</th>
                            <th>Estado</th>
                            <th>Fec Faturación</th>
                            <th>Fec Vencimiento</th>
                            <th>Valor</th>
                            <th>Impto</th>
                            <th>V. Neto</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@section('script')
    @parent
    <script src="{{ asset('js/billing/list.js') }}"></script>
    <script src="{{ asset('js/billing/new.js') }}"></script>
@stop