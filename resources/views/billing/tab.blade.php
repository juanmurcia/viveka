<fieldset >
    <legend>Configuración</legend>
    <div class="row">
        <div class="col-md-5 col-xs-12">
            <form action="#" class="form-horizontal form-label-left" id="frmConfBilling" name="frmConfBilling" onsubmit="return false;">
                <div class="form-group col-xs-12">
                    <label class="control-label text-right col-md-4 col-sm-3 col-xs-12">% IVA Facturación<span class="required">*</span></label>
                    <div class="col-md-8 col-xs-12">
                        <input class="form-control" type="text" id="txtIvaBilling" name="txtIvaBilling"/>
                    </div>
                </div>

                <div class="form-group col-xs-12">
                    <label class="control-label text-right col-md-4 col-sm-3 col-xs-12">Vencimiento Facturación <span class="required">*</span></label>
                    <div class="col-md-8 col-xs-12">
                        <select class="form-control" id="txtAgrementBillingDue" name="txtAgrementBillingDue">
                            <option value="0">0 Días</option>
                            <option value="30">30 Días</option>
                            <option value="60">60 Días</option>
                            <option value="90">90 Días</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-xs-12 text-center">
                    @can('agreement.edit')
                        <button type="button" id="billingAgreement" data-role="add"
                                class="btn btn-lg btn-round btn-success">
                            <i class="fa fa-check" aria-hidden="true"></i> Actualizar
                        </button>
                    @endcan()
                </div>
                {{ csrf_field() }}
            </form>
        </div>
    </div>
</fieldset>
<fieldset >
    <legend>Facturación Contrato</legend>
    @include('billing.list')
</fieldset>

@section('script')
    <script src="{{ asset('js/billing/tab.js') }}"></script>
    @parent
@stop