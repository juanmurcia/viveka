@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div class="page-title clearfix">
            <div style="float: left">
                <h1>Personas</h1>
            </div>
            @can('person.create')
                <div class="btn-group" style="float: right">
                    <button type="button" id="btnPerson" class="btn btn-lg btn-round btn-warning"
                            onclick="createPerson()" data-toggle="modal" data-target="#modPerson">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                </div>
            @endcan()
        </div>

        <div class="x_panel">
            <button type="button" style="float: right" class="btn btn-default dropdown-toggle" data-toggle="modal" data-target="#modSelectColumn">
                <i class="fa fa-sliders" aria-hidden="true"></i> Columnas
            </button>

            <table id="tbPerson" class="table table-striped table-hover col-12">
                <thead>
                <tr>
                    <th>T.Doc</th>
                    <th>Documento</th>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th>Direccion</th>
                    <th>Email</th>
                    <th>Teléfonos</th>
                    <th>Acción</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    @include('user.new')

    <!-- Modal -->
    <div class="modal fade" id="modPerson" role="dialog">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">Crear Persona</h3>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-body">
                    <form action="#" data-parsley-validate class="form-horizontal" id="frmPerson" name="frmPerson" onsubmit="return false;">
                        @include('person.form')
                        <div id="btnPerson">
                            <div class="ln_solid"></div>
                            <div class="col-xs-12 text-center">
                                <button type="submit" id="actionPerson" data-role="add"
                                        class="btn btn-lg btn-round btn-success">
                                    <i class="fa fa-check" aria-hidden="true"> </i> <span>Crear</span>
                                </button>
                            </div>
                            {{ csrf_field() }}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@stop

@section('script')
    @parent
    <script src="{{asset('js/person/table.js')}}"></script>
@stop