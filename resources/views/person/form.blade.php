<div class="row">
	<input type="hidden" name="txtPersonId" id="txtPersonId">
	<div class="form-group col-xs-12">
		<label class="control-label col-sm-3 col-xs-12" for="first-name">Tipo Documento <span class="required">*</span></label>
		<div class="col-sm-8 col-xs-12">
			<select required id="optDocument" name="optDocument" onchange="updateLastNameVisibility(this.value)" class="select2_single form-control" required="required">
				<option value="">..Seleccione uno ..</option>
				@foreach (App\Models\PersonDocumentType::all() as $key => $val)
					<option value="{{ $val->id }}">{{ $val->name }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="form-group col-xs-12">
		<label class="control-label col-sm-3 col-xs-12" for="first-name">Documento <span class="required">*</span></label>
		<div class="col-sm-8 col-xs-12">
			<input required type="text" id="txtIdent" name="txtIdent" required="required" class="form-control col-xs-12">
		</div>
	</div>
	<div class="form-group col-xs-12">
		<label class="control-label col-sm-3 col-xs-12" for="first-name">Tipo Contribuyente </label>
		<div class="col-sm-8 col-xs-12">
			<select required id="optTaxpayer" name="optTaxpayer" class="select2_single form-control"  >
				<option value="">..Seleccione uno ..</option>
				@foreach (App\Models\TaxpayerType::all() as $key => $val)
					<option value="{{ $val->id }}">{{ $val->name }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="form-group col-xs-12">
		<label class="control-label col-sm-3 col-xs-12" for="first-name">Nombre <span class="required">*</span></label>
		<div class="col-sm-8 col-xs-12">
			<input required type="text" id="txtName" name="txtName" required="required" class="form-control col-xs-12">
		</div>
	</div>
	<div class="form-group col-xs-12" id="txtLastNameContainer">
		<label class="control-label col-sm-3 col-xs-12" for="first-name">Apellidos <span class="required">*</span></label>
		<div class="col-sm-8 col-xs-12">
			<input type="text" id="txtLastName" name="txtLastName" required="required" class="form-control col-xs-12">
		</div>
	</div>
	<div class="form-group col-xs-12">
		<label class="control-label col-sm-3 col-xs-12" for="first-name">Dirección </label>
		<div class="col-sm-8 col-xs-12">
			<input type="text" id="txtAddress" name="txtAddress" class="form-control col-xs-12">
		</div>
	</div>
	<div class="form-group col-xs-12">
		<label class="control-label col-sm-3 col-xs-12" for="first-name">Email <span class="required">*</span></label>
		<div class="col-sm-8 col-xs-12">
			<input required type="email" id="txtEmail" name="txtEmail"  pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" required="required" class="form-control col-xs-12">
		</div>
	</div>
	<div class="form-group col-xs-12">
		<label class="control-label col-sm-3 col-xs-12" for="first-name">Telefonos <span class="required">*</span></label>
		<div class="col-sm-8 col-xs-12">
			<input required type="text" id="txtMobil" name="txtMobil" class="tags form-control col-xs-12">
		</div>
	</div>
</div>

@section('script')
	@parent
	<script src="{{asset('js/person/form.js')}}"></script>
@stop
