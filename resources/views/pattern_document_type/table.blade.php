@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
		<div>
			<div class="page-title clearfix">
				<div style="float: left">
                	<h1>Documentos Modelo</h1>
                </div>
				@can('pattern_document_type.create')
				<div class="btn-group" style="float: right">
					<button type="button" id="btnPatternDocumentType" class="btn btn-lg btn-round btn-warning" data-toggle="modal" data-target="#modPatternDocumentType">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				</div>
				@endcan()
			</div>
			<div class="x_panel">
				<table id="tbPatternDocumentType" class="table table-striped table-hover">
					<thead>
						<tr>
							<th>Modelo</th>
							<th>Nombre</th>
							<th>Estado</th>
							<th></th>
						</tr>
					</thead>
				</table>
			</div>
	    </div>
	</div>    

	<!-- Modal -->
	<div class="modal fade" id="modPatternDocumentType" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h3 class="modal-title" id="titleModal">Tipo de Documento</h3>
				  <div class="clearfix"></div>
				</div>
				<div class="modal-body">
					<form action="#" class="form-horizontal form-label-left" id="frmPatternDocumentType" name="frmPatternDocumentType" onsubmit="return false;">
						{{ csrf_field() }}
						<div class="form-group">
	                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Modelo <span class="required">*</span></label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        <select id="optPatternId" name="optPatternId" class="select2_single form-control"   required="required">
	                            	@foreach ($pattern as $key => $val)
								        <option value="{{ $val->id }}">{{ $val->name }}</option>
								    @endforeach
	                        	</select>
                        	</div>
                      	</div>
                      	<div class="form-group">
	                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre <span class="required">*</span></label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                          <input type="text" id="txtName" name="txtName" required="required" class="form-control col-md-7 col-xs-12">
	                        </div>
                      	</div>
                      	<div class="ln_solid"></div>
						<div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-center">
								<button type="button" id="actionPatternDocumentType" data-role="add" class="btn btn-lg btn-round btn-success">
									<i class="fa fa-check" aria-hidden="true"> </i> Generar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

@stop

@section('script')
	@parent
	<script src="{{asset('js/pattern_document_type/table.js')}}"></script>
@stop