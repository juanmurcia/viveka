<div class="row">
    <div class="form-group col-lg-6 col-xs-12">
        <div class="row">
            <div class="form-group col-xs-12">
                <label class="control-label col-lg-4 col-sm-3 col-xs-12">Porcentaje <span class="required">*</span></label>
                <div class="col-sm-8 col-xs-12">
                    <input type="number" min="0" step="1" id="txtProjectPercentage" name="txtProjectPercentage" class="form-control col-xs-12"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12">
                <label class="control-label col-lg-4 col-sm-3 col-xs-12">Estado <span class="required">*</span></label>
                <div class="col-sm-8 col-xs-12">
                    <select id="optProjectPatternState" name="optProjectPatternState" class="select2_single form-control"  >
                        <option value="">..Seleccione uno ..</option>
                        @foreach ($project_pattern_state as $key => $val)
                            <option value="{{ $val->id }}" >{{ $val->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group col-lg-6 col-xs-12">
        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Justificación <span class="required">*</span></label>
        <div class="col-sm-8 col-xs-12">
            <textarea rows="3" id="txtProjectJustification" name="txtProjectJustification" class="form-control col-xs-12"></textarea>
        </div>
    </div>
</div>