@extends('layouts.master')

@section('content')
	<div class="right_col" role="main">
		<div class="">
			<div class="">
				<h1>Proyecto <small id="project_name">Nombre del Proyecto</small></h1>
				<input type="hidden" id="project_id" value="{{ $id }}">
				<input type="hidden" id="agreement_id" value="{{ $agreement_id }}">
				<input type="hidden" id="model_id" value="{{ $id }}">
				<input type="hidden" id="pattern_id" value="{{ $pattern_id }}">
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="x_panel">
						<div class="crumbs">
							<ul>
								<li><a href="{{ route('home') }}">Dashboard</a></li>
								<li><a href="#">Contratos</a></li>
								<li><a href="#" id="agreement_name">Nombre del Contrato</a></li>
								<li><a href="#">Proyectos</a></li>
							</ul>
						</div>
						<div class="" role="tabpanel" data-example-id="togglable-tabs">
							<ul id="myTab1" class="nav nav-tabs bar_tabs right" role="tablist">
								<li role="presentation" class="active">
									<a href="#dashboard-tab" id="dashboard-tabb" role="tab" data-toggle="tab"
									   aria-controls="dashboard" aria-expanded="true">
										<i class="fa fa-bar-chart-o"></i>
										<span class="hidden-xs"> Dashboard</span>
									</a>
								</li>

								<li role="presentation" class="">
									<a href="#general-tab" role="tab" id="general-tabb" data-toggle="tab"
									   aria-controls="general" aria-expanded="false">
										<i class="fa fa-cogs"></i>
										<span class="hidden-xs"> General</span>
									</a>
								</li>

								<li role="presentation" class="">
									<a href="#activities-tab" role="tab" id="activities-tabb" data-toggle="tab"
									   aria-controls="activity" aria-expanded="false">
										<i class="fa fa-tasks"></i>
										<span class="hidden-xs"> Actividades</span>
									</a>
								</li>

								<li role="presentation">
									<a href="#documents-tab" id="documents-tabb" role="tab" data-toggle="tab"
									   aria-controls="general" aria-expanded="true">
										<i class="fa fa-files-o"></i>
										<span class="hidden-xs"> Documentos</span>
									</a>
								</li>
							</ul>
							<div id="myTabContent2" class="tab-content">
								<div role="tabpanel" class="tab-pane fade" id="general-tab">
									@include('project.general')
								</div>
								<div role="tabpanel" class="tab-pane fade active in" id="dashboard-tab">
									@include('project.dashboard')
								</div>
								<div role="tabpanel" class="tab-pane fade" id="activities-tab">
									@include('activity.tab')
								</div>
								<div role="tabpanel" class="tab-pane fade" id="documents-tab">
									@include('document.tab')
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('script')
	@parent
	<script src="{{ asset('js/project/view.js') }}"></script>
@stop