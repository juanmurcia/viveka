<div class="row">
    <div class="col-xs-12">
        @can('project.create')
            <div class="row">
                <div class="col-lg-4 col-sm-6 col-xs-12 projectCreate">
                    <button type="button" id="btnProject" class="btn btn-lg btn-round btn-warning" title="Agregar nuevo proyecto"
                            data-toggle="modal" data-target="#modProject">
                        <i class="fa fa-plus" aria-hidden="true" style="margin-right: 16px"></i>Nuevo Proyecto
                    </button>
                </div>
            </div>
        @endcan
        <div class="row" id="listProject"></div>
        @include('project.resume')
    </div>
</div>

<div class="modal fade" id="modProject" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                @can('project.create')
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title" id="titleModal">Crear Proyecto</h3>
                    <div class="clearfix"></div>
                @endcan
            </div>

            <div class="modal-body">
                <form action="#" data-parsley-validate class="form-horizontal form-label-left" id="frmProject" name="frmProject"
                      onsubmit="return false;">
                    @include('project.new')
                    <div id="btnProject">
                        <div class="ln_solid"></div>
                        <div class="text-center">
                            @can('project.create')
                                <button type="submit" id="newProject" class="btn btn-lg btn-round btn-success">
                                    <i class="fa fa-check" aria-hidden="true"></i> Guardar
                                </button>
                            @endcan
                        </div>
                    </div>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>

@section('script')
    @parent
    <script src="{{ asset('js/project/list.js') }}"></script>
@stop