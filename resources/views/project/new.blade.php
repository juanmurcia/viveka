<input type="hidden" id="txtProjectId" name="txtProjectId">
<div class="row">
    <div class="col-lg-6 col-xs-12">
        <div class="row">
            <div class="form-group col-xs-12">
                <label class="control-label col-lg-4 col-sm-3 col-xs-12">Nombre <span class="required">*</span></label>
                <div class="col-sm-8 col-xs-12">
                    <input required type="text" id="txtProjectName" name="txtProjectName" class="form-control col-xs-12"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12">
                <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fecha Inicio <span class="required">*</span></label>
                <div class="col-sm-8 col-xs-12">
                    <div class="input-group date myDatePicker2">
                        <input required type='text' id="txtProjectStartDate" name="txtProjectStartDate" class="form-control col-xs-12" placeholder="aaaa-mm-dd" />
                        <span class="input-group-addon">
							   <span class="glyphicon glyphicon-calendar"></span>
							</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12">
                <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fecha Fin <span class="required">*</span></label>
                <div class="col-sm-8 col-xs-12">
                    <div class="input-group date myDatePicker2">
                        <input required type='text' id="txtProjectEndDate" name="txtProjectEndDate" class="form-control col-xs-12" placeholder="aaaa-mm-dd" />
                        <span class="input-group-addon">
							   <span class="glyphicon glyphicon-calendar"></span>
							</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12">
                <label class="control-label col-lg-4 col-sm-3 col-xs-12">Valor <span class="required">*</span></label>
                <div class="col-sm-8 col-xs-12">
                    <input required type="number" id="txtProjectValue" name="txtProjectValue" class="form-control col-xs-12"/>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-xs-12">
        <div class="row">
            <div class="form-group col-xs-12">
                <label class="control-label col-lg-4 col-sm-3 col-xs-12">Responsable <span class="required">*</span></label>
                <div class="col-sm-8 col-xs-12">
                    <select required class="form-control" name="txtProjectResponsible" id="txtProjectResponsible"></select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12">
                <label class="control-label col-lg-4 col-sm-3 col-xs-12">Descripción <span class="required">*</span></label>
                <div class="col-sm-8 col-xs-12">
                    <textarea required rows="3" id="txtProjectDescription" name="txtProjectDescription" class="form-control col-xs-12"></textarea>
                </div>
            </div>
        </div>
    </div>
</div>

@section('script')
    @parent
    <script src="{{ asset('js/project/new.js') }}"></script>
@stop