<div class="col-lg-4 col-sm-6 col-xs-12" id="project-resume" style="display:none;">
    <div class="x_panel">
        <div class="x_title">
            @can('project.view')
                <a href="#" style="float:right; font-size:20px">
                    <i class="fa fa-folder-open"></i>
                </a>
            @endcan
            <h3>Nombre Proyecto</h3>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div style="float:right;">
                <input class="knobManual project-resume-percentage" data-width="100" data-height="100">
            </div>
            <div style="width: 60%; width: calc(100% - 110px)">
                <ul class="list-unstyled">
                    <li><i class="fa fa-user" style="width: 1.5em;"></i> <span class="project-resume-responsible"></span></li>
                    <li><i class="fa fa-calendar" style="width: 1.5em;"></i> Inicio: <span class="project-resume-start"></span></li>
                    <li><i class="fa fa-calendar-o" style="width: 1.5em;"></i> Fin: <span class="project-resume-end"></span></li>
                </ul>
            </div>
        </div>
    </div>
</div>