<fieldset >
    <legend>General</legend>
    <form action="#" data-parsley-validate class="form-horizontal" id="frmProjectBasics" name="frmProjectBasics" onsubmit="return false;">
        <div class="row">
            <div class="col-lg-10 col-xs-12 col-lg-offset-1">
                @include('project.new')
            </div>
        </div>
        <div class="form-group">
            @can('project.edit')
                <div class="col-xs-12 text-center">
                    <button type="submit" id="actionProjectBasics" class="btn btn-lg btn-round btn-success">
                        <i class="fa fa-check" aria-hidden="true"></i> Guardar
                    </button>
                </div>
            @endcan
        </div>
        {{ csrf_field() }}
    </form>
</fieldset>
<fieldset >
    <legend>Avance</legend>
    <form action="#" class="form-horizontal" id="frmProjectProgress" name="frmProjectProgress" onsubmit="return false;">
        <div class="row">
            <div class="col-lg-10 col-xs-12 col-lg-offset-1">
                @include('project.progress')
            </div>
        </div>
        <div class="form-group">
            @can('project.edit')
                <div class="col-xs-12 text-center">
                    <button type="button" id="actionProjectProgress" class="btn btn-lg btn-round btn-success">
                        <i class="fa fa-check" aria-hidden="true"></i> Guardar
                    </button>
                </div>
            @endcan
        </div>
        {{ csrf_field() }}
    </form>
</fieldset>

@section('script')
    <script src="{{ asset('js/project/form.js') }}"></script>
    @parent
@stop

