<form action="#" class="form-horizontal" id="frmSystemSearch" name="frmSystemSearch"
      onsubmit="return false;">
    <br>
    <div class="form-group col-xs-12">
        <div class="input-group">
            <input type="text" required name="txtSearch" id="txtSearch" class="form-control" placeholder="Buscar ....">
            <span class="input-group-btn">
                <button type="submit" class="btn btn-primary" id="btnSearch">
                    &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-search"></i>&nbsp;&nbsp;&nbsp;&nbsp;
                </button>
            </span>
        </div>
        <br>
        <style>
            #search-filters input{
                display: none;
            }

            #search-filters label{
                border: 2px solid;
                border-radius:1.2em;
                line-height:2em;
                padding: 0 1em;
                margin: 0.5em;
                user-select: none;
                cursor: pointer;
                color: #333333;
                font-weight: normal;
            }

            #search-filters input:not(:checked) + label{
                border-color: transparent !important;
            }

        </style>

        <div class="text-center" id="search-filters">
            <input id="comboCheck" checked type="checkbox" name="allModel">
            <label for="comboCheck">Todos</label>
            @foreach(App\Models\Pattern::whereIn('id',[1,2,3,4,5,6,7,11])->get() as $pattern)
                <input type="checkbox" checked class="filterPattern" name="filterPattern[]" id="input_search_{{$pattern->id}}" value="{{ $pattern->id }}" >
                <label for="input_search_{{$pattern->id}}">{{ $pattern->name }}</label>
            @endforeach
        </div>

    </div>
    {{ csrf_field() }}
</form>