@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div class="page-title clearfix">
            <div class="row" >
                <div class="col-md-6 col-md-offset-3 col-xs-12">@include('search.new')</div>
            </div>
        </div>

        <div class="x_panel">
            <div class="col-md-10 col-md-offset-1">
                <ul class="messages" id="list_search"></ul>
            </div>
        </div>
    </div>


    <div style="display: none;">
        <li class="result" id="data_search">
            <i class="fa fa-square" id="model"></i>
            <div class="message_date">
                <h3 class="date text-info" id="day"></h3>
                <p class="month" id="month"></p>
            </div>
            <div class="message_wrapper">
                <a id="view" target="_blank"><h4 class="heading" id="name"></h4></a>
                <blockquote class="message" id="message"></blockquote>
                <br />
                <p class="url" id="url" style="display: none;">
                    <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                    <a id="link"><i class="fa fa-paperclip"></i> Descargar doc </a>
                </p>
            </div>
        </li>
    </div>
@stop

@section('script')
    @parent
    <script src="{{asset('js/search/search.js')}}"></script>
@stop