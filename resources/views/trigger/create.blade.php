@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div>
            <div class="page-title clearfix">
                <div style="float: left">
                    <h1>Generador Disparadores</h1>
                </div>
            </div>
            <div class="x_panel">
                <form action="#" class="form-horizontal form-label-left" id="frmTrigger" name="frmTrigger" onsubmit="return false;" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Modelo </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="txtModel" name="txtModel" type="text" class="form-control" value="">
                        </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-12 text-center">
                            <button type="button" id="actionTrigger" data-role="add" class="btn btn-lg btn-round btn-success">
                                <i class="fa fa-check" aria-hidden="true"> </i> Generar
                            </button>
                        </div>
                    </div>
                    {{ csrf_field() }}
                </form>
            </div>
            <div class="x_panel">
                <textarea name="txtResult" id="txtResult" class="form-control" style="height: 35em;"></textarea>
            </div>
        </div>
    </div>
@stop

@section('script')
    @parent
    <script src="{{asset('js/trigger/create.js')}}"></script>
@stop