<div class="row">
    <div class="form-group col-lg-4 col-xs-12">
        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Porcentaje <span class="required">*</span></label>
        <div class="col-sm-8 col-xs-12">
            <input type="number" min="0" step="1" id="txtAgreementPercentage" name="txtAgreementPercentage" class="form-control col-xs-12"/>
        </div>
    </div>
    <div class="form-group col-lg-4 col-xs-12">
        <div class="row">
            <div class="form-group col-xs-12">
                <label class="control-label col-lg-4 col-sm-3 col-xs-12">Estado <span class="required">*</span></label>
                <div class="col-sm-8 col-xs-12">
                    <select id="optAgreementPatternState" name="optAgreementPatternState" class="select2_single form-control"  >
                        <option value="">..Seleccione uno ..</option>
                        @foreach ($agreement_pattern_state as $key => $val)
                            <option value="{{ $val->id }}" >{{ $val->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row hidden" id="agreementSigningDate">
            <div class="form-group col-xs-12">
                <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fecha Firma</label>
                <div class="col-sm-8 col-xs-12">
                    <div class="input-group date myDatePicker2">
                        <input type='text' id="txtAgreementSigningDate" name="txtAgreementSigningDate" class="form-control" placeholder="aaaa-mm-dd" />
                        <span class="input-group-addon">
                           <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group col-lg-4 col-xs-12">
        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Justificación <span class="required">*</span></label>
        <div class="col-sm-8 col-xs-12">
            <textarea rows="3" id="txtAgreementJustification" name="txtAgreementJustification" class="form-control col-xs-12"></textarea>
        </div>
    </div>
</div>