<div class="row">
	<div class="form-group col-lg-4 col-xs-12">
		<label class="control-label col-lg-4 col-sm-3 col-xs-12">T Adiciones <span class="required">*</span></label>
		<div class="col-sm-8 col-xs-12">
			<input type="number" id="txtAgreementAdditionValue" name="txtAgreementAdditionValue" class="form-control col-xs-12"/>
		</div>
	</div>
	<div class="form-group col-lg-4 col-xs-12">
		<label class="control-label col-lg-4 col-sm-3 col-xs-12">Fecha Inicio</label>
		<div class="col-sm-8 col-xs-12">
			<div class="input-group date myDatePicker2">
				<input type='text' id="txtAgreementStartDate" name="txtAgreementStartDate" class="form-control" placeholder="aaaa-mm-dd" />
				<span class="input-group-addon">
				   <span class="glyphicon glyphicon-calendar"></span>
				</span>
			</div>
		</div>
	</div>
	<div class="form-group col-lg-4 col-xs-12">
		<label class="control-label col-lg-4 col-sm-3 col-xs-12">Fecha Fin</label>
		<div class="col-sm-8 col-xs-12">
			<div class="input-group date myDatePicker2">
				<input type='text' id="txtAgreementEndDate" name="txtAgreementEndDate" class="form-control" placeholder="aaaa-mm-dd" />
				<span class="input-group-addon">
				   <span class="glyphicon glyphicon-calendar"></span>
				</span>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group col-lg-4 col-xs-12">
		<label class="control-label col-lg-4 col-sm-3 col-xs-12">Plazo (dias) <span class="required">*</span></label>
		<div class="col-sm-8 col-xs-12">
			<input type="number" min="0" step="1" id="txtAgreementExecutionTerm" name="txtAgreementExecutionTerm" class="form-control col-xs-12"/>
		</div>
	</div>
	<div class="form-group col-lg-4 col-xs-12">
		<label class="control-label col-lg-4 col-sm-3 col-xs-12">T Prorrogas (dias) <span class="required">*</span></label>
		<div class="col-sm-8 col-xs-12">
			<input type="number" step="1" id="txtAgreementAdditionTerm" name="txtAgreementAdditionTerm" class="form-control col-xs-12"/>
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group col-lg-4 col-xs-12">
		<label class="control-label col-lg-4 col-sm-3 col-xs-12">Categoría <span class="required">*</span></label>
		<div class="col-sm-8 col-xs-12">
			<select id="optAgreementCategory" name="optAgreementCategory" class="select2_single form-control"  >
				<option value="">..Seleccione uno ..</option>
				@foreach ($agreement_category as $key => $val)
					<option value="{{ $val->id }}" >{{ $val->iso }} - {{ $val->name }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="form-group col-lg-4 col-xs-12">
		<label class="control-label col-lg-4 col-sm-3 col-xs-12">Tipo Contratacion <span class="required">*</span></label>
		<div class="col-sm-8 col-xs-12">
			<select id="optAgreementProcessType" name="optAgreementProcessType" class="select2_single form-control"  >
				<option value="">..Seleccione uno ..</option>
				@foreach ($agreement_process_type as $key => $val)
					<option value="{{ $val->id }}" >{{ $val->name }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="form-group col-lg-4 col-xs-12">
		<label class="control-label col-lg-4 col-sm-3 col-xs-12">Tipo <span class="required">*</span></label>
		<div class="col-sm-8 col-xs-12">
			<select id="optAgreementType" name="optAgreementType" class="select2_single form-control"  >
				<option value="">..Seleccione uno ..</option>
				@foreach ($agreement_type as $key => $val)
					<option value="{{ $val->id }}" >{{ $val->iso }} - {{ $val->name }}</option>
				@endforeach
			</select>
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group col-lg-4 col-xs-12">
		<label class="control-label col-lg-4 col-sm-3 col-xs-12">Naturaleza <span class="required">*</span></label>
		<div class="col-sm-8 col-xs-12">
			<select id="optAgreementNature" name="optAgreementNature" class="select2_single form-control"  >
				<option value="">..Seleccione uno ..</option>
				@foreach ($agreement_nature as $key => $val)
					<option value="{{ $val->id }}" >{{ $val->name }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="form-group col-lg-4 col-xs-12">
		<label class="control-label col-lg-4 col-sm-3 col-xs-12">Servicio <span class="required">*</span></label>
		<div class="col-sm-8 col-xs-12">
			<select id="optAgreementService" name="optAgreementService" class="select2_single form-control"  >
				<option value="">..Seleccione uno ..</option>
				@foreach ($agreement_service as $key => $val)
					<option value="{{ $val->id }}" >{{ $val->name }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="form-group col-lg-4 col-xs-12">
		<label class="control-label col-lg-4 col-sm-3 col-xs-12">Cobertura <span class="required">*</span></label>
		<div class="col-sm-8 col-xs-12">
			<select id="optAgreementCoverage" name="optAgreementCoverage" class="select2_single form-control"  >
				<option value="">..Seleccione uno ..</option>
				@foreach ($agreement_coverage as $key => $val)
					<option value="{{ $val->id }}" >{{ $val->iso }} - {{ $val->name }}</option>
				@endforeach
			</select>
		</div>
	</div>
</div>

<div class="row">
	<div class="form-group col-lg-4 col-xs-12">
		<label class="control-label col-lg-4 col-sm-3 col-xs-12">Departamento <span class="required">*</span></label>
		<div class="col-sm-8 col-xs-12">
			<select id="optAgreementDepartment" name="optAgreementDepartment" class="select2_single form-control"  >
				<option value="">..Seleccione uno ..</option>
				@foreach ($department as $key => $val)
					<option value="{{ $val->id }}" >{{ $val->name }}</option>
				@endforeach
			</select>
		</div>
	</div>

	<div class="form-group col-lg-8 col-xs-12">
		<label class="control-label col-lg-2 col-sm-3 col-xs-12">Ruta Externa </label>
		<div class="col-lg-10 col-sm-8 col-xs-12">
			<input type="text" id="txtAgreementExternalURL" name="txtAgreementExternalURL" placeholder="http://www.example.com" class="form-control col-xs-12"/>
		</div>
	</div>
</div>