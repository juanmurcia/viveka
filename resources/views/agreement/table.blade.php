@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h1>Contratos</h1>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Buscar</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li class="dropdown" style="visibility: hidden;">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                </li>
                                <li style="visibility: hidden;"><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <form action="#" class="form-horizontal form-label-left" id="frmAgreement"
                                  name="frmAgreement" onsubmit="return false;">
                                <div class="row">
                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Número</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <input type="text" id="txtAgreementNumber" name="txtAgreementNumber"
                                                   class="form-control col-md-7 col-xs-12"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Nombre</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <input type="text" id="txtAgreementName" name="txtAgreementName"
                                                   class="form-control col-md-7 col-xs-12"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Estado</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <select id="optAgreementPatternState" name="optAgreementPatternState"
                                                    class="select2_single form-control col-xs-12" >
                                                <option value="">..Seleccione uno ..</option>
                                                @foreach ($agreement_pattern_state as $key => $val)
                                                    <option value="{{ $val->id }}">{{ $val->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Tipo
                                            Contratacion</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <select id="optAgreementProcessType" name="optAgreementProcessType"
                                                    class="select2_single form-control col-xs-12" >
                                                <option value="">..Seleccione uno ..</option>
                                                @foreach ($agreement_process_type as $key => $val)
                                                    <option value="{{ $val->id }}">{{ $val->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Tipo</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <select id="optAgreementType" name="optAgreementType"
                                                    class="select2_single form-control col-xs-12" >
                                                <option value="">..Seleccione uno ..</option>
                                                @foreach ($agreement_type as $key => $val)
                                                    <option value="{{ $val->id }}">{{ $val->iso }}
                                                        - {{ $val->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Categoria</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <select id="optAgreementCategory" name="optAgreementCategory"
                                                    class="select2_single form-control col-xs-12" >
                                                <option value="">..Seleccione uno ..</option>
                                                @foreach ($agreement_category as $key => $val)
                                                    <option value="{{ $val->id }}">{{ $val->iso }}
                                                        - {{ $val->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Naturaleza</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <select id="optAgreementNature" name="optAgreementNature"
                                                    class="select2_single form-control col-xs-12" >
                                                <option value="">..Seleccione uno ..</option>
                                                @foreach ($agreement_nature as $key => $val)
                                                    <option value="{{ $val->id }}">{{ $val->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Servicio</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <select id="optAgreementService" name="optAgreementService"
                                                    class="select2_single form-control col-xs-12" >
                                                <option value="">..Seleccione uno ..</option>
                                                @foreach ($agreement_service as $key => $val)
                                                    <option value="{{ $val->id }}">{{ $val->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Cobertura</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <select id="optAgreementCoverage" name="optAgreementCoverage"
                                                    class="select2_single form-control col-xs-12" >
                                                <option value="">..Seleccione uno ..</option>
                                                @foreach ($agreement_coverage as $key => $val)
                                                    <option value="{{ $val->id }}">{{ $val->iso }}
                                                        - {{ $val->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Moneda</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <select id="optAgreementCurrency" name="optAgreementCurrency"
                                                    class="select2_single form-control col-xs-12" >
                                                <option value="">..Seleccione uno ..</option>
                                                @foreach ($currency as $key => $val)
                                                    <option value="{{ $val->id }}">{{ $val->iso }}
                                                        - {{ $val->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Departamento</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <select id="optAgreementDepartment" name="optAgreementDepartment"
                                                    class="select2_single form-control col-xs-12" >
                                                <option value="">..Seleccione uno ..</option>
                                                @foreach ($department as $key => $val)
                                                    <option value="{{ $val->id }}">{{ $val->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fecha Inicio
                                            Desde</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="input-group date myDatePicker2">
                                                <input type='text' id="txtAgreementStartDateFrom" name="txtAgreementStartDateFrom"
                                                       class="form-control col-xs-12" placeholder="aaaa-mm-dd"/>
                                                <span class="input-group-addon">
												   <span class="glyphicon glyphicon-calendar"></span>
												</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fecha Inicio
                                            Hasta</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="input-group date myDatePicker2">
                                                <input type='text' id="txtAgreementStartDateTo" name="txtAgreementStartDateTo"
                                                       class="form-control col-xs-12" placeholder="aaaa-mm-dd"/>
                                                <span class="input-group-addon">
												   <span class="glyphicon glyphicon-calendar"></span>
												</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fecha Fin Desde</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="input-group date myDatePicker2">
                                                <input type='text' id="txtAgreementEndDateFrom" name="txtAgreementEndDateFrom"
                                                       class="form-control col-xs-12" placeholder="aaaa-mm-dd"/>
                                                <span class="input-group-addon">
												   <span class="glyphicon glyphicon-calendar"></span>
												</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fecha Fin Hasta</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="input-group date myDatePicker2">
                                                <input type='text' id="txtAgreementEndDateTo" name="txtAgreementEndDateTo"
                                                       class="form-control col-xs-12" placeholder="aaaa-mm-dd"/>
                                                <span class="input-group-addon">
												   <span class="glyphicon glyphicon-calendar"></span>
												</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="ln_solid"></div>
                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                        <button type="submit" id="actionAgreement" data-role="add"
                                                class="btn btn-lg btn-round btn-primary">
                                            <i class="fa fa-check" aria-hidden="true"> </i> Consultar
                                        </button>
                                        <button class="btn btn-lg btn-round btn-default" type="reset">
                                            <i class="fa fa-eraser" aria-hidden="true"> </i> Limpiar
                                        </button>
                                        @can('agreement.create')
                                            <button type="button" id="btnAddAgreement" data-role="add"
                                                    class="btn btn-lg btn-round btn-success" data-toggle="modal"
                                                    data-target="#modAddAgreement">
                                                <i class="fa fa-plus" aria-hidden="true"> </i> Nuevo
                                            </button>
                                        @endcan()
                                    </div>
                                </div>
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="x_panel">
                        <button type="button" style="float: right" class="btn btn-default dropdown-toggle" data-toggle="modal" data-target="#modSelectColumn">
                            <i class="fa fa-sliders" aria-hidden="true"></i> Columnas
                        </button>
                        <table id="tbAgreement" class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Estado</th>
                                <th>Tipo Contratación</th>
                                <th>Tipo</th>
                                <th>Categoria</th>
                                <th>Naturaleza</th>
                                <th>Servicio</th>
                                <th>Cobertura</th>
                                <th>Moneda</th>
                                <th>Dpto</th>
                                <th>Inicio</th>
                                <th>Fin</th>
                                <th>Acción</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modAddAgreement" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title" id="titleModalAddAgreement">Crear Contrato</h3>
                        <div class="clearfix"></div>
                    </div>

                    <div class="modal-body">
                        <form action="#" data-parsley-validate class="form-horizontal form-label-left" id="frmAddAgreement" data-parsley-validate name="frmAddAgreement" onsubmit="return false;">
                            @include('agreement.new')
                            <div class="ln_solid"></div>
                            <div class="row">
                                <div class="form-group col-xs-12 text-center">
                                    <button type="submit" id="newAgreement" data-role="add"
                                            class="btn btn-lg btn-round btn-success">
                                        <i class="fa fa-check" aria-hidden="true"></i> Guardar
                                    </button>
                                </div>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    @parent
    <script src="{{ asset('js/agreement/table.js') }}"></script>
@stop