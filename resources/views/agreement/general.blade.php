<fieldset >
    <legend>General</legend>
    <form action="#" data-parsley-validate class="form-horizontal" id="frmAgreementBasics" name="frmAgreementBasics" onsubmit="return false;">
        @include('agreement.new')
        @can('agreement.edit')
            <div class="form-group">
                <div class="col-xs-12 text-center">
                    <button type="submit" id="actionAgreementBasics" class="btn btn-lg btn-round btn-success">
                        <i class="fa fa-check" aria-hidden="true"></i> Guardar
                    </button>
                </div>
            </div>
        @endcan
        {{ csrf_field() }}
    </form>
</fieldset>

<fieldset>
    <legend>Detalles</legend>
    <form action="#" class="form-horizontal" id="frmAgreementDetails" name="frmAgreementDetails" onsubmit="return false;">
        @include('agreement.details')
        @can('agreement.edit')
            <div class="form-group">
                <div class="col-xs-12 text-center">
                    <button type="button" id="actionAgreementDetails" class="btn btn-lg btn-round btn-success">
                        <i class="fa fa-check" aria-hidden="true"></i> Guardar
                    </button>
                </div>
            </div>
        @endcan
        {{ csrf_field() }}
    </form>
</fieldset>

<fieldset>
    <legend>Avance</legend>
    <form action="#" class="form-horizontal" id="frmAgreementProgress" name="frmAgreementProgress" onsubmit="return false;">
        @include('agreement.progress')
        @can('agreement.edit')
            <div class="form-group">
                <div class="col-xs-12 text-center">
                    <button type="button" id="actionAgreementProgress" class="btn btn-lg btn-round btn-success">
                        <i class="fa fa-check" aria-hidden="true"></i> Guardar
                    </button>
                </div>
            </div>
        @endcan
        {{ csrf_field() }}
    </form>
</fieldset>
<div class="row">
    <div class="col-lg-6 col-xs-12">
        @can('agreement_team.index')
            <fieldset id="frmService">
                <legend>Equipo de Trabajo</legend>
                <form action="#" data-parsley-validate class="form-horizontal" id="frmAddAgreementTeam" name="frmAddAgreementTeam" onsubmit="return false;">
                    <input type="hidden" name="txtAgreementTeamAgreementId" value="{{ $id }}">
                    <div class="row">
                        <div class="form-group col-lg-8 col-xs-12">
                            <label class="control-label col-lg-4 col-sm-3 col-xs-12">Nombre <span class="required">*</span></label>
                            <div class="col-sm-8 col-xs-12">
                                <select required id="txtAgreementTeamProfessionalId" name="txtAgreementTeamProfessionalId" class="form-control"></select>
                            </div>
                        </div>
                        <div class="form-group col-lg-4 col-xs-12 text-center">
                            @can('agreement_team.create')
                                <button type="submit" id="actionAgreementTeam" data-role="add" onclick="addAgreementTeam('new');" class="btn btn-lg btn-round btn-primary">
                                    <i class="fa fa-plus" aria-hidden="true"> </i> Agregar
                                </button>
                            @endcan()
                        </div>
                    </div>
                    {{ csrf_field() }}
                </form>
                <div class="row">
                    <div class="col-md-10 col-xs-12 col-md-offset-1 text-center">
                        <div class="x_content">
                            <table id="tbAgreementTeam" class="table table-striped table-hover dt-responsive nowrap" cellspacing="0" style="width: 100%;">
                                <thead>
                                <tr>
                                    <th>Documento</th>
                                    <th>Nombre</th>
                                    <th></th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </fieldset>
        @endcan
    </div>
    <div class="col-lg-6 col-xs-12">
        @can('agreement_role_cost.index')
            <fieldset>
                <legend>Costo por hora presupuestado</legend>
                <div class="row">
                    <form action="#" class="form-horizontal form-label-left" id="frmAgreementRoleCost" name="frmAgreementRoleCost" onsubmit="return false;">
                        <input type="hidden" name="txtAgreementRoleCostAgreementId" value="{{ $id }}">
                        <div class="form-group col-md-4 col-xs-12">
                            <label class="control-label text-left col-md-3 col-xs-12">Rol<span class="required">*</span></label>
                            <div class="col-md-9 col-xs-12">
                                <select class="form-control" id="optAgreementRoleCostRoleId" name="optAgreementRoleCostRoleId">
                                    <option value="">..Seleccione uno ..</option>
                                    @foreach (\Caffeinated\Shinobi\Models\Role::all() as $key => $val)
                                        <option value="{{ $val->id }}" >{{ $val->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-4 col-xs-12">
                            <label class="control-label text-left col-md-3 col-xs-12">Valor<span class="required">*</span></label>
                            <div class="col-md-9 col-xs-12">
                                <input class="form-control" type="text" id="txtAgreementRoleCostValue" name="txtAgreementRoleCostValue"/>
                            </div>
                        </div>
                        <div class="form-group col-md-4 col-xs-12 text-center">
                            @can('agreement_role_cost.create')
                                <button type="button" id="actionAgreementRoleCost"
                                        class="btn btn-lg btn-round btn-primary"
                                        onclick="addAgreementRoleCost()">
                                    <i class="fa fa-check" aria-hidden="true"></i> Agregar
                                </button>
                            @endcan()
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
                <div class="row">
                    <table id="tbAgreementRoleCost" class="table table-striped table-hover dt-responsive nowrap" cellspacing="0" style="width: 100%;">
                        <thead>
                        <tr>
                            <th>Rol</th>
                            <th>Valor</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </fieldset>
        @endcan()
    </div>
</div>

@section('script')
    <script src="{{ asset('js/agreement/form.js') }}"></script>
    @parent
@stop

