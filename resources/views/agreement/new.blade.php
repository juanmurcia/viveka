<div class="row">
	<div class="col-lg-6 col-xs-12">
		<div class="row">
			<div class="form-group col-xs-12">
				<label class="control-label col-lg-4 col-sm-3 col-xs-12">Número <span class="required">*</span></label>
				<div class="col-sm-8 col-xs-12">
					<input required type="text" id="txtAgreementNumber" name="txtAgreementNumber" class="form-control col-xs-12"/>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-xs-12">
				@can('company.create')
					<div class="col-lg-1 col-sm-1">
						<button type="button" id="btnCompany" class="btn btn-sm btn-default btn-outline" onclick="createCompany()">
							<i class="fa fa-plus" aria-hidden="true"></i>
						</button>
					</div>
				@endcan()
				<label class="control-label col-lg-3 col-sm-3 col-xs-12">Contratante <span class="required">*</span></label>
				<div class="col-sm-8 col-xs-12">
					<select required id="txtAgreementContractor" name="txtAgreementContractor"></select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-xs-12">
				<label class="control-label col-lg-4 col-sm-3 col-xs-12">Contratista <span class="required">*</span></label>
				<div class="col-sm-8 col-xs-12">
                    <select required id="txtAgreementEmployer" name="txtAgreementEmployer"></select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-xs-12">
				<label class="control-label col-lg-4 col-sm-3 col-xs-12">Valor <span class="required">*</span></label>
				<div class="col-sm-8 col-xs-12">
					<input required type="number" id="txtAgreementValue" name="txtAgreementValue" class="form-control col-xs-12"/>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-xs-12">
				<label class="control-label col-lg-4 col-sm-3 col-xs-12">Moneda <span class="required">*</span></label>
				<div class="col-sm-8 col-xs-12">
					<select required id="optAgreementCurrency" name="optAgreementCurrency" class="form-control"  >
						<option value="">..Seleccione uno ..</option>
						@foreach ( App\Models\Currency::all() as $key => $val)
							<option value="{{ $val->id }}">{{ $val->iso }} - {{ $val->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-6 col-xs-12">
		<div class="row">
			<div class="form-group col-xs-12">
				<label class="control-label col-lg-4 col-sm-3 col-xs-12">Nombre <span class="required">*</span></label>
				<div class="col-sm-8 col-xs-12">
					<input required type="text" id="txtAgreementName" name="txtAgreementName" class="form-control col-xs-12"/>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-xs-12">
				<label class="control-label col-lg-4 col-sm-3 col-xs-12">Objeto <span class="required">*</span></label>
				<div class="col-sm-8 col-xs-12">
					<textarea required rows="6" id="txtAgreementObject" name="txtAgreementObject" class="form-control col-xs-12" ></textarea>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-xs-12">
				<label class="control-label col-lg-4 col-sm-3 col-xs-12">Responsable <span class="required">*</span></label>
				<div class="col-sm-8 col-xs-12">
                    <select required id="txtAgreementResponsible" name="txtAgreementResponsible"></select>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-md-2 col-sm-3 col-xs-12">Multi-Proyecto ?</label>
		<div class="col-md-10 col-sm-8 col-xs-12">
			<input type="checkbox" id="txtSingleProject" class="flat" name="txtSingleProject">
		</div>
	</div>
</div>

@include('company.new')

@section('script')
	@parent
	<script src="{{ asset('js/agreement/new.js') }}"></script>
@stop