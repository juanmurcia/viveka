@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div>
            <div class="page-title clearfix">
                <div style="float: left">
                    <h1>Portafolio</h1>
                </div>
                @can('agreement.create')
                    <div class="row" style="float: right">
                        <div class="col-lg-4 col-sm-6 col-xs-12 agreementCreate">
                            <button type="button" id="btnAddAgreement" class="btn btn-lg btn-round btn-warning" title="Agregar nuevo proyecto"
                                    data-toggle="modal" data-target="#modAddAgreement">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                <i class="hidden-xs" style="margin-left: 16px">Nuevo Contrato</i>
                            </button>
                        </div>
                    </div>
                @endcan()
                @can('agreement.flow')
                    <div class="row" style="float: right">
                        <div class="col-lg-4 col-sm-6 col-xs-12 agreementCreate">
                            <a href="/flujo_caja" id="btnAddAgreement" class="btn btn-lg btn-round btn-default">
                                <i class="fa fa-calculator" aria-hidden="true"></i>
                                <i class="hidden-xs" style="margin-left: 16px">Flujo Caja</i>
                            </a>
                        </div>
                    </div>
                @endcan()
            </div>
            @include('agreement.list')
        </div>
    </div>
@endsection