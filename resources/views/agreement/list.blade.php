<div class="row">
    <div class="col-xs-12">
        <div id="listAgreement" style="display: flex; align-items: stretch; flex-wrap: wrap; justify-content: center"></div>
        @include('agreement.resume')
    </div>
</div>

<div class="modal fade" id="modAddAgreement" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" id="titleModal">Crear Contrato</h3>
                <div class="clearfix"></div>
            </div>

            <div class="modal-body">
                <form action="#" data-parsley-validate class="form-horizontal form-label-left" id="frmAddAgreement"
                      name="frmAddAgreement" onsubmit="return false;">
                    {{ csrf_field() }}
                    @include('agreement.new')
                    <div class="ln_solid"></div>
                    <div class="row">
                        <div class="form-group col-xs-12 text-center">
                            <button type="submit" id="newAgreement" data-role="add" class="btn btn-lg btn-round btn-success">
                                <i class="fa fa-check" aria-hidden="true"></i> Guardar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@section('script')
    @parent
    <script src="{{ asset('js/agreement/list.js') }}"></script>
@stop