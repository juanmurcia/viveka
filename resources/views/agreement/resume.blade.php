<div id="agreement-resume" style="display:none; max-width: 40em; margin: 5px" class="x_panel">
    <div class="x_title">
        @can('agreement.view')
            <a href="#" style="float:right; font-size:20px">
                <i class="fa fa-folder-open"></i>
            </a>
        @endcan
        <h3>Nombre Contrato</h3>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-md-9">
                <div class="col-md-6 col-xs-6">
                    <i class="fa fa-file" title="Contratista" style="width: 1.5em"></i><span class="agreement-resume-employer"></span>
                </div>
                <div class="col-md-6 col-xs-6">
                    <i class="fa fa-file-o" title="Contratante" style="width: 1.5em"></i><span class="agreement-resume-contractor"></span>
                </div>
                <div class="col-md-6 col-xs-6">
                    <i class="fa fa-calendar" style="width: 1.5em"></i>Inicio: <span class="agreement-resume-start"></span>
                </div>
                <div class="col-md-6 col-xs-6">
                    <i class="fa fa-calendar-o" style="width: 1.5em"></i>Fin: <span class="agreement-resume-end"></span>
                </div>
                <div class="col-md-12 col-xs-12">
                    <i class="fa fa-user" style="width: 1.5em"></i><span class="agreement-resume-responsible"></span>
                </div>
                <div class="col-md-1 col-xs-1">
                    <i class="fa fa-money" style="width: 1.5em"></i><h4> </h4>
                </div>
                <div class="col-md-11 col-xs-11">
                    <h3 class="text-left agreement-resume-value"></h3>
                </div>
            </div>
            <div class="col-md-3" style="float:right;">
                <input class="knobManual agreement-resume-percentage" data-width="100" data-height="100">
            </div>
        </div>
    </div>
</div>