<!-- Modal -->
<div class="modal fade" id="modCompany" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" id="titleModal"></h3>
                <div class="clearfix"></div>
            </div>
            <div class="modal-body">
                <!-- Smart Wizard -->
                <div class="form_wizard wizard_horizontal">
                    <ul class="wizard_steps">
                        <li>
                            <a href="javascript:void(0)" id="step1" onclick="nextStep(this);" data-step="1">
                                <span class="step_no">1</span>
                                <span class="step_descr">Paso 1<br/>
                                        <small>Documento Identidad</small>
                                    </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" id="step2" onclick="nextStep(this);" data-step="2">
                                <span class="step_no">2</span>
                                <span class="step_descr">Paso 2<br/>
                                        <small>Datos Generales</small>
                                    </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" id="step3" onclick="nextStep(this);" data-step="3">
                                <span class="step_no">3</span>
                                <span class="step_descr">Paso 3<br/>
                                        <small>Info Adicional</small>
                                    </span>
                            </a>
                        </li>
                    </ul>

                    <div id="step-1" class="step" width="100%">
                        <form action="#" class="form-horizontal">
                            <div class="row">
                                <div class="form-group col-xs-12">
                                    <label class="control-label col-sm-3 col-xs-12">Documento <span class="required">*</span></label>
                                    <div class="col-sm-8 col-xs-12">
                                        <input type="text" id="TBUStxtPersonId" name="TBUStxtPersonId" class="form-control col-xs-12  has-feedback-left"/>
                                        <span class="fa fa-search form-control-feedback left" aria-hidden="true"></span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div id="step-2" class="step" width="100%">
                        <form action="#" class="form-horizontal" id="frmPerson" name="frmPerson" onsubmit="return false;">
                            @include('person.form')
                            {{ csrf_field() }}
                        </form>
                    </div>

                    <div id="step-3" class="step" width="100%">
                        <form action="#" class="form-horizontal" id="frmCompany" name="frmCompany" onsubmit="return false;">
                            <div class="row">
                                <input type="hidden" name="txtPerson" id="txtPerson">
                                <input type="hidden" name="txtCompanyId" id="txtCompanyId">

                                <div class="form-group col-xs-12">
                                    <label class="control-label col-sm-3 col-xs-12">Representante Legal <span class="required">*</span></label>
                                    <div class="col-sm-8 col-xs-12">
                                        <select required id="txtAgentId" name="txtAgentId"></select>
                                    </div>
                                </div>

                                <div class="form-group col-xs-12">
                                    <label class="control-label col-sm-3 col-xs-12">Num. Empleados</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <input type="number" id="txtEmployees" name="txtEmployees"
                                               class="form-control col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group col-xs-12">
                                    <label class="control-label col-sm-3 col-xs-12">URL</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <input type="url" id="txtUrl" name="txtUrl"
                                               class="form-control col-xs-12">
                                    </div>
                                </div>
                                {{ csrf_field() }}
                            </div>
                        </form>
                    </div>
                </div>
                <!-- End SmartWizard Content -->
            </div>

            <div class="modal-footer">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 col-xs-12">
                        <div class="row">
                            <button id="pre" class="btn btn-primary" type="button"
                                    onclick="nextStep(this);" data-step="1">
                                <i class="fa fa-long-arrow-left" aria-hidden="true"></i> Atrás
                            </button>
                            <button type="button" id="actionCompany" data-role="add"
                                    class="btn btn-primary btn-success">
                                <i class="fa fa-check" aria-hidden="true"></i> Finalizar
                            </button>
                            <button id="next" class="btn btn-primary btn-primary" type="button"
                                    onclick="nextStep(this);" data-step="2">
                                Siguiente <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('script')
    @parent
    <script src="{{asset('js/company/new.js')}}"></script>
@stop