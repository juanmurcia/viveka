<@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div>
            <div class="page-title clearfix">
                <div style="float: left">
                    <h1>Empresas</h1>
                </div>
                @can('company.create')
                <div class="btn-group" style="float: right">
                    <button type="button" id="btnCompany" class="btn btn-lg btn-round btn-warning" onclick="createCompany()">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                </div>
                @endcan()
            </div>
            <div class="x_panel">
                <button type="button" style="float: right" class="btn btn-default dropdown-toggle" data-toggle="modal" data-target="#modSelectColumn">
                    <i class="fa fa-sliders" aria-hidden="true"></i> Columnas
                </button>

                <table id="tbCompanies" class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>T.Doc</th>
                            <th>Doc</th>
                            <th>Nombre</th>
                            <th>Doc. Representante</th>
                            <th>Nom. Representante</th>
                            <th># Empleados</th>
                            <th>URL</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    @include('company.new')
@stop

@section('script')
    @parent
    <script src="{{asset('js/person/form.js')}}"></script>
    <script src="{{asset('js/company/table.js')}}"></script>
@stop