@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="">
                <h1>Contrato <small id="company_name">Nombre de la empresa</small></h1>
                <input type="hidden" id="company_id" value="{{ $id }}">
                <input type="hidden" id="model_id" value="{{ $id }}">
                <input type="hidden" id="pattern_id" value="{{ $pattern_id }}">
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="x_panel">
                        <div class="crumbs">
                            <ul>
                                <li><a href="{{ route('home') }}">General</a></li>
                                <li><a href="{{ route('company.index') }}">Empresas</a></li>
                            </ul>
                        </div>
                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                            <ul id="myTab1" class="nav nav-tabs bar_tabs right" role="tablist">
                                <li role="presentation"class="">
                                    <a href="#general-tab" role="tab" id="general-tabb" data-toggle="tab"
                                       aria-controls="general" aria-expanded="false">
                                        <i class="fa fa-cogs"></i>
                                        <span class="hidden-xs"> General</span>
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#documents-tab" id="documents-tabb" role="tab" data-toggle="tab"
                                       aria-controls="general" aria-expanded="true">
                                        <i class="fa fa-files-o"></i>
                                        <span class="hidden-xs"> Documentos</span>
                                    </a>
                                </li>
                            </ul>
                            <div id="myTabContent2" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade" id="general-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <fieldset>
                                                <legend>Info. Persona</legend>
                                                <form data-parsley-validate action="#" class="form-horizontal" id="frmPerson" name="frmPerson" onsubmit="return false;">
                                                    @include('person.form')
                                                    {{ csrf_field() }}
                                                </form>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-6">
                                            <fieldset>
                                                <legend>Info. Empresa</legend>
                                                <form action="#" data-parsley-validate class="form-horizontal" id="frmCompany" name="frmCompany" onsubmit="return false;">
                                                    <div class="row">
                                                        <input type="hidden" name="txtPerson" id="txtPerson">
                                                        <input type="hidden" name="txtCompanyId" id="txtCompanyId">

                                                        <div class="form-group col-xs-12">
                                                            <label class="control-label col-sm-3 col-xs-12">Representante Legal <span class="required">*</span></label>
                                                            <div class="col-sm-8 col-xs-12">
                                                                <select required id="txtAgentId" name="txtAgentId"></select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label class="control-label col-sm-3 col-xs-12">Num. Empleados</label>
                                                            <div class="col-sm-8 col-xs-12">
                                                                <input type="number" id="txtEmployees" name="txtEmployees"
                                                                       class="form-control col-xs-12">
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label class="control-label col-sm-3 col-xs-12">URL</label>
                                                            <div class="col-sm-8 col-xs-12">
                                                                <input type="url" id="txtUrl" name="txtUrl"
                                                                       class="form-control col-xs-12">
                                                            </div>
                                                        </div>
                                                        {{ csrf_field() }}
                                                    </div>
                                                </form>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="row text-center">
                                        @can('company.edit')
                                            <button type="submit" id="actionCompany" data-role="add"
                                                    class="btn btn-primary btn-success">
                                                <i class="fa fa-check" aria-hidden="true"></i> Guardar
                                            </button>
                                        @endcan()
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="documents-tab">
                                    @include('document.tab')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@stop

@section('script')
    @parent
    <script src="{{asset('js/person/form.js')}}"></script>
    <script src="{{ asset('js/company/view.js') }}"></script>
@stop