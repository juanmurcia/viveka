<div class="modal fade" id="modUser" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" id="titleModalUser">Datos Usuario</h3>
                <div class="clearfix"></div>
            </div>

            <div class="modal-body">
                <form action="#" class="form-horizontal form-label-left" id="frmUserPerson" name="frmUserPerson" onsubmit="return false;">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Perfil <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select id="optRole" name="optRole" class="select2_single form-control" required="required">
                                <option value="">..Seleccione uno ..</option>
                                @foreach ($role as $key => $val)
                                    <option value="{{ $val->id }}">{{ $val->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div id="divPasswords">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Contraseña <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="password" id="txtPass" name="txtPass" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Confirmar <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="password" id="txtPassC" name="txtPassC" placeholder="Repita su contraseña" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="hidden" id="txtNomUser" name="txtNomUser" required="required" class="form-control col-md-7 col-xs-12">
                            <input type="hidden" id="txtEmailUser" name="txtEmailUser" required="required" class="form-control col-md-7 col-xs-12">
                            <input type="hidden" id="txtIdUser" name="txtIdUser" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-center">
                            <button type="button" id="actionUser" data-role="add" class="btn btn-lg btn-round btn-success">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </button>
                            <button class="btn btn-lg btn-round btn-default" type="reset">
                                <i class="fa fa-eraser" aria-hidden="true"></i>
                            </button>
                            <button class="btn btn-lg btn-round btn-danger" data-dismiss="modal" type="button">
                                <i class="fa fa-close" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>

@section('script')
    @parent
    <script src="{{asset('js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('js/additional-methods.min.js')}}"></script>
    <script src="{{ asset('js/user/new.js') }}"></script>
@stop