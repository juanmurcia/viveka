@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div>
            <div class="page-title">
                <div class="title_left">
                    <h1>Usuarios</h1>
                </div>
            </div>
            <div class="x_panel">
                <button type="button" style="float: right" class="btn btn-default dropdown-toggle" data-toggle="modal" data-target="#modSelectColumn">
                    <i class="fa fa-sliders" aria-hidden="true"></i> Columnas
                </button>
                <table id="tbUsers" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Estado</th>
                        <th>Fec Cre</th>
                        <th>Fec Mod</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modUser" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title" id="titleModal">Modificar Usuario</h3>
                    <div class="clearfix"></div>
                </div>

                <div class="modal-body">
                    <form action="#" class="form-horizontal form-label-left" id="frmUser" name="frmUser"
                          onsubmit="return false;">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Perfil <span
                                        class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="optRole" name="optRole" class="select2_single form-control"
                                        required="required">
                                    <option value="">..Seleccione uno ..</option>
                                    @foreach ($roles as $key => $val)
                                        <option value="{{ $val->id }}">{{ $val->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre <span
                                        class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" readonly id="txtNomUser" name="txtNomUser" required="required"
                                       class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Email <span
                                        class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="email" readonly id="txtEmailUser" name="txtEmailUser" required="required"
                                       class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-center">
                                <button type="button" id="actionUser" data-role="add"
                                        class="btn btn-lg btn-round btn-success">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </button>
                                <button class="btn btn-lg btn-round btn-default" type="reset">
                                    <i class="fa fa-eraser" aria-hidden="true"></i>
                                </button>
                                <button class="btn btn-lg btn-round btn-danger" data-dismiss="modal" type="button">
                                    <i class="fa fa-close" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    @parent
    <script src="{{asset('js/user/table.js')}}"></script>
@stop