<div class="modal fade" id="modPasswordUser" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Datos Usuario</h3>
                <div class="clearfix"></div>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group" >
                            <h3>Usuario: <br> <small>{{ auth()->user()->name }}</small></h3>
                            <h3>Correo: <br> <small>{{ auth()->user()->email }}</small></h3>
                            <h3>Perfíl: <br> <small>{{ auth()->user()->roles[0]->name }}</small></h3>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <form action="#" class="form-horizontal form-label-left" id="frmPasswordUser" name="frmPasswordUser"
                              onsubmit="return false;">
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Contraseña Actual<span
                                            class="required">*</span></label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <input type="password" id="txtPassPrev" name="txtPassPrev" required="required"
                                           class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Nueva
                                    Contraseña <span class="required">*</span></label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <input type="password" id="txtPassUserNew" name="txtPassUserNew"
                                           placeholder="Nueva contraseña" required="required"
                                           class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Confirmar
                                    <span class="required">*</span></label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <input type="password" id="txtPassUserConf" name="txtPassUserConf"
                                           placeholder="Repita su contraseña" required="required"
                                           class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-3 text-center">
                                    <button type="button" id="actionPasswordUser" data-role="add"
                                            class="btn btn-lg btn-round btn-success">
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </button>
                                    <button class="btn btn-lg btn-round btn-danger" data-dismiss="modal" type="button">
                                        <i class="fa fa-close" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('script')
    <script src="{{asset('js/user/password.js')}}"></script>
@stop