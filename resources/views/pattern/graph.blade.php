<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Avance</h2>
                <div id="calendarRange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                    <span></span> <b class="caret"></b>
                </div>
                <a class="pull-right" href="/pattern/{{ $pattern_id  }}/{{ $id }}/cost_time_excel" target="_blank" download>
                    <button class="btn btn-default btn-sm">Excel</button>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="text-center col-md-6">
                    <div id="percentage_line" style="height : 300% ;"></div>
                </div>
                <div class="text-center col-md-6">
                    <div id="executed_line" style="height : 300% ;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="display: none">
    <form action="#" class="form-horizontal form-label-left" id="frmRoleTime" name="frmRoleTime" onsubmit="return false;">
        <input type='hidden' name='txtRoleTimePattern' value="{{ $pattern_id }}">
        <input type='hidden' name='txtRoleTimeModel' value="{{ $id }}">
        <div id="frmRoleTimeFields"></div>
        {{ csrf_field() }}
    </form>
</div>

@section('script')
    <script src="{{ asset('js/pattern/graph.js') }}"></script>
    <script src="{{ asset('js/chart/function_chart.js') }}"></script>
    @parent
@stop
