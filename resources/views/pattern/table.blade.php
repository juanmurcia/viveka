@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div>
            <div class="page-title clearfix">
                <div style="float: left">
                    <h1>Modelos</h1>
                </div>
                @can('pattern.create')
                    <div class="btn-group" style="float: right">
                        <button type="button" id="btnPattern" class="btn btn-lg btn-round btn-warning"
                                data-toggle="modal" data-target="#modPattern">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </button>
                    </div>
                @endcan()
            </div>
            <div class="x_panel">
                <table id="tbPattern" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Modelo</th>
                        <th>Nombre</th>
                        <th>Estado</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modPattern" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title" id="titleModal">Crear Modelo</h3>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-body">
                    <form action="#" class="form-horizontal form-label-left" id="frmPattern" name="frmPattern"
                          onsubmit="return false;">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Modelo <span
                                        class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="txtModel" name="txtModel" required="required"
                                       class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre <span
                                        class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="txtName" name="txtName" required="required"
                                       class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-center">
                                <button type="button" id="actionPattern" data-role="add"
                                        class="btn btn-lg btn-round btn-success">
                                    <i class="fa fa-check" aria-hidden="true"> </i> Crear
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@stop

@section('script')
    @parent
    <script src="{{asset('js/pattern/table.js')}}"></script>
@stop