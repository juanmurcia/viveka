<fieldset >
    <legend>General</legend>
    <form action="#" data-parsley-validate class="form-horizontal" id="frmActivityBasics" name="frmActivityBasics" onsubmit="return false;">
        <div class="row">
            <div class="col-lg-10 col-xs-12 col-lg-offset-1">
                @include('activity.new')
            </div>
        </div>
        <div class="form-group">
            @can('activity.edit')
                <div class="col-xs-12 text-center">
                    <button type="submit" id="actionActivityBasics" class="btn btn-lg btn-round btn-success">
                        <i class="fa fa-check" aria-hidden="true"></i> Guardar
                    </button>
                </div>
            @endcan
        </div>
        {{ csrf_field() }}
    </form>
</fieldset>
<fieldset >
    <legend>Avance</legend>
    <form action="#" class="form-horizontal" id="frmActivityProgress" name="frmActivityProgress" onsubmit="return false;">
        <div class="row">
            <div class="col-lg-10 col-xs-12 col-lg-offset-1">
                @include('activity.progress')
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12 text-center">
                @can('activity.edit')
                    <button type="button" id="actionActivityProgress" class="btn btn-lg btn-round btn-success">
                        <i class="fa fa-check" aria-hidden="true"></i> Guardar
                    </button>
                @endcan
            </div>
        </div>
        {{ csrf_field() }}
    </form>
</fieldset>

@section('script')
    <script src="{{ asset('js/activity/form.js') }}"></script>
    @parent
@stop

