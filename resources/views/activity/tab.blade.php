<fieldset >
    <legend>Presupuesto</legend>
    <div id="patternTimeCostResume" style="display: flex; flex-wrap: wrap; justify-content: center; align-items: flex-start"></div>
    <div id="pattern-time-resume" style="display:none; position: relative; padding: 10px 15px">
        <div class="text-center">
            <input class="knobManual" data-width="100" data-height="100" value="40">
        </div>
        <div class="text-center"><strong class="pattern-time-resume-rol">Rol</strong></div>
        <div class="text-center"><span class="pattern-time-resume-executed">Ejecutado</span>h / <span class="pattern-time-resume-estimated">Estimado</span>h </div>
    </div>
    <div id="pattern-cost-resume" style="display:none; position: relative; padding: 10px 15px">
        <div class="text-center">
            <input class="knobManual" data-width="100" data-height="100" value="40">
        </div>
        <div class="text-center"><strong class="pattern-cost-resume-type">Tipo</strong></div>
        <div class="text-center">$<span class="pattern-cost-resume-executed">Ejecutado</span> / $<span class="pattern-cost-resume-estimated">Estimado</span> </div>
    </div>
</fieldset>
<fieldset >
    <legend>Actividades</legend>
    @can('activity.create')
        <div class="text-center">
            @can('activity.create')
                <button type="button" id="btnProject" onclick="createActivity({{ $pattern_id }},{{ $id }})" class="btn btn-lg btn-round btn-warning" title="Agregar actividad"
                        data-toggle="modal" data-target="#modActivity">
                    <i class="fa fa-plus" aria-hidden="true" style="margin-right: 16px"></i>Nueva Actividad
                </button>
            @endcan
        </div>
    @endcan()
    <div class="row">
        <div class="col-md-10 col-xs-12 col-md-offset-1 text-center">
            <div class="x_content">
                <table id="tbActivities" class="table table-striped table-hover" data-pattern-id="{{ $pattern_id }}" data-model-id="{{ $id }}" >
                    <thead>
                    <tr>
                        <th>Inicio</th>
                        <th>Fin</th>
                        <th>Actividad</th>
                        <th>Estado</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modActivityTime" role="dialog">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title" id="titleModalActivityTime">Agregar horas a Actividad</h3>
                    <div class="clearfix"></div>
                </div>

                <div class="modal-body">
                    <form action="#" class="form-horizontal form-label-left" id="frmActivityTime"
                          name="frmActivityTime" onsubmit="return false;">
                        <input type="hidden" id="txtActivityTimeActivityId" name="txtActivityTimeActivityId">
                        <input type="hidden" id="txtActivityTimePersonId" name="txtActivityTimePersonId">
                        <div class="row">
                            <div class="form-group col-xs-12">
                                <label class="control-label col-sm-4 col-xs-12">Hora Fin <span class="required">*</span></label>
                                <div class="col-sm-7 col-xs-12">
                                    <div class="input-group date myDatePicker3">
                                        <input type='text' id="txtActivityTimeEndDate" name="txtActivityTimeEndDate" class="form-control col-xs-12" placeholder="aaaa-mm-dd hh:mm"/>
                                        <span class="input-group-addon">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-xs-12">
                                <label class="control-label col-sm-4 col-xs-12">Horas ejecutadas <span class="required">*</span></label>
                                <div class="col-sm-7 col-xs-12">
                                    <input type="number" min="0.01" step="0.01" id="txtActivityTimeExecuted" name="txtActivityTimeExecuted" class="form-control col-xs-12"/>
                                </div>
                            </div>
                            <div class="form-group col-xs-12">
                                <label class="control-label col-sm-4 col-xs-12">Descripción <span class="required">*</span></label>
                                <div class="col-sm-7 col-xs-12">
                                    <textarea rows="3" id="txtActivityTimeDescription" name="txtActivityTimeDescription" class="form-control col-xs-12" ></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="row">
                            <div class="form-group col-xs-12 text-center">
                                <button type="button" id="actionActivityTime" class="btn btn-lg btn-round btn-success">
                                    <i class="fa fa-check" aria-hidden="true"></i> Aceptar
                                </button>
                            </div>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</fieldset>

<!-- Modal -->
<div class="modal fade" id="modActivityEstimatedTime" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" id="titleModalActivityEstimatedTime">Presupuesto</h3>
                <div class="clearfix"></div>
            </div>

            <div class="modal-body">
                <form action="#" class="form-horizontal form-label-left" id="frmActivityEstimatedTime"
                      name="frmActivityEstimatedTime" onsubmit="return false;">
                    <input type="hidden" id="txtActivityEstimatedTimeAgreementId" name="txtActivityEstimatedTimeAgreementId">
                    <input type="hidden" id="txtActivityEstimatedTimePatternId" name="txtActivityEstimatedTimePatternId" >
                    <input type="hidden" id="txtActivityEstimatedTimeModelId" name="txtActivityEstimatedTimeModelId" >
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label class="control-label col-sm-4 col-xs-12">Rol <span class="required">*</span></label>
                            <div class="col-sm-7 col-xs-12">
                                <select id="optActivityEstimatedTimeRoleId" name="optActivityEstimatedTimeRoleId" class="select2_single form-control col-xs-12"  >
                                    <option value="">..Seleccione uno ..</option>
                                    @foreach (\Caffeinated\Shinobi\Models\Role::all() as $key => $val)
                                        <option value="{{ $val->id }}" >{{ $val->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label class="control-label col-sm-4 col-xs-12">Horas estimadas <span class="required">*</span></label>
                            <div class="col-sm-7 col-xs-12">
                                <input type="number" min="0" step="1" id="txtActivityEstimatedTimeEstimated" name="txtActivityEstimatedTimeEstimated" class="form-control col-xs-12"/>
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label class="control-label col-sm-4 col-xs-12">Justificación <span class="required">*</span></label>
                            <div class="col-sm-7 col-xs-12">
                                <textarea rows="3" id="txtActivityEstimatedTimeJustification" name="txtActivityEstimatedTimeJustification" class="form-control col-xs-12" ></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="row">
                        <div class="form-group col-xs-12 text-center">
                            <button type="button" id="actionActivityEstimatedTime" class="btn btn-lg btn-round btn-success">
                                <i class="fa fa-check" aria-hidden="true"></i> Aceptar
                            </button>
                        </div>
                    </div>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modActivity" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" id="titleModalActivity">Nueva Actividad</h3>
                <div class="clearfix"></div>
            </div>

            <div class="modal-body">
                <form action="#" data-parsley-validate class="form-horizontal form-label-left" id="frmActivity"
                      name="frmActivity" onsubmit="return false;">
                    <input type="hidden" id="txtActivityPatternId" name="txtActivityPatternId" >
                    <input type="hidden" id="txtActivityModelId" name="txtActivityModelId" >
                    @include('activity.new')
                    <div class="ln_solid"></div>
                    <div class="row">
                        <div class="form-group col-xs-12 text-center">
                            <button type="submit" id="actionActivity" class="btn btn-lg btn-round btn-success">
                                <i class="fa fa-check" aria-hidden="true"></i> Aceptar
                            </button>
                        </div>
                    </div>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>

@include('activity.new-time')
@include('activity.new-cost')

@section('script')
    <script src="{{ asset('js/activity_executed_time/times.js') }}"></script>
    <script src="{{ asset('js/activity/tab.js') }}"></script>
    @parent
@stop