@extends('layouts.master')

@section('content')
	<div class="right_col" role="main">
		<div class="">
			<div class="">
				<h1>Actividad <small id="activity_name">Nombre del Actividad</small></h1>
				<input type="hidden" id="activity_id" value="{{ $id }}">
				<input type="hidden" id="model_id" value="{{ $id }}">
				<input type="hidden" id="pattern_id" value="{{ $pattern_id }}">
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="x_panel">
						<div class="crumbs">
							<ul>
								<li><a href="{{ route('home') }}">Dashboard</a></li>
								<li><a href="#" id="activity_parent_name">Padre</a></li>
								<li><a href="{{ route('activity.index') }}">Actividades</a></li>
							</ul>
						</div>
						<div class="" role="tabpanel" data-example-id="togglable-tabs">
							<ul id="myTab1" class="nav nav-tabs bar_tabs right" role="tablist">
								<li role="presentation" class="active">
									<a href="#general-tab" role="tab" id="general-tabb" data-toggle="tab"
									   aria-controls="general" aria-expanded="false">
										<i class="fa fa-cogs"></i>
										<span class="hidden-xs"> General</span>
									</a>
								</li>
								<li role="presentation" class="">
									<a href="#times-tab" role="tab" id="times-tabb" data-toggle="tab"
									   aria-controls="time" aria-expanded="false">
										<i class="fa fa-tasks"></i>
										<span class="hidden-xs"> Tiempos</span>
									</a>
								</li>
								<li role="presentation" class="">
									<a href="#costs-tab" role="tab" id="costs-tabb" data-toggle="tab"
									   aria-controls="cost" aria-expanded="false">
										<i class="fa fa-tasks"></i>
										<span class="hidden-xs"> Costos</span>
									</a>
								</li>
								<li role="presentation">
									<a href="#documents-tab" id="documents-tabb" role="tab" data-toggle="tab"
									   aria-controls="general" aria-expanded="true">
										<i class="fa fa-files-o"></i>
										<span class="hidden-xs"> Documentos</span>
									</a>
								</li>
							</ul>
							<div id="myTabContent2" class="tab-content">
								<div role="tabpanel" class="tab-pane fade active in" id="general-tab">
									@include('activity.general')
								</div>
								<div role="tabpanel" class="tab-pane fade" id="times-tab">
									@include('activity.times')
								</div>
								<div role="tabpanel" class="tab-pane fade" id="costs-tab">
									@include('activity.costs')
								</div>
								<div role="tabpanel" class="tab-pane fade" id="documents-tab">
									@include('document.tab')
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
@stop

@section('script')
	@parent
	<script src="{{ asset('js/activity/view.js') }}"></script>
@stop