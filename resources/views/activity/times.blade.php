<fieldset >
    <legend>Persupuestado</legend>
    <div id="activityTimes" style="display: flex; flex-wrap: wrap; justify-content: center; align-items: flex-start"></div>
    <div id="activity-time-resume" style="display:none; position: relative; padding: 10px 15px">
        <div class="text-center">
            <input class="knobManual" data-width="100" data-height="100" value="40">
        </div>
        <div class="text-center"><strong class="activity-time-resume-rol">Rol</strong></div>
        <div class="text-center"><span class="activity-time-resume-executed">Ejecutado</span>h / <span class="activity-time-resume-estimated">Estimado</span>h</div>
        <a href="#" class="activity-time-resume-edit" style="position: absolute; top:10px; left: 10px; color: #C5C7CB" type="button" title="Editar presupuesto" data-toggle="modal" data-target="#modActivityEstimatedTime">
            <i class="fa fa-wrench"></i>
        </a>
    </div>
    <div id="activityTimeActions" style="display: none">
        <div style="width: 130px;height: 120px; display: flex; align-items: center; justify-content: center">
            <button type="button" class="btn btn-lg btn-round btn-warning" title="Agregar presupuesto para rol"
                    data-toggle="modal" data-target="#modActivityEstimatedTime" onclick="setActivityEstimatedTime('','','')">
                <i class="fa fa-plus" aria-hidden="true"></i>
            </button>
        </div>
    </div>
</fieldset>
<fieldset >
    <legend>Ejecutado</legend>
    @can('activity_time.create')
        <div class="text-center">
            <button type="button" id="btnActivityExecutedTime" class="btn btn-lg btn-round btn-warning" title="Agregar horas a actividad"
                        data-toggle="modal" data-target="#modActivityExecutedTime">
                    <i class="fa fa-plus" aria-hidden="true" style="margin-right: 16px"></i>Agregar Horas
            </button>
        </div>
    @endcan()
    <table id="tbActivityTimes" class="table table-striped table-hover" >
        <thead>
        <tr>
            <th>Fecha Fin</th>
            <th>H. Ejecutadas</th>
            <th>Descripción</th>
            <th>Persona</th>
        </tr>
        </thead>
    </table>
</fieldset>

<div class="modal fade" id="modActivityEstimatedTime" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" id="titleModalActivityEstimatedTime">Presupuesto</h3>
                <div class="clearfix"></div>
            </div>

            <div class="modal-body">
                <form action="#" class="form-horizontal form-label-left" id="frmActivityEstimatedTime"
                      name="frmActivityEstimatedTime" onsubmit="return false;">
                    <input type="hidden" id="txtActivityEstimatedTimeActivityId" name="txtActivityEstimatedTimeActivityId">
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label class="control-label col-sm-4 col-xs-12">Rol <span class="required">*</span></label>
                            <div class="col-sm-7 col-xs-12">
                                <select id="optActivityEstimatedTimeRoleId" name="optActivityEstimatedTimeRoleId" class="select2_single form-control col-xs-12"  >
                                    <option value="">..Seleccione uno ..</option>
                                    @foreach (\Caffeinated\Shinobi\Models\Role::all() as $key => $val)
                                        <option value="{{ $val->id }}" >{{ $val->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label class="control-label col-sm-4 col-xs-12">Horas estimadas <span class="required">*</span></label>
                            <div class="col-sm-7 col-xs-12">
                                <input type="number" min="0" step="1" id="txtActivityEstimatedTimeEstimated" name="txtActivityEstimatedTimeEstimated" class="form-control col-xs-12"/>
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label class="control-label col-sm-4 col-xs-12">Justificación <span class="required">*</span></label>
                            <div class="col-sm-7 col-xs-12">
                                <textarea rows="3" id="txtActivityEstimatedTimeJustification" name="txtActivityEstimatedTimeJustification" class="form-control col-xs-12" ></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="row">
                        <div class="form-group col-xs-12 text-center">
                            <button type="button" id="actionActivityEstimatedTime" class="btn btn-lg btn-round btn-success">
                                <i class="fa fa-check" aria-hidden="true"></i> Aceptar
                            </button>
                        </div>
                    </div>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>

@include('activity.new-time')

@section('script')
    <script src="{{ asset('js/activity/times.js') }}"></script>
    @parent
@stop