<div class="modal fade" id="modActivityExecutedTime" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" id="titleModalActivityExecutedTime">Agregar horas a Actividad</h3>
                <div class="clearfix"></div>
            </div>

            <div class="modal-body">
                <form action="#" class="form-horizontal form-label-left" id="frmActivityExecutedTime" name="frmActivityExecutedTime" onsubmit="return false;">
                    <input type="hidden" id="txtActivityExecutedTimeActivityId" name="txtActivityExecutedTimeActivityId"/>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label class="control-label col-sm-4 col-xs-12">Hora Inicio <span class="required">*</span></label>
                            <div class="col-sm-7 col-xs-12">
                                <div class="input-group date myDatePicker3">
                                    <input type='text' onchange="setExecutedTime()" id="txtActivityExecutedTimeStartDate" name="txtActivityExecutedTimeStartDate" class="form-control col-xs-12" placeholder="aaaa-mm-dd hh:mm"/>
                                    <span class="input-group-addon">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label class="control-label col-sm-4 col-xs-12">Hora Fin <span class="required">*</span></label>
                            <div class="col-sm-7 col-xs-12">
                                <div class="input-group date myDatePicker3">
                                    <input type='text' onchange="setExecutedTime()" id="txtActivityExecutedTimeEndDate" name="txtActivityExecutedTimeEndDate" class="form-control col-xs-12" placeholder="aaaa-mm-dd hh:mm"/>
                                    <span class="input-group-addon">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label class="control-label col-sm-4 col-xs-12">Horas ejecutadas <span class="required">*</span></label>
                            <div class="col-sm-7 col-xs-12">
                                <input type="number" readonly min="0.01" step="0.01" id="txtActivityExecutedTimeExecuted" name="txtActivityExecutedTimeExecuted" class="form-control col-xs-12"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label class="control-label col-sm-4 col-xs-12">Descripción
                                <span class="required">*</span></label>
                            <div class="col-sm-7 col-xs-12">
                                <textarea rows="3" id="txtActivityExecutedTimeDescription" name="txtActivityExecutedTimeDescription" class="form-control col-xs-12"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12 text-center">
                            <button type="button" id="actionActivityExecutedTime" class="btn btn-lg btn-round btn-success">
                                <i class="fa fa-check" aria-hidden="true"></i> Aceptar
                            </button>
                        </div>
                    </div>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>

@section('script')
    @parent
    <script src="{{ asset('js/activity/new-time.js') }}"></script>
@stop