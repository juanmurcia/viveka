<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-lg-4 col-sm-6 col-xs-12 activityCreate">
                @can('activity.create')
                    <button type="button" id="btnActivity" class="btn btn-lg btn-round btn-warning" title="Agregar nuevo proyecto"
                            data-toggle="modal" data-target="#modActivity">
                        <i class="fa fa-plus" aria-hidden="true" style="margin-right: 16px"></i>Nuevo Actividad
                    </button>
                @endcan
            </div>
        </div>
        <div class="row" id="listActivity">
        </div>
        @include('activity.resume')
    </div>
</div>

<div class="modal fade" id="modActivity" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" id="titleModal">Crear Actividad</h3>
                <div class="clearfix"></div>
            </div>

            <div class="modal-body">
                <form action="#" data-parsley-validate class="form-horizontal form-label-left" id="frmActivity" name="frmActivity"
                      onsubmit="return false;">
                    @include('activity.new')
                    <div id="btnActivity">
                        <div class="ln_solid"></div>
                        <div class="text-center">
                            @can('activity.create')
                                <button type="submit" id="newActivity" class="btn btn-lg btn-round btn-success">
                                    <i class="fa fa-check" aria-hidden="true"></i> Guardar
                                </button>
                            @endcan
                        </div>
                    </div>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>

@include('activity.new-time')

@section('script')
    @parent
    <script src="{{ asset('js/activity/list.js') }}"></script>
@stop