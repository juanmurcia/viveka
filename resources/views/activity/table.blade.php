@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div class="page-title">
            <div class="title_left">
                <h1>Actividades</h1>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Buscar</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li class="dropdown" style="visibility: hidden;">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            </li>
                            <li style="visibility: hidden;"><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form action="#" class="form-horizontal form-label-left" id="frmActivity"
                              name="frmActivity" onsubmit="return false;">
                            <div class="row">
                                <div class="form-group col-sm-6 col-xs-12">
                                    <label class="control-label col-lg-4 col-sm-3 col-xs-12">Nombre</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <input type="text" id="txtActivityName" name="txtActivityName"
                                               class="form-control col-md-7 col-xs-12"/>
                                    </div>
                                </div>
                                
                                <div class="form-group col-sm-6 col-xs-12">
                                    <label class="control-label col-lg-4 col-sm-3 col-xs-12">Estado</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <select id="optActivityPatternState" name="optActivityPatternState"
                                                class="select2_single form-control col-xs-12">
                                            <option value="">..Seleccione uno ..</option>
                                            @foreach ($activity_pattern_state as $key => $val)
                                                <option value="{{ $val->id }}">{{ $val->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="form-group col-sm-6 col-xs-12">
                                    <label class="control-label col-lg-4 col-sm-3 col-xs-12">Nombre del Modelo</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <input type="text" id="TBUStxtActivityModel"
                                               name="TBUStxtActivityModel"
                                               class="form-control col-xs-12  has-feedback-left"/>
                                        <span class="fa fa-search form-control-feedback left"
                                              aria-hidden="true"></span>
                                        <input type="hidden" id="txtActivityModel"
                                               name="txtActivityModel"/>
                                    </div>
                                </div>
                                <div class="form-group col-sm-6 col-xs-12">
                                    <label class="control-label col-lg-4 col-sm-3 col-xs-12">Responsable</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <select id="txtActivityResponsible" name="txtActivityResponsible"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="form-group col-sm-6 col-xs-12">
                                    <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fecha Inicio
                                        Desde</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <div class="input-group date myDatePicker2">
                                            <input type='text' id="txtAgreementStartDateFrom"
                                                   name="txtActivityStartDateFrom"
                                                   class="form-control col-xs-12" placeholder="aaaa-mm-dd"/>
                                            <span class="input-group-addon">
                                               <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-sm-6 col-xs-12">
                                    <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fecha Inicio
                                        Hasta</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <div class="input-group date myDatePicker2">
                                            <input type='text' id="txtAgreementStartDateTo"
                                                   name="txtActivityStartDateTo"
                                                   class="form-control col-xs-12" placeholder="aaaa-mm-dd"/>
                                            <span class="input-group-addon">
                                               <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                <div class="form-group col-sm-6 col-xs-12">
                                    <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fecha Fin
                                        Desde</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <div class="input-group date myDatePicker2">
                                            <input type='text' id="txtAgreementEndDateFrom"
                                                   name="txtActivityEndDateFrom"
                                                   class="form-control col-xs-12" placeholder="aaaa-mm-dd"/>
                                            <span class="input-group-addon">
                                               <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-sm-6 col-xs-12">
                                    <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fecha Fin
                                        Hasta</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <div class="input-group date myDatePicker2">
                                            <input type='text' id="txtAgreementEndDateTo"
                                                   name="txtActivityEndDateTo"
                                                   class="form-control col-xs-12" placeholder="aaaa-mm-dd"/>
                                            <span class="input-group-addon">
                                               <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="ln_solid"></div>
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <button type="submit" id="actionActivity" data-role="add"
                                            class="btn btn-lg btn-round btn-primary">
                                        <i class="fa fa-check" aria-hidden="true"> </i> Consultar
                                    </button>
                                    <button class="btn btn-lg btn-round btn-default" type="reset">
                                        <i class="fa fa-eraser" aria-hidden="true"> </i> Limpiar
                                    </button>
                                </div>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="x_panel">
                    <table id="tbActivities" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Modelo</th>
                            <th>Estado</th>
                            <th>Nombre</th>
                            <th>Descripión</th>
                            <th>% Avance</th>
                            <th>Fec Inicio</th>
                            <th>Fec Fin</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modActivityTime" role="dialog">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title" id="titleModalActivityTime">Agregar horas a Actividad</h3>
                    <div class="clearfix"></div>
                </div>

                <div class="modal-body">
                    <form action="#" class="form-horizontal form-label-left" id="frmActivityTime"
                          name="frmActivityTime" onsubmit="return false;">
                        <input type="hidden" id="txtActivityTimeActivityId" name="txtActivityTimeActivityId">
                        <input type="hidden" id="txtActivityTimePersonId" name="txtActivityTimePersonId">
                        <div class="row">
                            <div class="form-group col-xs-12">
                                <label class="control-label col-sm-4 col-xs-12">Hora Fin <span class="required">*</span></label>
                                <div class="col-sm-7 col-xs-12">
                                    <div class="input-group date myDatePicker3">
                                        <input type='text' id="txtActivityTimeEndDate" name="txtActivityTimeEndDate" class="form-control col-xs-12" placeholder="aaaa-mm-dd hh:mm"/>
                                        <span class="input-group-addon">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-xs-12">
                                <label class="control-label col-sm-4 col-xs-12">Horas ejecutadas <span class="required">*</span></label>
                                <div class="col-sm-7 col-xs-12">
                                    <input type="number" min="0.01" step="0.01" id="txtActivityTimeExecuted" name="txtActivityTimeExecuted" class="form-control col-xs-12"/>
                                </div>
                            </div>
                            <div class="form-group col-xs-12">
                                <label class="control-label col-sm-4 col-xs-12">Descripción <span class="required">*</span></label>
                                <div class="col-sm-7 col-xs-12">
                                    <textarea rows="3" id="txtActivityTimeDescription" name="txtActivityTimeDescription" class="form-control col-xs-12" ></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="row">
                            <div class="form-group col-xs-12 text-center">
                                <button type="button" id="actionActivityTime" class="btn btn-lg btn-round btn-success">
                                    <i class="fa fa-check" aria-hidden="true"></i> Aceptar
                                </button>
                            </div>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    @parent
    <script src="{{ asset('js/activity/table.js') }}"></script>
@stop