<fieldset >
    <legend>Persupuestado</legend>
    <div id="activityCosts" style="display: flex; flex-wrap: wrap; justify-content: center; align-items: flex-start"></div>
    <div id="activity-cost-resume" style="display:none; position: relative; padding: 10px 15px">
        <div class="text-center">
            <input class="knobManual" data-width="100" data-height="100" value="40">
        </div>
        <div class="text-center"><strong class="activity-cost-resume-type">Tipo</strong></div>
        <div class="text-center">$<span class="activity-cost-resume-executed">Ejecutado</span> / $<span class="activity-cost-resume-estimated">Estimado</span></div>
        <a href="#" class="activity-cost-resume-edit" style="position: absolute; top:10px; left: 10px; color: #C5C7CB" type="button" title="Editar presupuesto" data-toggle="modal" data-target="#modActivityEstimatedCost">
            <i class="fa fa-wrench"></i>
        </a>
    </div>
    <div id="activityCostActions" style="display: none">
        <div style="width: 130px;height: 120px; display: flex; align-items: center; justify-content: center">
            <button type="button" class="btn btn-lg btn-round btn-warning" title="Agregar presupuesto para tipo de costo"
                    data-toggle="modal" data-target="#modActivityEstimatedCost" onclick="setActivityEstimatedCost('','','')">
                <i class="fa fa-plus" aria-hidden="true"></i>
            </button>
        </div>
    </div>
</fieldset>
<fieldset >
    <legend>Ejecutado</legend>
    @can('activityExecutedCost.create')
        <div class="text-center">
            <button type="button" id="btnActivityExecutedCost" class="btn btn-lg btn-round btn-warning" title="Agregar costos a actividad"
                    data-toggle="modal" data-target="#modActivityExecutedCost">
                <i class="fa fa-plus" aria-hidden="true" style="margin-right: 16px"></i>Agregar Costos
            </button>
        </div>
    @endcan()
    <table id="tbActivityCosts" class="table table-striped table-hover" >
        <thead>
        <tr>
            <th>Fecha</th>
            <th>Categoría</th>
            <th>Sub-Categoria</th>
            <th>Costo</th>
            <th>Descripción</th>
            <th>Persona</th>
            <th></th>
        </tr>
        </thead>
    </table>
</fieldset>

<div class="modal fade" id="modActivityEstimatedCost" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" id="titleModalActivityEstimatedCost">Presupuesto</h3>
                <div class="clearfix"></div>
            </div>

            <div class="modal-body">
                <form action="#" class="form-horizontal form-label-left" id="frmActivityEstimatedCost"
                      name="frmActivityEstimatedCost" onsubmit="return false;">
                    <input type="hidden" id="txtActivityEstimatedCostActivityId" name="txtActivityEstimatedCostActivityId">
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label class="control-label col-sm-4 col-xs-12">Categoría <span class="required">*</span></label>
                            <div class="col-sm-7 col-xs-12">
                                <select id="optActivityExecutedCostActivityCostCategoryId" name="optActivityExecutedCostActivityCostCategoryId" class="select2_single form-control col-xs-12"  >
                                    <option value="">..Seleccione uno ..</option>
                                    @foreach (\App\Models\ActivityCostCategory::with(['activity_cost_subcategory'])->where('active',true)->get() as $key => $val)
                                        <option value="{{ $val->id }}" >{{ $val->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label class="control-label col-sm-4 col-xs-12">Costo estimado <span class="required">*</span></label>
                            <div class="col-sm-7 col-xs-12">
                                <input type="number" min="0" step="1" id="txtActivityEstimatedCostEstimated" name="txtActivityEstimatedCostEstimated" class="form-control col-xs-12"/>
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label class="control-label col-sm-4 col-xs-12">Justificación <span class="required">*</span></label>
                            <div class="col-sm-7 col-xs-12">
                                <textarea rows="3" id="txtActivityEstimatedCostJustification" name="txtActivityEstimatedCostJustification" class="form-control col-xs-12" ></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="row">
                        <div class="form-group col-xs-12 text-center">
                            <button type="button" id="actionActivityEstimatedCost" class="btn btn-lg btn-round btn-success">
                                <i class="fa fa-check" aria-hidden="true"></i> Aceptar
                            </button>
                        </div>
                    </div>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modDocActivityExecutedCost" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" id="titleModalDocument">Documentos Cargados</h3>
                <div class="clearfix"></div>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <div class="x_content">
                            <table id="tbDocuments" width="100%" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Documento</th>
                                    <th>Nombre</th>
                                    <th>Creador</th>
                                    <th>Fec Cre</th>
                                    <th>Fec Mod</th>
                                    <th>Palabras Clave</th>
                                    <th>Comentarios</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('activity.new-cost')

@section('script')
    @parent
    <script src="{{ asset('js/activity/costs.js') }}"></script>
@stop