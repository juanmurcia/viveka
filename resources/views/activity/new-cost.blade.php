<div class="modal fade" id="modActivityExecutedCost" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" id="titleModalActivityExecutedCost">Agregar Costos a Actividad</h3>
                <div class="clearfix"></div>
            </div>

            <div class="modal-body">
                <form action="#" class="form-horizontal form-label-left" id="frmActivityExecutedCost" name="frmActivityExecutedCost" onsubmit="return false;">
                    <input type="hidden" id="txtActivityExecutedCostActivityId" name="txtActivityExecutedCostActivityId"/>
                    <input type="hidden" id="txtActivityExecutedCostPersonId" name="txtActivityExecutedCostPersonId"/>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label class="control-label col-sm-4 col-xs-12">Persona <span
                                    class="required">*</span></label>
                            <div class="col-sm-7 col-xs-12">
                                <input type="text" id="TBUStxtActivityExecutedCostPersonId" name="TBUStxtActivityExecutedCostPersonId" class="form-control col-xs-12 has-feedback-left"/>
                                <span class="fa fa-search form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <div class="form-group col-xs-12">
                                <label class="control-label col-sm-4 col-xs-12">Categoría <span class="required">*</span></label>
                                <div class="col-sm-7 col-xs-12">
                                    <select onchange="load_subcategory();" id="optActivityExecutedCostActivityCostCategoryN" name="optActivityExecutedCostActivityCostCategoryN" class="select2_single form-control col-xs-12"  >
                                        <option value="">..Seleccione uno ..</option>
                                        @foreach (\App\Models\ActivityCostCategory::with(['activity_cost_subcategory'])->where('active',true)->get() as $key => $val)
                                        <option id="cat{{ $val->id }}" data-subcategory="{{$val->activity_cost_subcategory}}" value="{{ $val->id }}" >{{ $val->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-xs-12">
                                <label class="control-label col-sm-4 col-xs-12">Sub Categoría <span class="required">*</span></label>
                                <div class="col-sm-7 col-xs-12">
                                    <select id="optActivityExecutedCostActivityCostSubCategoryId" name="optActivityExecutedCostActivityCostSubCategoryId" class="select2_single form-control col-xs-12"  >
                                        <option value="">..Seleccione uno ..</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label class="control-label col-sm-4 col-xs-12">Fecha <span class="required">*</span></label>
                            <div class="col-sm-7 col-xs-12">
                                <div class="input-group date myDatePicker3">
                                    <input type='text' id="txtActivityExecutedCostEndDate" name="txtActivityExecutedCostEndDate" class="form-control col-xs-12" placeholder="aaaa-mm-dd hh:mm"/>
                                    <span class="input-group-addon">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label class="control-label col-sm-4 col-xs-12">Costo <span class="required">*</span></label>
                            <div class="col-sm-7 col-xs-12">
                                <input type="number" min="0.01" step="0.01" id="txtActivityExecutedCostExecuted" name="txtActivityExecutedCostExecuted" class="form-control col-xs-12"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label class="control-label col-sm-4 col-xs-12">Descripción
                                <span class="required">*</span></label>
                            <div class="col-sm-7 col-xs-12">
                                <textarea rows="3" id="txtActivityExecutedCostDescription" name="txtActivityExecutedCostDescription" class="form-control col-xs-12"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-xs-12 text-center">
                            <button type="button" id="actionActivityExecutedCost" class="btn btn-lg btn-round btn-success">
                                <i class="fa fa-check" aria-hidden="true"></i> Aceptar
                            </button>
                        </div>
                    </div>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>

@section('script')
    @parent
    <script src="{{ asset('js/activity/new-cost.js') }}"></script>
@stop