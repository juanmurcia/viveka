@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
		<div>
			<div class="page-title">
				<div class="title_left">
					<h1>Roles de Usuario</h1>
				</div>
			</div>
			<div class="x_panel">
				<table id="tbRoles" class="table table-striped table-hover">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Slug</th>
							<th>Descripción</th>
							<th>Special</th>
							<th>Fec Cre</th>
							<th></th>
						</tr>
					</thead>
				</table>
			</div>
	    </div>
	</div>    

	<!-- Modal -->
	<div class="modal fade" id="modRole" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h3 class="modal-title" id="titleModal">Asignar Permisos</h3>
				  <div class="clearfix"></div>
				</div>
				<div class="modal-body">
					<form action="#" class="form-horizontal form-label-left" id="frmRole" name="frmRole" onsubmit="return false;">
						<div class="form-group">
							@foreach ($permissions as $key => $model)
								<fieldset >
									<legend><stron>{{ $key }}</stron></legend>

									@foreach ($model as $key => $val)
										<div class="col-md-4 col-sm-4 col-xs-12">
											<div class="checkbox">
												<label>
													<input type="checkbox" id="chkPermission_{{ $val->id }}" name="chkPermission[]" value="{{ $val->id }}">{{ $val->name }} <small>({{ $val->description }})</small>
												</label>
											</div>
										</div>
									@endforeach
								</fieldset>
								<br>
							@endforeach
					    </div>
                      	<div class="ln_solid"></div>
						<div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-center">
								<button type="button" id="actionRole" data-role="add" class="btn btn-lg btn-round btn-success">
									<i class="fa fa-check" aria-hidden="true"></i>
								</button>
								<button class="btn btn-lg btn-round btn-default" type="reset">
									<i class="fa fa-eraser" aria-hidden="true"></i>
								</button>
								<button class="btn btn-lg btn-round btn-danger" data-dismiss="modal" type="button">
									<i class="fa fa-close" aria-hidden="true"></i>
								</button>
							</div>
						</div>
						{{ csrf_field() }}
					</form>
				</div>
			</div>
		</div>
	</div>  
@stop

@section('script')
	@parent
	<script src="{{asset('js/role/table.js')}}"></script>
@stop