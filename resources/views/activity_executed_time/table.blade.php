@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h1>Horas de Actividades</h1>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Buscar</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li class="dropdown" style="visibility: hidden;">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                </li>
                                <li style="visibility: hidden;"><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <form action="#" class="form-horizontal form-label-left" id="frmSearchActivityExecutedTimes"
                                  name="frmAgreement" onsubmit="return false;">
                                <div class="row">
                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Modelo</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <select id="optActivityExecutedTimePattern" name="optActivityExecutedTimePattern"
                                                    class="select2_single form-control col-xs-12" >
                                                <option value="">..Seleccione uno ..</option>
                                                <option value="1">Contrato</option>
                                                <option value="2">Proyecto</option>
                                                <option value="3">Producto</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fecha Fin Desde</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="input-group date myDatePicker2">
                                                <input type='text' id="txtActivityExecutedTimeEndDateFrom" name="txtActivityExecutedTimeEndDateFrom"
                                                       class="form-control col-xs-12" placeholder="aaaa-mm-dd"/>
                                                <span class="input-group-addon">
												   <span class="glyphicon glyphicon-calendar"></span>
												</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                        <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fecha Fin Hasta</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="input-group date myDatePicker2">
                                                <input type='text' id="txtActivityExecutedTimeEndDateTo" name="txtActivityExecutedTimeEndDateTo"
                                                       class="form-control col-xs-12" placeholder="aaaa-mm-dd"/>
                                                <span class="input-group-addon">
												   <span class="glyphicon glyphicon-calendar"></span>
												</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="ln_solid"></div>
                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                        <button type="submit" id="actionActivityExecutedTime" data-role="add"
                                                class="btn btn-lg btn-round btn-primary">
                                            <i class="fa fa-check" aria-hidden="true"> </i> Consultar
                                        </button>
                                        <button class="btn btn-lg btn-round btn-default" type="reset">
                                            <i class="fa fa-eraser" aria-hidden="true"> </i> Limpiar
                                        </button>
                                        @can('activity_executed_time.create')
                                            <button type="button" id="btnAddActivityExecutedTime" data-role="add"
                                                    class="btn btn-lg btn-round btn-success" data-toggle="modal"
                                                    data-target="#modActivityExecutedTime">
                                                <i class="fa fa-plus" aria-hidden="true"> </i> Nuevo
                                            </button>
                                        @endcan()
                                    </div>
                                </div>
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="x_panel">
                        <table id="tbActivityExecutedTime" class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>Fecha Fin</th>
                                <th>Horas</th>
                                <th>Persona</th>
                                <th>Descripción</th>
                                <th>Actividad</th>
                                <th>Modelo</th>
                                <th>Nombre Modelo</th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modActivityExecutedTime" role="dialog">
            <div class="modal-dialog modal-md">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title" id="titleModalActivityExecutedTime">Agregar horas a Actividad</h3>
                        <div class="clearfix"></div>
                    </div>

                    <div class="modal-body">
                        <form action="#" class="form-horizontal form-label-left" id="frmActivityExecutedTime"
                              name="frmActivityExecutedTime" onsubmit="return false;">
                            <input type="hidden" id="txtActivityExecutedTimePersonId" name="txtActivityExecutedTimePersonId">
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group col-xs-12">
                                        <label class="control-label col-xs-12">Contrato <span class="required">*</span></label>
                                        <select id="optActivityAgreement" name="optActivityAgreement"
                                                class="select2_single form-control col-xs-12" >
                                            <option value="">..Seleccione uno ..</option>
                                            @foreach($agreements as $key => $value)
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                        <form>
                                            <input type="hidden" name="">
                                            @csrf
                                        </form>
                                    </div>
                                    <div class="form-group col-xs-12">
                                        <label class="control-label col-xs-12">Proyecto <span class="required">*</span></label>
                                        <select id="optActivityProject" name="optActivityProject"
                                                class="select2_single form-control col-xs-12" >
                                            <option value="">..Seleccione uno ..</option>
                                            <option value="1">1</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-xs-12">
                                        <label class="control-label col-xs-12">Producto <span class="required">*</span></label>
                                        <select id="optActivityProduct" name="optActivityProduct"
                                                class="select2_single form-control col-xs-12" >
                                            <option value="">..Seleccione uno ..</option>
                                            <option value="1">1</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">

                                </div>


                                <div class="form-group col-xs-12">
                                    <label class="control-label col-sm-4 col-xs-12">Actividad <span class="required">*</span></label>
                                    <div class="col-sm-7 col-xs-12">
                                        <input type="text" id="TBUStxtActivityExecutedTimeActivityId" name="TBUStxtActivityExecutedTimeActivityId" class="form-control col-xs-12  has-feedback-left"/>
                                        <span class="fa fa-search form-control-feedback left" aria-hidden="true"></span>
                                        <input type="hidden" id="txtActivityExecutedTimeActivityId" name="txtActivityExecutedTimeActivityId"/>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12">
                                    <label class="control-label col-sm-4 col-xs-12">Hora Fin <span class="required">*</span></label>
                                    <div class="col-sm-7 col-xs-12">
                                        <div class="input-group date myDatePicker3">
                                            <input type='text' id="txtActivityExecutedTimeEndDate" name="txtActivityExecutedTimeEndDate" class="form-control col-xs-12" placeholder="aaaa-mm-dd hh:mm"/>
                                            <span class="input-group-addon">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12">
                                    <label class="control-label col-sm-4 col-xs-12">Horas ejecutadas <span class="required">*</span></label>
                                    <div class="col-sm-7 col-xs-12">
                                        <input type="number" min="0.01" step="0.01" id="txtActivityExecutedTimeExecuted" name="txtActivityExecutedTimeExecuted" class="form-control col-xs-12"/>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12">
                                    <label class="control-label col-sm-4 col-xs-12">Descripción <span class="required">*</span></label>
                                    <div class="col-sm-7 col-xs-12">
                                        <textarea rows="3" id="txtActivityExecutedTimeDescription" name="txtActivityExecutedTimeDescription" class="form-control col-xs-12" ></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="row">
                                <div class="form-group col-xs-12 text-center">
                                    <button type="button" id="actionActivityExecutedTime" class="btn btn-lg btn-round btn-success">
                                        <i class="fa fa-check" aria-hidden="true"></i> Aceptar
                                    </button>
                                </div>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    @parent
    <script src="{{ asset('js/activity_executed_time/table.js') }}"></script>
@stop