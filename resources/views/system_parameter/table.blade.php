@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
		<div>
			<div class="page-title clearfix">
				<div style="float: left">
					<h1>Par Sistema</h1>
				</div>
				@can('system_parameter.create')
				<div class="btn-group" style="float: right">
					<button type="button" id="btnSystemParameter" class="btn btn-lg btn-round btn-warning" data-toggle="modal" data-target="#modSystemParameter">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				</div>
				@endcan()
			</div>
			<div class="x_panel">
				<table id="tbParameter" class="table table-striped table-hover">
					<thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Valor</th>
                            <th>User Cre</th>
                            <th>Fec Cre</th>
                            <th></th>
                        </tr>
					</thead>
				</table>
			</div>
	    </div>
	</div>    

	<!-- Modal -->
	<div class="modal fade" id="modSystemParameter" role="dialog">
		<div class="modal-dialog modal-md">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h3 class="modal-title">Crear Parámetro</h3>
				  <div class="clearfix"></div>
				</div>
				<div class="modal-body">
					<form action="#" class="form-horizontal" id="frmSystemParameter" name="frmSystemParameterfrmSystemParameter" onsubmit="return false;">
                        <div class="form-group col-xs-12">
                            <label class="control-label col-sm-3 col-xs-12">Nombre <span class="required">*</span></label>
                            <div class="col-sm-8 col-xs-12">
                                <input type="text" id="txtNameParameter" name="txtNameParameter" required="required" class="form-control col-xs-12">
                            </div>
                        </div>

                        <div class="form-group col-xs-12">
                            <label class="control-label col-sm-3 col-xs-12" for="first-name">Valor <span class="required">*</span></label>
                            <div class="col-sm-8 col-xs-12">
                                <input type="text" id="txtValueParameter" name="txtValueParameter" required="required" class="form-control col-xs-12">
                            </div>
                        </div>

						<div id="btnSystemParameter">
							<div class="ln_solid"></div>
							<div class="col-xs-12 text-center">
								<button type="button" id="actionSystemParameter" data-role="add" class="btn btn-lg btn-round btn-success">
									<i class="fa fa-check" aria-hidden="true"> </i> <span>Guardar</span>
								</button>
							</div>
							{{ csrf_field() }}
						</div>
					</form><form action="#" class="form-horizontal" id="frmSystemParameter" name="frmSystemParameterfrmSystemParameter" onsubmit="return false;">
						<div class="form-group col-xs-12">
							<label class="control-label col-sm-3 col-xs-12">Nombre <span class="required">*</span></label>
							<div class="col-sm-8 col-xs-12">
								<input type="text" id="txtNameParameter" name="txtNameParameter" required="required" class="form-control col-xs-12">
							</div>
						</div>

						<div class="form-group col-xs-12">
							<label class="control-label col-sm-3 col-xs-12" for="first-name">Valor <span class="required">*</span></label>
							<div class="col-sm-8 col-xs-12">
								<input type="text" id="txtValueParameter" name="txtValueParameter" required="required" class="form-control col-xs-12">
							</div>
						</div>

						<div id="btnSystemParameter">
							<div class="ln_solid"></div>
							<div class="col-xs-12 text-center">
								<button type="button" id="actionSystemParameter" data-role="add" class="btn btn-lg btn-round btn-success">
									<i class="fa fa-check" aria-hidden="true"> </i> <span>Guardar</span>
								</button>
							</div>
							{{ csrf_field() }}
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@stop

@section('script')
	@parent
	<script src="{{asset('js/system_parameter/table.js')}}"></script>
@stop