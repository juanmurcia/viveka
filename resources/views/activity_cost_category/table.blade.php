@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div>
            <div class="page-title clearfix">
                <div style="float: left">
                    <h1>Categorías de Costo Actividad</h1>
                </div>
                @can('activity_cost_category.create')
                    <div class="btn-group" style="float: right">
                        <button type="button" id="btnActivityCostCategory" class="btn btn-lg btn-round btn-warning" data-toggle="modal" data-target="#modActivityCostCategory">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </button>
                    </div>
                @endcan()
            </div>
            <div class="x_panel">
                <table id="tbActivityCostCategory" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>User Cre</th>
                        <th>Fec Cre</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modActivityCostCategory" role="dialog">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">Crear Categoría de Costo</h3>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-body">
                    <form action="#" class="form-horizontal" id="frmActivityCostCategory" name="frmActivityCostCategory" onsubmit="return false;">
                        @include('activity_cost_category.new')
                        <div id="btnActivityCostCategory">
                            <div class="ln_solid"></div>
                            <div class="col-xs-12 text-center">
                                <button type="button" id="actionActivityCostCategory" data-role="add" class="btn btn-lg btn-round btn-success">
                                    <i class="fa fa-check" aria-hidden="true"> </i> <span>Crear</span>
                                </button>
                            </div>
                            {{ csrf_field() }}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modActivityCostSubCategory" role="dialog">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">Sub Categoría de Costo</h3>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-body">
                    <form action="#" class="form-horizontal" id="frmActivityCostSubCategory" name="frmActivityCostSubCategory" onsubmit="return false;">
                        @include('activity_cost_category.new')
                        <div class="col-xs-12 text-center">
                            <button type="button" id="actionActivityCostSubCategory" data-role="add" class="btn btn-lg btn-round btn-success">
                                <i class="fa fa-check" aria-hidden="true"> </i> <span>Crear</span>
                            </button>
                        </div>
                        <div class="clearfix"></div>

                        <div class="x_panel">
                            <table id="tbActivityCostSubCategory" width="100%" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>User Cre</th>
                                    <th>Fec Cre</th>
                                    <th></th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    @parent
    <script src="{{asset('js/activity_cost_category/table.js')}}"></script>
@stop