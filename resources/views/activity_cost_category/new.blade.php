<div class="row">
    <div class="form-group col-xs-12">
        <label class="control-label col-sm-3 col-xs-12">Nombre <span class="required">*</span></label>
        <div class="col-sm-8 col-xs-12">
            <input type="text" id="txtCategoryName" name="txtCategoryName" class="form-control col-xs-12"/>
            <input type="hidden" id="txtCategoryId" name="txtCategoryId">
        </div>
    </div>
</div>