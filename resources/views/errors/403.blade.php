@extends('layouts.empty')

@section('content')
    <div class="page">
        <div class="page-login" >
            <div class="row" >
                <div class="box-login col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4" style="margin-top: 10mm;">
                    <div class="row text-center">
                        <h1 class="error-number" style="color:white">403</h1>
                        <h2 style="color:white">Access denied</h2>
                        <p style="color:white">Full authentication is required to access this resource.</p>
                        @if(isset($error))
                            <h4 style="color:red">{{$error}}</h4>
                        @endif
                    </div>
                    <div class="row">
                        <div class="page-brand-info text-center">
                            <img src="{{ asset('/images/viveka_logo_texto.svg') }}" style="margin:2em 0; width: 40%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('style')
    <link href="{{asset('css/login.css')}}" rel="stylesheet">
@endsection

@section('script')
    <script>
        setTimeout(function(){
            window.history.back();
        });
    </script>
@stop