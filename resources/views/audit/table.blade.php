<div class="modal fade" id="modAudit" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" id="titleModalUser">Auditoria de Registro</h3>
                <div class="clearfix"></div>
            </div>

            <div class="modal-body">
                <table id="tbAudit" class="table table-striped table-hover dt-responsive nowrap" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Campo</th>
                            <th>Val Anterior</th>
                            <th>Val Nuevo</th>
                            <th>Usuario Cre</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

@section('script')
    @parent
    <script src="{{ asset('js/audit/table.js') }}"></script>
@stop