<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/images/ico.png" type="image/ico"/>

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    @include('partials.default-header')
</head>
<body style="background: white">
    <div class="top_nav">
        <div class="nav_menu">
            <nav>
                <input type="hidden" id="auth_person_id" value="{{ Auth::user()->person_id }}">
                <ul class="nav navbar-nav navbar-right">
                    <li class="">
                        <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                           aria-expanded="false">
                            <img src="{{ asset('/images/viveka_logo.svg' )}}" alt=""> {{ Auth::user()->name }}
                            <i class=" fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-usermenu pull-right">
                            <li>
                                <a href="javascript:;" onclick="user_password();">
                                    <i class="fa fa-user pull-right"></i> Perfil
                                </a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out pull-right"></i> Log Out
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">{{ csrf_field() }}</form>
                            </li>
                        </ul>
                    </li>

                    <li role="presentation" class="dropdown">
                        <a href="javascript:;" class="dropdown-toggle info-number" onclick="user_notification();"
                           data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-envelope-o"></i>
                            <span class="badge bg-orange span-notification" style="display: none"></span>
                        </a>
                        <ul class="dropdown-menu list-unstyled msg_list" role="menu">
                            <div id="menu1"></div>
                            <li>
                                <div class="text-center">
                                    <a href="/notificaciones"><strong>Ver todas</strong><i
                                                class="fa fa-angle-right"></i></a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </div>

    <div class="container body">
        <div class="main_container">
            @yield('content')

            @include('notification.template')
            @include('audit.table')

            <footer style="z-index: 100;">
                <div class="pull-right">
                    Viveka Software - Desarrollado por <a href="https://loopgear.co">LoopGear</a>
                </div>
                <div class="clearfix"></div>
            </footer>
        </div>
    </div>


    <!-- Scripts -->
    @include('partials.default-footer')
</body>
</html>
