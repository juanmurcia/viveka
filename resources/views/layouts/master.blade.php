<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/images/ico.png" type="image/ico"/>

    <title>{{ config('app.name', 'Laravel') }}</title>

    @include('partials.default-header')
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">

        <!-- top navigation -->
        @include('partials.navigation')

        <!-- /top navigation -->
        <!-- page content -->
        @yield('content')
        <!-- /page content -->
        @include('user.password')

        <!-- Modal Document-->
        <div class="modal fade" id="modDocument" role="dialog">
            <div class="modal-dialog modal-md">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title" id="titleModalDocument">Nuevo Documento</h3>
                        <div class="clearfix"></div>
                    </div>

                    <div class="modal-body">
                        @include('document.new-tab')
                    </div>
                </div>
            </div>
        </div>

        <footer style="z-index: 100">
            <div class="pull-right">
                Viveka Software - Desarrollado por <a href="https://loopgear.co">LoopGear</a>
            </div>
            <div class="clearfix"></div>
        </footer>
    </div>
</div>

@include('partials.default-footer')
</body>
</html>
