@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div>
            <div class="page-title clearfix">
                <div style="float: left">
                    <h1>Documentos</h1>
                </div>
                @can('document.create')
                    <div class="btn-group" style="float: right">
                        <button type="button" id="btnDocument" class="btn btn-lg btn-round btn-warning"
                                data-toggle="modal" data-target="#modDocument">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </button>
                    </div>
                @endcan()
            </div>
            <div class="x_panel">
                <table id="tbDocuments" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Tipo</th>
                        <th>Origen</th>
                        <th>Nombre</th>
                        <th>Creador</th>
                        <th>Fec Mod</th>
                        <th>Palabras Clave</th>
                        <th>Comentarios</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modDocument" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title" id="titleModal">Cargar Documento</h3>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-body">
                    <form action="#"  id="frmDocument" name="frmDocument" onsubmit="return false;" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Documento <spanclass="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="input-group">
                                    <input type="file" id="fileDocument" name="fileDocument" required="required" style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">
                                    <input type="text" id="txtDocumentKey" name="txtDocumentKey" required="required" style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">
                                    <input type="text" id="documentTxt" name="documentTxt" class="form-control" readonly="readonly">
                                    <span class="input-group-btn"><label for="document" class="btn btn-primary">Seleccionar</label></span>
                                </div>
                                <div class="row hidden" id="documentProgress">
                                    <div class="col-xs-9 nopadding">
                                        <div class="progress">
                                            <div class="progress-bar bg-green" style="width: 70%;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3 nopadding text-right">
                                        100%
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" >
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Modelo <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="optModel" name="optModel" class="select2_single form-control">
                                    <option value="">..Seleccione uno ..</option>
                                    <option value="agreement">Contrato</option>
                                    <option value="project">Proyecto</option>
                                    <option value="task">Tarea</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ID Modelo <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="optModelId" name="optModelId" required="required" class="select2_single form-control">
                                    <option value="">..Seleccione uno ..</option>
                                    <option value="1">1- CO_1000_4</option>
                                    <option value="2">2- PE_4321_7</option>
                                    <option value="3">3- RE_6452_3</option>
                                    <option value="4">4- TO_9894_2</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tipo de Documento <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="optModelDocumentTypeId" name="optModelDocumentTypeId" required="required" class="select2_single form-control">
                                    <option value="">..Seleccione uno ..</option>
                                    <option value="1">RUT</option>
                                    <option value="2">CAMARA DE COMERCIO</option>
                                    <option value="3">PARAFISCALES</option>
                                    <option value="4">OTROS</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Palabras Clave</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="txtKeywords" name="txtKeywords" type="text" class="tags form-control" value="">
                            </div>
                        </div>


                        <div class="form-group" >
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                   for="first-name">Comentario </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea rows="3" id="txtComment" name="txtComment" class="form-control col-md-7 col-xs-12"></textarea>
                            </div>
                        </div>
                        <div class="form-group" style="display: none">
                            <div class="col-12 text-center">
                                <button type="button" id="actionDocument" data-role="add"
                                        class="btn btn-lg btn-round btn-success">
                                    <i class="fa fa-check" aria-hidden="true"> </i> Cargar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('document.versions')
@stop
@section('script')
    @parent
    <script src="{{asset('js/document/table.js')}}"></script>
@stop