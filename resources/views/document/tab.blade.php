@can('document.create')
    <div class="text-center">
        <button type="button" id="btnProject" onclick="createDocument({{ $id }})" class="btn btn-lg btn-round btn-warning" title="Agregar documento"
                data-toggle="modal" data-target="#modDocument">
            <i class="fa fa-plus" aria-hidden="true" style="margin-right: 16px"></i>Nuevo Documento
        </button>
    </div>
@endcan()
<div class="row">
    <div class="col-xs-12 text-center">
        <div class="x_content">
            <table id="tbDocuments" width="100%" class="table table-striped table-hover" data-pattern-id="{{ $pattern_id }}" data-model-id="{{ $id }}" >
                <thead>
                <tr>
                    <th>Documento</th>
                    <th>Nombre</th>
                    <th>Creador</th>
                    <th>Fec Cre</th>
                    <th>Fec Mod</th>
                    <th>Palabras Clave</th>
                    <th>Comentarios</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@include('document.versions')

@section('script')
    <script src="{{ asset('js/document/tab.js') }}"></script>
    <script src="{{asset('js/document/new_tab.js')}}"></script>
    @parent
@stop