<!-- Modal -->
<div class="modal fade" id="modDocumentVersions" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" id="titleModalDocumentVersions">Versiones Anteriores</h3>
                <div class="clearfix"></div>
            </div>

            <div class="modal-body">

            </div>
            <div class="row" id="document-version-resume" style="display: none">
                <div class="col-xs-8">
                    <span class="document-name" style="font-weight: bold">Nombre_del_documento.pdf</span><br>
                    Agregado por <span class="font-weight-bold document-user-mod-name">NATALIA ARIAS</span><br>
                    <small class="document-date">2018-11-23 23:59</small>
                </div>
                <div class="col-xs-4 text-right ">
                    <a download href="/document/download/'.$id.'" target="_blank" class="btn btn-sm btn-default btn-icon btn-outline btn-round document-version" title="Descargar Documento" rol="tooltip" ><i class="fa fa-download" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

@section('script')
    @parent
    <script src="{{asset('js/document/versions.js')}}"></script>
@stop