<form action="#" class="form-horizontal form-label-left" id="frmDocument"
      name="frmDocument" onsubmit="return false;">
    <input type="hidden" id="txtDocumentId" name="txtDocumentId">
    <input type="hidden" id="txtDocumentModelId" name="txtDocumentModelId">
    <div class="row">
        <div class="form-group col-xs-12"  id="divTypeDocument">
            <label class="control-label col-sm-3 col-xs-12">
                Tipo de Documento <span class="required">*</span>
            </label>
            <div class="col-sm-8 col-xs-12">
                <select id="txtDocumentPatternDocumentTypeId" name="txtDocumentPatternDocumentTypeId" required="required"
                        class="select2_single form-control">
                    <option value="">..Seleccione uno ..</option>
                    @foreach (App\Models\PatternDocumentType::all() as $key => $val)
                        <option value="{{ $val->id }}">{{ $val->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group col-xs-12">
            <label class="control-label col-sm-3 col-xs-12" >Documento <span class="required">*</span></label>
            <div class="col-sm-8 col-xs-12">
                <div class="input-group">
                    <input type="file" id="fileDocument" name="fileDocument" value="" required="required" style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">
                    <input type="text" id="txtDocumentKey" name="txtDocumentKey" value="" required="required" style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">
                    <input type="text" id="txtDocumentName" name="txtDocumentName" value="" class="form-control" readonly="readonly">
                    <span class="input-group-btn"><label for="fileDocument" class="btn btn-primary">Seleccionar</label></span>
                </div>
                <div class="row hidden" id="documentProgress">
                    <div class="col-xs-9 nopadding">
                        <div class="progress">
                            <div class="progress-bar bg-green" style="width: 70%;">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-3 nopadding text-right">
                        100%
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group col-xs-12" id="divKeyWordDocument">
            <label class="control-label col-sm-3 col-xs-12">Palabras Clave</label>
            <div class="col-sm-8 col-xs-12">
                <input id="txtDocumentKeywords" name="txtDocumentKeywords" type="text" class="tags form-control">
            </div>
        </div>

        <div class="form-group col-xs-12" id="divCommentDocument">
            <label class="control-label col-sm-3 col-xs-12"
                   for="first-name">Comentario </label>
            <div class="col-sm-8 col-xs-12">
                <textarea rows="3" id="txtDocumentComment" name="txtDocumentComment" class="form-control col-xs-12"></textarea>
            </div>
        </div>
    </div>
    <div class="ln_solid"></div>
    <div class="row" id="divBtnDocument">
        <div class="form-group col-xs-12 text-center">
            <button type="button" id="actionDocument" class="btn btn-lg btn-round btn-success">
                <i class="fa fa-check" aria-hidden="true"></i> Aceptar
            </button>
        </div>
    </div>
    {{ csrf_field() }}
</form>