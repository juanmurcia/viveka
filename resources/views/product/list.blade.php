<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-lg-4 col-sm-6 col-xs-12 productCreate">
                @can('product.create')
                    <button type="button" id="btnProduct" class="btn btn-lg btn-round btn-warning" title="Agregar nuevo proyecto"
                            data-toggle="modal" data-target="#modProduct">
                        <i class="fa fa-plus" aria-hidden="true" style="margin-right: 16px"></i>Nuevo Producto
                    </button>
                @endcan
            </div>
        </div>
        <div class="row" id="listProduct">
        </div>
        @include('product.resume')
    </div>
</div>

<div class="modal fade" id="modProduct" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" id="titleModal">Crear Producto</h3>
                <div class="clearfix"></div>
            </div>

            <div class="modal-body">
                <form action="#" data-parsley-validate class="form-horizontal form-label-left" id="frmProduct" name="frmProduct"
                      onsubmit="return false;">
                    @include('product.new')
                    <div id="btnProduct">
                        <div class="ln_solid"></div>
                        <div class="text-center">
                            @can('product.create')
                                <button type="submit" id="newProduct" class="btn btn-lg btn-round btn-success">
                                    <i class="fa fa-check" aria-hidden="true"></i> Guardar
                                </button>
                            @endcan
                        </div>
                    </div>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>

@section('script')
    @parent
    <script src="{{ asset('js/product/list.js') }}"></script>
@stop