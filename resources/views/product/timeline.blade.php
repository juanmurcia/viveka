<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Linea Tiempo Estados</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="flex-parent">
                    <div class="input-flex-container"></div>
                    <div class="description-flex-container"></div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('style')
    @parent
    <link rel="stylesheet" href="{{asset('css/timeline.css')}}">
@stop

@section('script')
    @parent
    <script src="{{ asset('js/product/timeline.js') }}"></script>
@stop