@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div class="page-title">
            <div class="title_left">
                <h1>Flujo Caja
                    <small>Control dinero productos generados</small>
                </h1>
            </div>
        </div>
        <div class="clearfix"></div>
        <div id="dataUtil" style="display: none">
            <input type="hidden" id="agreements" value="{{ json_encode($agreements) }}">
            <input type="hidden" id="projects" value="{{ json_encode($projects) }}">
            <input type="hidden" id="products" value="{{ json_encode($products) }}">
            <input type="hidden" id="pattern_state" value="{{ json_encode($pattern_states) }}">
            <table>
                <tr id="util" >
                    <td><blockquote class="message nameProduct"></blockquote></td>
                    <td class="money col-1t">-</td>
                    <td class="money col-1e">-</td>
                    <td class="money col-2t">-</td>
                    <td class="money col-2e">-</td>
                    <td class="money col-3t">-</td>
                    <td class="money col-3e">-</td>
                    <td class="money col-4t">-</td>
                    <td class="money col-4e">-</td>
                </tr>
            </table>

        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Filtros</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a id="liFilter" class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form action="#" class="form-horizontal form-label-left" id="frmFlowCash" name="frmFlowCash" onsubmit="return false;">
                            <input type="hidden" id="fecFlow" name="fecFlow">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="control-label col-md-4 col-sm-3 col-xs-12">Contrato :</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <select id="optAgreementFlow" name="optAgreementFlow" onchange="make_project(this.value);" class="select2 form-control col-md-12">
                                            <option value="">.. Seleccione uno ..</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label class="control-label col-md-4 col-sm-3 col-xs-12">Proyecto :</label>
                                    <div class="col-sm-8 col-md-8 col-xs-12">
                                        <select id="optProjectFlow" name="optProjectFlow" onchange="make_product(this.value);" class="select2 form-control col-md-12">
                                            <option value="">.. Seleccione uno ..</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="control-label col-md-4 col-sm-3 col-xs-12">Producto :</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <select id="optProductFlow" name="optProductFlow" class="select2 form-control col-md-12">
                                            <option value="">.. Seleccione uno ..</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label col-md-4 col-sm-3 col-xs-12">Estado :</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <select id="optStateFlow" name="optStateFlow" class="select2 form-control col-md-12">
                                            <option value="">.. Seleccione uno ..</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div style="text-align: center">
            <button class="btn btn-round btn-md btn-default move-day" data-action="prev" style="float: left"><i class="fa fa-angle-left"></i></button>
            <button class="btn btn-round btn-md btn-default move-day" data-action="next" style="float: right"><i class="fa fa-angle-right"></i></button>
            <h3 class="monthTitle"></h3>&nbsp;
        </div>
        <style>
            .cash-flow{
                background-color: #ffffff;
                width: 100%;
                border: 1px solid rgb(230, 233, 237);
            }

            .cash-flow tfoot{
                border-top: 2px solid #7D7D7D;
            }

            .cash-flow thead{
                background-color: #d5d5d5;
                border-bottom: 2px solid #7D7D7D;
            }

            .cash-flow th{
                text-align: center;
                white-space: nowrap;
            }

            .cash-flow tr,
            .cash-flow td,
            .cash-flow th{
                padding: 0 0.5em;
            }
            .cash-flow .sub-header th{
                font-weight: normal !important;
            }

            .cash-flow .money{
                white-space: nowrap;
                text-align: right;
                padding-left: 1.5em;
            }
             /*
            .cash-flow .money:before{
                color: #777777;
                content: '$';
                display: inline;
                width: 1em;
                margin-left: -1.5em;
                float: left;
            }
               */

        </style>
        <table class="cash-flow">
            <thead>
                <tr>
                    <th rowspan="2"><h3>Detalle</h3></th>
                    <th colspan="2"><h4>S1 (1 - 7)</h4></th>
                    <th colspan="2"><h4>S2 (8 - 14)</h4></th>
                    <th colspan="2"><h4>S3 (15 - 22)</h4></th>
                    <th colspan="2"><h4>S4 (23 - 31)</h4></th>
                </tr>
                <tr class="sub-header">
                    <th>Total</th>
                    <th>Empresa</th>
                    <th>Total</th>
                    <th>Empresa</th>
                    <th>Total</th>
                    <th>Empresa</th>
                    <th>Total</th>
                    <th>Empresa</th>
                </tr>
            </thead>
            <tbody id="details">
            </tbody>
            <tfoot>
                <tr>
                    <td><h3>Total</h3></td>
                    <td class="money ttlcol-1t">0</td>
                    <td class="money ttlcol-1e">0</td>
                    <td class="money ttlcol-2t">0</td>
                    <td class="money ttlcol-2e">0</td>
                    <td class="money ttlcol-3t">0</td>
                    <td class="money ttlcol-3e">0</td>
                    <td class="money ttlcol-4t">0</td>
                    <td class="money ttlcol-4e">0</td>
                </tr>
            </tfoot>
        </table>
        <br>
    </div>
@stop

@section('script')
    @parent
    <script src="{{asset('js/product/cash-flow.js')}}"></script>
@stop