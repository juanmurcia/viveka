<div class="col-lg-4 col-sm-6 col-xs-12" id="product-resume" style="display:none;">
    <div class="x_panel">
        <div class="x_title">
            @can('product.view')
                <a href="#" style="float:right; font-size:20px">
                    <i class="fa fa-folder-open"></i>
                </a>
            @endcan
            <h3>Nombre Producto</h3>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div style="float:right;">
                <input class="knobManual product-resume-percentage" data-width="100" data-height="100">
            </div>
            <div style="width: 60%; width: calc(100% - 110px)">
                <ul class="list-unstyled">
                    <li><i class="fa fa-user"></i> <span class="product-resume-responsible"></span></li>
                    <li><i class="fa fa-calendar"></i> Inicio: <span class="product-resume-start"></span></li>
                    <li><i class="fa fa-calendar-o"></i> Fin: <span class="product-resume-end"></span></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="tui-full-calendar-popup-top-line" style="width:95%;position: absolute; top: 0px; height: 10px"></div>
</div>