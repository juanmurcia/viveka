@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
        <div class="page-title">
            <div class="title_left">
                <h1>Productos</h1>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Buscar</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li class="dropdown" style="visibility: hidden;">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            </li>
                            <li style="visibility: hidden;"><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form action="#" class="form-horizontal form-label-left" id="frmProduct"
                              name="frmProduct" onsubmit="return false;">
                            <div class="row">
                                <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                    <label class="control-label col-lg-4 col-sm-3 col-xs-12">Nombre</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <input type="text" id="txtProductName" name="txtProductName"
                                               class="form-control col-md-7 col-xs-12"/>
                                    </div>
                                </div>
                                <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                    <label class="control-label col-lg-4 col-sm-3 col-xs-12">Estado</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <select id="optProductPatternState" name="optProductPatternState"
                                                class="select2_single form-control col-xs-12">
                                            <option value="">..Seleccione uno ..</option>
                                            @foreach ($product_pattern_state as $key => $val)
                                                <option value="{{ $val->id }}">{{ $val->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                    <label class="control-label col-lg-4 col-sm-3 col-xs-12">Responsable</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <select id="txtProductResponsible" name="txtProductResponsible"></select>
                                    </div>
                                </div>

                                <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                    <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fecha Inicio
                                        Desde</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <div class="input-group date myDatePicker2">
                                            <input type='text' id="txtAgreementStartDateFrom"
                                                   name="txtProductStartDateFrom"
                                                   class="form-control col-xs-12" placeholder="aaaa-mm-dd"/>
                                            <span class="input-group-addon">
                                               <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                    <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fecha Inicio
                                        Hasta</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <div class="input-group date myDatePicker2">
                                            <input type='text' id="txtAgreementStartDateTo"
                                                   name="txtProductStartDateTo"
                                                   class="form-control col-xs-12" placeholder="aaaa-mm-dd"/>
                                            <span class="input-group-addon">
                                               <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                    <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fecha Fin
                                        Desde</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <div class="input-group date myDatePicker2">
                                            <input type='text' id="txtAgreementEndDateFrom"
                                                   name="txtProductEndDateFrom"
                                                   class="form-control col-xs-12" placeholder="aaaa-mm-dd"/>
                                            <span class="input-group-addon">
                                               <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-lg-4 col-sm-6 col-xs-12">
                                    <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fecha Fin
                                        Hasta</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <div class="input-group date myDatePicker2">
                                            <input type='text' id="txtAgreementEndDateTo"
                                                   name="txtProductEndDateTo"
                                                   class="form-control col-xs-12" placeholder="aaaa-mm-dd"/>
                                            <span class="input-group-addon">
                                               <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="ln_solid"></div>
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <button type="submit" id="actionProduct" data-role="add"
                                            class="btn btn-lg btn-round btn-primary">
                                        <i class="fa fa-check" aria-hidden="true"> </i> Consultar
                                    </button>
                                    <button class="btn btn-lg btn-round btn-default" type="reset">
                                        <i class="fa fa-eraser" aria-hidden="true"> </i> Limpiar
                                    </button>
                                </div>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="x_panel">
                    <button type="button" style="float: right" class="btn btn-default dropdown-toggle" data-toggle="modal" data-target="#modSelectColumn">
                        <i class="fa fa-sliders" aria-hidden="true"></i> Columnas
                    </button>
                    <table id="tbProducts" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Proyecto</th>
                            <th>Estado</th>
                            <th>Nombre</th>
                            <th>Descripión</th>
                            <th>% Avance</th>
                            <th>Fec Inicio</th>
                            <th>Fec Fin</th>
                            <th>Acción</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    @parent
    <script src="{{ asset('js/product/table.js') }}"></script>
@stop