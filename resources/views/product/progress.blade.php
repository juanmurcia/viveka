<div class="row">
    <div class="form-group col-lg-6 col-xs-12">
        <div class="row">
            <div class="form-group col-xs-12">
                <label class="control-label col-lg-4 col-sm-3 col-xs-12">Porcentaje <span class="required">*</span></label>
                <div class="col-sm-8 col-xs-12">
                    <input type="number" min="0" step="1" id="txtProductPercentage" name="txtProductPercentage" class="form-control col-xs-12"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12">
                <label class="control-label col-lg-4 col-sm-3 col-xs-12">Estado <span class="required">*</span></label>
                <div class="col-sm-8 col-xs-12">
                    <select id="optProductPatternState" name="optProductPatternState" class="select2_single form-control"  >
                        <option value="">..Seleccione uno ..</option>
                        @foreach ($product_pattern_state as $key => $val)
                            <option value="{{ $val->id }}" >{{ $val->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            @can('document.create')
                <div class="text-center">
                    <button type="button" id="btnProject" onclick="createDocument({{ $id }})" class="btn btn-sm btn-default" title="Adjuntar documento"
                            data-toggle="modal" data-target="#modDocument">
                        <i class="fa fa-paperclip" aria-hidden="true" style="margin-right: 16px"></i>Adjuntar Documento
                    </button>
                </div>
            @endcan()
        </div>
    </div>
    <div class="form-group col-lg-6 col-xs-12">
        <div class="row">
            <label class="control-label col-lg-4 col-sm-3 col-xs-12">Justificación <span class="required">*</span></label>
            <div class="col-sm-8 col-xs-12">
                <textarea rows="2" id="txtProductJustification" name="txtProductJustification" class="form-control col-xs-12"></textarea>
            </div>
        </div>
        <div class="row">
            <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fecha Mod <span class="required">*</span></label>
            <div class="col-sm-8 col-xs-12">
                <div class="input-group date myDatePicker2">
                    <input required type='text' id="txtProductFecMod" name="txtProductFecMod" class="form-control col-xs-12" placeholder="aaaa-mm-dd" />
                    <span class="input-group-addon">
                       <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>