<input type="hidden" id="txtProductId" name="txtProductId">
<div class="row">
    <div class="col-lg-6 col-xs-12">
        <div class="row">
            <div class="form-group col-xs-12">
                <label class="control-label col-lg-4 col-sm-3 col-xs-12">Nombre <span class="required">*</span></label>
                <div class="col-sm-8 col-xs-12">
                    <input required type="text" id="txtProductName" name="txtProductName" class="form-control col-xs-12"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12">
                <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fecha Inicio <span class="required">*</span></label>
                <div class="col-sm-8 col-xs-12">
                    <div class="input-group date myDatePicker2">
                        <input required type='text' id="txtProductStartDate" name="txtProductStartDate" class="form-control col-xs-12" placeholder="aaaa-mm-dd" />
                        <span class="input-group-addon">
							   <span class="glyphicon glyphicon-calendar"></span>
							</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12">
                <label class="control-label col-lg-4 col-sm-3 col-xs-12">Fecha Fin <span class="required">*</span></label>
                <div class="col-sm-8 col-xs-12">
                    <div class="input-group date myDatePicker2">
                        <input required type='text' id="txtProductEndDate" name="txtProductEndDate" class="form-control col-xs-12" placeholder="aaaa-mm-dd" />
                        <span class="input-group-addon">
                           <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12">
                <label class="control-label col-lg-4 col-sm-3 col-xs-12">Valor <span class="required">*</span></label>
                <div class="col-sm-8 col-xs-12">
                    <input required type="number" id="txtProductValue" name="txtProductValue" class="form-control col-xs-12"/>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-xs-12">
        <div class="row">
            <div class="form-group col-xs-12">
                <label class="control-label col-lg-4 col-sm-3 col-xs-12">Responsable <span class="required">*</span></label>
                <div class="col-sm-8 col-xs-12">
                    <select required id="txtProductResponsible" name="txtProductResponsible"></select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12">
                <label class="control-label col-lg-4 col-sm-3 col-xs-12">Descripción <span class="required">*</span></label>
                <div class="col-sm-8 col-xs-12">
                    <textarea required rows="3" id="txtProductDescription" name="txtProductDescription" class="form-control col-xs-12"></textarea>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="contractInfo" style="display: none;">
    <hr>
    <div class="row">
        <div class="col-md-6 col-xs-12">
            <div class="form-group col-xs-12">
                <label class="control-label col-lg-4 col-sm-3 col-xs-12">Nombre Contractual <span class="required">*</span></label>
                <div class="col-sm-8 col-xs-12">
                    <input type="text" id="txtProductNameContract" name="txtProductNameContract" class="form-control col-xs-12"/>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xs-12">
            <div class="form-group col-xs-12">
                <label class="control-label col-lg-4 col-sm-3 col-xs-12">Descripción Contractual <span class="required">*</span></label>
                <div class="col-sm-8 col-xs-12">
                    <textarea rows="3" id="txtProductDescriptionContract" name="txtProductDescriptionContract" class="form-control col-xs-12"></textarea>
                </div>
            </div>
        </div>
    </div>
</div>

@section('script')
    @parent
    <script src="{{ asset('js/product/new.js') }}"></script>
@stop