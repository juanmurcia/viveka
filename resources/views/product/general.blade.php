<fieldset >
    <legend>General</legend>
    <form action="#" data-parsley-validate class="form-horizontal" id="frmProductBasics" name="frmProductBasics" onsubmit="return false;">
        <div class="row">
            <div class="col-lg-10 col-xs-12 col-lg-offset-1">
                @include('product.new')
            </div>
        </div>
        <div class="form-group">
            @can('product.edit')
                <div class="col-xs-12 text-center">
                    <button type="submit" id="actionProductBasics" class="btn btn-lg btn-round btn-success">
                        <i class="fa fa-check" aria-hidden="true"></i> Guardar
                    </button>
                </div>
            @endcan
        </div>
        {{ csrf_field() }}
    </form>
</fieldset>
<fieldset >
    <legend>Avance</legend>
    <form action="#" class="form-horizontal" id="frmProductProgress" name="frmProductProgress" onsubmit="return false;">
        <div class="row">
            <div class="col-lg-10 col-xs-12 col-lg-offset-1">
                @include('product.progress')
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12 text-center">
                @can('product.edit')
                    <button type="button" id="actionProductProgress" class="btn btn-lg btn-round btn-success">
                        <i class="fa fa-check" aria-hidden="true"></i> Guardar
                    </button>
                @endcan
            </div>
        </div>
        {{ csrf_field() }}
    </form>
</fieldset>

<fieldset >
    <legend>Auditoría</legend>
    
    <div class="row">
        <div class="col-md-6">
            <legend class="text-right">Cambio Estado</legend>
            <table id="tbAudState" class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Estado</th>
                    <th>Usuario</th>
                    <th>Observación</th>
                    <th>Fecha</th>
                </tr>
                </thead>
            </table>
        </div>
        <div class="col-md-6">
            <legend class="text-right">Cambio Avance</legend>
            <table id="tbAudPercentage" class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Avance</th>
                    <th>Usuario</th>
                    <th>Observación</th>
                    <th>Fecha</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</fieldset>

@section('script')
    <script src="{{ asset('js/product/form.js') }}"></script>
    @parent
@stop

