@extends('layouts.app')

@section('content')

    <div class="right_col" role="main">
        <div class="page-title clearfix">
            <div class="title_left">
                <h3>Calendar
                    <small>tiempos registrados de actividad</small>
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                  <button class="btn btn-default" type="button">Go!</button>
                </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="x_panel" style="height: 630px">
            <div id="lnb">
                <div class="lnb-new-schedule">
                    <button id="btn-new-schedule" type="button" class="btn btn-default btn-block lnb-new-schedule-btn">
                        Registrar Tiempo
                    </button>
                </div>
                <div id="lnb-calendars" class="lnb-calendars">
                    <div>
                        <div class="lnb-calendars-item">
                            <label>
                                <input class="tui-full-calendar-checkbox-square" type="checkbox" value="all" checked>
                                <span></span>
                                <strong>Ver Todos</strong>
                            </label>
                        </div>
                    </div>
                    <div id="calendarList" class="lnb-calendars-d1"></div>
                </div>
            </div>

            <div id="right" style="width: 85%;">
                <div id="menu">
                    <span class="dropdown">
                        <button id="dropdownMenu-calendarType" class="btn btn-default btn-sm dropdown-toggle"
                                type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i id="calendarTypeIcon" class="calendar-icon ic_view_month" style="margin-right: 4px;"></i>
                            <span id="calendarTypeName">Dropdown</span>&nbsp;
                            <i class="calendar-icon tui-full-calendar-dropdown-arrow"></i>
                        </button>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu-calendarType">
                            <li role="presentation">
                                <a class="dropdown-menu-title" role="menuitem" data-action="toggle-daily">
                                    <i class="calendar-icon ic_view_day"></i> Día
                                </a>
                            </li>
                            <li role="presentation">
                                <a class="dropdown-menu-title" role="menuitem" data-action="toggle-weekly">
                                    <i class="calendar-icon ic_view_week"></i> Semana
                                </a>
                            </li>
                            <li role="presentation">
                                <a class="dropdown-menu-title" role="menuitem" data-action="toggle-monthly">
                                    <i class="calendar-icon ic_view_month"></i> Mes
                                </a>
                            </li>
                            <li role="presentation">
                                <a class="dropdown-menu-title" role="menuitem" data-action="toggle-weeks2">
                                    <i class="calendar-icon ic_view_week"></i> 2 Semanas
                                </a>
                            </li>
                            <li role="presentation">
                                <a class="dropdown-menu-title" role="menuitem" data-action="toggle-weeks3">
                                    <i class="calendar-icon ic_view_week"></i> 3 Semanas
                                </a>
                            </li>
                            <li role="presentation" class="dropdown-divider"></li>
                            <li role="presentation">
                                <a role="menuitem" data-action="toggle-workweek">
                                    <input type="checkbox" class="tui-full-calendar-checkbox-square"
                                           value="toggle-workweek" checked>
                                    <span class="checkbox-title"></span>Ver Semanas
                                </a>
                            </li>
                            <li role="presentation">
                                <a role="menuitem" data-action="toggle-start-day-1">
                                    <input type="checkbox" class="tui-full-calendar-checkbox-square"
                                           value="toggle-start-day-1">
                                    <span class="checkbox-title"></span>Iniciar semana en Lunes
                                </a>
                            </li>
                        </ul>
                    </span>

                    <span id="menu-navi">
                        <button type="button" class="btn btn-default btn-sm move-today"
                                data-action="move-today">Hoy</button>
                        <button type="button" class="btn btn-default btn-sm move-day" data-action="move-prev">
                            <i class="calendar-icon ic-arrow-line-left" data-action="move-prev"></i>
                        </button>
                        <button type="button" class="btn btn-default btn-sm move-day" data-action="move-next">
                            <i class="calendar-icon ic-arrow-line-right" data-action="move-next"></i>
                        </button>
                    </span>
                    <span id="renderRange" class="render-range"></span>
                </div>

                <div id="calendar"></div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modCalendar" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title" id="titleModalUser">Registrar Tiempo</h3>
                    <div class="clearfix"></div>
                </div>

                <div class="modal-body">
                    <form action="#" data-parsley-validate class="form-horizontal" onsubmit="return false;">
                        <div id="completeInfo">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="control-label col-md-4 col-sm-3 col-xs-12">Contrato :</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <select id="optAgreementCalendar" name="optAgreementCalendar" onchange="make_project(this.value);make_activity(1,this.value)" class="select2 form-control col-md-12">
                                            <option value="">.. Seleccione uno ..</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label class="control-label col-md-4 col-sm-3 col-xs-12">Proyecto :</label>
                                    <div class="col-sm-8 col-md-8 col-xs-12">
                                        <select id="optProjectCalendar" name="optProjectCalendar" onchange="make_product(this.value);make_activity(2,this.value);" class="select2 form-control col-md-12">
                                            <option value="">.. Seleccione uno ..</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="control-label col-md-4 col-sm-3 col-xs-12">Producto :</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <select id="optProductCalendar" name="optProductCalendar" onchange="make_activity(3,this.value)"
                                                class="select2 form-control col-md-12">
                                            <option value="">.. Seleccione uno ..</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label class="control-label col-md-4 col-sm-3 col-xs-12">Actividad :</label>
                                    <div class="col-sm-8 col-md-8 col-xs-12">
                                        <select id="optActivityCalendar" required name="optActivityCalendar" class="select2 form-control col-md-12">
                                            <option value="">.. Seleccione uno ..</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Observación :</label>
                            <div class="col-md-10">
                                <textarea id="txtObservationCalendar" name="txtObservationCalendar" required class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <label class="control-label col-md-4 col-sm-3 col-xs-12"> Inicio :</label>
                                <div class="input-group date myDatePicker3">
                                    <input type='text' id="calendarStartDate" name="calendarStartDate" required  class="form-control col-xs-12" placeholder="Inicio (aaaa-mm-dd hh:mm)"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label col-md-4 col-sm-3 col-xs-12"> Fin :</label>
                                <div class="input-group date myDatePicker3">
                                    <input type='text' id="calendarEndDate" name="calendarEndDate"
                                           required  class="form-control col-xs-12" placeholder="Fin (aaaa-mm-dd hh:mm)"/>
                                    <span class="input-group-addon"><span
                                                class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-center">
                                <button type="submit" id="actionNewCalendar" data-role="add"
                                        class="btn btn-lg btn-round btn-success">
                                    <i class="fa fa-check" aria-hidden="true"></i> Guardar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="util" style="display: none">
        <input type="text" id="agreements" value="{{ json_encode($agreements) }}">
        <input type="text" id="projects" value="{{ json_encode($projects) }}">
        <input type="text" id="products" value="{{ json_encode($products) }}">
        <input type="text" id="activities" value="{{ json_encode($activities) }}">
    </div>

    <form action="#" data-parsley-validate style="display: none" id="frmActivityExecutedTime" name="frmActivityExecutedTime" onsubmit="return false;">
        <input type="text" id="txtActivityExecutedTimeActivityId" name="txtActivityExecutedTimeActivityId">
        <input type="text" id="txtActivityExecutedTimeDescription" name="txtActivityExecutedTimeDescription">
        <input type="text" id="txtActivityExecutedTimeEndDate" name="txtActivityExecutedTimeEndDate">
        <input type="text" id="txtActivityExecutedTimeExecuted" name="txtActivityExecutedTimeExecuted">
        {{ csrf_field() }}
    </form>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://uicdn.toast.com/tui.code-snippet/latest/tui-code-snippet.min.js"></script>
    <script type="text/javascript" src="https://uicdn.toast.com/tui.time-picker/latest/tui-time-picker.min.js"></script>
    <script type="text/javascript" src="https://uicdn.toast.com/tui.date-picker/latest/tui-date-picker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chance/1.0.13/chance.min.js"></script>
@stop

@section('style')
    @parent
    <link rel="stylesheet" href="{{asset('build/css/calendar.css')}}">
@stop

@section('script')
    @parent
    <script src="{{asset('build/js/calendar.js')}}"></script>
    <script src="{{asset('js/time/calendar.js')}}"></script>
@stop