'use strict';

/* eslint-disable require-jsdoc */
/* eslint-env jquery */
/* global moment, tui, chance */
/* global findCalendar, CalendarList, ScheduleList, generateSchedule */

(function(window, Calendar) {
    var cal, resizeThrottled;
    var useCreationPopup = false;
    var useDetailPopup = true;
    var datePicker, selectedCalendar;

    cal = new Calendar('#calendar', {
        defaultView: 'week',
        useCreationPopup: useCreationPopup,
        useDetailPopup: useDetailPopup,
        calendars: CalendarList,
        moreLayerSize: {height : '550px'},
        template: {
            milestone: function(model) {
                return '<span class="calendar-font-icon ic-milestone-b"></span> <span style="background-color: ' + model.bgColor + '">' + model.title + '</span>';
            },
            allday: function(schedule) {
                return getTimeTemplate(schedule, true);
            },
            time: function(schedule) {
                return getTimeTemplate(schedule, false);
            }
        },
        month: {
            daynames: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
            startDayOfWeek: 0,
            narrowWeekend: true
        },
        week: {
            daynames: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
            startDayOfWeek: 0,
            narrowWeekend: true
        }
    });

    // event handlers
    cal.on({
        'clickMore': function(e) {
            console.log('clickMore', e);
        },
        'clickSchedule' : function(e) {
            detailSchedule(e);
        },
        'clickDayname': function(date) {
            console.log('clickDayname', date);
        },
        'beforeCreateSchedule': function(e) {
            if(e.triggerEventName == 'mouseup'){
                createNewSchedule(e);
            }else{
                saveNewSchedule(e);
            }
        },
        'beforeUpdateSchedule': function(e) {
            if(e.triggerEventName == 'click'){
                formUpdateSchedule(e);
            }else{
                updateSchedule(e);
            }
        },
        'beforeDeleteSchedule': function(e) {
            deleteSchedule(e)
        },
        'afterRenderSchedule': function(e) {

        },
        'clickTimezonesCollapseBtn': function(timezonesCollapsed) {
            console.log('timezonesCollapsed', timezonesCollapsed);

            if (timezonesCollapsed) {
                cal.setTheme({
                    'week.daygridLeft.width': '77px',
                    'week.timegridLeft.width': '77px'
                });
            } else {
                cal.setTheme({
                    'week.daygridLeft.width': '60px',
                    'week.timegridLeft.width': '60px'
                });
            }

            return true;
        }
    });

    document.getElementById('calendar').addEventListener('keydown', e => {
        console.log('keydown', e);
    });

    /**
     * Get time template for time and all-day
     * @param {Schedule} schedule - schedule
     * @param {boolean} isAllDay - isAllDay or hasMultiDates
     * @returns {string}
     */
    function getTimeTemplate(schedule, isAllDay) {
        var html = [];
        var start = moment(schedule.start.toUTCString());
        if (!isAllDay) {
            html.push('<strong>' + start.format('HH:mm') + '</strong> ');
        }
        if (schedule.isPrivate) {
            html.push('<span class="calendar-font-icon ic-lock-b"></span>');
            html.push(' Private');
        } else {
            if (schedule.isReadOnly) {
                html.push('<span class="calendar-font-icon ic-readonly-b"></span>');
            } else if (schedule.recurrenceRule) {
                html.push('<span class="calendar-font-icon ic-repeat-b"></span>');
            } else if (schedule.attendees.length) {
                html.push('<span class="calendar-font-icon ic-user-b"></span>');
            } else if (schedule.location) {
                html.push('<span class="calendar-font-icon ic-location-b"></span>');
            }
            html.push(' ' + schedule.title);
        }

        return html.join('');
    }

    /**
     * A listener for click the menu
     * @param {Event} e - click event
     */
    function onClickMenu(e) {
        var target = $(e.target).closest('a[role="menuitem"]')[0];
        var action = getDataAction(target);
        var options = cal.getOptions();
        var viewName = '';

        switch (action) {
            case 'toggle-daily':
                viewName = 'day';
                break;
            case 'toggle-weekly':
                viewName = 'week';
                break;
            case 'toggle-monthly':
                options.month.visibleWeeksCount = 4;
                viewName = 'month';
                break;
            case 'toggle-weeks2':
                options.month.visibleWeeksCount = 2;
                viewName = 'month';
                break;
            case 'toggle-weeks3':
                options.month.visibleWeeksCount = 3;
                viewName = 'month';
                break;
            case 'toggle-narrow-weekend':
                options.month.narrowWeekend = !options.month.narrowWeekend;
                options.week.narrowWeekend = !options.week.narrowWeekend;
                viewName = cal.getViewName();

                target.querySelector('input').checked = options.month.narrowWeekend;
                break;
            case 'toggle-start-day-1':
                options.month.startDayOfWeek = options.month.startDayOfWeek ? 0 : 1;
                options.week.startDayOfWeek = options.week.startDayOfWeek ? 0 : 1;
                viewName = cal.getViewName();

                target.querySelector('input').checked = options.month.startDayOfWeek;
                break;
            case 'toggle-workweek':
                options.month.workweek = !options.month.workweek;
                options.week.workweek = !options.week.workweek;
                viewName = cal.getViewName();

                target.querySelector('input').checked = !options.month.workweek;
                break;
            default:
                break;
        }

        cal.setOptions(options, true);
        cal.changeView(viewName, true);

        setDropdownCalendarType();
        setRenderRangeText();
    }

    function onClickNavi(e) {
        var action = getDataAction(e.target);

        switch (action) {
            case 'move-prev':
                cal.prev();
                break;
            case 'move-next':
                cal.next();
                break;
            case 'move-today':
                cal.today();
                break;
            default:
                return;
        }

        setRenderRangeText();
    }

    function onChangeNewScheduleCalendar(e) {
        var target = $(e.target).closest('a[role="menuitem"]')[0];
        var calendarId = getDataAction(target);
        changeNewScheduleCalendar(calendarId);
    }

    function changeNewScheduleCalendar(calendarId) {
        var calendarNameElement = document.getElementById('calendarName');
        var calendar = findCalendar(calendarId);
        var html = [];

        html.push('<span class="calendar-bar" style="background-color: ' + calendar.bgColor + '; border-color:' + calendar.borderColor + ';"></span>');
        html.push('<span class="calendar-name">' + calendar.name + '</span>');

        calendarNameElement.innerHTML = html.join('');
        selectedCalendar = calendar;
    }

    function createNewSchedule(event) {
        var start = event.start ? moment(event.start.getTime()).format('YYYY-MM-DD H:mm') : moment().format('YYYY-MM-DD H:mm');
        var end = event.end ? moment(event.end.getTime()).format('YYYY-MM-DD H:mm') : moment().add(30, 'minutes').format('YYYY-MM-DD H:mm');

        make_agreement(0);
        make_project(0);
        make_product(0);
        make_activity(0,0);

        $("#calendarStartDate").val(start);
        $("#calendarEndDate").val(end);
        $("#completeInfo").show();
        $("#actionNewCalendar").data("role","add");
        $('#modCalendar').modal({show: "true"});
    }

    function formUpdateSchedule(e){
        $('#frmActivityExecutedTime')[0].reset();
        var date1 = moment(e.schedule.end.getTime()),
            date2 = moment(e.schedule.start.getTime());

        $("#completeInfo").hide();
        $("#calendarStartDate").val(date2.format("YYYY-MM-DD H:mm"));
        $("#calendarEndDate").val(date1.format("YYYY-MM-DD H:mm"));
        $("#txtObservationCalendar").val(e.schedule.location);

        $("#actionNewCalendar").data("role","change");
        $("#actionNewCalendar").data("id",e.schedule.id);

        $('#modCalendar').modal({show: "true"});
    }

    function updateSchedule(e){
        $('#frmActivityExecutedTime')[0].reset();
        var date1 = moment(e.end.getTime()),
            date2 = moment(e.start.getTime());
        var duration = moment.duration(date1.diff(date2)).asMinutes();

        $("#txtActivityExecutedTimeDescription").val(e.schedule.location);
        $("#txtActivityExecutedTimeActivityId").val(e.schedule.id);
        $("#txtActivityExecutedTimeEndDate").val(date1.format("YYYY-MM-DD H:mm"));
        $("#txtActivityExecutedTimeExecuted").val(duration);

        requestAjax({
            type: 'PUT',
            url: '/activity_executed_time/'+e.schedule.id,
            form: "frmActivityExecutedTime",
            retorna: true,
            resetForm: true,
        });

        e.schedule.start = e.start;
        e.schedule.end = e.end;
        cal.updateSchedule(e.schedule.id, e.schedule.calendarId, e.schedule);
    }

    function onChangeCalendars(e) {
        var calendarId = e.target.value;
        var checked = e.target.checked;
        var viewAll = document.querySelector('.lnb-calendars-item input');
        var calendarElements = Array.prototype.slice.call(document.querySelectorAll('#calendarList input'));
        var allCheckedCalendars = true;

        if (calendarId === 'all') {
            allCheckedCalendars = checked;

            calendarElements.forEach(function(input) {
                var span = input.parentNode;
                input.checked = checked;
                span.style.backgroundColor = checked ? span.style.borderColor : 'transparent';
            });

            CalendarList.forEach(function(calendar) {
                calendar.checked = checked;
            });
        } else {
            findCalendar(calendarId).checked = checked;

            allCheckedCalendars = calendarElements.every(function(input) {
                return input.checked;
            });

            if (allCheckedCalendars) {
                viewAll.checked = true;
            } else {
                viewAll.checked = false;
            }
        }

        refreshScheduleVisibility();
    }

    function setDropdownCalendarType() {
        var calendarTypeName = document.getElementById('calendarTypeName');
        var calendarTypeIcon = document.getElementById('calendarTypeIcon');
        var options = cal.getOptions();
        var type = cal.getViewName();
        var iconClassName;

        if (type === 'day') {
            type = 'Día';
            iconClassName = 'calendar-icon ic_view_day';
        } else if (type === 'week') {
            type = 'Semana';
            iconClassName = 'calendar-icon ic_view_week';
        } else if (options.month.visibleWeeksCount === 2) {
            type = '2 Semanas';
            iconClassName = 'calendar-icon ic_view_week';

            var tab = $(".tui-full-calendar-month").children();
            $(tab[1]).css("height","450px");
        } else if (options.month.visibleWeeksCount === 3) {
            type = '3 Semanas';
            iconClassName = 'calendar-icon ic_view_week';

            var tab = $(".tui-full-calendar-month").children();
            $(tab[1]).css("height","450px");
        } else {
            type = 'Mes';
            iconClassName = 'calendar-icon ic_view_month';

            var tab = $(".tui-full-calendar-month").children();
            $(tab[1]).css("height","450px");
        }

        calendarTypeName.innerHTML = type;
        calendarTypeIcon.className = iconClassName;
    }

    function setRenderRangeText() {
        var renderRange = document.getElementById('renderRange');
        var options = cal.getOptions();
        var viewName = cal.getViewName();
        var html = [];
        if (viewName === 'day') {
            html.push(moment(cal.getDate().getTime()).format('YYYY.MM.DD'));
        } else if (viewName === 'month' &&
            (!options.month.visibleWeeksCount || options.month.visibleWeeksCount > 4)) {
            html.push(moment(cal.getDate().getTime()).format('YYYY.MM'));
        } else {
            html.push(moment(cal.getDateRangeStart().getTime()).format('YYYY.MM.DD'));
            html.push(' ~ ');
            html.push(moment(cal.getDateRangeEnd().getTime()).format(' MM.DD'));
        }

        renderRange.innerHTML = html.join('');
    }

    function setEventListener() {
        $('#menu-navi').on('click', onClickNavi);
        $('.dropdown-menu a[role="menuitem"]').on('click', onClickMenu);
        $('#lnb-calendars').on('change', onChangeCalendars);
        $('#btn-new-schedule').on('click', createNewSchedule);
        $('#dropdownMenu-calendars-list').on('click', onChangeNewScheduleCalendar);

        window.addEventListener('resize', resizeThrottled);
    }

    function getDataAction(target) {
        return target.dataset ? target.dataset.action : target.getAttribute('data-action');
    }

    resizeThrottled = tui.util.throttle(function() {
        cal.render();
    }, 50);
    window.cal = cal;

    setDropdownCalendarType();
    setRenderRangeText();
    setEventListener();
})(window, tui.Calendar);

// set calendars
(function() {
    var calendarList = document.getElementById('calendarList');
    var html = [];
    CalendarList.forEach(function(calendar) {
        html.push('<div class="lnb-calendars-item"><label>' +
            '<input type="checkbox" class="tui-full-calendar-checkbox-round" value="' + calendar.id + '" checked>' +
            '<span style="border-color: ' + calendar.borderColor + '; background-color: ' + calendar.borderColor + ';"></span>' +
            '<span>' + calendar.name + '</span>' +
            '</label></div>'
        );
    });
    calendarList.innerHTML = html.join('\n');
})();

function saveNewSchedule(scheduleData) {
    var calendar = scheduleData.calendar || findCalendar(scheduleData.calendarId);
    var schedule = {
        id: scheduleData.id,
        title: scheduleData.title,
        isAllDay: scheduleData.isAllDay,
        start: scheduleData.start,
        end: scheduleData.end,
        category : scheduleData.isAllDay ? 'allday' : 'time',
        dueDateClass: '',
        color: calendar.color,
        bgColor: calendar.bgColor,
        dragBgColor: calendar.bgColor,
        borderColor: calendar.borderColor,
        location: scheduleData.location,
        comingDuration : scheduleData.comingDuration,
        raw: {
            class: scheduleData.raw['class']
        },
        state: scheduleData.state
    };
    if (calendar) {
        schedule.calendarId = calendar.id;
        schedule.color = calendar.color;
        schedule.bgColor = calendar.bgColor;
        schedule.borderColor = calendar.borderColor;
    }

    cal.createSchedules([schedule]);
    refreshScheduleVisibility();
}

function refreshScheduleVisibility() {
    var calendarElements = Array.prototype.slice.call(document.querySelectorAll('#calendarList input'));

    CalendarList.forEach(function(calendar) {
        cal.toggleSchedules(calendar.id, !calendar.checked, false);
    });

    cal.render(true);

    calendarElements.forEach(function(input) {
        var span = input.nextElementSibling;
        span.style.backgroundColor = input.checked ? span.style.borderColor : 'transparent';
    });
}

function detailSchedule(e){
    var activity_id = e.schedule.title.split(".");

    $(".tui-full-calendar-schedule-title").attr("style", "cursor:pointer");
    $(".tui-full-calendar-schedule-title").click(function(){
        window.open("/actividad/"+activity_id[0], '_blank');
    });
}

function deleteSchedule(e){
    if(confirm("Segudo desea eliminar el registro ?")){
        var response = requestAjax({
            type: 'DELETE',
            url: '/activity_executed_time/'+e.schedule.id,
            form: "frmActivityExecutedTime",
            retorna: true,
            resetForm: true,
        });

        $.when(response).done(function () {
            if(response.success){
                cal.deleteSchedule(e.schedule.id, e.schedule.calendarId);
                refreshScheduleVisibility();
            }
        });
    }
}