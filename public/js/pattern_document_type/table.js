function tablePatternDocumentType(){
    tableSearch({
        id:"tbPatternDocumentType",
        type:"GET",
        url:"/pattern_document_types",
        destroy:true,
        createdRow:1,
        columns: [
            {data: "pattern_name", name: "pattern"},
            {data: "name", name: "name"},
            {data: "state", name: "state"},
            {data: "buttons", name: "buttons", orderable: false, searchable: true}
        ]
    });
}

tablePatternDocumentType();

function editPatternDocumentType(id) {

    var response = requestAjax({
        type: "GET",
        url: "/pattern_document_type/"+id,
        form: "frmPatternDocumentType",
        retorna: true,
    });

    //Carga Formulario
    $("#actionPatternDocumentType").data("role","change");
    $("#actionPatternDocumentType").data("ref",id);
    $("#titleModal").text("Modificar Documento Modelo "+response.name);
    $("#optPattern").val(response.pattern_id);
    $("#txtName").val(response.name);

    $("#modPatternDocumentType").modal({show: "true"});
}

function destroyPatternDocumentType(id) {
    if(confirm("Seguro desea inactivar el documento modelo ?")){
        var response = requestAjax({
            type: "DELETE",
            url: "/pattern_document_type/"+id,
            form: "frmPatternDocumentType",
            success: "Registro Inactivado",
            retorna: true,
        });
        $.when(response).done(function () {
            tablePatternDocumentType();
        });
    }
}

function activatePatternDocumentType(id) {
    if(confirm("Seguro desea activar el documento modelo ?")){
        var response = requestAjax({
            type: "POST",
            url: "/pattern_document_type/"+id,
            form: "frmPatternDocumentType",
            success: "Registro Activado",
            retorna: true,
        });
        $.when(response).done(function () {
            tablePatternDocumentType();
        });
    }
}

//Funciones Botones
$("#btnPatternDocumentType").click(function() {
    $("#frmPatternDocumentType")[0].reset();
    $("#actionPatternDocumentType").data("role","add");
    $("#titleModal").text("Crear Tipo de Documento");
});

$("#actionPatternDocumentType").click(function() {
    var role = $(this).data("role");
    hideTime("#actionPatternDocumentType");

    if(role == "add"){
        var response = requestAjax({
            type: "POST",
            url: "/pattern_document_type",
            modal: "modPatternDocumentType",
            form: "frmPatternDocumentType",
            success: "Registro creado",
            resetForm: true,
        });
        $.when(response).done(function () {
            tablePatternDocumentType();
        });

        requestAjax(objButton);
    }else if(role== "change"){
        var id = $(this).data("ref");
        var response = requestAjax({
            type: "PUT",
            url: "/pattern_document_type/"+id,
            modal: "modPatternDocumentType",
            form: "frmPatternDocumentType",
            success: "Registro Modificado",
            resetForm: true,
        });
        $.when(response).done(function () {
            tablePatternDocumentType();
        });
    }
});