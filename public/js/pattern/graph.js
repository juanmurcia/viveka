function activityEstimatedTimeGraph(date_from, date_to){
    $("#frmRoleTimeFields").empty()
    .append("<input type='hidden' id='txtDateFrom' name='txtRoleTimeDateFrom' value='"+date_from+"'>")
    .append("<input type='hidden' id='txtDateTo' name='txtRoleTimeDateTo' value='"+date_to+"'>");

    var response = requestAjax({
        type: 'POST',
        form: "frmRoleTime",
        url: '/pattern_graphic_data',
        retorna: true,
    });

    $.when( response ).done(function ( response ) {
        if(typeof response.data_role_graph == 'undefined'){
            return;
        }
        lineBar({
            id: 'executed_line',
            tool: true,
            title :'',
            data: response.data_role_graph,
            legend: response.labels,
            series: response.series_role_graph
        });

        lineBar({
            id: 'percentage_line',
            tool: false,
            data: response.data_progress_graph,
            legend: response.labels,
            series: response.series_progress_graph
        });
    });
}

function initializeGraphDateRange(date_from_graph,date_to_graph) {
    if(date_from_graph==='' || date_to_graph===''){
        activityEstimatedTimeGraph('','');
    }else {
        if (moment(date_to_graph).diff(moment()) > 0) {
            date_to_graph = moment().format('YYYY-MM-DD');
        }

        activityEstimatedTimeGraph(date_from_graph,date_to_graph);

        $('#calendarRange span').html(date_from_graph + ' - ' + date_to_graph);

        $('#calendarRange').daterangepicker({
                startDate: moment(date_from_graph),
                endDate: moment(date_to_graph),
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                ranges: {
                    'Hoy': [moment(), moment()],
                    'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Últimos 7 Días': [moment().subtract(6, 'days'), moment()],
                    'Últimos 30 Días': [moment().subtract(29, 'days'), moment()],
                    'Este Mes': [moment().startOf('month'), moment().endOf('month')],
                    'Último Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'left',
                buttonClasses: ['btn btn-default'],
                applyClass: 'btn-small btn-primary',
                cancelClass: 'btn-small',
                format: 'YYYY-MM-DD',
                separator: ' to ',
                locale: {
                    applyLabel: 'Consultar',
                    cancelLabel: 'Limpiar',
                    fromLabel: 'Desde',
                    toLabel: 'Hasta',
                    customRangeLabel: 'Seleccionar',
                    daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    firstDay: 1
                }
            },
            function (start, end, label) {
                console.log('Nueva fecha seleccionada',start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'));
                activityEstimatedTimeGraph(start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'));
            }
        );
    }
}

/*
var date_from_graph = $('#txtAgreementStartDate').val();
    var date_to_graph = $('#txtAgreementEndDate').val();
 */