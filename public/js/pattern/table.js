function consultar(){
    var obj = {
        id:"tbPattern",
        type:"GET",
        url:"/patterns",
        destroy:true,
        createdRow:1,
        columns: [
            {data: "model", name: "model"},
            {data: "name", name: "name"},
            {data: "state", name: "state"},
            {data: "buttons", name: "buttons", orderable: false, searchable: true}
        ]
    };

    tableSearch(obj);
}

consultar();

//Funciones Botones
$("#btnPattern").click(function() {
    $("#frmPattern")[0].reset();
    $("#actionPattern").data("role","add");
    $("#titleModal").text("Crear Patterna ");
});

$("#actionPattern").click(function() {
    var role = $(this).data("role");
    hideTime("#actionPattern");

    if(!correoValido()){
        return false;
    }

    if(role == "add"){
        var objButton = {
            type: "POST",
            url: "/pattern",
            modal: "modPattern",
            form: "frmPattern",
            success: "Registro creado",
            resetForm: true,
            funcion : consultar(),
        };

        requestAjax(objButton);
    }else if(role== "change"){
        var id = $(this).data("ref");

        var objButton = {
            type: "PUt",
            url: "/pattern/"+id,
            modal: "modPattern",
            form: "frmPattern",
            success: "Registro Modificado",
            resetForm: true,
            funcion : consultar(),
        };

        requestAjax(objButton);
    }
});

function editPattern(id) {

    var objButton = {
        type: "GET",
        url: "/pattern/"+id,
        form: "frmPattern",
        retorna: true,
    };

    var response = requestAjax(objButton);

    //Carga Formulario
    $("#actionPattern").data("role","change");
    $("#actionPattern").data("ref",id);
    tipo_doc(response.document_type_id);
    $("#titleModal").text("Modificar Patterna "+response.name+" "+response.last_name);
    $("#optTaxpayer").val(response.taxpayer_type_id);
    $("#optDocument").val(response.document_type_id);
    $("#txtIdent").val(response.document);
    $("#txtName").val(response.name);
    $("#txtLastName").val(response.last_name);
    $("#txtAddress").val(response.address);
    $("#txtMobil").val(response.phone);
    $("#txtEmail").val(response.email);

    $("#modPattern").modal({show: "true"});
}

function destroyPattern(id) {
    if(confirm("Seguro Desea anular el modelo ?")){

        var objButton = {
            type: "DELETE",
            url: "/pattern/"+id,
            form: "frmPattern",
            success: "Registro Inactivado",
            retorna: true,
            funcion : consultar(),
        };

        requestAjax(objButton);
    }
}

function activatePattern(id) {
    if(confirm("Seguro desea activar el modelo ?")){

        var objButton = {
            type: "POST",
            url: "/pattern/"+id,
            form: "frmPattern",
            success: "Registro Activado",
            retorna: true,
            funcion : consultar(),
        };

        requestAjax(objButton);
    }
}