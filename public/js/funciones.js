//Toastr para los mensajes
toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-top-left",
    "preventDuplicates": true,
    "showDuration": "1000",
    "hideDuration": "1000",
    "timeOut": "3500",
    "extendedTimeOut": "1000",
    "showEasing": "linear",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

$('.myDatePicker2').datetimepicker({
    format: 'YYYY-MM-DD'
});

$('.myDatePicker3').datetimepicker({
    format: 'YYYY-MM-DD HH:mm'
});

$('.myDateRangePicker').daterangepicker({
    timePicker: true,
    timePicker24Hour: true,
    startDate: moment().startOf('day'),
    endDate: moment().endOf('day'),
    locale: {
        format: 'YYYY-MM-DD HH:mm'
    }
});

//La expresión regular encuentra la primer letra de cada palabra dentro de la oracíon ingresada y la transforma en mayúsculas.
String.prototype.capitalize = function(){
    // \b encuentra los limites de una palabra
    // \w solo los meta-carácter  [a-zA-Z0-9].                
    return this.toLowerCase().replace( /\b\w/g, function (m) {
        return m.toUpperCase();
    });
};
/*
    * Input:
    #@obj: Variable de tipo Object
    - type : Accion que va a realizar (Obligatorio)
    - url : Url donde se va a realizar la peticion ajax (Obligatorio)
    - form : Formulario desde donde se van a extraer los campos (Obligatorio)
    - resetForm (true|false) : Indica si resetea el formulario
    - cache :(true|false) Indica si la consulta va a ser cacheada en el navegador (No obligatorio)
    - success : Mensaje a mostrar si la transaccion es satisfactoria (No obligatoria)
    - funcion : Funciona a llamar si la peticion es satisfactoria (No)
    - div : Div donde se encuentra el formulario (No)
    - hide :(true|false) Indica si oculta el div donde se encuentra el formulario (No)
    - retorna : Indica si retorna la respuesta del servidor
*/

function requestAjax(obj){
    
    /**
    * Convierto los campos en mayuscula antes de enviarlos a las validaciones
    */
    /*$('#'+obj.form+' input[type=text]').val (function ()
    {
        return this.value.toUpperCase().trim();
    });*/

    var result='';
    $.ajax({
        type:obj.type,
        url:obj.url,
        cache: ((obj.hasOwnProperty('cache')) ? obj.cache : false),
        data:$('#'+obj.form).serialize(),
        dataType: 'json',
        async: false,
        success: function(rta){
            if(typeof rta != 'undefined')
            {
                if(rta.ErrorBd!=null){//Si existe erro de la base de datos muestro el mensaje
                    toastr["error"](rta.ErrorBd, rta.TlErrorBd);
                }else if(rta.Error){
                    toastr["error"](rta.ErrorPeril, rta.TlErrorPeril);
                    window.location.reload(true);
                }else if(rta.error){
                    toastr["error"](rta.error, '');
                }else{
                    //Si debe mostrar un mensaje de respuesta satisfactoria
                    if(obj.hasOwnProperty('success')){
                        toastr["success"](obj.success, '');
                    }
                    //Ocultar el div donde se encuentra el formulario
                    if(obj.hasOwnProperty('div') && obj.hasOwnProperty('hide') && obj.hide==true){
                        ocultar_div('#'+obj.div);
                    }

                    //Verifica si debe llamar a una funcion adicional
                    if(obj.hasOwnProperty('funcion')){
                        var fn = window[obj.funcion];
                    }

                    //Resetear el formulario
                    if(obj.hasOwnProperty('recarga') && obj.recarga==true){
                       reCarga();
                    }
                    //Resetear el formulario
                    if(obj.hasOwnProperty('form') && obj.hasOwnProperty('resetForm') && obj.resetForm==true){
                       $('#'+obj.form).trigger('reset');
                    }

                    if(obj.hasOwnProperty('modal')){
                        $('#'+obj.modal).modal('hide');
                    }

                    //Click boton, para que busque una vez se modifique un registro
                    if(obj.hasOwnProperty('clickButton') && obj.hasOwnProperty('clickButton')){
                        $("#"+obj.clickButton).trigger( "click" );
                    }
                }
                result = rta;
            }else{
                toastr["error"]("No Existe Respuesta Json!!", "");
            }
        },
        error: function(data){
            //Procesa si genero errores la peticion ajax.
            var error = data.responseJSON;
            var msj = "";
            $.each(error.errors, function(index, value) {
                msj += "<li>" + value + "</li>";
            });

            if(msj !== ""){
                toastr["error"]("<ul>"+msj+"</ul>", "Error Validación");
            }

            if(error.message){
                toastr["error"](error.message, "");
            }
        }
    });
    if(obj.retorna==true)return result;
}

function reCarga(){
    location.reload();
}

/*
    * Input:
    #objData: Variable de tipo Object
    onSelect:solo el nombre de la funcion des pues de cargar el campo ejp onSelect:'buscar_persona_cliente'
*/
function textBuscador(ArrObjData){
    var ArrData = new Array();
    if(ArrObjData instanceof Array){
        ArrData = ArrObjData;
        for(i = 0; i < ArrData.length; i++){
            var objData = ArrData[i];
            cargaTextBuscador(objData);
        }
    }else{
        cargaTextBuscador(ArrObjData);
    }
    return true;
}

function cargaTextBuscador(objData){
    var length = 2;
    
    if(objData.length){
        length = objData.length;
    }

    $('#'+ objData.field).autocomplete({
        source: function(request, response) {
            var data={
                search : request.term,
                _token: $('meta[name="csrf-token"]').attr('content')
            };
            if(objData.data){
                data=$.extend(data,objData.data);
            }
            $.ajax({
                url: objData.url,
                type: objData.type,
                dataType: "json",
                data: data,
                success: function(data) {
                    response(data);
                }
            });
        },
        minLength: length,
        autoFocus: true,
        zIndex: 9999,
        select: function(event, ui) {
            if(objData.idField){
                $('#'+ objData.idField ).val(ui.item.id);
            }
        },
        change: function(event, ui) { // <=======
            if(ui.item === null){
                $('#'+ objData.idField ).val('');
            }
        }
    });
}

function loadSelect2(objData){
    $("#"+objData.id).select2({
        minimumInputLength: objData.length ? objData.length : 0,
        quietMillis: 250,
        allowClear : true,
        placeholder: "Buscar ...",
        width : "100%",
        ajax: {
            url: objData.url,
            dataType: "json",
            type: objData.type
        }
    });
}

function hideTime(div,seg){
    /*$(div).fadeOut(10);
    setTimeout(function() {
        $(div).fadeIn(1500);
    },seg);*/
    $(div).css("pointer-events", "none");
    //do something
    setTimeout(function() {
        $(div).css("pointer-events", "auto");
    },seg);

}

// funcion para validar el correo
function correoValido(){    
    
    var email = $("input[type=email]").val();
    var caract = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);

    if (caract.test(email) == false){
        //$(div).hide().removeClass('hide').slideDown('fast');
        toastr['error']('Debe ingresar un correo válido', "");
        $("input[type=email]").focus();
        return false;
    }else{
        //$(div).hide().addClass('hide').slideDown('slow');
//        $(div).html('');        
        return true;
    }
}

function arrayDateRange(from_date, to_date){
    from_date = moment(from_date);
    to_date = moment(to_date);

    var count = to_date.diff(from_date, 'days');
    var dias = 0;
    var date_array = [from_date.format('YYYY-MM-DD')];

    while (dias < count){
        var date = from_date;
        date.add(1, 'days');
        date_array.push(date.format('YYYY-MM-DD'));
        dias += 1;
    }

    return date_array;
}