function createActivityEstimatedTime(pattern_id,model_id,role_id) {
    $("#frmActivityEstimatedTime")[0].reset();
    $("#txtActivityEstimatedTimePatternId").val(pattern_id);
    $("#txtActivityEstimatedTimeModelId").val(model_id);
    $("#optActivityEstimatedTimeRoleId").val(role_id);
    var response = requestAjax({
        type: 'GET',
        url: '/activity_estimated_time/' + pattern_id + '/' + model_id + '/' + role_id ,
        retorna: true,
    });
    if(response.data.length>0){
        $("#txtActivityEstimatedTimeEstimated").val(response.data[0].estimated);
    }

}

function activityEstimatedTimes(pattern_id,model_id) {
    var response = requestAjax({
        type: 'GET',
        url: '/activity_estimated_times/' + pattern_id + '/' + model_id ,
        retorna: true,
    });

    $("#activityEstimatedTimes").empty();

    $.each(response.data, function (index, val) {
        showTime(val);
    });

    $('#activityEstimatedTimes > div').fadeIn(1500);

    $("#activityEstimatedTimes").append($("#actionsActivityEstimatedTime > *").clone());
}

function showTime(val) {

    var div = $("#pattern-estimated-time-resume").clone();
    $(div).attr("id",null);
    $(div).find(".pattern-estimated-time-resume-rol").text(val.role);
    $(div).find(".pattern-estimated-time-resume-estimated").text(val.estimated);
    $(div).find(".pattern-estimated-time-resume-executed").text(val.executed);
    $(div).find(".pattern-estimated-time-resume-edit").click(function () {
        createActivityEstimatedTime(val.pattern_id,val.model_id,val.role_id);
    });
    val.percentage_executed=0;
    if(parseFloat(val.estimated)>0){
        val.percentage_executed=100*parseFloat(val.executed)/parseFloat(val.estimated);
    }
    $(div).find(".knobManual").val(val.percentage_executed);

    var max=100;
    var fgcolor="#F39100";
    var percentage_executed=parseInt(val.percentage_executed);
    if(percentage_executed>max){
        max=percentage_executed;
        fgcolor="#d9534f";
    }

    $("#activityEstimatedTimes").append(div);
    $(div).find(".knobManual").knob( {
        thickness: ".2",
        readOnly: true,
        fgColor:fgcolor,
        max: max,
        parse: function (v) {return parseInt(v);},
        format: function (v) {return v + "%";}
    });
    $(div).fadeIn(1500);
}

$("#actionActivityEstimatedTime").click(function () {
    hideTime("#actionActivityEstimatedTime");

    var response = requestAjax({
        type: "POST",
        url: "/activity_estimated_times",
        form: "frmActivityEstimatedTime",
        success: "Registro creado",
        resetForm: true,
        modal: "modActivityEstimatedTime",
    });
    $.when(response).done(function () {
        //activityEstimatedTimes($("#pattern_id").val(),$("#model_id").val());
        $("#activities-tabb").click();
    });
});