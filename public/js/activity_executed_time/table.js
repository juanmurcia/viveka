cargaTextBuscador({
    field:"TBUStxtActivityExecutedTimeActivityId",
    idField:"txtActivityExecutedTimeActivityId",
    url:"/activities",
    type:"POST",
    length:"1"
});

$('#actionActivityExecutedTime').click(function(){
    tableSearch({
        id:'tbActivityExecutedTime',
        type:'POST',
        url:'/activity_executed_times',
        form:'frmSearchActivityExecutedTimes',
        destroy:true,
        sort:[[0,'desc']],
        columns: [
            {data: 'end_date', name: 'end_date'},
            {data: 'executed', name: 'executed'},
            {data: 'person_name', name: 'person_name'},
            {data: 'description', name: 'description'},
            {data: 'activity_name', name: 'activity'},
            {data: 'pattern', name: 'pattern'},
            {data: 'pattern_name', name: 'pattern_name'},
            {data: 'buttons', name: 'buttons', orderable: false, searchable: true}
        ]


    });
});

$('#actionActivityExecutedTime').click();