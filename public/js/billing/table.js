$('#actionBilling').click(function(){
    tableSearch({
        id:'tbBilling',
        type:'POST',
        url:'/billings',
        form:'frmBilling',
        destroy:true,
        columns: [
            {data: 'number', name: 'number', className: "text-right"},
            {data: 'agreement.number', name: 'agreement', className: "text-right"},
            {data: 'pattern_state.name', name: 'pattern_state', className: "text-right"},
            {data: 'billing_date', name: 'billing_date', className: "text-center"},
            {data: 'due_date', name: 'due_date', className: "text-center"},
            {data: 'value', name: 'value', className: "text-right"},
            {data: 'taxes', name: 'taxes', className: "text-right"},
            {data: 'net_value', name: 'net_value', className: "text-right"},
            {data: 'buttons', name: 'buttons', orderable: false, searchable: false}
        ]
    });
});

$('#actionBilling').click();