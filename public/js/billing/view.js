var billing_id = $('#billingId').val();
var agreement_id = 0;
var document_added = [];

var tbProduct = $('#tbProduct').DataTable({
    responsive: true,
    searching: false,
    lengthMenu: [[25,50,100, -1], [25,50, 100, "Todo"]],
    columns: [
        {data: 'name', name: 'name'},
        {data: 'end_date', name: 'end_date', className: "text-center"},
        {data: 'responsible', name: 'responsible', className: "text-right"},
        {data: 'val', name: 'val', className: "text-right"},
        {data: 'buttons', name: 'buttons'}
    ]
});

var tbDetBilling = $('#tbDetBilling').DataTable({
    responsive: true,
    searching: false,
    lengthMenu: [[25,50,100, -1], [25,50, 100, "Todo"]],
    columns: [
        {data: 'id', name: 'id'},
        {data: 'name', name: 'name'},
        {data: 'end_date', name: 'end_date', className: "text-center"},
        {data: 'description', name: 'description', className: "text-left"},
        {data: 'val', name: 'val', className: "text-right"},
        {data: 'buttons', name: 'buttons'}
    ]
});

var tbDocumentBilling = $('#tbBillingDocuments').DataTable({
    responsive: true,
    searching: false,
    lengthMenu: [[25,50,100, -1], [25,50, 100, "Todo"]],
    columns: [
        {data: "pattern_document_type_name", name: "pattern_document_type"},
        {data: "name", name: "name", class: "text-left"},
        {data: "user_mod_name", name: "user_mod_name", class: "text-left"},
        {data: "created_at", name: "created_at"},
        {data: "updated_at", name: "updated_at"},
        {data: "buttons", name: "buttons", orderable: false, searchable: false}
    ]
});

var tbDocumentVersion = $('#tbDocumentsVersion').DataTable({
    responsive: true,
    searching: true,
    lengthMenu: [[25,50,100, -1], [25,50, 100, "Todo"]],
    columns: [
        {data: "name", name: "name", class: "text-left"},
        {data: "keywords", name: "keywords", class: "text-left"},
        {data: "user_name", name: "user_name", class: "text-left"},
        {data: "date", name: "date"},
        {data: "buttons", name: "buttons", orderable: false, searchable: false}
    ]
});

var tbDocumentAdded = $('#tbDocumentsAddded').DataTable({
    responsive: true,
    searching: true,
    lengthMenu: [[25,50,100, -1], [25,50, 100, "Todo"]],
    columns: [
        {data: "type", name: "type", class: "text-center"},
        {data: "name", name: "name", class: "text-left"},
        {data: "date", name: "date"},
        {data: "buttons", name: "buttons", orderable: false, searchable: false}
    ]
});

billing_detail(false);

function billing_detail(loaded){
    var response = requestAjax({
        type: "GET",
        url: "/billing/"+billing_id,
        retorna: true,
    });

    $.when( response ).done(function ( request ) {
        if(request.id){
            agreement_id = request.agreement_id;

            if(!loaded){
                var div = $("#address").clone();
                $(div).find("#name").text(request.agreement.contractor.person.document+' - '+request.agreement.contractor.person.name);
                $(div).find("#address").text(request.agreement.contractor.person.address);
                $(div).find("#phone").text(request.agreement.contractor.person.phone);
                $(div).find("#email").text(request.agreement.contractor.person.email);
                $("#from").append(div);

                div = $("#address").clone();
                $(div).find("#name").text(request.agreement.employer.person.document+' - '+request.agreement.employer.person.name);
                $(div).find("#address").text(request.agreement.employer.person.address);
                $(div).find("#phone").text(request.agreement.employer.person.phone);
                $(div).find("#email").text(request.agreement.employer.person.email);
                $("#to").append(div);

                $('#divBilling').find("#created").text(request.billing_date);
                $('#divBilling').find("#due_date").text(request.due_date);
                $('#txtBillingDate').val(request.billing_date);
                $('#divBilling').find("#number").text(request.number);
                $('#numberI').val(request.number);
                $('#divBilling').find("#agreement").text(request.agreement.number);
                $('#divBilling').find("#txtDetailBilling").text(request.comment);
                $('#divBilling').find("#divDetailBilling").text(request.comment);
            }

            $('#btnBillingDocument').data('document',request.agreement.contractor.id);
            $('#divBilling').find("#value").empty();
            $('#divBilling').find("#tax").empty();
            $('#divBilling').find("#net_value").empty();

            $('#divBilling').find("#value").text(numberFormat(request.value,0,',','.'));
            $('#divBilling').find("#tax").text(numberFormat(request.taxes,0,',','.'));
            $('#divBilling').find("#net_value").text(numberFormat(request.net_value,0,',','.'));

            tbDetBilling.clear().draw();
            $.each(response.billing_detail, function (index, val) {

                addColumnDataTable(tbDetBilling,{
                    "id":  val.product.id,
                    "name":  val.product.name,
                    "end_date":  val.product.end_date,
                    "description":  val.product.description,
                    "val":  '$ '+numberFormat(val.value,0,',','.'),
                    "buttons" : '<button class="btn btn-danger btn-xs del-product" onclick="delProduct('+val.id+');"><i class="fa fa-close"></i></button>',
                });
            });

            document_added = JSON.parse( response.documentation );
            tbDocumentAdded.clear().draw();
            $.each(document_added, function (index, val) {
                if(val.document_id){
                    var link = '/document/download/'+val.document_id+'/'+val.version+'/'+val.name;
                    addColumnDataTable(tbDocumentAdded,{
                        "type":  val.type,
                        "name":  val.name,
                        "date":  val.date,
                        "buttons" : '<a href="'+link+'" class="btn btn-default btn-md"><i class="fa fa-download"></i></a><button class="btn btn-danger btn-md" onclick="delDocument('+val.document_id+');"><i class="fa fa-close"></i></button>',
                    });
                }
            });

            $('#divBillingDate').show();
            $('#created').hide();
            $('#numberI').show();
            $('#number').hide();
            $('#txtDetailBilling').show();
            $('#divDetailBilling').hide();
            $('#btnGeneratedBilling').show();
            $('#btnCloseBilling').hide();

            if(request.pattern_state_id == 45){
                $('#divBillingDate').hide();
                $('#created').show();
                $('#numberI').hide();
                $('#number').show();
                $('#txtDetailBilling').hide();
                $('#divDetailBilling').show();
                $('#btnProductBilling').hide();
                $('.del-product').hide();
                $('#btnGeneratedBilling').hide();
                $('#btnCloseBilling').show();
            }
        }
    });
}

function productBilling(){
    var response = requestAjax({
        type: "GET",
        url: "/product_billing/"+agreement_id,
        retorna: true,
    });

    tbProduct.clear().draw();
    $.when( response ).done(function ( request ) {
        if(Array.isArray(request.data)){
            $.each(response.data, function (index, val) {
                addColumnDataTable(tbProduct,{
                    "name":  val.name,
                    "end_date":  val.end_date,
                    "responsible":  val.responsible.person.name+' '+val.responsible.person.last_name,
                    "val":  numberFormat(val.value,0,',','.'),
                    "buttons" : '<button class="btn btn-success btn-xs" onclick="addProduct('+val.id+');"><i class="fa fa-check"></i></button>',
                });
            });
        }

        $('#modProductBilling').modal({show: 'true'});
    });
}

function addProduct(product_id){
    requestAjax({
        type: "GET",
        url: "/billing/"+billing_id+'/'+product_id,
    });

    billing_detail(true);
    productBilling();
}

function delProduct(detail_id){
    var request = requestAjax({
        type: "GET",
        url: "/billing_detail/"+detail_id,
        success: "Producto Eliminado",
    });

    billing_detail(true);
}

function delBilling(){
    if(confirm('Seguro desea anular la facturación ?')){
        requestAjax({
            type: "DELETE",
            url: "/billing/"+billing_id,
            success: "Facturación Anulada",
            form : 'frmUpBilling'
        });

        billing_detail(true);
    }
}

function saveBilling(){
    requestAjax({
        type: "PUT",
        url: "/billing/"+billing_id,
        success: "Facturación Actualziada",
        form : 'frmUpBilling'
    });

    billing_detail(true);
}

function closeBilling(){
    requestAjax({
        type: "GET",
        url: "/billing_close/"+billing_id,
        success: "Facturación Cerrada",
        form : 'frmUpBilling'
    });

    billing_detail(true);
}

function sendBilling(){
    $.each(document_added, function (index, val) {
        $(location).attr('href','/document/download/'+val.document_id+'/'+val.version+'/'+val.name);
    });
}

// Document Billing

function searchDocument(){
    var company_id = $('#btnBillingDocument').data('document');

    var request = requestAjax({
        type: "GET",
        url: "/documents/6/"+company_id,
        retorna : true,
    });

    tbDocumentBilling.clear().draw();
    $.each(request.data, function (index, val) {
        addColumnDataTable(tbDocumentBilling,{
            "pattern_document_type_name":  val.pattern_document_type_name,
            "name":  val.name,
            "user_mod_name":  val.user_mod_name,
            "created_at":  val.created_at,
            "updated_at":  val.updated_at,
            "buttons" : '<button class="btn btn-default btn-xs" onclick="versionDocumentBilling('+val.id+');"><i class="fa fa-clock-o"></i></button>',
        });
    });
}

function versionDocumentBilling(id) {

    var response = requestAjax({
        type: "GET",
        url: "/document/versions/"+id,
        form: "frmDocument",
        retorna: true,
    });

    tbDocumentVersion.clear().draw();
    $.each(response.data, function (index, val) {
        var version = "'"+val.version+"'";
        addColumnDataTable(tbDocumentVersion,{
            "name":  val.name,
            "keywords":  val.keywords,
            "user_name":  val.user_name,
            "date":  val.date,
            "buttons" : '<button class="btn btn-success btn-xs" onclick="addDocumentBilling('+id+','+version+');"><i class="fa fa-check"></i></button>',
        });
    });

    $("#modBillingDocument").modal('toggle');
    $("#modDocumentVersion").modal({show: "true"});
}

function addDocumentBilling(id, version){
    requestAjax({
        type: "GET",
        url: "/billing_document/"+billing_id+"/"+id+"/"+version,
    });

    $("#modBillingDocument").modal({show: "true"});
    $("#modDocumentVersion").modal('toggle');

    billing_detail(true);
}

function delDocument(document_id){
    requestAjax({
        type: "GET",
        url: "/billing_document/"+billing_id+"/"+document_id,
    });

    billing_detail(true);
}