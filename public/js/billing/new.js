loadSelect2({
    id : "txtBillingAgreement",
    url: "/agreement_search",
    type:"GET"
});

$('#newBilling').click(function () {
    if(confirm('Desea generar una facturación para este contrato ?')){
        var response = requestAjax({
            type: "POST",
            url: "/billing",
            form: "frmNewBilling",
            retorna: true,
        });

        $.when( response ).done(function ( request ) {
            if(request.id){
                $(location).attr('href','/facturacion/'+request.id);
            }
        });
    }
});