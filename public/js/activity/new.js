$("#newActivity").click(function() {
    hideTime("#actionActivity");
    //Guarda campos extra antes del envío
    $("#txtActivityPatternId").remove();
    $("<input>").attr({
        type: "hidden",
        id: "txtActivityPatternId",
        name: "txtActivityPatternId",
        value: $("#pattern_id").val()
    }).appendTo("#frmActivity");
    $("#txtActivityModelId").remove();
    $("<input>").attr({
        type: "hidden",
        id: "txtActivityModelId",
        name: "txtActivityModelId",
        value: $("#id").val()
    }).appendTo("#frmActivity");

   var response = requestAjax({
        type: "POST",
        url: "/activity",
        form: "frmActivity",
        success: "Registro creado",
        resetForm: true,
        modal: "modActivity",
   });
   $.when(response).done(function () {
       $("#activities-tabb").click();
   });
});