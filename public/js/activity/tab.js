function tableActivities(){
    var pattern_id = $("#tbActivities").data("pattern-id");
    var model_id = $("#tbActivities").data("model-id");
    tableSearch({
        id:'tbActivities',
        type:'GET',
        url:"/activities/"+pattern_id+"/"+model_id,
        destroy:true,
        checkInactive:true,
        sort:[[0,'asc'],[1,'asc']],
        columns: [
            {data: "start_date", name: "start_date", className: "text-left"},
            {data: "end_date", name: "end_date", className: "text-left"},
            {data: "name", name: "name", className: "text-left"},
            {data: "pattern_state.name", name: "state", className: "text-center"},
            {data: "buttons", name: "buttons", orderable: false, searchable: false}
        ]
    });
}

function createActivity(pattern_id,model_id) {
    $("#frmActivity")[0].reset();
    $("#actionActivity").data("role","add");
    $("#titleModalActivity").html("Nueva Actividad");
    $("#txtActivityPatternId").val(pattern_id);
    $("#txtActivityModelId").val(model_id);
}

function editActivity(activity_id) {
    $("#frmActivity")[0].reset();
    $("#actionActivity").data("role","change")
    $("#titleModalActivity").html("Editar Actividad");
    $("#txtActivityId").val(activity_id);

    var activity = requestAjax({
        type: "GET",
        url: "/activity/"+activity_id,
        form: "frmPerson",
        retorna: true,
    });

    $("#txtActivityPatternId").val(activity.pattern_id);
    $("#txtActivityModelId").val(activity.model_id);
    $("#txtActivityDescription").val(activity.description);
    $("#txtActivityName").val(activity.name);
    $("#txtActivityDateRange").val(activity.start_date+' - '+activity.end_date);
    $("#optActivityPatternStateId").val(activity.pattern_state_id);

    $('#modActivity').modal('show');
}

$("#actionActivity").click(function () {
    var role = $(this).data("role");
    var pattern_id = $("#txtActivityPatternId").val();
    var model_id = $("#txtActivityModelId").val();
    hideTime("#actionActivity");

    if(role == "add"){
        var response = requestAjax({
            type: "POST",
            url: "/activity",
            form: "frmActivity",
            success: "Registro creado",
            resetForm: true,
            modal: "modActivity",
        });
        $.when(response).done(function () {
            //tableActivities(pattern_id,model_id);
            $("#activities-tabb").click();
        });
    }else if(role== "change"){
        var id = $("#txtActivityId").val();

        var response = requestAjax({
            type: "PUT",
            url: "/activity/"+id,
            form: "frmActivity",
            success: "Registro Modificado",
            resetForm: true,
            modal: "modActivity",
        });
        $.when(response).done(function () {
            //tableActivities(pattern_id,model_id);
            $("#activities-tabb").click();
        });
    }
});

function patternTimeCostResume(pattern_id,model_id) {
    var times = requestAjax({
        type: 'GET',
        url: '/pattern/' + pattern_id + '/' + model_id + '/times',
        retorna: true,
    });

    var costs = requestAjax({
        type: 'GET',
        url: '/pattern/' + pattern_id + '/' + model_id + '/costs',
        retorna: true,
    });

    $("#patternTimeCostResume").empty();

    $.each(times.data, function (index, val) {
        showPatternTime(val);
    });

    $.each(costs.data, function (index, val) {
        showPatternCost(val);
    });

    $('#patternTimeCostResume > div').fadeIn(1500);
}

function showPatternTime(val) {
    if(parseFloat(val.estimated)===0.0 && parseFloat(val.executed)===0.0){
        return true;
    }

    var div = $("#pattern-time-resume").clone();

    $(div).attr("id",null);
    $(div).find(".pattern-time-resume-rol").text(val.role);
    $(div).find(".pattern-time-resume-estimated").text($.number(val.estimated));
    $(div).find(".pattern-time-resume-executed").text($.number(val.executed));

    val.percentage_executed=0;
    if(parseFloat(val.estimated)>0){
        val.percentage_executed=100*parseFloat(val.executed)/parseFloat(val.estimated);
    }
    $(div).find(".knobManual").val(val.percentage_executed);

    var max=100;
    var fgcolor="#F39100";
    var percentage_executed=parseInt(val.percentage_executed);
    if(percentage_executed>max){
        max=percentage_executed;
        fgcolor="#d9534f";
    }

    $("#patternTimeCostResume").append(div);
    $(div).find(".knobManual").knob( {
        thickness: ".2",
        readOnly: true,
        fgColor:fgcolor,
        max: max,
        parse: function (v) {return parseInt(v);},
        format: function (v) {return v + "%";}
    });
    //$(div).fadeIn(1500);
}

function showPatternCost(val) {
    if(parseFloat(val.estimated)===0.0 && parseFloat(val.executed)===0.0){
        return true;
    }

    var div = $("#pattern-cost-resume").clone();

    $(div).attr("id",null);
    $(div).find(".pattern-cost-resume-type").text(val.activity_cost_category);
    $(div).find(".pattern-cost-resume-estimated").text($.number(val.estimated));
    $(div).find(".pattern-cost-resume-executed").text($.number(val.executed));

    val.percentage_executed=0;
    if(parseFloat(val.estimated)>0){
        val.percentage_executed=100*parseFloat(val.executed)/parseFloat(val.estimated);
    }
    $(div).find(".knobManual").val(val.percentage_executed);

    var max=100;
    var fgcolor="#F39100";
    var percentage_executed=parseInt(val.percentage_executed);
    if(percentage_executed>max){
        max=percentage_executed;
        fgcolor="#d9534f";
    }

    $("#patternTimeCostResume").append(div);
    $(div).find(".knobManual").knob( {
        thickness: ".2",
        readOnly: true,
        fgColor:fgcolor,
        max: max,
        parse: function (v) {return parseInt(v);},
        format: function (v) {return v + "%";}
    });
    //$(div).fadeIn(1500);
}

function addActivityTime(activity_id) {
    $("#frmActivityTime")[0].reset();
    $("#txtActivityTimeActivityId").val(activity_id);
    $("#txtActivityTimePersonId").val($("#auth_person_id").val());

    $('#modActivityTime').modal('show');
}

function viewActivityTimes(activity_id) {
    console.log('entro');
    tableSearch({
        id:'tbActivityTimes',
        type:'GET',
        url:"/activity_times/"+activity_id,
        destroy:true,
        sort:[[0,'desc']],
        columns: [
            {data: "end_date", name: "end_date", className: "text-left"},
            {data: "executed", name: "executed", className: "text-left"},
            {data: "description", name: "description", className: "text-left"},
            {data: "person.name", name: "person_name", className: "text-center"}
        ]
    });

    $('#modViewActivityTimes').modal('show');
}

$("#actionActivityTime").click(function () {
    hideTime("#actionActivityTime");

    var response = requestAjax({
        type: "POST",
        url: "/activity_time",
        form: "frmActivityTime",
        success: "Registro creado",
        resetForm: true,
        modal: "modActivityTime",
    });
    $.when(response).done(function () {
        //tableActivities(pattern_id,model_id);
        $("#activities-tabb").click();
    });
});



