cargaTextBuscador({
    field:"TBUStxtActivityExecutedTimePersonId",
    idField:"txtActivityExecutedTimePersonId",
    url:"/people",
    type:"POST",
    length:"1"
});

function editActivity(id){
    var activity = requestAjax({
        type: "GET",
        url: "/activity/"+id,
        form: "frmActivity",
        retorna: true,
    });
    //view
    $('#activity_name').text(activity.name);
    $('#activity_parent_name').text(activity.parent_name);
    $('#activity_parent_name').attr("href",activity.parent_url);
    //basic
    $("#txtActivityName").val(activity.name);
    $("#txtActivityDescription").val(activity.description);
    $("#txtActivityStartDate").val(activity.start_date);
    $("#txtActivityEndDate").val(activity.end_date);
    //progress
    $("#optActivityPatternState").val(activity.pattern_state_id);
    $("#txtActivityPercentage").val(activity.percentage).attr('min',activity.percentage);
    $("#txtActivityJustification").val(activity.justification);
}

$("#actionActivityBasics").click(function() {
    hideTime("#actionActivityBasics");
    requestAjax({
        type: "PUT",
        url: "/activity/"+$("#activity_id").val(),
        form: "frmActivityBasics",
        success: "Registro modificado",
    });
});

$("#actionActivityProgress").click(function() {
    hideTime("#actionActivityProgress");
    requestAjax({
        type: "PUT",
        url: "/activity/"+$("#activity_id").val()+"/progress",
        form: "frmActivityProgress",
        success: "Registro modificado",
    });
});