$("#menu_activity").addClass('current-page');

$("#general-tabb").click(function(){
    editActivity($("#activity_id").val());
});

$("#times-tabb").click(function(){
    activityTimes($("#activity_id").val());
    tableActivityTimes();
});

$("#costs-tabb").click(function(){
    activityCosts($("#activity_id").val());
    tableActivityCosts();
});

$("#documents-tabb").click(function(){
    tableDocuments();
});

editActivity($("#activity_id").val())
$( document ).ready(function() {
    $("#general-tabb").click();
});