cargaTextBuscador({
    field:"TBUStxtActivityExecutedCostPersonId",
    idField:"txtActivityExecutedCostPersonId",
    url:"/people",
    type:"POST",
    length:"1"
});

function load_subcategory(){
    var category = $( "#optActivityExecutedCostActivityCostCategoryN option:selected" ).val();
    var subcategory = $( "#cat"+category ).data('subcategory');

    $('#optActivityExecutedCostActivityCostSubCategoryId').empty().append('<option value="">..Seleccione uno ..</option>');
    $.each(subcategory, function (index, val) {
        if(val.active){
            $('#optActivityExecutedCostActivityCostSubCategoryId').append('<option value="'+val.id+'">'+val.name+'</option>');
        }
    });
}

function createActivityExecutedCost(id){
    $('#frmActivityExecutedCost')[0].reset();
    $("#txtActivityExecutedCostActivityId").val(id);
    $('#modActivityExecutedCost').modal({show: 'true'});
    $("#txtActivityExecutedCostPersonId").val($("#auth_person_id").val());
    $("#TBUStxtActivityExecutedCostPersonId").val('Yo');
}

$("#actionActivityExecutedCost").click(function () {
    hideTime("#actionActivityExecutedCost");

    if($("#txtActivityExecutedCostActivityId").val() == ""){
        $("#txtActivityExecutedCostActivityId").val($("#activity_id").val());
    }

    var response = requestAjax({
        type: "POST",
        url: "/activity_executed_costs",
        form: "frmActivityExecutedCost",
        success: "Registro creado",
        retorna: true,
        modal: "modActivityExecutedCost",
    });

    $.when(response).done(function (response) {
        if(response.id){
            $('#txtDocumentId').val(response.id);
            $('#txtDocumentModelId').val(13);
            $("#txtDocumentPatternDocumentTypeId option[value=1]").attr("selected",true);
            $("#frmDocument")[0].reset();
            $("#actionDocument").data("role","add");
            $('#modDocument').modal({show: 'true'});
            $('#modActivityExecutedCost').modal('hide');

            $("#costs-tabb").click();
        }
    });
});