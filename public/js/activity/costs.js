function tableActivityCosts(){
    var activity_id = $("#activity_id").val();
    tableSearch({
        id : 'tbActivityCosts',
        type : 'GET',
        url : "/activity/"+activity_id+"/executed_costs",
        destroy : true,
        exportButtons : true,
        checkInactive : true,
        sort : [[0,'asc'],[1,'asc']],
        columns : [
            {data: "end_date", name: "end_date", className: "text-left"},
            {data: "activity_cost_subcategory.activity_cost_category.name", name: "category", className: "text-left"},
            {data: "activity_cost_subcategory.name", name: "subcategory", className: "text-left"},
            {data: "executed", name: "executed", className: "text-right",render: $.fn.dataTable.render.number( '.', '.', 0, '$' )},
            {data: "description", name: "description", className: "text-left"},
            {data: "person_name", name: "person_name", className: "text-center"},
            {data: "buttons", name: "buttons", orderable: false, searchable: false}
        ]
    });
}

function setActivityEstimatedCost(activity_id,role_id,estimated) {
    $("#frmActivityEstimatedCost")[0].reset();
    $("#txtActivityEstimatedCostActivityId").val(activity_id);
    $("#optActivityEstimatedCostRoleId").val(role_id);
    $("#txtActivityEstimatedCostEstimated").val(estimated);
}

function activityCosts(activity_id) {
    var response = requestAjax({
        type: 'GET',
        url: '/activity/' + activity_id + '/costs',
        retorna: true,
    });

    $("#activityCosts").empty();

    $.each(response.data, function (index, val) {
        showActivityCost(val,activity_id);
    });

    $('#activityCosts > div').fadeIn(1500);

    $("#activityCosts").append($("#activityCostActions > *").clone());


    $("#txtActivityExecutedCostPersonId").val($("#auth_person_id").val());
    $("#TBUStxtActivityExecutedCostPersonId").val('Yo');
}

function documentActivityExecutedCost(activity_cost_id){
    $("#tbDocuments").data("pattern-id", '13');
    $("#tbDocuments").data("model-id",activity_cost_id);
    tableDocuments();
}

function showActivityCost(val,activity_id) {
    if(parseFloat(val.estimated)===0.0 && parseFloat(val.executed)===0.0){
        return true;
    }

    var div = $("#activity-cost-resume").clone();
    $(div).attr("id",null);
    $(div).find(".activity-cost-resume-type").text(val.activity_cost_category);
    $(div).find(".activity-cost-resume-estimated").text($.number(val.estimated));
    $(div).find(".activity-cost-resume-executed").text($.number(val.executed));
    $(div).find(".activity-cost-resume-edit").click(function () {
        setActivityEstimatedCost(activity_id,val.role_id,val.estimated);
    });
    val.percentage_executed=0;
    if(parseFloat(val.estimated)>0){
        val.percentage_executed=100*parseFloat(val.executed)/parseFloat(val.estimated);
    }
    $(div).find(".knobManual").val(val.percentage_executed);

    var max=100;
    var fgcolor="#F39100";
    var percentage_executed=parseInt(val.percentage_executed);
    if(percentage_executed>max){
        max=percentage_executed;
        fgcolor="#d9534f";
    }

    $("#activityCosts").append(div);
    $(div).find(".knobManual").knob( {
        thickness: ".2",
        readOnly: true,
        fgColor:fgcolor,
        max: max,
        parse: function (v) {return parseInt(v);},
        format: function (v) {return v + "%";}
    });
    $(div).fadeIn(1500);
}

$("#actionActivityEstimatedCost").click(function () {
    hideTime("#actionActivityEstimatedCost");

    $("#txtActivityEstimatedCostActivityId").val($("#activity_id").val());

    var response = requestAjax({
        type: "POST",
        url: "/activity_estimated_costs",
        form: "frmActivityEstimatedCost",
        success: "Registro creado",
        resetForm: true,
        modal: "modActivityEstimatedCost",
    });
    $.when(response).done(function () {
        $("#costs-tabb").click();
    });
});