cargaTextBuscador({
    field:"TBUStxtActivityModel",
    idField:"txtActivityModel",
    url:"/pattern_names",
    type:"POST",
    length:"1"
});

loadSelect2({
    id : "txtActivityResponsible",
    url: "/agreement_team/"+$("#agreement_id").val(),
    type:"GET"
});

$('#actionActivity').click(function(){
    tableSearch({
        id:'tbActivities',
        type:'POST',
        url:'/activities',
        form:'frmActivity',
        destroy:true,
        columns: [
            {data: 'pattern.name', name: 'project', className: "text-right"},
            {data: 'pattern_state.name', name: 'state'},
            {data: 'name', name: 'name'},
            {data: 'description', name: 'description'},
            {data: 'percentage', name: 'percentage'},
            {data: 'start_date', name: 'start_date'},
            {data: 'end_date', name: 'end_date'},
            {data: 'buttons', name: 'buttons', orderable: false, searchable: true}
        ]


    });
});

$('#actionActivity').click();

function addActivityCallBack() {
    $('#actionActivity').click();
};