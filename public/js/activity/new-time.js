function createActivityExecutedTime(id){
    $('#frmActivityExecutedTime')[0].reset();
    $("#txtActivityExecutedTimeActivityId").val(id);
    $('#modActivityExecutedTime').modal({show: 'true'});
}

$("#actionActivityExecutedTime").click(function () {
    hideTime("#actionActivityExecutedTime");

    if($("#txtActivityExecutedTimeActivityId").val() == ""){
        $("#txtActivityExecutedTimeActivityId").val($("#activity_id").val());
    }

    var response = requestAjax({
        type: "POST",
        url: "/activity_executed_time",
        form: "frmActivityExecutedTime",
        success: "Registro creado",
        resetForm: true,
        modal: "modActivityExecutedTime",
    });
    $.when(response).done(function () {
        $("#times-tabb").click();
    });
});

function setExecutedTime(){
    var start = $("#txtActivityExecutedTimeStartDate").val();
    var end = $("#txtActivityExecutedTimeEndDate").val();

    if(start && end){
        var date1 = moment(end,"YYYY-MM-DD H:mm"),
            date2 = moment(start,"YYYY-MM-DD H:mm");
        var duration = moment.duration(date1.diff(date2)).asMinutes();
        if(date1 > date2){
            $("#txtActivityExecutedTimeExecuted").val(duration);
        }
    }
}