function tableActivityTimes(){
    var activity_id = $("#activity_id").val();
    tableSearch({
        id:'tbActivityTimes',
        type:'GET',
        url:"/activity/"+activity_id+"/executed_times",
        destroy:true,
        checkInactive:true,
        sort:[[0,'asc'],[1,'asc']],
        columns: [
            {data: "end_date", name: "end_date", className: "text-left"},
            {data: "executed", name: "executed", className: "text-left"},
            {data: "description", name: "description", className: "text-left"},
            {data: "person_name", name: "person_name", className: "text-center"},
            {data: "buttons", name: "buttons", orderable: false, searchable: false}
        ]
    });
}

function setActivityEstimatedTime(activity_id,role_id,estimated) {
    $("#frmActivityEstimatedTime")[0].reset();
    $("#txtActivityEstimatedTimeActivityId").val(activity_id);
    $("#optActivityEstimatedTimeRoleId").val(role_id);
    $("#txtActivityEstimatedTimeEstimated").val(estimated);
}

function activityTimes(activity_id) {
    var response = requestAjax({
        type: 'GET',
        url: '/activity/' + activity_id + '/times',
        retorna: true,
    });

    $("#activityTimes").empty();

    $.each(response.data, function (index, val) {
        showActivityTime(val,activity_id);
    });

    $('#activityTimes > div').fadeIn(1500);

    $("#activityTimes").append($("#activityTimeActions > *").clone());
}

function showActivityTime(val,activity_id) {
    if(parseFloat(val.estimated)===0.0 && parseFloat(val.executed)===0.0){
        return true;
    }

    var div = $("#activity-time-resume").clone();
    $(div).attr("id",null);
    $(div).find(".activity-time-resume-rol").text(val.role);
    $(div).find(".activity-time-resume-estimated").text($.number(val.estimated));
    $(div).find(".activity-time-resume-executed").text($.number(val.executed));
    $(div).find(".activity-time-resume-edit").click(function () {
        setActivityEstimatedTime(activity_id,val.role_id,val.estimated);
    });
    val.percentage_executed=0;
    if(parseFloat(val.estimated)>0){
        val.percentage_executed=100*parseFloat(val.executed)/parseFloat(val.estimated);
    }
    $(div).find(".knobManual").val(val.percentage_executed);

    var max=100;
    var fgcolor="#F39100";
    var percentage_executed=parseInt(val.percentage_executed);
    if(percentage_executed>max){
        max=percentage_executed;
        fgcolor="#d9534f";
    }

    $("#activityTimes").append(div);
    $(div).find(".knobManual").knob( {
        thickness: ".2",
        readOnly: true,
        fgColor:fgcolor,
        max: max,
        parse: function (v) {return parseInt(v);},
        format: function (v) {return v + "%";}
    });
    $(div).fadeIn(1500);
}

$("#actionActivityEstimatedTime").click(function () {
    hideTime("#actionActivityEstimatedTime");

    $("#txtActivityEstimatedTimeActivityId").val($("#activity_id").val());

    var response = requestAjax({
        type: "POST",
        url: "/activity_estimated_times",
        form: "frmActivityEstimatedTime",
        success: "Registro creado",
        resetForm: true,
        modal: "modActivityEstimatedTime",
    });
    $.when(response).done(function () {
        $("#times-tabb").click();
    });
});