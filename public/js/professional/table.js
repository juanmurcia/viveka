var field = ['T. Doc','Doc','Nombre','Nacionalidad','Genero','Edad'];

$.each(field, function (key, value) {
    $('.to_do').append('<li><p><input type="checkbox" checked class="toggle-vis" data-column="'+key+'">&nbsp;'+value+' </p></li>');
})

cargaTextBuscador({
    field:"TBUStxtPersonId",
    idField:"txtPersonId",
    url:"/people",
    type:"POST",
    length:"1"
});

function nextStep (obj){
    var step = $(obj).data('step');

    switch (step){
        case 1:
            /*Usabilidad botones*/
            $('#pre').attr('disabled',true);
            $('#actionProfessional').attr('disabled',true);
            $('#next').attr('disabled',false);

            /*Usabilidad Step*/
            $('#step1').removeClass().addClass('selected');
            $('#step2').removeClass().addClass('disabled');
            $('#step3').removeClass().addClass('disabled');

            /*Asigna siguiente paso*/
            $('#next').data('step', 2);

            break;
        case 2:

            if($('#TBUStxtPersonId').val() == ''){
                toastr['error']('Debe Ingresar un documento','');
                return false;
            }
            /*Completa datos persona*/
            var person_id=$("#txtPersonId").val();
            if(person_id != '') {
                editPerson(person_id);
            } else {
                createPerson();
                $('#txtIdent').val($("#TBUStxtPersonId").val().split(" ")[0]);
            }

            /*Usabilidad botones*/
            $('#pre').attr('disabled',false);
            $('#actionProfessional').attr('disabled',true);
            $('#next').attr('disabled',false);

            /*Usabilidad Step*/
            $('#step1').removeClass().addClass('done');
            $('#step2').removeClass().addClass('selected');
            $('#step3').removeClass().addClass('disabled');

            /*Asigna siguiente paso*/
            $('#pre').data('step', 1);
            $('#next').data('step', 3);

            break;

        case 3:
            /*Ejecuta funcion*/
            if($("#txtPersonId").val() == ''){
                var response = requestAjax({
                    type: "POST",
                    url: "/person",
                    form: "frmPerson",
                    success: "Registro creado",
                    retorna: true
                });

                editPerson(response.id);

            } else {
                var id = $("#txtPersonId").val();
                requestAjax({
                    type: "PUT",
                    url: "/person/"+id,
                    form: "frmPerson",
                    success: "Registro Modificado",
                });
            }

            /*Trae el id de la persona*/
            $("#txtPerson").val($("#txtPersonId").val());

            /*Usabilidad botones*/
            $('#pre').attr('disabled',false);
            $('#actionProfessional').attr('disabled',false);
            $('#next').attr('disabled',true);

            /*Usabilidad Step*/
            $('#step1').removeClass().addClass('done');
            $('#step2').removeClass().addClass('done');
            $('#step3').removeClass().addClass('selected');

            /*Asigna siguiente paso*/
            $('#pre').data('step', 2);
            break;
    }

    /*Muestra step si ejecuta funcion*/
    $('.step').hide();
    $('#step-'+step).show();
}

//Funciones Botones
function createProfessional() {
    createPerson();
    $('#frmProfessional')[0].reset();
    $('#TBUStxtPersonId').val("");
    $('#titleModal').text('Crear Profesional');

    $('#pre').data('step', 1);
    nextStep($('#pre'));

    $('#modProfessional').modal({show: 'true'});
}

$('#actionProfessional').click(function() {
    var role=$('#actionProfessional').data("role");
    if(role == 'add'){
        var response = requestAjax({
            type: 'POST',
            url: '/professional',
            modal: 'modProfessional',
            form: 'frmProfessional',
            success: 'Registro creado',
        });
        $.when(response).done(function () {
            tableProfessional();
        });
    }else if(role== 'change'){
        var professionalId = $('#txtProfessionalId').val();
        var response = requestAjax({
            type: 'PUT',
            url: '/professional/'+professionalId,
            modal: 'modProfessional',
            form: 'frmProfessional',
            success: 'Registro Modificado',
        });
        $.when(response).done(function () {
            tableProfessional();
        });
    }
});

function editProfessional(id) {

    var response = requestAjax({
        type: 'GET',
        url: '/professional/'+id,
        form: 'frmProfessional',
        retorna: true,
    });

    editPerson(response.person_id);

    //Carga Formulario
    $("#actionProfessional").data("role","change");
    $('#titleModal').text('Modificar Profesional');
    $('#txtProfessionalId').val(response.id);
    $('#TBUStxtPersonId').val(response.person.document+' - '+response.person.name+' '+response.person.last_name);
    $('#txtBirthDate').val(response.birth_date.substring(0, 10));
    $('#optNationality').val(response.nationality_id);
    $('#txtGender'+response.gender).parent().click();

    $('#pre').data('step', 2);
    nextStep($('#pre'));
    $('#modProfessional').modal({show: 'true'});
}

function destroyProfessional(id) {
    if(confirm('Seguro Desea anular el profesional ?')){
        var response = requestAjax({
            type: 'DELETE',
            url: '/professional/'+id,
            form: 'frmProfessional',
            success: 'Registro Inactivado',
            retorna: true,
        });
        $.when(response).done(function () {
            tableProfessional();
        });
    }        
}

function activateProfessional(id) {
    if(confirm('Seguro Desea Activar el profesional ?')){
        var response = requestAjax({
            type: 'POST',
            url: '/professional/'+id,
            form: 'frmProfessional',
            success: 'Registro Activado',
            retorna: true,
        });
        $.when(response).done(function () {
            tableProfessional();
        });
    }
}

function tableProfessional(){
    tableSearch({
        id:'tbProfessional',
        type:'GET',
        url:'/professionals',
        destroy:true,
        checkInactive:true,
        serverSide: true,
        columns: [
            {data: "person.person_document_type.slug", name: "person.person_document_type.slug"},
            {data: 'person.document', name: 'person.document'},
            {data: 'person.name', name: 'person.name'},
            {data: 'nationality.name', name: 'nationality.name'},
            {data: 'gender', name: 'gender', className: "text-center"},
            {data: 'age', name: 'age', className: "text-center"},
            {data: 'buttons', name: 'buttons', orderable: false, searchable: true}
        ]
    });
}

tableProfessional();
