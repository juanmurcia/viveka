$('#actionTrigger').click(function() {
    var objButton = {
        type: 'POST',
        url: '/triggerBuilder',
        form: 'frmTrigger',
        success: 'Generando SQL',
        retorna: true,
    };
    $('#txtResult').val('');
    var request = requestAjax(objButton);
    $('#txtResult').val(request.sql);
});
