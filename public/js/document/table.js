$("#txtKeywords").tagsInput({
    "defaultText":"+ palabra"
});

function consultar(){
    var obj = {
        id:"tbDocuments",
        type:"GET",
        url:"/documents",
        destroy:true,
        sort:[[4,'desc']],
        columns: [
            {data: "pattern_document_type_name", name: "pattern_document_type_name"},
            {data: "origin", name: "origin"},
            {data: "name", name: "name"},
            {data: "user_mod_name", name: "user_mod_name"},
            {data: "updated_at", name: "updated_at"},
            {data: "keywords", name: "keywords"},
            {data: "comment", name: "comment"},
            {data: "buttons", name: "buttons", orderable: false, searchable: true}
        ]
    };
    tableSearch(obj);
}

consultar();




function editDocument(id) {

    var objButton = {
        type: "GET",
        url: "/document/"+id,
        form: "frmDocument",
        retorna: true,
    };

    var response = requestAjax(objButton);
    //Carga Formulario
    $("#actionDocument").data("role","change");
    $("#actionDocument").data("ref",id);
    $("#txtKeywords").removeTag("");$("#txtKeywords").removeTag("");
    $("#titleModal").text("Modificar Documento "+response.id);

    $("#modDocument").modal({show: "true"});
}

function destroyDocument(id) {
    if(confirm("Seguro desea inactivar el documento ?")){

        var objButton = {
            type: "DELETE",
            url: "/document/"+id,
            form: "frmDocument",
            success: "Registro Inactivado",
            retorna: true,
            funcion : consultar()
        };

        requestAjax(objButton);
    }
}

function activateDocumento(id) {
    if(confirm("Seguro desea activar el documento ?")){

        var objButton = {
            type: "POST",
            url: "/document/"+id,
            form: "frmDocument",
            success: "Registro Activado",
            retorna: true,
            funcion : consultar(),
        };

        requestAjax(objButton);
    }
}