function versionDocument(id) {

    var response = requestAjax({
        type: "GET",
        url: "/document/versions/"+id,
        form: "frmDocument",
        retorna: true,
    });

    $("#modDocumentVersions .modal-body").empty();

    $.each(response.data, function (index, val) {
        showDocumentVersion(val);
    });

    $("#modDocumentVersions").modal({show: "true"});

}

function showDocumentVersion(val) {
    var div = $("#document-version-resume").clone();
    $(div).removeAttr("id");
    $(div).find(".document-name").text(val.name);
    $(div).find(".document-user-mod-name").text(val.user_name);
    $(div).find(".document-date").text(val.date);
    $(div).find(".document-version").attr('href','/document/download/1/'+val.version+'/'+val.name);
    $("#modDocumentVersions .modal-body").append(div);
    $("#modDocumentVersions .modal-body").append('<hr>');

    $(div).fadeIn(1500);
}