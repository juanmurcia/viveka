function editDocument(id) {
    $("#frmDocument")[0].reset();
    $("#actionDocument").data("role","change");
    $("#titleModalDocument").text("Modificar Documento");

    var response = requestAjax({
        type: "GET",
        url: "/document/"+id,
        form: "frmDocument",
        retorna: true,
    });

    $.when(response).done(function (response) {
        $("#txtDocumentId").val(response.id);
        $("#txtDocumentModelId").val(response.model_id);
        $("#txtDocumentPatternDocumentTypeId").val(response.pattern_document_type_id);
        $("#txtDocumentName").val(response.name);
        $("#txtDocumentKey").val('document/'+response.id);
        $("#txtDocumentKeywords").val(response.keywords);
        $("#txtDocumentKeywords").removeTag("");
    });
}

function createDocument(model_id) {
    $("#frmDocument")[0].reset();
    $("#actionDocument").data("role","add");
    $("#titleModalDocument").text("Crear Documento");
    $("#txtDocumentModelId").val(model_id);

    $("#txtDocumentKeywords").removeTag("");
}

function destroyDocument(id) {
    if(confirm("Seguro desea inactivar el documento ?")){
        var response = requestAjax({
            type: "DELETE",
            url: "/document/"+id,
            form: "frmDocument",
            success: "Registro Inactivado",
            retorna: true,
        });
        $.when(response).done(function () {
            tableDocuments()
        });
    }
}

function activateDocumento(id) {
    if(confirm("Seguro desea activar el documento ?")){
        var response = requestAjax({
            type: "POST",
            url: "/document/"+id,
            form: "frmDocument",
            success: "Registro Activado",
            retorna: true,
        });
        $.when(response).done(function () {
            tableDocuments()
        });
    }
}

function tableDocuments(){
    var pattern_id = $("#tbDocuments").data("pattern-id");
    var model_id = $("#tbDocuments").data("model-id");
    tableSearch({
        id:"tbDocuments",
        type:"GET",
        url:"/documents/"+pattern_id+"/"+model_id,
        destroy:true,
        columns: [
            {data: "pattern_document_type_name", name: "pattern_document_type"},
            {data: "name", name: "name", class: "text-left"},
            {data: "user_mod_name", name: "user_mod_name", class: "text-left"},
            {data: "created_at", name: "created_at"},
            {data: "updated_at", name: "updated_at"},
            {data: "keywords", name: "keywords"},
            {data: "comment", name: "comment", class: "text-left"},
            {data: "buttons", name: "buttons", orderable: false, searchable: true}
        ]
    });


}
