$("#txtDocumentKeywords").tagsInput({
    "defaultText":"+ palabra"
});

$("#actionDocument").click(function() {
    var role = $(this).data("role");
    hideTime("#actionDocument");
    console.log('click');
    console.log($('#frmDocument').serialize());
    if(role == "add"){
        var response = requestAjax({
            type: "POST",
            url: "/document",
            modal: "modDocument",
            form: "frmDocument",
            success: "Registro creado",
            resetForm: true,
        });
        $.when(response).done(function () {
            if(typeof tableDocuments === 'function' && jQuery.isFunction( tableDocuments )) {
                tableDocuments();
            }
        });

    }else if(role == "change"){
        var id = $("#txtDocumentId").val();

        var response = requestAjax({
            type: "PUT",
            url: "/document/"+id,
            modal: "modDocument",
            form: "frmDocument",
            success: "Registro Modificado",
            resetForm: true,
        });
        $.when(response).done(function () {
            if(typeof tableDocuments === 'function' && jQuery.isFunction( tableDocuments )) {
                tableDocuments();
            }
        });
    }
});

$("#fileDocument").change(function() {
    console.log('Inicio File');
    $("#txtDocumentName").val("");
    $.ajax({
        url: "/document/preSignedURL",
        success: function(result){
            console.log('result');
            console.log(result);
            var theFormFile = $("#fileDocument").get()[0].files[0];

            $.ajax({
                type: "PUT",
                url: result["url"],
                data: theFormFile,
                processData: false,
                cache: false,
                timeout: 6000000,
                beforeSend: function (request, xhr) {
                    $("#documentProgress .progress-bar").width("0%");
                    $("#documentProgress .text-right").html("0%");
                    $("#documentProgress").removeClass("hidden");
                },
                xhr: function(){
                    var xhr = $.ajaxSettings.xhr() ;
                    xhr.upload.onprogress = function(data){
                        var perc = Math.round((data.loaded / data.total) * 100);
                        var prec_str= perc + "%";
                        $("#documentProgress .progress-bar").css("width",prec_str);
                        $("#documentProgress .text-right").html(prec_str);
                    };
                    return xhr ;
                },
                success: function (data) {
                    console.log('data');
                    console.log(theFormFile.name);
                    console.log('key');
                    console.log(result["key"]);
                    $("#txtDocumentName").val(theFormFile.name);
                    $("#txtDocumentKey").val(result["key"]);
                    $("#documentProgress").addClass("hidden");
                    console.log('end Document');
                },
                error: function (e) {
                    toastr["error"]("Ocurrió un error al subir el archivo", "");
                }
            });

            return false;
        },
        error: function (e) {
            toastr["error"]("Ocurrió un error al autorizar carga de archivo", "");
        }
    });
    console.log('fin File');
});