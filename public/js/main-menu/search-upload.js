var agreement_list = JSON.parse($("#agreements").val());
var project_list = JSON.parse($("#projects").val());
var product_list = JSON.parse($("#products").val());
var activity_list = JSON.parse($("#activities").val());

$('#util').empty();
function make_agreement(filter){
    $('#optAgreementCalendar option').remove();
    $('#optAgreementCalendar').append($("<option></option>").text('... Seleccione uno ...'));

    var data = agreement_list;
    if(filter > 0){
        data = agreement_list.filter(function (i,n){ return n.id ===  filter; });
    }
    var checked = data.length > 1 ? false : true;
    $.each(data, function (index, val) {
        $('#optAgreementCalendar')
            .append($("<option></option>")
                .attr("value",val.id)
                .attr("selected",checked)
                .text(val.number+' '+val.name));
    });
}

function make_project(filter){
    $('#optProjectCalendar option').remove();
    $('#optProjectCalendar').append($("<option></option>").text('... Seleccione uno ...'));

    var data = project_list;
    if(filter > 0){
        data = $.grep(project_list, function (project) {
            return project.agreement_id == filter;
        });
    }
    var checked = data.length > 1 ? false : true;
    $.each(data, function (index, val) {
        $('#optProjectCalendar')
            .append($("<option></option>")
                .attr("value",val.id)
                .attr("selected",checked)
                .text(val.name));
    });
}

function make_product(filter){
    $('#optProductCalendar option').remove();
    $('#optProductCalendar').append($("<option></option>").text('... Seleccione uno ...'));

    var data = product_list;
    if(filter > 0){
        data = $.grep(product_list, function (product) {
            return product.project_id == filter;
        });
    }
    var checked = data.length > 1 ? false : true;
    $.each(data, function (index, val) {
        $('#optProductCalendar')
            .append($("<option></option>")
                .attr("value",val.id)
                .attr("selected",checked)
                .text(val.name));
    });
}

function make_activity(pattern, id){
    $('#optActivityCalendar option').remove();
    $('#optActivityCalendar').append($("<option></option>").text('... Seleccione uno ...'));

    var data = activity_list;
    if(pattern > 0){
        data = $.grep(activity_list, function (activity) {
            return activity.pattern_id == pattern;
        });

        data = $.grep(data, function (activity) {
            return activity.model_id == id;
        });
    }
    var checked = data.length > 1 ? false : true;
    $.each(data, function (index, val) {
        $('#optActivityCalendar')
            .append($("<option></option>")
                .attr("value",val.id)
                .attr("selected",checked)
                .text(val.name));
    });
}

make_agreement();
make_project();
make_product();
make_activity();

$("#actionSearchUpload").click(function(){
   if($("#optActivityCalendar").val() != '... Seleccione uno ...'){
       var id = $("#optActivityCalendar").val();
       var option = $("[name='type_search']:checked").val();

       if(option == 'C'){
           $("#modSearchUpload").modal('hide');
           createActivityExecutedCost(id);
       }else if(option == 'D'){
           $("#txtDocumentId").val(id);
           $("#txtDocumentModelId").val(4);
           $("#frmDocument")[0].reset();
           $("#actionDocument").data("role","add");
           $("#modSearchUpload").modal('hide');
           $("#modDocument").modal({show: 'true'});
       }
   }
});