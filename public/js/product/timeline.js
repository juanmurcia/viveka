var response = requestAjax({
    type: "GET",
    url: '/audit_product/1/'+$("#product_id").val(),
    retorna: true
});

$.when(response).done(function () {
    $.each(response.data, function (index, val) {
        var date = moment(val.created_at);

        $(".input-flex-container").append('<div class="input"><span data-year="'+date.format("MMM Do YY")+'" data-info="'+val.field+'"></span></div>');
        $(".description-flex-container").append("<p>"+val.justification+"</p>");
    });
});

$(function(){
    var inputs = $(".input");
    var paras = $(".description-flex-container").find("p");

    $(inputs).click(function(){
        var t = $(this),
            ind = t.index(),
            matchedPara = $(paras).eq(ind);

        $(t).add(matchedPara).addClass("active");
        $(inputs).not(t).add($(paras).not(matchedPara)).removeClass('active');
    });
});