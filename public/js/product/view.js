$("#menu_product").addClass('current-page');

$("#general-tabb").click(function(){
    editProduct($("#product_id").val());
});

$("#dashboard-tabb").click(function(){
    initializeGraphDateRange($('#txtProductStartDate').val(),$('#txtProductEndDate').val());
});

$("#activities-tabb").click(function(){
    patternTimeCostResume($("#pattern_id").val(),$("#model_id").val());
    tableActivities();
});

$("#documents-tabb").click(function(){
    tableDocuments();
});

editProduct($("#product_id").val());
$( document ).ready(function() {
    $("#dashboard-tabb").click();
});