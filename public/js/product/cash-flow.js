var colorState = {
    31 : "#FDC387", //Programado
    32 : "#A42725", //En Ejecución
    33 : "#166F99", //Presentado
    34 : "#10AA6A", //Aprobado
    35 : "#A2592E", //Facturado
    36 : "#D3D0CA" //Pagado
};

var date = moment();
var total = [];

var agreement_list = JSON.parse($("#agreements").val());
var project_list = JSON.parse($("#projects").val());
var product_list = JSON.parse($("#products").val());
var pattern_state_list = JSON.parse($("#pattern_state").val());

$('#optStateFlow').append($("<option></option>").attr("value","0").text("... Seleccione uno ..."));
$.each(pattern_state_list, function (index, val) {
    $("#optStateFlow")
        .append($("<option></option>")
            .attr("value",val.id)
            .text(val.name));
});

//$("#dataUtil").empty();
function make_agreement(filter){
    $('#optAgreementFlow option').remove();
    $('#optAgreementFlow').append($("<option></option>").text('... Seleccione uno ...'));

    var data = agreement_list;
    if(filter > 0){
        data = agreement_list.filter(function (i,n){ return n.id ===  filter; });
    }
    var checked = data.length > 1 ? false : true;
    $.each(data, function (index, val) {
        $('#optAgreementFlow')
            .append($("<option></option>")
                .attr("value",val.id)
                .attr("selected",checked)
                .text(val.number+' '+val.name));
    });
}

function make_project(filter){
    $('#optProjectFlow option').remove();
    $('#optProjectFlow').append($("<option></option>").text('... Seleccione uno ...'));

    var data = project_list;
    if(filter > 0){
        data = $.grep(project_list, function (project) {
            return project.agreement_id == filter;
        });
    }
    var checked = data.length > 1 ? false : true;
    $.each(data, function (index, val) {
        $('#optProjectFlow')
            .append($("<option></option>")
                .attr("value",val.id)
                .attr("selected",checked)
                .text(val.name));
    });
}

function make_product(filter){
    $('#optProductFlow option').remove();
    $('#optProductFlow').append($("<option></option>").text('... Seleccione uno ...'));

    var data = product_list;
    if(filter > 0){
        data = $.grep(product_list, function (product) {
            return product.project_id == filter;
        });
    }
    var checked = data.length > 1 ? false : true;
    $.each(data, function (index, val) {
        $('#optProductFlow')
            .append($("<option></option>")
                .attr("value",val.id)
                .attr("selected",checked)
                .text(val.name));
    });
}

function form_date() {
    $(".monthTitle").text("Mes: " + date.format("MMMM"));
    $(".scol-4").text("Semana: 23 - " + date.endOf('month').format('DD'));
    $("#fecFlow").val(date.format("YYYY-MM"));
}

$(".move-day").click(function(){
    var action = $(this).data("action");
    date = (action == "prev") ? date.subtract(1,"months") : date.add(1,"months");

    form_date();
    search();
});

function search(){
    var objButton = {
        type: 'POST',
        url: '/cash_flow',
        form: 'frmFlowCash',
        retorna: true,
    };

    var response = requestAjax(objButton);
    $.when( response ).done(function ( request ) {
        $("#details").empty();

        var end_date = "";
        var column = "";
        reset_total();
        $.each(request.data, function (index, val) {
            var div = $("#util").clone();
            //$(div).removeTag('id');
            end_date = val.end_date.split("-");

            $(div).find(".nameProduct").css("border-left","10px solid "+colorState[val.pattern_state_id]);
            $(div).find(".nameProduct").text(val.name);

            if(parseInt(end_date[2]) >= 1 && parseInt(end_date[2]) <= 7){
                column = "col-1";
            }else if(parseInt(end_date[2]) >= 8 && parseInt(end_date[2]) <= 14){
                column = "col-2";
            }else if(parseInt(end_date[2]) >= 15 && parseInt(end_date[2]) <= 22){
                column = "col-3";
            }else if(parseInt(end_date[2]) >= 23){
                column = "col-4";
            }

            total["ttl"+column+"t"] += val.value;
            total["ttl"+column+"e"] += val.value;

            $(div).find("."+column+"t").text(numberFormat(val.value,0,',','.'));
            $(div).find("."+column+"e").text(numberFormat(val.value,0,',','.'));

            $("#details").append(div);
            console.log('appended');
        });

        for(i = 1; i <= 4; i++){
            $(".ttlcol-"+i+"t").text(numberFormat(total["ttlcol-"+i+"t"],0,',','.'));
            $(".ttlcol-"+i+"e").text(numberFormat(total["ttlcol-"+i+"e"],0,',','.'));
        }
    });
}

function reset_total(){
    for(i = 1; i <= 4; i++){
        total["ttlcol-"+i+"t"] = 0;
        total["ttlcol-"+i+"e"] = 0;
    }
}

$(".select2").on("change",function () {
    search();
});

$("#liFilter").trigger('click');
make_agreement();
make_project();
make_product();
form_date();
search();