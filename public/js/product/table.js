var field = ['Proyecto','Estado','Nombre','Descripción','% Avance', 'Fec Inicio', 'Fec Fin'];

$.each(field, function (key, value) {
    $('.to_do').append('<li><p><input type="checkbox" checked class="toggle-vis" data-column="'+key+'">&nbsp;'+value+' </p></li>');
})

$('#actionProduct').click(function(){
    tableSearch({
        id:'tbProducts',
        type:'POST',
        url:'/products',
        form:'frmProduct',
        destroy:true,
        checkInactive:true,
        serverSide: true,
        exportButtons : true,
        columns: [
            {data: 'project.name', name: 'project.name', className: "text-right"},
            {data: 'pattern_state.name', name: 'pattern_state.name'},
            {data: 'name', name: 'name'},
            {data: 'description', name: 'description'},
            {data: 'percentage', name: 'percentage'},
            {data: 'start_date', name: 'start_date'},
            {data: 'end_date', name: 'end_date'},
            {data: 'buttons', name: 'buttons', orderable: false, searchable: true}
        ]


    });
});

loadSelect2({
    id : "txtProductResponsible",
    url: "/agreement_team/"+$("#agreement_id").val(),
    type:"GET"
});

$('#actionProduct').click();

function addProductCallBack() {
    $('#actionProduct').click();
};