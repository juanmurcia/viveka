function productProject(projectId) {
    var objButton = {
        type: 'GET',
        url: '/products/' + projectId,
        form: 'frmProduct',
        retorna: true,
    };

    var response = requestAjax(objButton);
    $("#listProduct").empty();

    $.each(response.data, function (index, val) {
        showProduct(val);
    });

    $("#listProduct .knob").knob( {
        thickness: ".2",
        readOnly: true,
        fgColor:"#F39100",
        parse: function (v) {return parseInt(v);},
        format: function (v) {return v + "%";}
    });
}

var colorState = {
    31 : "#FDC387", //Programado
    32 : "#A42725", //En Ejecución
    33 : "#166F99", //Presentado
    34 : "#10AA6A", //Aprobado
    35 : "#A2592E", //Facturado
    36 : "#D3D0CA" //Pagado
};

function showProduct(val) {
    var div = $("#product-resume").clone();
    $(div).removeAttr("id");
    $(div).find("h3").text(val.name);
    //$(div).find(".product-resume-description").text(val.description);
    $(div).find(".product-resume-responsible").text(val.responsible.person.name+' '+val.responsible.person.last_name);
    $(div).find(".product-resume-start").text(val.start_date);
    $(div).find(".product-resume-end").text(val.end_date);
    $(div).find(".product-resume-percentage").val(val.percentage);
    $(div).find("a").attr("href", "/producto/"+val.id);
    $(div).find(".tui-full-calendar-popup-top-line").css("background-color", colorState[val.pattern_state_id]);

    $("#listProduct").append(div);
    $(div).find(".knobManual").knob( {
        thickness: ".2",
        readOnly: true,
        fgColor:"#F39100",
        parse: function (v) {return parseInt(v);},
        format: function (v) {return v + "%";}
    });


    $(div).fadeIn(1500);
}