loadSelect2({
    id : "txtProductResponsible",
    url: "/agreement_team/"+$("#agreement_id").val(),
    type:"GET"
});

$("#newProduct").click(function() {
    hideTime("#actionProduct");
    //Guarda campos extra antes del envío
    $("#txtProductProject").remove();
    $("<input>").attr({
        type: "hidden",
        id: "txtProductProject",
        name: "txtProductProject",
        value: $("#project_id").val()
    }).appendTo("#frmProduct");

    var response = requestAjax({
        type: "POST",
        url: "/product",
        form: "frmProduct",
        success: "Registro creado",
        resetForm: true,
        modal: "modProduct",
    });
    $.when(response).done(function () {
        productProject($("#project_id").val());
    });
});