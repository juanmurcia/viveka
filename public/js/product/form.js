function editProduct(id){
    var product = requestAjax({
        type: "GET",
        url: "/product/"+id,
        //form: "frmPerson",
        retorna: true,
    });
    //view
    $('#product_name').text(product.name);
    $('#agreement_name').text(product.project.agreement.name);
    $('#agreement_name').attr("href","/contrato/"+product.project.agreement.id);
    $('#project_name').text(product.project.name);
    $('#project_name').attr("href","/proyecto/"+product.project.id);
    //basics
    $('#txtProductName').val(product.name);
    var newOption = new Option(product.responsible.person.document+' - '+product.responsible.person.name+' '+product.responsible.person.last_name, product.responsible_id, false, false);
    $('#txtProductResponsible').append(newOption);
    
    $('#txtProductDescription').val(product.description);
    $('#txtProductStartDate').val(product.start_date);
    $('#txtProductEndDate').val(product.end_date);
    $('#txtProductValue').val(product.value);
    //progress
    $("#optProductPatternState").val(product.pattern_state_id);
    $("#txtProductPercentage").val(product.percentage);
    $("#txtProductPercentage").attr('min',product.percentage);
    $("#txtProductJustification").val(product.justification);
    $("#txtProductNameContract").val(product.contract_name);
    $("#txtProductDescriptionContract").val(product.contract_description);
    $("#contractInfo").show();

}

$("#actionProductBasics").click(function() {
    hideTime("#actionProductBasics");
    requestAjax({
        type: "PUT",
        url: "/product/"+$("#product_id").val(),
        form: "frmProductBasics",
        success: "Registro modificado",
    });
});

$("#actionProductDetails").click(function() {
    hideTime("#actionProductDetails");
    requestAjax({
        type: "PUT",
        url: "/product/"+$("#product_id").val()+"/details",
        form: "frmProductDetails",
        success: "Registro modificado",
    });
});

$("#actionProductProgress").click(function() {
    hideTime("#actionProductProgress");
    requestAjax({
        type: "PUT",
        url: "/product/"+$("#product_id").val()+"/progress",
        form: "frmProductProgress",
        success: "Registro modificado",
    });
});

tableSearch({
    id:'tbAudState',
    type:'GET',
    url:'/audit_product/1/'+$("#product_id").val(),
    destroy:true,
    columns: [
        {data: 'field', name: 'field'},
        {data: 'user', name: 'user'},
        {data: 'justification', name: 'justification'},
        {data: 'created_at', name: 'created_at'}
    ]


});

tableSearch({
    id:'tbAudPercentage',
    type:'GET',
    url:'/audit_product/2/'+$("#product_id").val(),
    destroy:true,
    columns: [
        {data: 'field', name: 'field'},
        {data: 'user', name: 'user'},
        {data: 'justification', name: 'justification'},
        {data: 'created_at', name: 'created_at'}
    ]


});