function tableParameter(){
    tableSearch({
        id:"tbParameter",
        type:"GET",
        url:"/system_parameter",
        destroy:true,
        columns: [
            {data: "id", name: "id"},
            {data: "name", name: "name"},
            {data: "value", name: "value"},
            {data: "user_cre.name", name: "user_cre"},
            {data: "created_at", name: "created_at"},
            {data: "buttons", name: "buttons", orderable: false, searchable: true}
        ]
    });
}

tableParameter();

//Funciones Botones
$('#btnSystemParameter').click(function() {
    $('#frmSystemParameter')[0].reset();
    $('#actionSystemParameter').data('role','add');
    $('#titleModal').text('Crear Parametro');
});

$("#actionSystemParameter").click(function() {
    var role = $(this).data("role");
    hideTime("#actionSystemParameter");
    
    if(role == "add"){
        var response = requestAjax({
            type: "POST",
            url: "/system_parameter",
            modal: "modSystemParameter",
            form: "frmSystemParameter",
            success: "Registro creado",
            resetForm: true,
        });
        $.when(response).done(function () {
            tableParameter();
        });
    }else if(role== "change"){
        var id = $(this).data("ref");
        var response = requestAjax({
            type: "PUT",
            url: "/system_parameter/"+id,
            modal: "modSystemParameter",
            form: "frmSystemParameter",
            success: "Registro Modificado",
            resetForm: true,
        });
        $.when(response).done(function () {
            tableParameter();
        });
    }
});

function editSystemParameter(id) {

    var response = requestAjax({
        type: 'GET',
        url: '/system_parameter/'+id,
        form: 'frmSystemParameter',
        retorna: true,
    });
    //Carga Formulario
    $('#actionSystemParameter').data('role','change');
    $('#actionSystemParameter').data('ref',id);
    $('#titleModal').text('Modificar Parámetro');

    $('#txtNameParameter').val(response.name);
    $('#txtValueParameter').val(response.value);

    $('#modSystemParameter').modal({show: 'true'});
}