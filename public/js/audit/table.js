var tbAudit = $('#tbAudit').DataTable({
    responsive: true,
    searching: false,
    lengthMenu: [[25,50,100, -1], [25,50, 100, "Todo"]],
    columns: [
        {data: 'created_at', name: 'created_at'},
        {data: 'field', name: 'field'},
        {data: 'old_value', name: 'old_value', className: "text-right"},
        {data: 'new_value', name: 'new_value', className: "text-right"},
        {data: 'user', name: 'user', className: "text-right"}
    ]
});

function audit(pattern_id, model_id){
    tbAudit.clear().draw();
    var response = requestAjax({
        type: 'GET',
        url: '/audit/'+pattern_id+'/'+model_id,
        retorna: true,
    });

    var old_value = 0;
    var new_value = 0;
    $.each(response.data, function(index, val) {
        old_value = (isNaN(val.old_value) ? val.old_value : numberFormat(val.old_value,2,',','.'));
        new_value = (isNaN(val.new_value) ? val.new_value : numberFormat(val.new_value,2,',','.'));

        addColumnDataTable(tbAudit,{
            "created_at":  val.created_at,
            "field":  val.field,
            "old_value":  old_value,
            "new_value":  new_value,
            "user":  val.user
        });
    });

    $('#modAudit').modal({show: 'true'});
}