var tbActivityCostSubCategory = $('#tbActivityCostSubCategory').DataTable({
    responsive: true,
    searching: false,
    lengthMenu: [[25,50,100, -1], [25,50, 100, "Todo"]],
    columns: [
        {data: "name", name: "name"},
        {data: "user", name: "user"},
        {data: "created_at", name: "created_at"},
        {data: "buttons", name: "buttons", orderable: false, searchable: true}
    ]
});

function tableActivityCostCategory(){
    tableSearch({
        id:"tbActivityCostCategory",
        type:"GET",
        url:"/activity_cost_categories",
        destroy:true,
        checkInactive:true,
        columns: [
            {data: "name", name: "name"},
            {data: "user_cre.name", name: "user"},
            {data: "created_at", name: "created_at"},
            {data: "buttons", name: "buttons", orderable: false, searchable: true}
        ]
    });
}

tableActivityCostCategory();

$("#btnActivityCostCategory").click(function() {
    $("#frmActivityCostCategory")[0].reset();
    $("#actionActivityCostCategory").data("role","add");
    $("#actionActivityCostCategory span").text("Crear");
    $("#modActivityCostCategory h3").text("Crear Costo Categoría");
});

function editActivityCostCategory(id){
    var objButton = {
        type: "GET",
        url: "/activity_cost_categories/"+id,
        form: "frmPerson",
        retorna: true,
    };

    var response = requestAjax(objButton);

    //Carga Formulario
    $("#frmActivityCostCategory")[0].reset();
    $("#txtCategoryId").val(response.id);
    $("#actionActivityCostCategory").data("role","change");
    $("#actionActivityCostCategory span").text("Editar");
    $("#modActivityCostCategory h3").text("Modificar Costo Categoría");
    $("#txtCategoryName").val(response.name);

    $("#modActivityCostCategory").modal({show: "true"});
}

function destroyActivityCostCategory(id) {
    if(confirm("Seguro desea inactivar la categoría ?")){
        var response = requestAjax({
            type: "DELETE",
            form: "frmActivityCostCategory",
            url: "/activity_cost_categories/"+id,
            success: "Registro Inactivado",
        });
        $.when(response).done(function () {
            tableActivityCostCategory();
        });
    }
}

function activateActivityCostCategory(id) {
    if(confirm("Seguro desea activar la categoría ?")){
        var response = requestAjax({
            type: "POST",
            form: "frmActivityCostCategory",
            url: "/activity_cost_categories/"+id,
            success: "Registro Activado",
            retorna: true,
        });
        $.when(response).done(function () {
            tableActivityCostCategory();
        });
    }
}

$("#actionActivityCostCategory").click(function() {
    var role = $(this).data("role");
    hideTime("#actionActivityCostCategory");

    if(role == "add"){
        var response = requestAjax({
            type: "POST",
            url: "/activity_cost_categories",
            modal: "modActivityCostCategory",
            form: "frmActivityCostCategory",
            success: "Registro creado",
            resetForm: true,
        });
        $.when(response).done(function () {
            tableActivityCostCategory();
        });
    }else if(role== "change"){
        var id = $("#txtCategoryId").val();
        var response = requestAjax({
            type: "PUT",
            url: "/activity_cost_categories/"+id,
            modal: "modActivityCostCategory",
            form: "frmActivityCostCategory",
            success: "Registro Modificado",
            resetForm: true,
        });
        $.when(response).done(function () {
            tableActivityCostCategory();
        });
    }
});

var category_id = 0;
function viewActivityCostSubCategory(id){
    var objButton = {
        type: "GET",
        url: "/activity_cost_categories/"+id,
        retorna: true,
    };

    var response = requestAjax(objButton);
    category_id = id;

    //Carga Formulario
    $("#frmActivityCostSubCategory")[0].reset();
    $("#frmActivityCostSubCategory").find("#txtCategoryId").val(response.id);
    $("#actionActivityCostSubCategory").data("role","add");
    $("#actionActivityCostSubCategory span").text("Agregar");

    tbActivityCostSubCategory.clear().draw();
    $.each(response.activity_cost_subcategory, function (index, val) {
        if(val.active == 1){
            addColumnDataTable(tbActivityCostSubCategory,{
                "name":  val.name,
                "user":  val.user_cre.name,
                "created_at":  val.created_at,
                "buttons" : '<button class="btn btn-default btn-xs" onclick="editActivityCostSubCategory('+val.id+');"><i class="fa fa-pencil"></i></button><button class="btn btn-danger btn-xs" onclick="delActivityCostSubCategory('+val.id+');"><i class="fa fa-close"></i></button>',
            });
        }
    });

    $("#modActivityCostSubCategory").modal({show: "true"});
}

function editActivityCostSubCategory(id){
    var objButton = {
        type: "GET",
        url: "/activity_cost_subcategories/"+id,
        retorna: true,
    };

    var response = requestAjax(objButton);

    //Carga Formulario
    $("#frmActivityCostSubCategory")[0].reset();
    $("#frmActivityCostSubCategory").find("#txtCategoryId").val(response.id);
    $("#actionActivityCostSubCategory").data("role","change");
    $("#actionActivityCostSubCategory span").text("Editar");
    $("#frmActivityCostSubCategory").find("#txtCategoryName").val(response.name);
}

function delActivityCostSubCategory(id) {
    if(confirm("Seguro desea inactivar la sub-categoría ?")){
        var response = requestAjax({
            type: "DELETE",
            form: "frmActivityCostSubCategory",
            url: "/activity_cost_subcategories/"+id,
            success: "Registro Inactivado",
        });
        $.when(response).done(function () {
            viewActivityCostSubCategory(category_id);
        });
    }
}

$("#actionActivityCostSubCategory").click(function() {
    var role = $(this).data("role");
    hideTime("#actionActivityCostSubCategory");


    if(role == "add"){
        var response = requestAjax({
            type: "POST",
            url: "/activity_cost_subcategories",
            form: "frmActivityCostSubCategory",
            success: "Registro creado",
            resetForm: true,
        });
        $.when(response).done(function () {
            viewActivityCostSubCategory(category_id);
        });
    }else if(role== "change"){
        var id = $("#frmActivityCostSubCategory").find("#txtCategoryId").val();
        var response = requestAjax({
            type: "PUT",
            url: "/activity_cost_subcategories/"+id,
            form: "frmActivityCostSubCategory",
            success: "Registro Modificado",
            resetForm: true,
        });
        $.when(response).done(function () {
            viewActivityCostSubCategory(category_id);
        });
    }
});