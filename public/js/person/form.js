function createPerson() {
    $("#frmPerson")[0].reset();
    $("#actionPerson").data("role","add");
    $("#actionPerson span").text("Crear");
    $("#modPerson h3").text("Crear Persona");

    $('#txtMobil').removeTag("");
}

$("#actionPerson").click(function() {
    var role = $(this).data("role");
    hideTime("#actionPerson");

    if(!correoValido()){
        return false;
    }

    if(role == "add"){
        var response = requestAjax({
            type: "POST",
            url: "/person",
            modal: "modPerson",
            form: "frmPerson",
            success: "Registro creado",
            resetForm: true,
        });
        $.when(response).done(function () {
            tablePerson();
        });
    }else if(role== "change"){
        var id = $("#txtPersonId").val();
        var response = requestAjax({
            type: "PUT",
            url: "/person/"+id,
            modal: "modPerson",
            form: "frmPerson",
            success: "Registro Modificado",
            resetForm: true,
        });
        $.when(response).done(function () {
            tablePerson();
        });
    }
});

$("#txtMobil").tagsInput({
    "defaultText":"+ teléfono"
});
$('#txtMobil').removeTag("");

function editPerson(id) {

    var objButton = {
        type: "GET",
        url: "/person/"+id,
        form: "frmPerson",
        retorna: true,
    };

    var response = requestAjax(objButton);

    //Carga Formulario
    $("#frmPerson")[0].reset();
    $("#txtPersonId").val(response.id);
    $("#actionPerson").data("role","change");
    $("#actionPerson span").text("Editar");
    $("#modPerson h3").text("Modificar Persona");
    $("#optTaxpayer").val(response.taxpayer_type_id);
    $("#optDocument").val(response.person_document_type_id);
    $("#txtIdent").val(response.document);
    $("#txtName").val(response.name);
    updateLastNameVisibility(response.person_document_type_id);
    $("#txtLastName").val(response.last_name);
    $("#txtAddress").val(response.address);
    $("#txtMobil").val(response.phone);
    $("#txtEmail").val(response.email);

    $('#txtMobil').removeTag("");
}

function updateLastNameVisibility(person_document_type_id){
    $("#txtLastName").val('');
    if(person_document_type_id == 2){
        $("#txtLastNameContainer").hide();
    }else{
        $("#txtLastNameContainer").show();
    }
}