var field = ['T. Doc','Documento','Nombre','Apellidos','Dirección','Email','Teléfonos'];

$.each(field, function (key, value) {
    $('.to_do').append('<li><p><input type="checkbox" checked class="toggle-vis" data-column="'+key+'">&nbsp;'+value+' </p></li>');
})

function tablePerson(){
    tableSearch({
        id:"tbPerson",
        type:"GET",
        url:"/people",
        form: 'frmPerson',
        serverSide: true,
        destroy : true,
        checkInactive : false,
        exportButtons : true,
        columns: [
            {data: "person_document_type.slug", name: "person_document_type.slug"},
            {data: "document", name: "document"},
            {data: "name", name: "name"},
            {data: "last_name", name: "last_name"},
            {data: "address", name: "address"},
            {data: "email", name: "email"},
            {data: "phone", name: "phone"},
            {data: "buttons", name: "buttons", orderable: false, searchable: true}
        ]
    });
}

tablePerson();

function destroyPerson(id) {
    if(confirm("Seguro desea inactivar la persona ?")){
        var response = requestAjax({
            type: "DELETE",
            url: "/person/"+id,
            form: "frmPerson",
            success: "Registro Inactivado",
            retorna: true,
        });
        $.when(response).done(function () {
            tablePerson();
        });
    }
}

function activatePerson(id) {
    if(confirm("Seguro desea activar la persona ?")){
        var response = requestAjax({
            type: "POST",
            url: "/person/"+id,
            form: "frmPerson",
            success: "Registro Activado",
            retorna: true,
        });
        $.when(response).done(function () {
            tablePerson();
        });
    }
}