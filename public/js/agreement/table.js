var field = ['Número','Nombre','Estado','Tipo Contratación','Tipo','categoría', 'Naturaleza', 'Servicio', 'Cobertura', 'Moneda', 'Departamento', 'Inicio', 'Fin'];

$.each(field, function (key, value) {
    $('.to_do').append('<li><p><input type="checkbox" checked class="toggle-vis" data-column="'+key+'">&nbsp;'+value+' </p></li>');
})

$('#actionAgreement').click(function(){
    tableSearch({
        id:'tbAgreement',
        type:'POST',
        url:'/agreements',
        form:'frmAgreement',
        destroy:true,
        exportButtons : true,
        checkInactive:true,
        serverSide: true,
        columns: [
            {data: 'number', name: 'number', className: "text-right"},
            {data: 'name', name: 'name', className: "text-right"},
            {data: 'pattern_state.name', name: 'pattern_state.name'},
            {data: 'agreement_process_type.name', name: 'agreement_process_type.name'},
            {data: 'agreement_type.name', name: 'agreement_type.name'},
            {data: 'agreement_category.name', name: 'agreement_category.name'},
            {data: 'agreement_nature.name', name: 'agreement_nature.name'},
            {data: 'agreement_service.name', name: 'agreement_service.name'},
            {data: 'agreement_coverage.name', name: 'agreement_coverage.name'},
            {data: 'currency.name', name: 'currency.name'},
            {data: 'department.name', name: 'department.name'},
            {data: 'start_date', name: 'start_date'},
            {data: 'end_date', name: 'end_date'},
            {data: 'buttons', name: 'buttons', orderable: false, searchable: true}
        ]
    });
});

$('#actionAgreement').click();

function addAgreementCallBack() {
    $('#actionAgreement').click();
};