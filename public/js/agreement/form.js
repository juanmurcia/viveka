loadSelect2({
    id : "txtAgreementTeamProfessionalId",
    url: "/select_professionals",
    type: "GET"
});

function tableTeam(id){
    tableSearch({
        id:'tbAgreementTeam',
        type:'GET',
        url:'/agreement_teams/'+id,
        destroy:true,
        checkInactive:true,
        columns: [
            {data: "document", name: "document", className: "text-right"},
            {data: "name", name: "name", className: "text-left"},
            {data: "buttons", name: "buttons", orderable: false, searchable: false}
        ]
    });
}

function editAgreement(id){

    var agreement = requestAjax({
        type: "GET",
        url: "/agreement/"+id,
        form: "frmPerson",
        retorna: true,
    });

    //view
    $('#agreement_name').text(agreement.name);
    //basic
    $("#txtAgreementNumber").val(agreement.number);
    $("#txtAgreementName").val(agreement.name);
    $("#txtAgreementObject").val(agreement.object);

    var newOption = new Option(agreement.employer.person.document+" - "+agreement.employer.person.name, agreement.employer_id, false, false);
    $('#txtAgreementEmployer').append(newOption);
    newOption = new Option(agreement.contractor.person.document+" - "+agreement.contractor.person.name, agreement.contractor_id, false, false);
    $('#txtAgreementContractor').append(newOption);
    newOption = new Option(agreement.responsible.person.document+" - "+agreement.responsible.person.name+' '+agreement.responsible.person.last_name, agreement.responsible_id, false, false);
    $('#txtAgreementResponsible').append(newOption);

    $("#txtAgreementValue").val(agreement.value);
    $("#optAgreementCurrency").val(agreement.currency_id);

    //details
    $("#optAgreementProcessType").val(agreement.agreement_process_type_id);
    $("#optAgreementType").val(agreement.agreement_type_id);
    $("#optAgreementCategory").val(agreement.agreement_category_id);
    $("#optAgreementNature").val(agreement.agreement_nature_id);
    $("#optAgreementService").val(agreement.agreement_service_id);
    $("#optAgreementCoverage").val(agreement.agreement_coverage_id);
    $("#optAgreementDepartment").val(agreement.department_id);
    $("#txtAgreementExecutionTerm").val(agreement.execution_term);
    $("#txtAgreementStartDate").val(agreement.start_date);
    $("#txtAgreementEndDate").val(agreement.end_date);
    $("#txtAgreementAdditionTerm").val(agreement.addition_term);
    $("#txtAgreementAdditionValue").val(agreement.addition_value);
    $("#txtAgreementExternalURL").val(agreement.external_url);

    //progress
    $("#optAgreementPatternState").val(agreement.pattern_state_id);
    $("#optAgreementPatternState").change();
    $("#txtAgreementSigningDate").val(agreement.signing_date);
    $("#txtAgreementPercentage").val(agreement.percentage);
    $("#txtAgreementPercentage").attr('min',agreement.percentage);
    $("#txtAgreementJustification").val(agreement.justification);

    $("#txtIvaBilling").val(agreement.iva_billing);
    $("#txtAgrementBillingDue").val(agreement.billing_due);

    $("#optAgreementPatternState").change();
}

$("#optAgreementPatternState").change(function () {
    if($("#optAgreementPatternState").val()>11){
        $("#agreementSigningDate").removeClass('hidden');
    }else{
        $("#agreementSigningDate").addClass('hidden');
    }
});

$("#actionAgreementBasics").click(function() {
    hideTime("#actionAgreementBasics");
    requestAjax({
        type: "PUT",
        url: "/agreement/"+$("#agreement_id").val(),
        form: "frmAgreementBasics",
        success: "Registro modificado",
    });
});

$("#actionAgreementDetails").click(function() {
    hideTime("#actionAgreementDetails");
    requestAjax({
        type: "PUT",
        url: "/agreement/"+$("#agreement_id").val()+"/details",
        form: "frmAgreementDetails",
        success: "Registro modificado",
    });
});

$("#actionAgreementProgress").click(function() {
    hideTime("#actionAgreementProgress");
    requestAjax({
        type: "PUT",
        url: "/agreement/"+$("#agreement_id").val()+"/progress",
        form: "frmAgreementProgress",
        success: "Registro modificado",
    });
});

function addAgreementTeam(){
    hideTime("#actionAgreementTeam",10);
    var response = requestAjax({
        type: "POST",
        url: "/agreement_team",
        form: "frmAddAgreementTeam",
        success: "Registro Creado",
        resetForm: true,
    });
    $.when(response).done(function () {
        tableTeam($("#agreement_id").val());
    });
};

function deleteAgreementTeam(id){
    var response = requestAjax({
        type: "DELETE",
        url: "/agreement_team/"+id,
        form: "frmAddAgreementTeam",
        success: "Registro Inactivado",
    });
    $.when(response).done(function () {
        tableTeam($("#agreement_id").val());
    });
};

function tableAgreementRoleCost(id){
    tableSearch({
        id:'tbAgreementRoleCost',
        type:'GET',
        url:'/agreement_role_costs/'+id,
        destroy:true,
        checkInactive:true,
        searching:false,
        paging:false,
        columns: [
            {data: "role", name: "role", className: "text-left"},
            {data: "value", name: "value", className: "text-right"},
            {data: "buttons", name: "buttons", orderable: false, searchable: false}
        ]
    });
}

function addAgreementRoleCost(){
    hideTime("#actionAgreementRoleCost",10);
    var response = requestAjax({
        type: "POST",
        url: "/agreement_role_cost",
        form: "frmAgreementRoleCost",
        success: "Registro Creado",
        resetForm: true,
    });
    $.when(response).done(function () {
        tableAgreementRoleCost($("#agreement_id").val());
    });
};