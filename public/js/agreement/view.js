$("#menu_agreement").addClass('current-page');

$("#general-tabb").click(function(){
    editAgreement($("#agreement_id").val());
    tableTeam($("#agreement_id").val());
    tableAgreementRoleCost($("#agreement_id").val());
});

$("#dashboard-tabb").click(function(){
	projectAgreement($("#agreement_id").val());
    initializeGraphDateRange($('#txtAgreementStartDate').val(),$('#txtAgreementEndDate').val());
});

$("#activities-tabb").click(function(){
    patternTimeCostResume($("#pattern_id").val(),$("#model_id").val());
    tableActivities();
});

$("#documents-tabb").click(function(){
    tableDocuments();
});

$("#billings-tabb").click(function(){
});

editAgreement($("#agreement_id").val())
$( document ).ready(function() {
    $("#dashboard-tabb").click();
});