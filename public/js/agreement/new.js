loadSelect2({
    id : "txtAgreementContractor",
    url: "/select_companies",
    type:"GET"
});

loadSelect2({
    id : "txtAgreementEmployer",
    url: "/select_companies",
    type:"GET"
});

loadSelect2({
    id : "txtAgreementResponsible",
    url: "/select_professionals?role_slugs=director",
    type:"GET"
});

$("#addAgreement").click(function() {
    hideTime("#btnAddAgreement");
    $("#frmAddAgreement")[0].reset();
    $("#titleModalAddAgreement").text("Crear Contrato ");  
});


$("#newAgreement").click(function() {
    hideTime("#newAgreement");
   
    var request = requestAjax({
        type: "POST",
        url: "/agreement",
        form: "frmAddAgreement",
        success: "Registro creado",
        resetForm: true,
        modal: "modAddAgreement",
    });


    $.when(request).done(function () {
        if ( $.isFunction(window.addAgreementCallBack)){
            addAgreementCallBack();
        }
    });
});