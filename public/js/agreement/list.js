function agreementSystem() {
    var objButton = {
        type: 'GET',
        url: '/agreements',
        form: 'frmAgreement',
        retorna: true,
    };

    var response = requestAjax(objButton);
    $("#listAgreement").empty();

    $.each(response.data, function (index, val) {
        showAgreement(val);
    });

    $("#listAgreement .knob").knob( {
        thickness: ".2",
        readOnly: true,
        fgColor:"#F39100",
        parse: function (v) {return parseInt(v);},
        format: function (v) {return v + "%";}
    });
}

function showAgreement(val) {
    var div = $("#agreement-resume").clone();
    $(div).removeAttr("id");
    $(div).find("h3").text(val.name);
    $(div).find(".agreement-resume-value").text(val.string_value);
    $(div).find(".agreement-resume-responsible").text(val.responsible.person.name+' '+val.responsible.person.last_name);
    $(div).find(".agreement-resume-start").text(val.start_date);
    $(div).find(".agreement-resume-end").text(val.end_date);
    $(div).find(".agreement-resume-employer").text(val.employer.person.name);
    $(div).find(".agreement-resume-contractor").text(val.contractor.person.name);
    $(div).find(".agreement-resume-percentage").val(val.percentage);

    if(val.single_project){
        if(val.projects[0]){
            $(div).find("a").attr("href", "/proyecto/"+val.projects[0].id);
        }
    }else{
        $(div).find("a").attr("href", "/contrato/"+val.id);
    }


    $("#listAgreement").append(div);
    $(div).find(".knobManual").knob( {
        thickness: ".2",
        readOnly: true,
        fgColor:"#F39100",
        parse: function (v) {return parseInt(v);},
        format: function (v) {return v + "%";}
    });


    $(div).fadeIn(1500);
}

agreementSystem();

function addAgreementCallBack() {
    agreementSystem();
};