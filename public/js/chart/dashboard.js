$( document ).ready(function() {
    //Configura calendario rango
    
    var cb = function(start, end, label) {
      console.log(start.toISOString(), end.toISOString(), label);
      $('#calendarRange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    };

    var optionSet1 = {
      startDate: moment().startOf('month'),
      endDate: moment(),
      minDate: '01/01/2018',
      maxDate: '12/31/2030',
      dateLimit: {
        days: 30
      },
      showDropdowns: true,
      showWeekNumbers: true,
      timePicker: false,
      timePickerIncrement: 1,
      timePicker12Hour: true,
      ranges: {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Últimos 7 Días': [moment().subtract(6, 'days'), moment()],
        'Últimos 30 Días': [moment().subtract(29, 'days'), moment()],
        'Este Mes': [moment().startOf('month'), moment().endOf('month')],
        'Último Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      opens: 'left',
      buttonClasses: ['btn btn-default'],
      applyClass: 'btn-small btn-primary',
      cancelClass: 'btn-small',
      format: 'YYYY/MM/DD',
      separator: ' to ',
      locale: {
        applyLabel: 'Consultar',
        cancelLabel: 'Limpiar',
        fromLabel: 'Desde',
        toLabel: 'Hasta',
        customRangeLabel: 'Seleccionar',
        daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        firstDay: 1
      }
    };
    
    $('#calendarRange span').html(moment().startOf('month').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
    $('#calendarRange').daterangepicker(optionSet1, cb);
    $('#calendarRange').on('apply.daterangepicker', function(ev, picker) {      
        runDashboard(picker.startDate.format('YYYY-MM-DD'),picker.endDate.format('YYYY-MM-DD'));
    });

    function runDashboard(fecD, fecH){
        console.log('Fecha Dasboard '+fecD+' '+fecH);

        $("#dvId").empty();
        $("#dvId").append("<input type='hidden' id='txtFecD' name='txtFecD' value='"+fecD+"'><input type='hidden' id='txtFecH' name='txtFecH' value='"+fecH+"'>");

        var objButton = {
            type: 'POST',
            url: '/infoDashboard',
            form: 'frmDasboard',
            success: 'Dashboard',
            retorna: true,            
        };

        var request = requestAjax(objButton);
        console.log(request);

        $.each(request.info_box.arr_ttl, function(index, val) {
            $('#'+index).html(' $'+numberFormat(val,0,'.',',')+' K');
        });

        var obj_pie = {
            id:'barber_pie2',
            legend: request.info_barbero.legen_barber,
            data: request.info_barbero.total_barber,
        }

        pieAreaRun(obj_pie);

        var obj_goal = {
            id:'goal_pie',
            legend: request.info_barbero.legen_barber,
            text: 'Avance',
            subText: 'Meta propuesta',
            link: '#',
            data: request.info_barbero.cant_barber,
        }

        goalArea(obj_goal); 

        var obj_horizontal = {
            id:'bar_horizontal',
            legend: request.info_service.legend_service,
            text: 'Movimiento',
            subText: 'Comparativo mes anterior',            
            data: request.info_service.cant_service,
            name: request.info_service.name_service,
        }

        horizontalBar(obj_horizontal);

        var obj_horizontal = {
            id:'vertical_bar',
            legend: request.info_box.legend_box,
            text: 'Movimiento',
            subText: 'Diario agencia',            
            data: request.info_box.ttl_box,
            name: ['Ingresos', 'Pronóstico','Salidas','Pago Pronóstico']
        }

        verticalBar(obj_horizontal);
    }

    runDashboard(moment().startOf('month').format('YYYY-MM-DD'), moment().format('YYYY-MM-DD'));
    
});