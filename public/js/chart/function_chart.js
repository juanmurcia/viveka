var dataStyle = {
	normal: {
		label: {
			show: false
		},
		labelLine: {
			show: false
		}
	}
};

var placeHolderStyle = {
	normal: {
	  color: 'rgba(0,0,0,0)',
	  label: {
		show: false
	  },
	  labelLine: {
		show: false
	  }
	},
	emphasis: {
	  color: 'rgba(0,0,0,0)'
	}
};

var theme = {
  color: ['#d87c7c','#919e8b', '#d7ab82',  '#6e7074','#61a0a8','#efa18d', '#787464', '#cc7e63', '#724e58', '#4b565b'],

  title: {
	  itemGap: 8,
	  textStyle: {
		  fontWeight: 'normal',
		  color: '#000000'
	  }
  },

  dataRange: {
	  color: ['#1f610a', '#97b58d']
  },

  toolbox: {
	  color: ['#408829', '#408829', '#408829', '#408829']
  },

  tooltip: {
	  backgroundColor: 'rgba(0,0,0,0.5)',
	  axisPointer: {
		  type: 'line',
		  lineStyle: {
			  color: '#408829',
			  type: 'dashed'
		  },
		  crossStyle: {
			  color: '#408829'
		  },
		  shadowStyle: {
			  color: 'rgba(200,200,200,0.3)'
		  }
	  }
  },

  dataZoom: {
	  dataBackgroundColor: '#eee',
	  fillerColor: 'rgba(64,136,41,0.2)',
	  handleColor: '#E67E22'
  },
  grid: {
	  borderWidth: 0
  },

  categoryAxis: {
	  axisLine: {
		  lineStyle: {
			  color: '#E67E22'
		  }
	  },
	  splitLine: {
		  lineStyle: {
			  color: ['#eee']
		  }
	  }
  },

  valueAxis: {
	  axisLine: {
		  lineStyle: {
			  color: '#E67E22'
		  }
	  },
	  splitArea: {
		  show: true,
		  areaStyle: {
			  color: ['rgba(250,250,250,0.1)', 'rgba(200,200,200,0.1)']
		  }
	  },
	  splitLine: {
		  lineStyle: {
			  color: ['#eee']
		  }
	  }
  },
  timeline: {
	  lineStyle: {
		  color: '#E67E22'
	  },
	  controlStyle: {
		  normal: {color: '#34495E'},
		  emphasis: {color: '#34495E'}
	  }
  },

  k: {
	  itemStyle: {
		  normal: {
			  color: '#E67E22',
			  color0: '#a9cba2',
			  lineStyle: {
				  width: 1,
				  color: '#E67E22',
				  color0: '#86b379'
			  }
		  }
	  }
  },
  map: {
	  itemStyle: {
		  normal: {
			  areaStyle: {
				  color: '#ddd'
			  },
			  label: {
				  textStyle: {
					  color: '#c12e34'
				  }
			  }
		  },
		  emphasis: {
			  areaStyle: {
				  color: '#99d2dd'
			  },
			  label: {
				  textStyle: {
					  color: '#c12e34'
				  }
			  }
		  }
	  }
  },
  force: {
	  itemStyle: {
		  normal: {
			  linkStyle: {
				  strokeColor: '#E67E22'
			  }
		  }
	  }
  },
  chord: {
	  padding: 4,
	  itemStyle: {
		  normal: {
			  lineStyle: {
				  width: 1,
				  color: 'rgba(128, 128, 128, 0.5)'
			  },
			  chordStyle: {
				  lineStyle: {
					  width: 1,
					  color: 'rgba(128, 128, 128, 0.5)'
				  }
			  }
		  },
		  emphasis: {
			  lineStyle: {
				  width: 1,
				  color: 'rgba(128, 128, 128, 0.5)'
			  },
			  chordStyle: {
				  lineStyle: {
					  width: 1,
					  color: 'rgba(128, 128, 128, 0.5)'
				  }
			  }
		  }
	  }
  },
  gauge: {
	  startAngle: 225,
	  endAngle: -45,
	  axisLine: {
		  show: true,
		  lineStyle: {
			  color: [[0.2, '#86b379'], [0.8, '#68a54a'], [1, '#408829']],
			  width: 8
		  }
	  },
	  axisTick: {
		  splitNumber: 10,
		  length: 12,
		  lineStyle: {
			  color: 'auto'
		  }
	  },
	  axisLabel: {
		  textStyle: {
			  color: 'auto'
		  }
	  },
	  splitLine: {
		  length: 18,
		  lineStyle: {
			  color: 'auto'
		  }
	  },
	  pointer: {
		  length: '90%',
		  color: 'auto'
	  },
	  title: {
		  textStyle: {
			  color: '#333'
		  }
	  },
	  detail: {
		  textStyle: {
			  color: 'auto'
		  }
	  }
  },
  textStyle: {
	  fontFamily: 'Arial, Verdana, sans-serif'
  }
};

function pieAreaRun(obj){
	var option = {
	    title : {
	        text: '',
	        subtext: '',
	        x:'center'
	    },
	    tooltip : {
	        trigger: 'item',
	        formatter: "{a} <br/>{b} : {c} ({d}%)"
	    },
	    legend: {
	        x : 'center',
	        y : 'bottom',
	        data: obj.legend
	    },
	    toolbox: {
	        show : true,
	        feature : {
	            mark : {show: true},
	            dataView : {show: false, readOnly: false},
	            magicType : {
	                show: true,
	                type: ['pie', 'funnel']
	            },
	            restore : {show: false},
	            saveAsImage : {show: true}
	        }
	    },
	    calculable : true,
	    series : [
	        {
	            name:'Productividad',
	            type:'pie',
	            radius: [25, 90],
			  	center: ['50%', 170],
	            roseType : 'area',
	            data: obj.data.sort(function (a, b) { return a.value - b.value; }),
	            roseType: 'radius',
	           	nimationType: 'scale',
	            animationEasing: 'elasticOut',
	            animationDelay: function (idx) {
	                return Math.random() * 200;
	            }
	        }
	    ]
	};

 	var myChart = echarts.init(document.getElementById(obj.id),theme); 
 	myChart.setOption(option);
}


function goalArea(obj){
	//Tratamiento Data
	var rad_i = 120;
	var rad_d = 15;
	var rad_act = 0;
	arrData = [];
	data = [];

	$.each(obj.data, function(index, val) {
    	rad_act = rad_i - rad_d;
    	
    	data = {
    		name : val[1]['name'],
    		value : val[1]['value'],
    		itemStyle: placeHolderStyle
    	};

    	arrData.push({
			'name': obj.legend[index],
			'type': 'pie',
			'clockWise': false,
			'radius': [rad_act, rad_i],
			'itemStyle': dataStyle,
			'data': [
				val[0], data
			]
		});

        rad_i = rad_act;
    });

	var option = {
		title: {
		  text: obj.text,
		  subtext: obj.subText,
		  sublink: obj.link,
		  x: 170,
		  y: 45,
		  itemGap: 20,
		  textStyle: {
			color: 'rgba(30,144,255,0.8)',
			fontFamily: '微软雅黑',
			fontSize: 35,
			fontWeight: 'bolder'
		  }
		},
		tooltip: {
		  show: true,
		  formatter: "{a} <br/>{b} : {c} ({d}%)"
		},
		legend: {		  
		  x: 'center',
		  y: 'bottom',
		  itemGap: 12,
		  data: obj.legend,
		},
		toolbox: {
		  show: true,
		  feature: {
			mark: {
			  show: true
			},
			dataView: {
			  show: false,
			  title: "Text View",
			  lang: [
				"Text View",
				"Close",
				"Refresh",
			  ],
			  readOnly: false
			},
			restore: {
			  show: false,
			  title: "Restore"
			},
			saveAsImage: {
			  show: true,
			  title: "Save Image"
			}
		  }
		},
		series: arrData
	  };

	var myChart = echarts.init(document.getElementById(obj.id), theme);
	myChart .setOption(option);			  
}
	
function horizontalBar(obj){
	var option = {
		title: {
		  text: obj.text,
		  subtext: obj.subText
		},
		tooltip: {
		  trigger: 'axis'
		},
		legend: {
		  x: 100,
		  data: obj.name
		},
		toolbox: {
		  show: true,
		  feature: {
			saveAsImage: {
			  show: true,
			  title: "Save Image"
			}
		  }
		},
		calculable: true,
		xAxis: [{
		  type: 'value',
		  boundaryGap: [0, 0.01]
		}],
		yAxis: [{
		  type: 'category',
		  data: obj.legend
		}],
		series: obj.data
  	};

 	var myChart = echarts.init(document.getElementById(obj.id),theme); 
 	myChart.setOption(option);
}

function verticalBar(obj){
	arrData = [];

	$.each(obj.data, function(index, val) {
    	arrData.push({
			name: val.name,
			type: 'bar',
			data: val.data,
			markPoint: {
				data: [{
				  type: 'max',
				  name: 'Máximo '+val.name
				}, {
				  type: 'min',
				  name: 'Mínimo '+val.name
				}]
			},
			markLine: {
				data: [{
				  type: 'average',
				  name: 'Prom '+val.name
				}]
			}
		});
    });

	var option = {
		title: {
		  text: obj.text,
		  subtext: obj.subText
		},
		tooltip: {
		  trigger: 'axis'
		},
		legend: {
		  data: obj.name
		},
		toolbox: {
		  show: false
		},
		calculable: false,
		xAxis: [{
		  type: 'category',
		  data: obj.legend
		}],
		yAxis: [{
		  type: 'value'
		}],
		series: arrData
	  };

 	var myChart = echarts.init(document.getElementById(obj.id),theme); 
 	myChart.setOption(option);
}

function lineBar(obj){
	var echartLine = echarts.getInstanceByDom(document.getElementById(obj.id));
    if(typeof echartLine == "undefined"){
        echartLine = echarts.init(document.getElementById(obj.id), theme);
    }

    echartLine.setOption({
        title: {
            text: obj.title,
            subtext: ''
        },
        tooltip: {
            trigger: 'axis',
        },
        legend: {
            x: 10,
            y: 10,
            data: obj.data
        },
        toolbox: {
            show: true,
            feature: {
                magicType: {
                    show: obj.tool,
                    title: {
                        line: 'Line',
                        bar: 'Bar',
                        stack: 'Stack',
                        tiled: 'Tiled'
                    },
                    type: ['line', 'bar', 'stack', 'tiled']
                },
                saveAsImage: {
                    show: true,
                    title: "Guardar Imagen"
                }
            }
        },
        calculable: true,
        xAxis: [{
            type: 'category',
            boundaryGap: false,
            data: obj.legend
        }],
        yAxis: [{
            type: 'value'
        }],
        series: obj.series
    });
}