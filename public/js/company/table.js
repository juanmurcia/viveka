var field = ['T. Doc','Doc','Nombre','Doc. Representante','Nom. Representante','# Empleados','URL'];

$.each(field, function (key, value) {
    $('.to_do').append('<li><p><input type="checkbox" checked class="toggle-vis" data-column="'+key+'">&nbsp;'+value+' </p></li>');
})

function editCompany(id){

    var response = requestAjax({
        type: 'GET',
        url: '/company/'+id,
        form: 'frmCompany',
        retorna: true,
    });

    editPerson(response.person_id);

    //Carga Formulario
    $("#actionCompany").data("role","change");
    $('#titleModal').text('Modificar Empresa');
    $('#txtCompanyId').val(response.id);
    $('#TBUStxtPersonId').val(response.person.document+' - '+response.person.name+' '+response.person.last_name);
    $('#TBUStxtAgentId').val(response.agent.document+' - '+response.agent.name+' '+response.agent.last_name);
    $('#txtAgentId').val(response.agent_id);
    $('#txtUrl').val(response.url);
    $('#txtEmployees').val(response.number_employees);

    $('#pre').data('step', 2);
    nextStep($('#pre'));
    $('#modCompany').modal({show: 'true'});
}

function destroyCompany(id){

    if(confirm('Seguro Desea inactivar la empresa ?')){
        var response = requestAjax({
            type: 'DELETE',
            url: '/company/'+id,
            form: 'frmCompany',
            success: 'Registro Inactivado',
            retorna: true,
        });
        $.when(response).done(function () {
            tableComapny();
        });
    }    
}

function activateCompany(id){
    if(confirm('Seguro Desea Activar la empresa ?')){
        var response = requestAjax({
            type: 'POST',
            url: '/company/'+id,
            form: 'frmCompany',
            success: 'Registro Activado',
            retorna: true,
        });
        $.when(response).done(function () {
            tableComapny();
        });
    }
}

function tableComapny(){
    tableSearch({
        id:'tbCompanies',
        type:'GET',
        url:'/companies',
        destroy:true,
        checkInactive:true,
        serverSide: true,
        destroy : true,
        checkInactive : false,
        columns: [
            {data: "person.person_document_type.slug", name: "person.person_document_type.slug"},
            {data: 'person.document', name: 'person.document'},
            {data: 'person.name', name: 'person.name'},
            {data: 'agent.document', name: 'agent.document'},
            {data: 'agent.name', name: 'agent.name'},
            {data: 'number_employees', name: 'number_employees', className: "text-center"},
            {data: 'url', name: 'url'},
            {data: 'buttons', name: 'buttons', orderable: false, searchable: true}
        ]
    });
}

tableComapny();