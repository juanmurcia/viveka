loadSelect2({
    id : "txtAgentId",
    url: "/select_people",
    type:"GET",
    length: 2
});

cargaTextBuscador({
    field:"TBUStxtPersonId",
    idField:"txtPersonId",
    url:"/people",
    type:"POST",
    length:"1"
});

function nextStep (obj){
    var step = $(obj).data('step');

    switch (step){
        case 1:
            /*Usabilidad botones*/
            $('#pre').attr('disabled',true);
            $('#actionCompany').attr('disabled',true);
            $('#next').attr('disabled',false);

            /*Usabilidad Step*/
            $('#step1').removeClass().addClass('selected');
            $('#step2').removeClass().addClass('disabled');
            $('#step3').removeClass().addClass('disabled');

            /*Asigna siguiente paso*/
            $('#next').data('step', 2);

            break;
        case 2:

            if($('#TBUStxtPersonId').val() == ''){
                toastr['error']('Debe Ingresar un documento','');
                return false;
            }
            /*Completa datos persona*/
            var person_id=$("#txtPersonId").val();
            if(person_id != '') {
                editPerson(person_id);
            } else {
                createPerson();
                $('#txtIdent').val($("#TBUStxtPersonId").val().split(" ")[0]);
            }

            /*Usabilidad botones*/
            $('#pre').attr('disabled',false);
            $('#actionCompany').attr('disabled',true);
            $('#next').attr('disabled',false);

            /*Usabilidad Step*/
            $('#step1').removeClass().addClass('done');
            $('#step2').removeClass().addClass('selected');
            $('#step3').removeClass().addClass('disabled');

            /*Asigna siguiente paso*/
            $('#pre').data('step', 1);
            $('#next').data('step', 3);

            break;

        case 3:
            /*Ejecuta funcion*/
            if($("#txtPersonId").val() == ''){
                var response = requestAjax({
                    type: "POST",
                    url: "/person",
                    form: "frmPerson",
                    success: "Registro creado",
                    retorna: true
                });

                editPerson(response.id);

            } else {
                var id = $("#txtPersonId").val();
                requestAjax({
                    type: "PUT",
                    url: "/person/"+id,
                    form: "frmPerson",
                    success: "Registro Modificado",
                });
            }

            /*Trae el id de la persona*/
            $("#txtPerson").val($("#txtPersonId").val());

            /*Usabilidad botones*/
            $('#pre').attr('disabled',false);
            $('#actionCompany').attr('disabled',false);
            $('#next').attr('disabled',true);

            /*Usabilidad Step*/
            $('#step1').removeClass().addClass('done');
            $('#step2').removeClass().addClass('done');
            $('#step3').removeClass().addClass('selected');

            /*Asigna siguiente paso*/
            $('#pre').data('step', 2);
            break;
    }

    /*Muestra step si ejecuta funcion*/
    $('.step').hide();
    $('#step-'+step).show();
}

function createCompany() {
    createPerson();
    $('#frmCompany')[0].reset();
    $('#TBUStxtPersonId').val("");
    $('#titleModal').text('Crear Empresa');

    $('#pre').data('step', 1);
    nextStep($('#pre'));

    $('#modCompany').modal({show: 'true'});
}

$('#actionCompany').click(function() {
    var role = $(this).data("role");
    hideTime("#actionCompany");

    if(role == 'add'){
        var response = requestAjax({
            type: 'POST',
            url: '/company',
            modal: 'modCompany',
            form: 'frmCompany',
            success: 'Registro creado',
            resetForm: true,
        });
        $.when(response).done(function () {
            tableComapny();
        });
    }else if(role == 'change'){
        var id = $("#txtCompanyId").val();
        var response = requestAjax({
            type: 'PUT',
            url: '/company/'+id,
            modal: 'modCompany',
            form: 'frmCompany',
            success: 'Registro Modificado',
            resetForm: true,
        });
        $.when(response).done(function () {
            tableComapny();
        });
    }
});