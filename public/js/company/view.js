var company_id  = $('#company_id').val();
var person_id  = 0;

loadSelect2({
    id : "txtAgentId",
    url: "/select_people",
    type:"GET",
    length: 2
});

function editCompany(id){

    var response = requestAjax({
        type: 'GET',
        url: '/company/'+id,
        form: 'frmCompany',
        retorna: true,
    });

    person_id = response.person_id;
    editPerson(response.person_id);

    //Carga Formulario
    $('#txtCompanyId').val(response.id);
    $('#TBUStxtPersonId').val(response.person.document+' - '+response.person.name+' '+response.person.last_name);

    var newOption = new Option(response.agent.document+' - '+response.agent.name+' '+response.agent.last_name, response.agent_id, false, false);
    $('#txtAgentId').append(newOption);

    $('#txtPerson').val(response.person_id);
    $('#txtUrl').val(response.url);
    $('#txtEmployees').val(response.number_employees);
}

$('#actionCompany').click(function() {
    hideTime("#actionCompany");
    requestAjax({
        type: "PUT",
        url: "/person/"+person_id,
        form: "frmPerson",
        success: "Registro Modificado",
    });

    requestAjax({
        type: 'PUT',
        url: '/company/'+company_id,
        form: 'frmCompany',
    });
});

editCompany(company_id);
$( document ).ready(function() {
    $("#general-tabb").click();
});

$("#documents-tabb").click(function(){
    tableDocuments();
});