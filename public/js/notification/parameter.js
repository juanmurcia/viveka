tableSearch({
    id: "tbParNotification",
    type: "GET",
    url: "/par_notifications",
    destroy: true,
    sort: [[4, 'desc']],
    columns: [
        {data: "id", name: "id"},
        {data: "name", name: "name"},
        {data: "description", name: "description"},
        {data: "time", name: "time"},
        {data: "role", name: "role"},
        {data: "buttons", name: "buttons", orderable: false, searchable: true}
    ]
});
