function user_notification(){
    var template = '';
    var objButton = {
        type: 'GET',
        url: '/userNotification',
        retorna: true,
    };

    var response = requestAjax(objButton);
    $.when( response ).done(function ( request ) {
        $('.span-notification').hide();
        if(request.count > 0){
            $('.span-notification').text(request.count);
            $('.span-notification').show();
        }

        $('#menu1').empty();

        $.each(request.data, function(index, val) {
            template = $('#t_notification').html();
            $('#menu1').append(
                template.replace('|sender|', val.sender)
                    .replace('|link|', val.link)
                    .replace('|time|', val.time)
                    .replace('|message|', val.message)
            );
        });
    });
}

user_notification();