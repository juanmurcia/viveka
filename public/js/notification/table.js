$('.myDatepicker').datetimepicker({
    format: 'YYYY-MM-DD HH:mm',
});

let type = 'received';

function consultar(selection){
    var template = '';
    type = selection;

    var objButton = {
        type: 'GET',
        url: '/notifications/'+type,
        retorna: true,
    };

    var response = requestAjax(objButton);

    $.when( response ).done(function ( request ) {
        $('#preMessage').empty();

        $.each(request.data, function(index, val) {
            template = $('#t_message').html();

            $('#preMessage').append(
                template.replace('|sender|',val.sender)
                    .replace('|link|',val.link)
                    .replace('|action|',val.action)
                    .replace('|time|',val.time)
                    .replace('|message|',val.message)
                    .replace('|fa-class|',val.class)
                    .replace('|fa-type|',val.type)
                    .replace('|id|',val.id)
            );
        });
    });
}

function show_message(id){
    var objButton = {
        type: "GET",
        url: "/notification/"+id+"/"+type,
        retorna: true
    };

    var response = requestAjax(objButton);
    $('.message-selected').removeClass('bg-orange');
    $("#dateNotification").text(response.date);
    $("#issueNotification").text(response.name);
    $("#messageNotification").html(response.description);
    $("#senderNotification").text(response.user_cre.name);
    $("#mailNotification").text('('+response.user_cre.email+')');
    $("#addressee").text(response.user.name+' ('+response.user.email+')');

    $('#list'+id).addClass('bg-orange');
    $('.inbox-body').show();
}

function update(id) {
    var objButton = {
        type: "GET",
        url: "/notification/"+id,
        form: "frmNotification",
        retorna: true
    };

    var response = requestAjax(objButton);

    //Carga Formulario
    $("#actionNotification").data("role","change").data("ref",id);
    $("#titleModal").text("Modificar Notificación "+response.name);
    $("#txtName").val(response.name);
    $("#txtDescription").val(response.description);
    $("#optUserId").val(response.user_id);
    $("#txtDateNotification").val(response.notification_date);

    $("#modNotification").modal({show: "true"});
}

function destroy(id) {
    if(confirm("Seguro desea inactivar la notificación ?")){

        var objButton = {
            type: "DELETE",
            url: "/notification/"+id,
            form: "frmNotification",
            success: "Registro Inactivado",
            retorna: true,
            funcion : consultar(),
        };

        requestAjax(objButton);
    }
}

function activate(id) {
    if(confirm("Seguro desea activar la notificación ?")){

        var objButton = {
            type: "POST",
            url: "/notification/"+id,
            form: "frmNotification",
            success: "Registro Activado",
            retorna: true,
            funcion : consultar(),
        };

        requestAjax(objButton);
    }
}

$("#actionNotification").click(function () {
    hideTime(this);
    $('#txtDescription').val($('#editor-one').html());

    var objButton = {
        type: "POST",
        url: "/notification",
        form: "frmNotification",
        success: "Registro Creado",
        resetForm: true,
    };

    var response = requestAjax(objButton);
    $.when( response ).done(function (request) {
        $('#composeMesasage').hide(1500);
        $('#txtDescription').val('');
    });

});

consultar('received');
var url = window.location;
url = url.hash;
if(url != ''){
    var id = url.replace('#','');
    show_message(id);
}