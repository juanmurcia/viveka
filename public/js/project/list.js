function projectAgreement(agreementId) {
    var objButton = {
        type: 'GET',
        url: '/projects/' + agreementId,
        retorna: true,
    };

    var response = requestAjax(objButton);
    $("#listProject").empty();

    $.each(response.data, function (index, val) {
        showProject(val);
    });

    $("#listProject .knob").knob( {
        thickness: ".2",
        readOnly: true,
        fgColor:"#F39100",
        parse: function (v) {return parseInt(v);},
        format: function (v) {return v + "%";}
    });
}

function showProject(val) {
    var div = $("#project-resume").clone();
    $(div).removeAttr("id");
    $(div).find("h3").text(val.name);
    //$(div).find(".project-resume-description").text(val.description);
    $(div).find(".project-resume-responsible").text(val.responsible.person.name+' '+val.responsible.person.last_name);
    $(div).find(".project-resume-start").text(val.start_date);
    $(div).find(".project-resume-end").text(val.end_date);
    $(div).find(".project-resume-percentage").val(val.percentage);
    $(div).find("a").attr("href", "/proyecto/"+val.id);

    $("#listProject").append(div);
    $(div).find(".knobManual").knob( {
        thickness: ".2",
        readOnly: true,
        fgColor:"#F39100",
        parse: function (v) {return parseInt(v);},
        format: function (v) {return v + "%";}
    });


    $(div).fadeIn(1500);
}