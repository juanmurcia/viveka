$("#menu_project").addClass('current-page');

$("#general-tabb").click(function(){
    editProject($("#project_id").val());
});

$("#dashboard-tabb").click(function(){
    productProject($("#project_id").val());
    initializeGraphDateRange($('#txtProjectStartDate').val(),$('#txtProjectEndDate').val());
});

$("#activities-tabb").click(function(){
    patternTimeCostResume($("#pattern_id").val(),$("#model_id").val());
    tableActivities();
});

$("#documents-tabb").click(function(){
    tableDocuments();
});

editProject($("#project_id").val());
$( document ).ready(function() {
    $("#dashboard-tabb").click();
});