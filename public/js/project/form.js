function editProject(id){
    var project = requestAjax({
        type: "GET",
        url: "/project/"+id,
        //form: "frmPerson",
        retorna: true,
    });
    //view
    $('#project_name').text(project.name);
    $('#agreement_name').text(project.agreement.name);
    $('#agreement_name').attr("href","/contrato/"+project.agreement.id);
    //basics
    $('#txtProjectName').val(project.name);
    var newOption = new Option(project.responsible.person.document+' - '+project.responsible.person.name+' '+project.responsible.person.last_name, project.responsible_id, false, false);
    $('#txtProjectResponsible').append(newOption);

    $('#txtProjectDescription').val(project.description);
    $('#txtProjectStartDate').val(project.start_date);
    $('#txtProjectEndDate').val(project.end_date);
    $('#txtProjectValue').val(project.value);
    //progress
    $("#optProjectPatternState").val(project.pattern_state_id);
    $("#txtProjectPercentage").val(project.percentage);
    $("#txtProjectPercentage").attr('min',project.percentage);
    $("#txtProjectJustification").val(project.justification);

}

$("#actionProjectBasics").click(function() {
    hideTime("#actionProjectBasics");
    requestAjax({
        type: "PUT",
        url: "/project/"+$("#project_id").val(),
        form: "frmProjectBasics",
        success: "Registro modificado",
    });
});

$("#actionProjectDetails").click(function() {
    hideTime("#actionProjectDetails");
    requestAjax({
        type: "PUT",
        url: "/project/"+$("#project_id").val()+"/details",
        form: "frmProjectDetails",
        success: "Registro modificado",
    });
});

$("#actionProjectProgress").click(function() {
    hideTime("#actionProjectProgress");
    requestAjax({
        type: "PUT",
        url: "/project/"+$("#project_id").val()+"/progress",
        form: "frmProjectProgress",
        success: "Registro modificado",
    });
});