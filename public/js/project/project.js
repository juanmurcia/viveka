function action(){
    setTimeout(function(){
        $('.see-project, del-project').unbind('click');
        
        $('.see-project').click(function() {            
            var idV = $(this).data('toolbar');
            window.location.href = '/project/'+idV;
        });

        $('.del-project').click(function() {            
            if(confirm('Seguro Desea anular el proyecto ?')){
                var id = $(this).data('toolbar');
                var response = requestAjax({
                    type: 'DELETE',
                    url: '/project/'+id,
                    form: 'frmProject',
                    success: 'Registro Inactivado',
                    retorna: true,
                });
                $.when(response).done(function () {
                    consultar();
                });
            }        
        });  

        if ($(".progress .progress-bar")[0]) {
            console.log('progress');
            $('.progress .progress-bar').progressbar();
        }      
    },1200);
}