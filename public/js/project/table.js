var field = ['Contrato','Estado','Nombre','Descripción','% Avance', 'Fec Inicio', 'Fec Fin'];

$.each(field, function (key, value) {
    $('.to_do').append('<li><p><input type="checkbox" checked class="toggle-vis" data-column="'+key+'">&nbsp;'+value+' </p></li>');
})

$('#actionProject').click(function(){
    tableSearch({
        id:'tbProjects',
        type:'POST',
        url:'/projects',
        form:'frmProject',
        destroy:true,
        exportButtons : true,
        checkInactive:true,
        serverSide: true,
        columns: [
            {data: 'agreement.name', name: 'agreement.name', className: "text-right"},
            {data: 'pattern_state.name', name: 'pattern_state.name'},
            {data: 'name', name: 'name'},
            {data: 'description', name: 'description'},
            {data: 'percentage', name: 'percentage'},
            {data: 'start_date', name: 'start_date'},
            {data: 'end_date', name: 'end_date'},
            {data: 'buttons', name: 'buttons', orderable: false, searchable: true}
        ]


    });
});
loadSelect2({
    id : "txtProjectResponsible",
    url: "/agreement_team/0",
    type:"GET"
});

$('#actionProject').click();

function addProjectCallBack() {
    $('#actionProject').click();
};