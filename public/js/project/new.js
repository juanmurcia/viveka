loadSelect2({
    id : "txtProjectResponsible",
    url: "/agreement_team/"+$("#agreement_id").val(),
    type:"GET"
});

$("#newProject").click(function() {
    hideTime("#actionProject");
    //Guarda campos extra antes del envío
    $("#txtProjectAgreement").remove();
    $("<input>").attr({
        type: "hidden",
        id: "txtProjectAgreement",
        name: "txtProjectAgreement",
        value: $("#agreement_id").val()
    }).appendTo("#frmProject");

   var response = requestAjax({
        type: "POST",
        url: "/project",
        form: "frmProject",
        success: "Registro creado",
        resetForm: true,
        modal: "modProject",
   });
   $.when(response).done(function () {
       projectAgreement($("#agreement_id").val());
   });
});