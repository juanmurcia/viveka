function tableRole(){
    tableSearch({
        id:'tbRoles',
        type:'GET',
        url:'/roles',
        destroy:true,
        columns: [
            {data: 'name', name: 'name'},
            {data: 'slug', name: 'slug'},
            {data: 'description', name: 'description'},
            {data: 'special', name: 'special'},
            {data: 'created_at', name: 'fec_cre'},
            {data: 'buttons', name: 'buttons', orderable: false, searchable: true}
        ]
    });
}
tableRole();

//Funciones Botones
$('#btnRole').click(function() {
    $('#frmRole')[0].reset();
    $('#actionRole').data('role','add');
    $('#titleModal').text('Asignar Permisos');
});

$('#actionRole').click(function() {
    hideTime('#actionRole');
    var role = $(this).data('role');

    if(role == 'add'){
        var response = requestAjax({
            type: 'POST',
            url: '/role',
            form: 'frmRole',
            modal: 'modRole',
            success: 'Registro creado satisfactoriamente',
            resetForm: true,
        });
        $.when(response).done(function () {
            tableRole();
        });
    }else if(role== 'change'){
        var id = $(this).data('ref');
        var response = requestAjax({
            type: 'PUT',
            url: '/role/'+id,
            form: 'frmRole',
            modal: 'modRole',
            success: 'Registro Modificado satisfactoriamente',
            resetForm: true,
        });
        $.when(response).done(function () {
            tableRole();
        });
    }
});

function editRole(id) {

    var response = requestAjax({
        type: 'GET',
        url: '/role/'+id,
        form: 'frmRole',
        retorna: true,
    });
    //Carga Formulario
    $('#actionRole').data('role','change');
    $('#actionRole').data('ref',id);
    $('#titleModal').text('Asignar permisos Rol : '+response.name);

    $.each(response.permissions, function(index, val) {
        $('#chkPermission_'+val.id).attr('checked',true);
    });

    $('#modRole').modal({show: 'true'});
}