var atribModel = {
    1 : {
        "color" : "#014364",
        "view" : "contrato"
    },
    2 : {
        "color" : "#097040",
        "view" : "proyecto"
    },
    3 : {
        "color" : "#A42725",
        "view" : "producto"
    },
    4 : {
        "color" : "#A2592E",
        "view" : "actividad"
    },
    5 : {
        "color" : "#F79982"
    },
    6 : {
        "color" : "#FDC387",
        "view" : "empresa"
    },
    7 : {
        "color" : "#5E9DBC",
    },
    11 : {
        "color" : "#10AA6A",
    }
};

$.each(atribModel, function (key, data) {
    $("[for=input_search_"+key+"]").attr("style","border-color:"+data.color+";");
});


$('#btnSearch').click(function(){

    if($("#txtSearch").val()){
        var response = requestAjax({
            type: "POST",
            url: "/search",
            form: "frmSystemSearch",
            retorna: true,
        });

        $.when( response ).done(function ( request ) {
            $(".messages").empty();
            $.each(request, function (index, val) {
                if(val.length > 0){
                    $.each(val, function (key, data) {
                        var date = moment(data.created_at);
                        var div = $("#data_search").clone();

                        $(div).find("#model").css("color",atribModel[index].color);
                        $(div).find("#name").text(data.name);
                        $(div).find("#message").text(data.detail);
                        $(div).find("#day").text(date.format('Do'));
                        $(div).find("#month").text(date.format('MMM')+', '+date.format('YY'));

                        if(index == 11){
                            $(div).find("#url").show();
                            $(div).find("#link").attr("href","/document/download/"+data.id);
                        }

                        if(atribModel[index].view){
                            $(div).find("#view").attr("href","/"+atribModel[index].view+"/"+data.id);
                        }
                        /*
                        $(div).find("#phone").text(request.agreement.contractor.person.phone);
                        $(div).find("#email").text(request.agreement.contractor.person.email);
                        */
                        $("#list_search").append(div);
                    });
                }
            });
        });
    }
});

$('#comboCheck').change(function(){
    if($(this).is(':checked')){
        $(".filterPattern").prop('checked', true);
    }
    else{
        $(".filterPattern").prop('checked', false);
    }
});