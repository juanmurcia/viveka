function userPerson(id) {
    $("#actionUser").data('role', 'add');
    var response = requestAjax({
        type: "GET",
        url: "/person/"+id,
        retorna: true,
    });

    //Carga Formulario
    var nombre = response.name.split(" ")[0]+" "+response.last_name.split(" ")[0];

    if(response.users){
        response = requestAjax({
            type: "GET",
            url: "/user/"+response.users.id,
            retorna: true,
        });

        //Carga Formulario
        nombre = response.name;
        $('#divPasswords').hide();
        $("#optRole").val(response.role_id);
        $("#txtPass").val(nombre);
        $("#txtPassC").val(nombre);
        $("#actionUser").data('role', 'change');
    }

    $("#txtEmailUser").val(response.email);
    $("#txtNomUser").val(nombre);
    $("#txtIdUser").val(response.id);

    $("#modUser").modal({show: "true"});
}

$("#actionUser").click(function() {
    var role = $("#actionUser").data('role');
    if(role == "add"){
        var response = requestAjax({
            type: "POST",
            url: "/user",
            modal: "modUser",
            form: "frmUserPerson",
            success: "Registro creado",
            resetForm: true,
        });
    }else if(role== "change"){
        var id = $("#txtIdUser").val();

        var response = requestAjax({
            type: "PUT",
            url: "/user/"+id,
            modal: "modUser",
            form: "frmUserPerson",
            success: "Registro Modificado",
            resetForm: true,
        });
    }
});
// Validación contraseña
var pass = "";
var pass2 = "";

$( "#txtPass, #txtPassC" ).keyup(function() {
    $("#actionUser").attr("disabled","disabled");

    pass = $("#txtPass").val();
    pass2 = $("#txtPassC").val();
    if(pass == pass2){
        $("#actionUser").removeAttr("disabled");
    }
});

jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
});

$( "#frmUserPerson" ).validate({
    rules: {
        txtPass: "required",
        txtPassC: {
            equalTo: "#txtPass"
        }
    }
});