function user_password(){
    $("#modPasswordUser").modal({show: "true"});
}

$("#actionPasswordUser").click(function () {
    hideTime(this);

    var response = requestAjax({
        type: "POST",
        url: "/user/change_password/0",
        modal: "modPasswordUser",
        form: "frmPasswordUser",
        success: "Contraseña actualizada",
        resetForm: true,
        retorna : true
    });
    $.when(response).done(function () {
        //Click en cerrar sesión para validar contraseña
        if(response.success){
            $("#logout-form").submit();
        }
    });
});