var field = ['Nombre','Email','Role','Estado','Fec Cre','Fec Mod'];

$.each(field, function (key, value) {
    $('.to_do').append('<li><p><input type="checkbox" checked class="toggle-vis" data-column="'+key+'">&nbsp;'+value+' </p></li>');
})

function tableUser(){
    tableSearch({
        id:"tbUsers",
        type:"GET",
        url:"/users",
        destroy:true,
        serverSide: true,
        checkInactive:true,
        createdRow:1,
        columns: [
            {data: "name", name: "name"},
            {data: "email", name: "email"},
            {data: "role_id", name: "role_id"},
            {data: "state", name: "state", className: "text-center"},
            {data: "created_at", name: "created_at"},
            {data: "updated_at", name: "updated_at"},
            {data: "buttons", name: "buttons", orderable: false, searchable: true}
        ]
    });
}

tableUser();

function updateUser(id) {
    
    var response = requestAjax({
        type: "GET",
        url: "/user/"+id,
        form: "frmUser",
        retorna: true
    });

    //Carga Formulario
    $("#actionUser").data("role","change");
    $("#actionUser").data("ref",id);
    $("#titleModal").text("Modificar Usuario "+response.name);
    $("#txtNomUser").val(response.name);
    $("#txtEmailUser").val(response.email);
    $("#optRole").val(response.role_id);

    $("#modUser").modal({show: "true"});
}

function destroyUser(id) {
    if(confirm("Seguro desea inactivar el usuario ?")){
        var response = requestAjax({
            type: "DELETE",
            url: "/user/"+id,
            form: "frmUser",
            success: "Registro Inactivado",
            retorna: true,
        });
        $.when(response).done(function () {
            tableUser();
        });
    }
}

function activateUser(id) {
    if(confirm("Seguro desea activar el usuario ?")){
        var response = requestAjax({
            type: "POST",
            url: "/user/"+id,
            form: "frmUser",
            success: "Registro Activado",
            retorna: true,
        });
        $.when(response).done(function () {
            tableUser();
        });
    }
}

$("#actionUser").click(function () {
    var role = $(this).data("role");
    hideTime(this);

    if(role== "change"){
        var id = $(this).data("ref");
        var response = requestAjax({
            type: "PUT",
            url: "/user/"+id,
            modal: "modUser",
            form: "frmUser",
            success: "Registro Modificado",
            resetForm: true,
        });
        $.when(response).done(function () {
            tableUser();
        });
    }
});