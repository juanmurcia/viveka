var agreement_list = JSON.parse($("#agreements").val());
var project_list = JSON.parse($("#projects").val());
var product_list = JSON.parse($("#products").val());
var activity_list = JSON.parse($("#activities").val());

$('#util').empty();
function make_agreement(filter){
    $('#optAgreementCalendar option').remove();
    $('#optAgreementCalendar').append($("<option></option>").text('... Seleccione uno ...'));

    var data = agreement_list;
    if(filter > 0){
        data = agreement_list.filter(function (i,n){ return n.id ===  filter; });
    }
    var checked = data.length > 1 ? false : true;
    $.each(data, function (index, val) {
        $('#optAgreementCalendar')
            .append($("<option></option>")
                .attr("value",val.id)
                .attr("selected",checked)
                .text(val.number+' '+val.name));
    });
}

function make_project(filter){
    $('#optProjectCalendar option').remove();
    $('#optProjectCalendar').append($("<option></option>").text('... Seleccione uno ...'));

    var data = project_list;
    if(filter > 0){
        data = $.grep(project_list, function (project) {
            return project.agreement_id == filter;
        });
    }
    var checked = data.length > 1 ? false : true;
    $.each(data, function (index, val) {
        $('#optProjectCalendar')
            .append($("<option></option>")
                .attr("value",val.id)
                .attr("selected",checked)
                .text(val.name));
    });
}

function make_product(filter){
    $('#optProductCalendar option').remove();
    $('#optProductCalendar').append($("<option></option>").text('... Seleccione uno ...'));

    var data = product_list;
    if(filter > 0){
        data = $.grep(product_list, function (product) {
            return product.project_id == filter;
        });
    }
    var checked = data.length > 1 ? false : true;
    $.each(data, function (index, val) {
        $('#optProductCalendar')
            .append($("<option></option>")
                .attr("value",val.id)
                .attr("selected",checked)
                .text(val.name));
    });
}

function make_activity(pattern, id){
    $('#optActivityCalendar option').remove();
    $('#optActivityCalendar').append($("<option></option>").text('... Seleccione uno ...'));

    var data = activity_list;
    if(pattern > 0){
        data = $.grep(activity_list, function (activity) {
            return activity.pattern_id == pattern;
        });

        data = $.grep(data, function (activity) {
            return activity.model_id == id;
        });
    }
    var checked = data.length > 1 ? false : true;
    $.each(data, function (index, val) {
        $('#optActivityCalendar')
            .append($("<option></option>")
                .attr("value",val.id)
                .attr("selected",checked)
                .text(val.name));
    });
}

$("#actionNewCalendar").click(function(){
    $('#frmActivityExecutedTime')[0].reset();
    var date1 = moment($( "#calendarEndDate" ).val(),"YYYY-MM-DD H:mm"),
        date2 = moment($( "#calendarStartDate" ).val(),"YYYY-MM-DD H:mm");
    var duration = moment.duration(date1.diff(date2)).asMinutes();


    $("#txtActivityExecutedTimeDescription").val($( "#txtObservationCalendar" ).val());
    $("#txtActivityExecutedTimeActivityId").val($( "#optActivityCalendar option:selected" ).val());
    $("#txtActivityExecutedTimeEndDate").val($( "#calendarEndDate" ).val());
    $("#txtActivityExecutedTimeExecuted").val(duration);

    if($(this).data("role") == "add"){
        var response = requestAjax({
            type: 'POST',
            url: '/activity_executed_time',
            modal: "modCalendar",
            form: "frmActivityExecutedTime",
            retorna: true,
            resetForm: true,
        });

        $.when(response).done(function () {
            $('#frmActivityExecutedTime')[0].reset();
            if(response.id){
                saveNewSchedule({
                    id : response.id,
                    calendarId : 1,
                    url : '/actividad/'+$( "#optActivityCalendar option:selected" ).val(),
                    title : $( "#optActivityCalendar option:selected" ).text(),
                    isAllDay : false,
                    start :  $( "#calendarStartDate" ).val(),
                    end : $( "#calendarEndDate" ).val(),
                    location: $( "#txtObservationCalendar" ).val(),
                    raw : {"class" : "public"}
                });
            }
        });
    }else{
        $("#txtActivityExecutedTimeActivityId").val(1);
        requestAjax({
            type: 'PUT',
            url: '/activity_executed_time/'+$(this).data("id"),
            form: "frmActivityExecutedTime",
            modal: "modCalendar",
            retorna: true,
            resetForm: true,
        });
    }
});

//Tiempos Actividades Portafolio
var response = requestAjax({
    type: 'GET',
    url: '/activity_executed_time/calendar',
    retorna: true,
});

$.when(response).done(function () {
    $.each(response.data, function (index, val) {
        var start = moment(val.end_date,"YYYY-MM-DD H:mm");
        start.subtract(val.executed,'minutes');

        saveNewSchedule({
            id : val.id,
            calendarId : 1,
            title : val.activity_id+". "+val.activity.name,
            isAllDay : false,
            start :  start.format("YYYY-MM-DD H:mm"),
            end : val.end_date,
            comingDuration : val.executed+':00',
            location: val.description,
            raw : {"class" : "public"},
            state: val.person.name+" "+val.person.last_name
        });
    });
});

//Productos de la semana
var response = requestAjax({
    type: 'GET',
    url: "/calendar/products",
    retorna: true,
});

$.when(response).done(function () {
    $.each(response, function (index, val) {
        var start = moment(val.end_date,"YYYY-MM-DD");

        saveNewSchedule({
            id : val.id,
            calendarId : 1,
            title : val.id+". "+val.name,
            isAllDay : true,
            start :  start.format("YYYY-MM-DD"),
            end : start.format("YYYY-MM-DD"),
            comingDuration : val.percentage+' %',
            location: val.description,
            raw : {"class" : "public"},
            state: ""
        });
    });
});