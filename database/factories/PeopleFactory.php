<?php

use Faker\Generator as Faker;
use App\Models\Person;

$factory->define(Person::class, function (Faker $faker) {
    return [
        'person_document_type_id' => 1,
        'taxpayer_type_id' => 1,
        'document' => $faker->numberBetween(1,999999999),
        'name' => $faker->name,
        'last_name' => $faker->lastName,
        'address' => $faker->address,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->phoneNumber,
        'user_cre_id' => 1
    ];
});
