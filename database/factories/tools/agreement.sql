INSERT INTO `agreement_categories` (`id`, `name`, `iso`) VALUES(1, 'ASISTENCIA', 'A');
INSERT INTO `agreement_categories` (`id`, `name`, `iso`) VALUES(2, 'CONSULTORÍA', 'C');
INSERT INTO `agreement_categories` (`id`, `name`, `iso`) VALUES(3, 'ESTRUCUTRACIÓN', 'E');
INSERT INTO `agreement_categories` (`id`, `name`, `iso`) VALUES(4, 'EVALUACIÓN', 'l');
INSERT INTO `agreement_categories` (`id`, `name`, `iso`) VALUES(5, 'GERENCIA', 'G');
INSERT INTO `agreement_categories` (`id`, `name`, `iso`) VALUES(6, 'SUPERVISIÓN', 'S');
INSERT INTO `agreement_categories` (`id`, `name`, `iso`) VALUES(7, 'VALORACIÓN', 'V');
INSERT INTO `agreement_natures` (`id`, `name`) VALUES(1, 'PRIVADO');
INSERT INTO `agreement_natures` (`id`, `name`) VALUES(2, 'PUBLICO');
INSERT INTO `agreement_process_types` (`id`, `name`) VALUES(1, 'DIRECTA');
INSERT INTO `agreement_process_types` (`id`, `name`) VALUES(2, 'LICITACIÓN');
INSERT INTO `agreement_types` (`id`, `name`, `iso`) VALUES(1, 'ASESORÍA APP','A');
INSERT INTO `agreement_types` (`id`, `name`, `iso`) VALUES(2, 'BANCA DE INVERSIÓN','B');
INSERT INTO `agreement_types` (`id`, `name`, `iso`) VALUES(3, 'CONSULTORÍA INTEGRAL','C');
INSERT INTO `agreement_types` (`id`, `name`, `iso`) VALUES(4, 'CONSULTORÍA ESTRATÉGICA Y FINANCIERA','R');
INSERT INTO `agreement_types` (`id`, `name`, `iso`) VALUES(5, 'CONSULTORÍA FINANCIERA','N');
INSERT INTO `agreement_types` (`id`, `name`, `iso`) VALUES(6, 'ESTRUCTURACIÓN FINANCIERA','F');
INSERT INTO `agreement_types` (`id`, `name`, `iso`) VALUES(7, 'ESTRUCTURACIÓN FINANCIERA Y LEGAL','I');
INSERT INTO `agreement_types` (`id`, `name`, `iso`) VALUES(8, 'ESTRUCTURACIÓN INTEGRAL','E');
INSERT INTO `agreement_types` (`id`, `name`, `iso`) VALUES(9, 'ESTRUCTURACIÓN LEGAL','G');
INSERT INTO `agreement_types` (`id`, `name`, `iso`) VALUES(10, 'ESTRUCTURACIÓN TÉCNICA','T');
INSERT INTO `agreement_types` (`id`, `name`, `iso`) VALUES(11, 'EVALUACIÓN Y VALIDACIÓN APP','L');
INSERT INTO `agreement_types` (`id`, `name`, `iso`) VALUES(12, 'PERITAZGO','P');
INSERT INTO `agreement_types` (`id`, `name`, `iso`) VALUES(13, 'SUPERVISIÓN DE PROYECTO','S');
INSERT INTO `agreement_types` (`id`, `name`, `iso`) VALUES(14, 'VALORACIÓN FINANCIERA','V');
INSERT INTO `agreement_types` (`id`, `name`, `iso`) VALUES(15, 'GERENCIA INTEGRAL','Z');
INSERT INTO `agreement_coverages` (`id`, `name`, `iso`) VALUES(1, 'NACIONAL','N');
INSERT INTO `agreement_coverages` (`id`, `name`, `iso`) VALUES(2, 'REGIONAL','R');
INSERT INTO `agreement_coverages` (`id`, `name`, `iso`) VALUES(3, 'DEPARTAMENTAL','D');
INSERT INTO `agreement_coverages` (`id`, `name`, `iso`) VALUES(4, 'MUNICIPAL','M');
INSERT INTO `agreement_coverages` (`id`, `name`, `iso`) VALUES(5, 'INTERNACIONA','I');
INSERT INTO `agreement_services` (`id`, `name`) VALUES(1, 'ESTRUTURACIÓN DE PROYECTOS');
INSERT INTO `agreement_services` (`id`, `name`) VALUES(2, 'ASESORÍA EN ASOCIACIONES PÚBLICO PRIVADAS APP');
INSERT INTO `agreement_services` (`id`, `name`) VALUES(3, 'CONSULTORÍA FINANCIERA Y BANCA DE INVERSIÓN');