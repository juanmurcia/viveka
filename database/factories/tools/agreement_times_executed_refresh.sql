DROP PROCEDURE IF EXISTS `activity_estimated_times_executed_refresh`;
CREATE PROCEDURE `activity_estimated_times_executed_refresh`(IN `p_agreement_id` INT unsigned) NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
BEGIN
  UPDATE activity_estimated_times
    INNER JOIN (
                 SELECT
                   agreement_id,
                   role_id,
                   COALESCE(SUM(executed), 0) AS executed
                 FROM (
                        (SELECT
                           agreement_id,
                           role_id,
                           executed
                         FROM agreement_activity_times)
                        UNION
                        (SELECT
                           agreement_id,
                           role_id,
                           executed
                         FROM project_activity_times)
                        UNION
                        (SELECT
                           agreement_id,
                           role_id,
                           executed
                         FROM product_activity_times)
                      ) AS join_table
                 GROUP BY agreement_id, role_id
               ) AS agreement_total
      ON agreement_total.agreement_id = activity_estimated_times.model_id
         AND activity_estimated_times.pattern_id = 1
         AND activity_estimated_times.role_id = agreement_total.role_id
  SET activity_estimated_times.executed = agreement_total.executed
  WHERE agreement_total.agreement_id=p_agreement_id;

  UPDATE activity_estimated_times
    INNER JOIN (
                 SELECT
                   project_id,
                   role_id,
                   COALESCE(SUM(executed), 0) AS executed
                 FROM (
                        (SELECT
                           project_id,
                           role_id,
                           executed
                         FROM project_activity_times
                         WHERE agreement_id=p_agreement_id)
                        UNION
                        (SELECT
                           project_id,
                           role_id,
                           executed
                         FROM product_activity_times
                         WHERE agreement_id=p_agreement_id)
                      ) AS join_table

                 GROUP BY project_id, role_id
               ) AS project_total
      ON project_total.project_id = activity_estimated_times.model_id
         AND activity_estimated_times.pattern_id = 2
         AND activity_estimated_times.role_id = project_total.role_id
  SET activity_estimated_times.executed = project_total.executed;

  UPDATE activity_estimated_times
    INNER JOIN (
                 SELECT
                   product_id,
                   role_id,
                   COALESCE(SUM(executed), 0) AS executed
                 FROM product_activity_times
                 WHERE agreement_id=p_agreement_id
               ) AS product_total
      ON product_total.product_id = activity_estimated_times.model_id
         AND activity_estimated_times.pattern_id = 3
         AND activity_estimated_times.role_id = product_total.role_id
  SET activity_estimated_times.executed = product_total.executed;
END
