INSERT INTO `system_parameters` (`id`, `name`, `value`, `user_cre_id`, `user_mod_id`, `created_at`, `updated_at`) VALUES ('1', 'iva_billing', '19', '1', '1', '2018-09-03 00:00:00', '2018-09-03 00:00:00');

INSERT INTO `agreements` (`id`, `active`, `number`, `iva_billing`, `billing_due`, `name`, `object`, `employer_id`, `contractor_id`, `responsible_id`, `pattern_state_id`, `percentage`, `justification`, `currency_id`, `agreement_process_type_id`, `agreement_category_id`, `agreement_nature_id`, `agreement_type_id`, `agreement_coverage_id`, `agreement_service_id`, `department_id`, `signing_date`, `start_date`, `end_date`, `execution_term`, `addition_term`, `value`, `addition_value`, `external_url`, `user_cre_id`, `user_mod_id`, `created_at`, `updated_at`) VALUES
  (1, 1, 'VC-C-02-2018', 19, 30, 'FINDETER Bata Blanca', 'VALIDACION TECNICA, LEGAL y FINANCIERA DEL PROYECTO EN FASE DE FACTIBILIDAD DENOMINADO "ASOCIACIÓN PÚBLICO PRIVADA PARA LA CONSTRUCCIÓN Y/O ADECUACIÓN, OPERACIÓN, MANTENIMIENTO, Flt,¡ANCIAMIENTO, EQUIPAMIENTO y PRESTACIÓN DEL SERVICIO MÉDICO (BATA BLANCA) DE 3 CENTROS DE ATENCIÓN PRIORITARIA - CAPS, EN LA CIUDAD DE BOGOTÁ", LOS CUALES SERÁN DESARROLLADOS BAJO LA MODALIDAD DE ASOCIACIÓN PÚBLICO PRIVADA (APP) DE INICIATIVA PRIVADA, POR LA SECRETARIA DE SALUD DE BOGOTÁD.C.', 2, 1, 4, 13, '60.00', 'Se revisa la entrega de la primera entrega', 1, 1, 1, 1, 3, 1, 1, 11, '2018-04-11', '2018-04-11', '2018-08-24', 0, 0, '419419665.00', '0.00', NULL, 1, 1, '2018-07-19 00:17:27', '2018-07-19 03:16:24'),
  (2, 1, 'CO-003-2018', 19, 30, 'Creación de Polla Mundialista', 'Creacion de una polla mundialista para la familia Arias Echeverri y para Viveka SAS', 2, 1, 2, 14, '100.00', 'Se terminó el mundial', 1, 1, 2, 1, 1, 4, 1, 11, '2018-05-14', '2018-05-14', '2018-06-14', 30, 0, '790000.00', '200000.00', NULL, 1, 1, '2018-07-19 00:21:24', '2018-07-19 00:48:22'),
  (4, 1, 'CO-004-2018', 19, 30, 'Sistema Educativo de Viveka', 'Implementar un sistema educativo con videos, tareas y actividades para capacitar al personal de VIVEKA en APPs', 2, 1, 2, 13, '30.00', 'Primer avance listo', 1, 1, 2, 2, 5, 2, 1, 8, '2018-07-04', '2018-07-04', '2018-09-04', 60, 0, '5700000.00', '0.00', NULL, 1, 1, '2018-07-19 00:42:17', '2018-07-19 01:00:56');

INSERT INTO `agreement_teams` (`id`, `active`, `professional_id`, `agreement_id`, `user_mod_id`, `created_at`, `updated_at`) VALUES
  (2, 1, 2, 2, 1, '2018-07-19 00:56:08', '2018-07-19 00:56:08'),
  (4, 1, 2, 4, 1, '2018-07-19 01:37:28', '2018-07-19 01:52:14'),
  (5, 1, 4, 1, 1, '2018-07-19 01:59:45', '2018-07-19 01:59:45'),
  (6, 1, 2, 1, 1, '2018-07-19 01:59:53', '2018-07-19 01:59:53'),
  (7, 1, 1, 1, 1, '2018-07-19 02:01:40', '2018-07-19 02:01:40'),
  (8, 1, 5, 1, 3, '2018-07-19 09:41:50', '2018-07-19 09:41:50');

INSERT INTO `projects` (`id`, `active`, `agreement_id`, `pattern_state_id`, `responsible_id`, `value`, `name`, `description`, `percentage`, `justification`, `start_date`, `end_date`, `user_cre_id`, `user_mod_id`, `created_at`, `updated_at`) VALUES
  (1, 1, 1, 22, 4, 20000000.00, 'VALIDACION TECNICA, LEGAL y FINANCIERA', 'VALIDACION TECNICA, LEGAL y FINANCIERA DEL PROYECTO EN FASE DE FACTIBILIDAD DENOMINADO "ASOCIACIÓN PÚBLICO PRIVADA PARA LA CONSTRUCCIÓN Y/O ADECUACIÓN, OPERACIÓN, MANTENIMIENTO, Flt,¡ANCIAMIENTO, EQUIPAMIENTO y PRESTACIÓN DEL SERVICIO MÉDICO (BATA BLANCA) DE 3 CENTROS DE ATENCIÓN PRIORITARIA - CAPS, EN LA CIUDAD DE BOGOTÁ", LOS CUALES SERÁN DESARROLLADOS BAJO LA MODALIDAD DE ASOCIACIÓN PÚBLICO PRIVADA (APP) DE INICIATIVA PRIVADA, POR LA SECRETARIA DE SALUD DE BOGOTÁD.C.', '30.00', 'Recibi', '2018-04-11', '2018-08-24', 1, 3, '2018-07-19 04:06:33', '2018-07-19 08:53:57'),
  (2, 1, 1, 21, 4, 4000000.00, 'Generación de manuales en video', 'Creación de los videos de como realizar diferentes operaciones en el sistema', '0.00', NULL, '2018-08-24', '2018-08-31', 1, NULL, '2018-07-19 04:08:10', '2018-07-19 04:08:10');

INSERT INTO `products` (`id`, `active`, `project_id`, `pattern_state_id`, `responsible_id`, `value`,  `name`, `description`, `percentage`, `justification`, `start_date`, `end_date`, `user_cre_id`, `user_mod_id`, `created_at`, `updated_at`) VALUES
  (1, 1, 1, 33, 4, 125825900.00, 'Primer informe de Validación', 'deberá contener un análisis sobre el marco situacional del proyecto, el informe inicial respecto de la revisión técnica, legal y financiera, y los requerimientos de ajustes a los estudios a que haya lugar por parte del originador del proyecto, dentro de la metodología iterativa a emplear', '100.00', 'Se recibe la primera entrega', '2018-04-11', '2018-06-25', 1, 1, '2018-07-18 23:11:20', '2018-07-18 23:20:56'),
  (2, 1, 1, 31, 4, 125825900.00, 'Segundo informe de Validación', 'deberá contener el informe respecto de la evolución de los ajustes solicitados al originador y su estado, tanto del componente técnico, como del legal y financiero, y los requerimientos adicionales para ajustar definitivamente los estudios a fin de que cumplan con la normatividad vigente para proyectos APP, y la demás normatividad local o nacional que aplique, asi como a los requerimientos de la Secretaria de Salud del distrito. Así mismo deberá contener el CONCEPTO TECNICO que analice la posibilidad de construir un Hospital de Alla. Complejidad en el Predio El Virrey en USME.', '0.00', NULL, '2018-06-25', '2018-08-24', 1, NULL, '2018-07-18 23:12:41', '2018-07-18 23:12:41'),
  (3, 1, 1, 31, 4, 167767865.00, 'Informe Final de Validación', 'deberá contener el análisis definitivo respecto de la factibilidad, el concepto de validación o no validación, y en este último caso las recomendaciqnes para llevar el proyecto a una validación positiva. Finalmente, las conclusiones definitivas de la revisión realizada. En caso de ser positiva la validación, se deben incluir los términos de referencia para la licitación del proyecto.', '0.00', NULL, '2018-06-25', '2018-08-24', 1, NULL, '2018-07-18 23:12:41', '2018-07-18 23:12:41');

INSERT INTO `activities` (`id`, `active`, `pattern_id`, `model_id`, `pattern_state_id`, `name`, `description`, `start_date`, `end_date`, `user_cre_id`, `user_mod_id`, `created_at`, `updated_at`) VALUES
  (1, 1, 1, 1, 43, 'Formalizacion Contrato', 'Se redactara y firmara el contrato', '2018-04-11 00:00:00', '2018-04-11 00:00:00', 1, NULL, '2018-04-11 21:26:02', '2018-04-11 21:26:02'),
  (2, 1, 3, 1, 43, 'Desarrollo', 'Ninguno', '2018-04-11 00:00:00', '2018-06-25 00:00:00', 3, NULL, '2018-04-12 03:00:00', '2018-04-12 03:00:00'),
  (3, 1, 2, 1, 41, 'Planeacion', 'Vamos a hacer x cosas', '2018-07-05 00:00:00', '2018-07-18 00:00:00', 3, 3, '2018-07-19 05:23:07', '2018-07-19 05:24:55');

INSERT INTO `activity_executed_times` (`id`, `active`, `activity_id`, `person_id`, `role_id`, `description`, `end_date`, `executed`, `user_cre_id`, `user_mod_id`, `created_at`, `updated_at`) VALUES
  (1, 1, 1, 2,  5, 'Reunion Firma de Contrato', '2018-04-11 05:00:00', '1.00', 1, NULL, '2018-04-11 21:26:02', '2018-04-11 21:26:02'),
  (2, 1, 1, 3,  2, 'Reunion Firma de Contrato', '2018-04-11 05:00:00', '1.00', 1, NULL, '2018-04-11 21:26:03', '2018-04-11 21:26:03'),
  (3, 1, 1, 5,  3, 'Reunion Firma de Contrato', '2018-04-11 05:00:00', '1.00', 1, NULL, '2018-04-11 21:26:04', '2018-04-11 21:26:04'),
  (4, 1, 2, 2,  5, 'Avance', '2018-04-11 22:00:00', '16.00', 3, NULL, '2018-04-12 03:00:00', '2018-04-12 03:00:00'),
  (5, 1, 2, 2,  5, 'Horass trabajadas', '2018-06-01 05:00:00', '200.00', 3, NULL, '2018-06-01 10:00:00', '2018-06-01 10:00:00'),
  (6, 1, 2, 2,  5, 'Horas Trabajadas', '2018-07-18 19:04:00', '500.00', 3, 3, '2018-07-19 00:04:57', '2018-07-19 00:20:27'),
  (7, 1, 1, 5,  3, 'Estructuracion del contrato', '2018-07-18 22:23:00', '3.00', 3, NULL, '2018-07-19 03:23:57', '2018-07-19 03:23:57'),
  (8, 1, 1, 3,  2, 'Estructuracion del contrato', '2018-07-18 22:23:00', '3.00', 3, NULL, '2018-07-19 03:23:58', '2018-07-19 03:23:58'),
  (9, 1, 1, 2,  5, 'Runion para firmar contrato', '2018-07-18 20:42:00', '2.00', 3, NULL, '2018-07-19 04:45:02', '2018-07-19 04:45:02'),
  (10, 1, 1, 3, 2, 'Runion para firmar contrato', '2018-07-18 20:42:00', '2.00', 3, NULL, '2018-07-19 04:45:03', '2018-07-19 04:45:03'),
  (11, 1, 1, 5, 3, 'Runion para firmar contrato', '2018-07-18 20:42:00', '2.00', 3, NULL, '2018-07-19 04:45:04', '2018-07-19 04:45:04'),
  (12, 1, 3, 2, 5, 'Planeacion', '2018-07-18 00:22:00', '4.00', 3, 3, '2018-07-19 05:23:07', '2018-07-19 05:24:55'),
  (13, 1, 3, 5, 2, 'Planeacion', '2018-07-18 00:22:00', '4.00', 3, 3, '2018-07-19 05:23:06', '2018-07-19 05:24:56');

INSERT INTO `activity_estimated_times` (`id`, `active`, `activity_id`, `role_id`, `estimated`, `justification`, `user_mod_id`, `created_at`, `updated_at`) VALUES
  (2, 1, 1, 2, '2.00', 'presupuesto', 1, '2018-04-12 02:26:02', '2018-04-12 02:26:02'),
  (3, 1, 1, 3, '2.00', 'Presupuesto', 1, '2018-04-12 02:26:02', '2018-04-12 02:26:02'),
  (4, 1, 1, 5, '2.00', 'POrque si', 3, '2018-04-12 02:26:02', '2018-07-19 09:12:13'),
  (5, 1, 3, 2, '5.00', 'Presupuesto', 3, '2018-04-12 02:26:02', '2018-04-12 02:26:02'),
  (6, 1, 3, 3, '5.00', 'Presupuesto', 3, '2018-04-12 02:26:02', '2018-04-12 02:26:02'),
  (7, 1, 3, 5, '980.00', 'Presupuesto', 3, '2018-04-12 02:26:02', '2018-04-12 02:26:02'),
  (8, 1, 2, 2, '2.00', 'Presupuesto', 3, '2018-04-12 02:26:02', '2018-04-12 02:26:02'),
  (9, 1, 2, 4, '5.00', 'Presupuesto', 3, '2018-04-12 02:26:02', '2018-04-12 02:26:02'),
  (10, 1, 2, 5, '450.00', 'Presupuesto', 3, '2018-04-12 02:26:02', '2018-07-19 05:32:03'),
  (11, 1, 1, 4, '20.00', 'Presupuesto', 3, '2018-07-19 08:22:59', '2018-07-19 08:22:59');

INSERT INTO `audit_activity_estimated_times` (`audit_id`, `id_parent`, `field`, `old_value`, `new_value`, `user_cre_id`, `created_at`) VALUES
  (2, 2, 'estimated','0.00', '2.00',  1, '2018-04-12 02:26:02'),
  (3, 3, 'estimated','0.00', '2.00',  1, '2018-04-12 02:26:02'),
  (4, 4, 'estimated','0.00', '2.00',  3, '2018-07-19 09:12:13'),
  (5, 5, 'estimated','0.00', '5.00',  3, '2018-04-12 02:26:02'),
  (6, 6, 'estimated','0.00', '5.00',  3, '2018-04-12 02:26:02'),
  (7, 7, 'estimated','0.00', '980.00',3, '2018-04-12 02:26:02'),
  (8, 8, 'estimated','0.00', '2.00',  3, '2018-04-12 02:26:02'),
  (9, 9, 'estimated','0.00', '5.00',  3, '2018-04-12 02:26:02'),
  (10,10,'estimated','0.00', '450.00',3, '2018-07-19 05:32:03'),
  (11,11,'estimated','0.00', '20.00', 3, '2018-07-19 08:22:59');

INSERT INTO `audit_agreements` (`audit_id`, `id_parent`, `field`, `old_value`, `new_value`, `user_cre_id`, `created_at`) VALUES
  (1, 2, 'execution_term', '0', '30', 1, '2018-07-19 00:48:19'),
  (2, 2, 'value', '0.00', '790000.00', 1, '2018-07-19 00:48:19'),
  (3, 2, 'addition_value', '0.00', '200000.00', 1, '2018-07-19 00:48:19'),
  (4, 2, 'pattern_state_id', '11', '14', 1, '2018-07-19 00:48:22'),
  (5, 2, 'percentage', '0.00', '100.00', 1, '2018-07-19 00:48:22'),
  (6, 4, 'execution_term', '0', '60', 1, '2018-07-19 01:00:43'),
  (7, 4, 'value', '0.00', '5700000.00', 1, '2018-07-19 01:00:43'),
  (8, 4, 'pattern_state_id', '11', '13', 1, '2018-07-19 01:00:56'),
  (9, 4, 'percentage', '0.00', '30.00', 1, '2018-07-19 01:00:56'),
  (10, 1, 'value', '0.00', '32600000.00', 1, '2018-04-12 03:00:20'),
  (12, 1, 'pattern_state_id', '11', '12', 1, '2018-04-12 03:00:29'),
  (13, 1, 'percentage', '0.00', '1.00', 1, '2018-04-12 03:00:29'),
  (14, 1, 'justification', NULL, 'Firmado', 1, '2018-04-12 03:00:29'),
  (15, 1, 'pattern_state_id', '12', '13', 1, '2018-04-27 03:04:55'),
  (16, 1, 'percentage', '1.00', '10.00', 1, '2018-04-27 03:04:55'),
  (17, 1, 'justification', 'Firmado', 'Avance visto', 1, '2018-04-27 03:04:55'),
  (18, 1, 'percentage', '10.00', '25.00', 1, '2018-06-02 03:14:34'),
  (19, 1, 'justification', 'Avance visto', 'Se evidencio avance', 1, '2018-06-02 03:14:34'),
  (20, 1, 'percentage', '25.00', '60.00', 1, '2018-07-19 03:16:24'),
  (21, 1, 'justification', 'Se evidencio avance', 'Se revisa la entrega de la primera entrega', 1, '2018-07-19 03:16:24');

INSERT INTO `audit_products` (`audit_id`, `id_parent`, `field`, `old_value`, `new_value`, `user_cre_id`, `created_at`) VALUES
  (1, 1, 'pattern_state_id', '31', '32', 1, '2018-04-12 04:13:44'),
  (2, 1, 'percentage', '0.00', '1.00', 1, '2018-04-12 04:13:44'),
  (3, 1, 'percentage', '1.00', '20.00', 1, '2018-04-27 04:17:16'),
  (4, 1, 'justification', 'Aprobado', 'Primera Muestra de avance', 1, '2018-04-27 04:17:16'),
  (5, 1, 'percentage', '20.00', '50.00', 1, '2018-06-02 04:18:18'),
  (6, 1, 'justification', 'Primera Muestra de avance', 'Segunda muestra de avnace', 1, '2018-06-02 04:18:18'),
  (7, 1, 'pattern_state_id', '32', '33', 1, '2018-07-19 04:20:56'),
  (8, 1, 'percentage', '50.00', '100.00', 1, '2018-07-19 04:20:56'),
  (9, 1, 'justification', 'Segunda muestra de avnace', 'Se recibe la primera entrega', 1, '2018-07-19 04:20:56');

INSERT INTO `pattern_document_types` (`id`, `active`, `pattern_id`, `name`, `user_cre_id`, `user_mod_id`, `created_at`, `updated_at`) VALUES
  (1, 1, 1, 'Contrato', 2, NULL, '2018-08-04 03:19:30', '2018-08-04 03:19:30'),
  (2, 1, 1, 'Propuesta', 2, NULL, '2018-08-04 03:42:34', '2018-08-04 03:42:34');

INSERT INTO `documents` (`id`, `active`, `model_id`, `pattern_document_type_id`, `name`, `keywords`, `comment`, `version`, `user_mod_id`, `created_at`, `updated_at`) VALUES
  (1, 1, 1, 1, 'contrato viveka.pdf', 'pdf,firmado', 'Firmado', 'E40Fhdw8PkT6..nWDqmvMkl.I7u1Fhdn', 2, '2018-08-04 03:21:56', '2018-08-04 03:40:56'),
  (2, 1, 1, 2, 'Propuesta_6.pdf', 'Aprobada', 'Propuesta Aprobada', 'gu2SfDci79_IswnbC9KmiXlwJjIv5i8K', 2, '2018-08-04 03:43:34', '2018-08-04 03:44:09');

INSERT INTO `audit_documents` (`audit_id`, `id_parent`, `field`, `old_value`, `new_value`, `user_cre_id`, `created_at`) VALUES
  (1, 1, 'version', '', 'dw.7g6pQcVvjAFf2pGMsEIAD5EZ957Th', 2, '2018-08-04 03:21:57'),
  (2, 1, 'name', 'Contrato_V2.doc', 'Contrato_V3.doc', 2, '2018-08-04 03:38:33'),
  (3, 1, 'comment', 'Version 2', 'Version 3', 2, '2018-08-04 03:38:33'),
  (4, 1, 'version', 'dw.7g6pQcVvjAFf2pGMsEIAD5EZ957Th', 'bVkvF7GLqiJo0pPXpkBFB0bAPm8lLYO2', 2, '2018-08-04 03:38:33'),
  (5, 1, 'name', 'Contrato_V3.doc', 'contrato viveka.pdf', 2, '2018-08-04 03:40:56'),
  (6, 1, 'keywords', 'word', 'pdf,firmado', 2, '2018-08-04 03:40:56'),
  (7, 1, 'comment', 'Version 3', 'Firmado', 2, '2018-08-04 03:40:56'),
  (8, 1, 'version', 'bVkvF7GLqiJo0pPXpkBFB0bAPm8lLYO2', 'E40Fhdw8PkT6..nWDqmvMkl.I7u1Fhdn', 2, '2018-08-04 03:40:56'),
  (9, 2, 'version', '', '3Vw8SrdfrALTGZULNVFkHAT3DGbUPCUD', 2, '2018-08-04 03:43:34'),
  (10, 2, 'name', 'Propuesta_2.pdf', 'Propuesta_6.pdf', 2, '2018-08-04 03:44:09'),
  (11, 2, 'version', '3Vw8SrdfrALTGZULNVFkHAT3DGbUPCUD', 'gu2SfDci79_IswnbC9KmiXlwJjIv5i8K', 2, '2018-08-04 03:44:09');