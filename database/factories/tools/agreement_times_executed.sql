DROP PROCEDURE IF EXISTS `activity_estimated_times_executed`;
CREATE PROCEDURE `activity_estimated_times_executed`(IN `p_pattern_id` INT unsigned, IN `p_model_id` INT unsigned, IN `p_role_id` INT unsigned,  OUT `total_hours` DECIMAL(6,2) unsigned) NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN
    SET total_hours=0;

    IF p_pattern_id=1 THEN
      SET total_hours = total_hours + (
        SELECT COALESCE(SUM(executed),0)
        FROM agreement_activity_times
        WHERE agreement_id=p_model_id
              AND role_id=p_role_id
      );

      SET total_hours = total_hours + (
        SELECT COALESCE(SUM(executed),0)
        FROM project_activity_times
        WHERE agreement_id=p_model_id
              AND role_id=p_role_id
      );

      SET total_hours = total_hours + (
        SELECT COALESCE(SUM(executed),0)
        FROM product_activity_times
        WHERE agreement_id=p_model_id
              AND role_id=p_role_id
      );
    END IF;

    IF p_pattern_id=2 THEN
      SET total_hours = total_hours + (
        SELECT COALESCE(SUM(executed),0)
        FROM project_activity_times
        WHERE project_id=p_model_id
              AND role_id=p_role_id
      );

      SET total_hours = total_hours + (
        SELECT COALESCE(SUM(executed),0)
        FROM product_activity_times
        WHERE project_id=p_model_id
              AND role_id=p_role_id
      );
    END IF;

    IF p_pattern_id=3 THEN
      SET total_hours = total_hours + (
        SELECT COALESCE(SUM(executed),0)
        FROM product_activity_times
        WHERE product_id=p_model_id
              AND role_id=p_role_id
      );
    END IF;
  END