CREATE OR REPLACE VIEW product_activity_times AS
	SELECT 	products.id AS product_id,
			MAX(projects.id) AS project_id,
			MAX(projects.agreement_id) AS agreement_id,
			activity_executed_times.role_id AS role_id,
			SUM(activity_executed_times.executed) AS executed
	FROM activities
		INNER JOIN products
			ON activities.pattern_id=3
			AND activities.model_id=products.id
			AND activities.active=1
		INNER JOIN projects
			ON projects.id=products.project_id
		INNER JOIN agreement_teams
			ON agreement_teams.agreement_id=projects.agreement_id
			AND agreement_teams.active=1
		INNER JOIN professionals
			ON agreement_teams.professional_id=professionals.id
		INNER JOIN activity_executed_times
			ON activity_executed_times.person_id=professionals.person_id
			AND activity_executed_times.activity_id=activities.id
	GROUP BY products.id, activity_executed_times.role_id;

CREATE OR REPLACE VIEW project_activity_times AS
	SELECT 	projects.id AS project_id,
			MAX(projects.agreement_id) AS agreement_id,
			activity_executed_times.role_id AS role_id,
			SUM(activity_executed_times.executed) AS executed
	FROM activities
		INNER JOIN projects
			ON activities.pattern_id=2
			AND activities.model_id=projects.id
			AND activities.active=1
		INNER JOIN agreement_teams
			ON agreement_teams.agreement_id=projects.agreement_id
			AND agreement_teams.active=1
		INNER JOIN professionals
			ON agreement_teams.professional_id=professionals.id
		INNER JOIN activity_executed_times
			ON activity_executed_times.person_id=professionals.person_id
			AND activity_executed_times.activity_id=activities.id
	GROUP BY projects.id, activity_executed_times.role_id;

CREATE OR REPLACE VIEW agreement_activity_times AS
	SELECT 	activities.model_id AS agreement_id,
			activity_executed_times.role_id AS role_id,
			SUM(activity_executed_times.executed) AS executed
	FROM activities
		INNER JOIN agreement_teams
			ON activities.pattern_id=1
			AND activities.active=1
			AND agreement_teams.agreement_id=activities.model_id
			AND agreement_teams.active=1
		INNER JOIN professionals
			ON agreement_teams.professional_id=professionals.id
		INNER JOIN activity_executed_times
			ON activity_executed_times.person_id=professionals.person_id
			AND activity_executed_times.activity_id=activities.id
	GROUP BY activities.model_id, activity_executed_times.role_id;