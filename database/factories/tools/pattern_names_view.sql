CREATE OR REPLACE VIEW pattern_names AS
  SELECT
    1               AS pattern_id,
    agreements.id   AS model_id,
    agreements.name AS name
  FROM agreements

  UNION

  SELECT
    2             AS pattern_id,
    projects.id   AS model_id,
    projects.name AS name
  FROM projects

  UNION

  SELECT
    3             AS pattern_id,
    products.id   AS model_id,
    products.name AS name
  FROM products

  UNION

  SELECT
    6             AS pattern_id,
    companies.id   AS model_id,
    people.name AS name
  FROM companies
  INNER JOIN people ON people.id = companies.person_id

 UNION

  SELECT
    13             AS pattern_id,
    activity_executed_costs.id   AS model_id,
    CONCAT(activities.name,' - ',activity_executed_costs.description) AS name
  FROM activity_executed_costs
  INNER JOIN activities ON activities.id = activity_executed_costs.activity_id;