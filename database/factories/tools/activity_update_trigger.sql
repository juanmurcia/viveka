SET activity_pattern_id=NEW.pattern_id;
SET activity_model_id=NEW.model_id;

IF activity_pattern_id=3 AND activity_model_id IS NOT NULL THEN
  SET activity_agreement_id = (
    SELECT projects.agreement_id
      FROM products
        INNER JOIN projects
          ON products.project_id = projects.id
             AND products.id = activity_model_id
  );
END IF;

IF activity_pattern_id=2 AND activity_model_id IS NOT NULL THEN
  SET activity_agreement_id = (
    SELECT projects.agreement_id
    FROM projects
    WHERE id=activity_model_id
  );
END IF;

IF activity_pattern_id=1 AND activity_model_id IS NOT NULL THEN
  SET activity_agreement_id = activity_model_id;
END IF;

IF activity_agreement_id IS NOT NULL THEN
  CALL activity_estimated_times_executed_refresh(activity_agreement_id);
END IF;