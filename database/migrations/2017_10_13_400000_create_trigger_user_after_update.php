<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Http\Controllers\TriggerController;

class CreateTriggerUserAfterUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $trigger = new TriggerController();
        $sql = $trigger->make('users', false );

        DB::unprepared("
        CREATE TRIGGER user_after_update AFTER UPDATE ON users
        FOR EACH ROW BEGIN
            $sql
            IF OLD.role_id != NEW.role_id THEN
                UPDATE role_user SET role_id = NEW.role_id WHERE user_id = NEW.id;      
            END IF;
        END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `user_after_update`');
    }
}
