<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatternStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pattern_states', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(TRUE);
            $table->integer('pattern_id')->unsigned();
            $table->string('name');
            $table->timestamps();


            //Claves
            $table->foreign('pattern_id')->references('id')->on('patterns');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pattern_states');
    }
}
