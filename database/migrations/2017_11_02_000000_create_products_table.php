<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(TRUE);
            $table->integer('project_id')->unsigned();
            $table->integer('pattern_state_id')->unsigned()->default(31);
            $table->integer('responsible_id')->unsigned();
            $table->string('name');
            $table->text('description');
            $table->decimal('percentage',6,2)->default(0);
            $table->text('justification')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->decimal('value',20,2)->default(0);
            $table->integer('user_cre_id')->unsigned();
            $table->integer('user_mod_id')->nullable()->unsigned();
            $table->timestamps();
            //Claves
            $table->foreign('pattern_state_id')->references('id')->on('pattern_states');
            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('responsible_id')->references('id')->on('professionals');
            $table->foreign('user_cre_id')->references('id')->on('users');
            $table->foreign('user_mod_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
