<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerUserAfterInsert extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TRIGGER user_after_insert AFTER INSERT ON users
        FOR EACH ROW 
        BEGIN
            INSERT INTO role_user (role_id, user_id) VALUES (NEW.role_id, NEW.id);
            UPDATE people SET user = TRUE, user_mod_id = NEW.user_cre_id WHERE id = NEW.person_id;
        END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `user_after_insert`');
    }
}
