<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityEstimatedTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_estimated_times', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(TRUE);
            $table->integer('activity_id')->unsigned();
            $table->integer('role_id')->unsigned();
            $table->decimal('estimated',6,2)->unsigned()->default(0);
            $table->text('justification');
            $table->integer('user_mod_id')->unsigned()->nullable();
            $table->timestamps();
            //claves
            $table->foreign('activity_id')->references('id')->on('activities');
            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('user_mod_id')->references('id')->on('users');
            //unique
            $table->unique(['activity_id','role_id']);
            //index
            $table->index(['activity_id','role_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_estimated_times');
    }
}
