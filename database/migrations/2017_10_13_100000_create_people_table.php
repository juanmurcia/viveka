<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(TRUE);
            $table->integer('person_document_type_id')->unsigned();
            $table->integer('taxpayer_type_id')->unsigned();
            $table->string('document');
            $table->string('name');
            $table->string('last_name')->nullable();
            $table->string('address')->nullable();
            $table->string('email');
            $table->string('phone')->nullable();
            $table->boolean('user')->default(FALSE)->comment('Campo que nos indica si la persona tiene usaurio registrado');
            $table->integer('user_cre_id')->unsigned();
            $table->integer('user_mod_id')->unsigned()->nullable();
            $table->timestamps();
            //Claves
            $table->foreign('taxpayer_type_id')->references('id')->on('taxpayer_types');
            $table->foreign('person_document_type_id')->references('id')->on('person_document_types');
            $table->foreign('user_cre_id')->references('id')->on('users');
            $table->foreign('user_mod_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
