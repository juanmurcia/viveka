<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(TRUE);
            $table->boolean('seen')->default(FALSE);
            $table->integer('user_id')->unsigned();
            $table->integer('pattern_id')->unsigned()->nullable();
            $table->integer('model_id')->unsigned()->nullable();
            $table->string('name');
            $table->text('description');
            $table->timestamp('notification_date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('user_cre_id')->nullable()->unsigned();
            $table->integer('user_mod_id')->nullable()->unsigned();
            $table->timestamps();
            //Claves

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('pattern_id')->references('id')->on('patterns');
            $table->foreign('user_cre_id')->references('id')->on('users');
            $table->foreign('user_mod_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
