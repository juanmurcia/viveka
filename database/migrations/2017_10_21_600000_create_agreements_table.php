<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgreementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreements', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(TRUE);
            $table->string('number');
            $table->string('name');
            $table->text('object');
            $table->integer('employer_id')->unsigned();
            $table->integer('contractor_id')->unsigned();
            $table->integer('responsible_id')->unsigned();
            $table->integer('pattern_state_id')->unsigned()->default(11);
            $table->decimal('percentage',6,2)->default(0);
            $table->integer('billing_due')->default(0);
            $table->text('justification')->nullable();
            $table->boolean('single_project')->default(false);

            $table->integer('currency_id')->unsigned()->nullable();
            $table->integer('agreement_process_type_id')->unsigned()->nullable();
            $table->integer('agreement_category_id')->unsigned()->nullable();
            $table->integer('agreement_nature_id')->unsigned()->nullable();
            $table->integer('agreement_type_id')->unsigned()->nullable();
            $table->integer('agreement_coverage_id')->unsigned()->nullable();
            $table->integer('agreement_service_id')->unsigned()->nullable();
            $table->integer('department_id')->unsigned()->nullable();

            $table->date('signing_date')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->integer('execution_term')->default(0);
            $table->integer('addition_term')->default(0);
            $table->decimal('value',20,2)->default(0);
            $table->decimal('addition_value',20,2)->default(0);
            $table->decimal('iva_billing',20,2)->default(0);
            $table->decimal('due_billing',20,2)->default(0);
            $table->string('external_url')->nullable();
            $table->integer('user_cre_id')->unsigned();
            $table->integer('user_mod_id')->unsigned()->nullable();
            $table->timestamps();

            //Claves
            $table->foreign('responsible_id')->references('id')->on('professionals');
            $table->foreign('employer_id')->references('id')->on('companies');
            $table->foreign('contractor_id')->references('id')->on('companies');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('agreement_service_id')->references('id')->on('agreement_services');
            $table->foreign('agreement_process_type_id')->references('id')->on('agreement_process_types');
            $table->foreign('agreement_category_id')->references('id')->on('agreement_categories');
            $table->foreign('pattern_state_id')->references('id')->on('pattern_states');
            $table->foreign('agreement_type_id')->references('id')->on('agreement_types');
            $table->foreign('agreement_coverage_id')->references('id')->on('agreement_coverages');
            $table->foreign('agreement_nature_id')->references('id')->on('agreement_natures');
            $table->foreign('department_id')->references('id')->on('departments');
            $table->foreign('user_cre_id')->references('id')->on('users');
            $table->foreign('user_mod_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreements');
    }
}
