<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professionals', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(TRUE);
            $table->integer('person_id')->unsigned()->unique();
            $table->integer('nationality_id')->unsigned();
            $table->date('birth_date');
            $table->string('gender');
            $table->integer('user_cre_id')->unsigned();
            $table->integer('user_mod_id')->unsigned()->nullable();
            $table->timestamps();
            //llaves
            $table->foreign('person_id')->references('id')->on('people');
            $table->foreign('nationality_id')->references('id')->on('countries');
            $table->foreign('user_cre_id')->references('id')->on('users');
            $table->foreign('user_mod_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professionals');
    }
}
