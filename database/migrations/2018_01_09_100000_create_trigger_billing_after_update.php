<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Http\Controllers\TriggerController;

class CreateTriggerBillingAfterUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $trigger = new TriggerController();
        $sql = $trigger->make('billing', false );

        DB::unprepared("
        CREATE TRIGGER billing_after_update AFTER UPDATE ON billings
        FOR EACH ROW         
        BEGIN            
            
            $sql
            
            IF OLD.pattern_state_id = 44 AND NEW.pattern_state_id = 46 THEN
                UPDATE products SET pattern_state_id = 33, user_mod_id = NEW.user_mod_id WHERE id IN (SELECT product_id FROM billing_details WHERE billing_id = NEW.id);
            END IF;
        END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `billing_after_update`');
    }
}
