<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityExecutedTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_executed_times', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(TRUE);
            $table->integer('activity_id')->unsigned();
            $table->integer('person_id')->unsigned();
            $table->integer('role_id')->unsigned();
            $table->text('description');
            $table->dateTime('end_date');
            $table->decimal('executed',6,2)->unsigned()->default(0);
            $table->integer('user_cre_id')->unsigned()->nullable();
            $table->integer('user_mod_id')->unsigned()->nullable();
            $table->timestamps();
            //claves
            $table->foreign('activity_id')->references('id')->on('activities');
            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('person_id')->references('id')->on('people');
            $table->foreign('user_cre_id')->references('id')->on('users');
            $table->foreign('user_mod_id')->references('id')->on('users');
            //index
            $table->index(['activity_id','role_id']);
            $table->index(['person_id','role_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_executed_times');
    }
}
