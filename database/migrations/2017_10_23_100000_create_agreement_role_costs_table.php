<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgreementRoleCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreement_role_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(TRUE);
            $table->integer('role_id')->unsigned();
            $table->integer('agreement_id')->unsigned();
            $table->decimal('value',10,2)->unsigned();
            $table->integer('user_mod_id')->unsigned()->nullable();
            $table->timestamps();
            //claves
            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('agreement_id')->references('id')->on('agreements');
            $table->foreign('user_mod_id')->references('id')->on('users');
            //unique
            $table->unique(['agreement_id','role_id']);
            //index
            $table->index(['agreement_id','role_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreement_role_costs');
    }
}
