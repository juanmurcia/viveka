<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agreement_id')->unsigned();
            $table->string('number')->nullable();
            $table->integer('pattern_state_id')->unsigned();
            $table->dateTime('billing_date');
            $table->dateTime('payment_date')->nullable();
            $table->dateTime('due_date')->nullable();
            $table->integer('value')->default(0);
            $table->integer('taxes')->default(0);
            $table->integer('net_value')->default(0);
            $table->string('comment')->nullable();
            $table->text('documentation')->nullable();
            $table->integer('user_cre_id')->unsigned();
            $table->integer('user_mod_id')->unsigned()->nullable();
            $table->timestamps();
            //Claves
            $table->foreign('agreement_id')->references('id')->on('agreements');
            $table->foreign('pattern_state_id')->references('id')->on('pattern_states');
            $table->foreign('user_cre_id')->references('id')->on('users');
            $table->foreign('user_mod_id')->references('id')->on('users');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billings');
    }
}
