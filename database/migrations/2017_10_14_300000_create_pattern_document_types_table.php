<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatternDocumentTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pattern_document_types', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(TRUE);
            $table->integer('pattern_id')->unsigned();
            $table->string('name');
            $table->integer('user_cre_id')->unsigned()->nullable();
            $table->integer('user_mod_id')->unsigned()->nullable();
            $table->timestamps();
            //Claves
            $table->foreign('pattern_id')->references('id')->on('patterns');
            $table->foreign('user_cre_id')->references('id')->on('users');
            $table->foreign('user_mod_id')->references('id')->on('users');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pattern_document_types');
    }
}
