<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(TRUE);
            $table->string('name');
            $table->string('email')->unique();
            $table->string('role_id');
            $table->string('password');
            $table->integer('person_id')->unsigned()->nullable();
            $table->integer('user_cre_id')->unsigned()->nullable();
            $table->integer('user_mod_id')->unsigned()->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->unique('email','U_email');            
            $table->foreign('user_cre_id')->references('id')->on('users');
            $table->foreign('user_mod_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
