<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgreementTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreement_teams', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(TRUE);
            $table->integer('professional_id')->unsigned();
            $table->integer('agreement_id')->unsigned();
            $table->integer('user_mod_id')->unsigned()->nullable();
            $table->timestamps();
            //claves
            $table->foreign('professional_id')->references('id')->on('professionals');
            $table->foreign('agreement_id')->references('id')->on('agreements');
            $table->foreign('user_mod_id')->references('id')->on('users');
            //unique
            $table->unique(['agreement_id','professional_id']);
            //index
            $table->index(['agreement_id','professional_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreement_teams');
    }
}
