<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(TRUE);
            $table->integer('pattern_id')->unsigned();
            $table->integer('model_id')->unsigned();
            $table->integer('pattern_state_id')->unsigned()->default(41);
            $table->string('name');
            $table->text('description')->nullable();
            $table->decimal('percentage',6,2)->default(0);
            $table->text('justification')->nullable();
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->integer('user_cre_id')->unsigned();
            $table->integer('user_mod_id')->unsigned()->nullable();
            $table->timestamps();
            //Claves
            $table->foreign('pattern_id')->references('id')->on('patterns');
            $table->foreign('pattern_state_id')->references('id')->on('pattern_states');
            $table->foreign('user_cre_id')->references('id')->on('users');
            $table->foreign('user_mod_id')->references('id')->on('users');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
