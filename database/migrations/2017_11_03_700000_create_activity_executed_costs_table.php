<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityExecutedCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_executed_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(TRUE);
            $table->integer('activity_id')->unsigned();
            $table->integer('activity_cost_subcategory_id')->unsigned();
            $table->integer('person_id')->unsigned();
            $table->decimal('executed',20,2)->default(0);
            $table->text('description');
            $table->dateTime('end_date');
            $table->integer('user_cre_id')->unsigned()->nullable();
            $table->integer('user_mod_id')->unsigned()->nullable();
            $table->timestamps();
            //claves
            $table->foreign('activity_id')->references('id')->on('activities');
            $table->foreign('activity_cost_subcategory_id')->references('id')->on('activity_cost_subcategories');
            $table->foreign('person_id')->references('id')->on('people');
            $table->foreign('user_cre_id')->references('id')->on('users');
            $table->foreign('user_mod_id')->references('id')->on('users');
            //index
            $table->index(['activity_id','activity_cost_subcategory_id'])->name('idx_activity_cost_type');
            $table->index(['person_id','activity_cost_subcategory_id'])->name('idx_person_cost_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_executed_costs');
    }
}
