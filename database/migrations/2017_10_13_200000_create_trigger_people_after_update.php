<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Http\Controllers\TriggerController;

class CreateTriggerPeopleAfterUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $trigger = new TriggerController();
        $sql = $trigger->make('people', false );

        DB::unprepared("
        CREATE TRIGGER people_after_update AFTER UPDATE ON people
        FOR EACH ROW         
        BEGIN
            DECLARE s_name varchar(191);
            
            $sql
            
            IF(NEW.user IS TRUE)THEN
                IF OLD.name != NEW.name OR OLD.last_name != NEW.last_name THEN
                    SET s_name = CONCAT(SUBSTRING_INDEX(NEW.name, ' ', 1),' ', SUBSTRING_INDEX(NEW.last_name, ' ', 1));
                    
                    UPDATE users SET name = s_name, user_mod_id = NEW.user_mod_id, updated_at = now() WHERE person_id = NEW.id;      
                END IF;

                IF OLD.email != NEW.email THEN                                        
                    UPDATE users SET email = NEW.email, user_mod_id = NEW.user_mod_id, updated_at = now() WHERE person_id = NEW.id;      
                END IF;
            END IF;
        END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `people_after_update`');
    }
}
