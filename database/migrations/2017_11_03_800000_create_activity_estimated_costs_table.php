<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityEstimatedCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_estimated_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(TRUE);
            $table->integer('activity_id')->unsigned();
            $table->integer('activity_cost_category_id')->unsigned();
            $table->decimal('estimated',20,2)->default(0);
            $table->text('justification');
            $table->integer('user_mod_id')->unsigned()->nullable();
            $table->timestamps();
            //claves
            $table->foreign('activity_id')->references('id')->on('activities');
            $table->foreign('activity_cost_category_id')->references('id')->on('activity_cost_categories');
            $table->foreign('user_mod_id')->references('id')->on('users');
            //unique
            $table->unique(['activity_id','activity_cost_category_id'])->name('unique_activity_cost_category');
            //index
            $table->index(['activity_id','activity_cost_category_id'])->name('idx_activity_cost_category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_estimated_costs');
    }
}
