<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Http\Controllers\TriggerController;

class CreateTriggerBillingDetailsAfterInsertDelete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
        CREATE TRIGGER billing_details_after_insert AFTER INSERT ON billing_details
        FOR EACH ROW         
        BEGIN                        
            DECLARE i_balance INT DEFAULT 0;
            DECLARE i_iva INT DEFAULT 0;
        
            SELECT iva_billing INTO i_iva FROM agreements WHERE id IN (
                SELECT agreement_id FROM billings WHERE id = NEW.billing_id
            );
            
            UPDATE products SET pattern_state_id = 34, user_mod_id = NEW.user_cre_id WHERE id = NEW.product_id;
        
            SELECT COALESCE(SUM(value),0) INTO i_balance FROM billing_details WHERE billing_id = NEW.billing_id;
        
            UPDATE billings SET 
                value = i_balance,
                taxes = (i_balance * i_iva)/100,
                net_value = value - taxes
            WHERE id = NEW.billing_id;
        END
        ");

        DB::unprepared("
        CREATE TRIGGER billing_details_before_delete BEFORE DELETE ON billing_details
        FOR EACH ROW         
        BEGIN                        
            DECLARE i_balance INT DEFAULT 0;
            DECLARE i_iva INT DEFAULT 0;
        
            SELECT iva_billing INTO i_iva FROM agreements WHERE id IN (
                SELECT agreement_id FROM billings WHERE id = OLD.billing_id
            );
            
            UPDATE products SET pattern_state_id = 33, user_mod_id = OLD.user_mod_id WHERE id = OLD.product_id; 
        
            SELECT COALESCE(SUM(value),0) INTO i_balance FROM billing_details WHERE billing_id = OLD.billing_id AND product_id != OLD.product_id;
        
            UPDATE billings SET 
                value = i_balance,
                taxes = (i_balance * i_iva)/100,
                net_value = value - taxes
            WHERE id = OLD.billing_id;
        END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `billing_details_after_insert`');
        DB::unprepared('DROP TRIGGER `billing_details_before_delete`');
    }
}
