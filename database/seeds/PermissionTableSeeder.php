<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = 'database/factories/tools/permission.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Permission table seeded!');

        $path = 'database/factories/tools/permission_role.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('PermissionRole table seeded!');
    }
}
