<?php

use Illuminate\Database\Seeder;
use App\Models\ActivityCostCategory;

class ActivityCostTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $activity_cost_type = new ActivityCostCategory();
        $activity_cost_type->name = 'Transporte';
        $activity_cost_type->save();
        //
        $activity_cost_type = new ActivityCostCategory();
        $activity_cost_type->name = 'Teléfono, Papeleria, Representación';
        $activity_cost_type->save();
        //
        $activity_cost_type = new ActivityCostCategory();
        $activity_cost_type->name = 'Gasto Legal';
        $activity_cost_type->save();
        //
        $activity_cost_type = new ActivityCostCategory();
        $activity_cost_type->name = 'Subcontratista';
        $activity_cost_type->save();
        //
        $activity_cost_type = new ActivityCostCategory();
        $activity_cost_type->name = 'Otro';
        $activity_cost_type->save();
    }
}