<?php

use Illuminate\Database\Seeder;
use App\Models\SystemParameter;

class SystemParameterTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $system_parameter = new SystemParameter();
        $system_parameter->id = '1';
        $system_parameter->name = 'IVA Facturación';
        $system_parameter->value = '19';
        $system_parameter->save();
    }
}
