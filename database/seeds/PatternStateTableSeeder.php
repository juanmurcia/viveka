<?php

use Illuminate\Database\Seeder;
use App\Models\PatternState;

class PatternStateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** Agreement */
        $agreement_pattern_id = App\Models\Agreement::$pattern_id;

        $pattern_state = new PatternState();
        $pattern_state->id = 11;
        $pattern_state->name = 'Pendiente';
        $pattern_state->pattern_id = $agreement_pattern_id;
        $pattern_state->save();

        $pattern_state = new PatternState();
        $pattern_state->id = 12;
        $pattern_state->name = 'Firmado';
        $pattern_state->pattern_id = $agreement_pattern_id;
        $pattern_state->save();

        $pattern_state = new PatternState();
        $pattern_state->id = 13;
        $pattern_state->name = 'Ejecución';
        $pattern_state->pattern_id = $agreement_pattern_id;
        $pattern_state->save();

        $pattern_state = new PatternState();
        $pattern_state->id = 14;
        $pattern_state->name = 'Liquidación';
        $pattern_state->pattern_id = $agreement_pattern_id;
        $pattern_state->save();

        /** Project */
        $project_pattern_id = App\Models\Project::$pattern_id;

        $pattern_state = new PatternState();
        $pattern_state->id = 21;
        $pattern_state->name = 'Programado';
        $pattern_state->pattern_id = $project_pattern_id;
        $pattern_state->save();

        $pattern_state = new PatternState();
        $pattern_state->id = 22;
        $pattern_state->name = 'Iniciado';
        $pattern_state->pattern_id = $project_pattern_id;
        $pattern_state->save();

        $pattern_state = new PatternState();
        $pattern_state->id = 23;
        $pattern_state->name = 'Finalizado';
        $pattern_state->pattern_id = $project_pattern_id;
        $pattern_state->save();

        /** Product */
        $product_pattern_id = App\Models\Product::$pattern_id;

        $pattern_state = new PatternState();
        $pattern_state->id = 31;
        $pattern_state->name = 'Programado';
        $pattern_state->pattern_id = $product_pattern_id;
        $pattern_state->save();

        $pattern_state = new PatternState();
        $pattern_state->id = 32;
        $pattern_state->name = 'Iniciado';
        $pattern_state->pattern_id = $product_pattern_id;
        $pattern_state->save();

        $pattern_state = new PatternState();
        $pattern_state->id = 33;
        $pattern_state->name = 'Finalizado';
        $pattern_state->pattern_id = $product_pattern_id;
        $pattern_state->save();

        $pattern_state = new PatternState();
        $pattern_state->id = 34;
        $pattern_state->name = 'Facturado';
        $pattern_state->pattern_id = $product_pattern_id;
        $pattern_state->save();

        /** Activity */
        $activity_pattern_id = App\Models\Activity::$pattern_id;

        $pattern_state = new PatternState();
        $pattern_state->id = 41;
        $pattern_state->name = 'Programada';
        $pattern_state->pattern_id = $activity_pattern_id;
        $pattern_state->save();

        $pattern_state = new PatternState();
        $pattern_state->id = 42;
        $pattern_state->name = 'Iniciada';
        $pattern_state->pattern_id = $activity_pattern_id;
        $pattern_state->save();

        $pattern_state = new PatternState();
        $pattern_state->id = 43;
        $pattern_state->name = 'Finalizada';
        $pattern_state->pattern_id = $activity_pattern_id;
        $pattern_state->save();

        /** Billing */
        $activity_pattern_id = App\Models\Billing::$pattern_id;

        $pattern_state = new PatternState();
        $pattern_state->id = 44;
        $pattern_state->name = 'Generada';
        $pattern_state->pattern_id = $activity_pattern_id;
        $pattern_state->save();

        $pattern_state = new PatternState();
        $pattern_state->id = 45;
        $pattern_state->name = 'Cerrada';
        $pattern_state->pattern_id = $activity_pattern_id;
        $pattern_state->save();

        $pattern_state = new PatternState();
        $pattern_state->id = 46;
        $pattern_state->name = 'Anulada';
        $pattern_state->pattern_id = $activity_pattern_id;
        $pattern_state->save();

        $pattern_state = new PatternState();
        $pattern_state->id = 47;
        $pattern_state->name = 'Pagada';
        $pattern_state->pattern_id = $activity_pattern_id;
        $pattern_state->save();
    }
}

