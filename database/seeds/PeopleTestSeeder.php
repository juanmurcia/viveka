<?php

use Illuminate\Database\Seeder;
use App\Models\Person;

class PeopleTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * php artisan db:seed --class=PeopleTestSeeder
     * @return void
     */
    public function run()
    {
        factory(Person::class, 10000)->create();
    }
}