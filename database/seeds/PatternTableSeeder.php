<?php

use Illuminate\Database\Seeder;
use App\Models\Pattern;

class PatternTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $pattern = new Pattern();
        $pattern->id=1;
        $pattern->name = 'Contrato';
        $pattern->model = 'agreement';
        $pattern->save();
        //
        $pattern = new Pattern();
        $pattern->id=2;
        $pattern->name = 'Proyecto';
        $pattern->model = 'project';
        $pattern->save();
        //
        $pattern = new Pattern();
        $pattern->id=3;
        $pattern->name = 'Producto';
        $pattern->model = 'product';
        $pattern->save();
        //
        $pattern = new Pattern();
        $pattern->id=4;
        $pattern->name = 'Actividad';
        $pattern->model = 'activity';
        $pattern->save();
        //
        $pattern = new Pattern();
        $pattern->id=5;
        $pattern->name = 'Persona';
        $pattern->model = 'person';
        $pattern->save();
        //
        $pattern = new Pattern();
        $pattern->id=6;
        $pattern->name = 'Empresa';
        $pattern->model = 'company';
        $pattern->save();
        //
        $pattern = new Pattern();
        $pattern->id=7;
        $pattern->name = 'Profesional';
        $pattern->model = 'professional';
        $pattern->save();
        //
        $pattern = new Pattern();
        $pattern->id=8;
        $pattern->name = 'Factura';
        $pattern->model = 'billing';
        $pattern->save();
        //
        $pattern = new Pattern();
        $pattern->id=9;
        $pattern->name = 'Usuarios';
        $pattern->model = 'user';
        $pattern->save();
        //
        $pattern = new Pattern();
        $pattern->id=10;
        $pattern->name = 'Parametros Sistema';
        $pattern->model = 'system_parameter';
        $pattern->save();
        //
        $pattern = new Pattern();
        $pattern->id=11;
        $pattern->name = 'Documentos';
        $pattern->model = 'document';
        $pattern->save();
        //
        $pattern = new Pattern();
        $pattern->id=12;
        $pattern->name = 'Notificaciones';
        $pattern->model = 'notification';
        $pattern->save();
        //
        $pattern = new Pattern();
        $pattern->id=13;
        $pattern->name = 'Categoría Costo Actividad';
        $pattern->model = 'activity_cost_category';
        $pattern->save();
    }
}
