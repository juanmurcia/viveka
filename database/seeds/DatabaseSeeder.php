<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * factory(App\Models\Person::class())->create();
     * @return void
     */
    public function run()
    {
        $this->call([
            PatternTableSeeder::class,
            PatternStateTableSeeder::class,
            RoleTableSeeder::class,
            PersonDocumentTypeTableSeeder::class,
            PermissionTableSeeder::class,
            TaxpayerTypeTableSeed::class,
            UserTableSeeder::class,
            ActivityCostTypeTableSeeder::class,
            PeopleTestSeeder::class
        ]);

        $path = 'database/factories/tools/countries.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Countries table seeded!');

        $path = 'database/factories/tools/currency.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Currencies table seeded!');

        $path = 'database/factories/tools/department.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Departments table seeded!');

        $path = 'database/factories/tools/agreement.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Agreements table seeded!');

        $path = 'database/factories/tools/people.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('People table seeded!');

        $path = 'database/factories/tools/professional.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Professionals table seeded!');

        $path = 'database/factories/tools/companies.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Companies table seeded!');

        $path = 'database/factories/tools/pattern_document_types.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Tipo documento table seeded!');

    }
}