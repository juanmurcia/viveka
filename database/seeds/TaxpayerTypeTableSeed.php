<?php

use Illuminate\Database\Seeder;
use App\Models\TaxpayerType;

class TaxpayerTypeTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $taxpayer_type = new taxpayerType();
        $taxpayer_type->id = '1';
        $taxpayer_type->name = 'Gran Contribuyente';
        $taxpayer_type->save();

        $taxpayer_type = new taxpayerType();
        $taxpayer_type->id = '2';
        $taxpayer_type->name = 'Empresa del Estado';
        $taxpayer_type->save();

        $taxpayer_type = new taxpayerType();
        $taxpayer_type->id = '3';
        $taxpayer_type->name = 'Regimen Común';
        $taxpayer_type->save();

        $taxpayer_type = new taxpayerType();
        $taxpayer_type->id = '4';
        $taxpayer_type->name = 'Regimnen Simplificado';
        $taxpayer_type->save();

        $taxpayer_type = new taxpayerType();
        $taxpayer_type->id = '5';
        $taxpayer_type->name = 'Regimnen Simplificado No Residente';
        $taxpayer_type->save();
    }
}
