<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'id'=>1,
            'name'=>'Administrador',
            'slug'=>'admin',
            'special'=>'all-access',
        ]);

        Role::create([
            'id'=>2,
            'name'=>'Gerente',
            'slug'=>'manager',
        ]);

        Role::create([
            'id'=>3,
            'name'=>'Director',
            'slug'=>'director',
        ]);

        Role::create([
            'id'=>4,
            'name'=>'Coordinador',
            'slug'=>'coordinator',
        ]);

        Role::create([
            'id'=>5,
            'name'=>'Analista',
            'slug'=>'analyst',
        ]);

        Role::create([
            'id'=>6,
            'name'=>'Asistente',
            'slug'=>'assistant',
        ]);
    }
}
