<?php

use Illuminate\Database\Seeder;
use App\Models\PersonDocumentType;

class PersonDocumentTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $document_type = new PersonDocumentType();
        $document_type->name = 'Cédula de Ciudadania';
        $document_type->slug = 'C.C';
        $document_type->save();
        //
        $document_type = new PersonDocumentType();
        $document_type->name = 'NIT';
        $document_type->slug = 'NIT';
        $document_type->save();
        //
        $document_type = new PersonDocumentType();
        $document_type->name = 'Cédula Extranjería';
        $document_type->slug = 'C.E';
        $document_type->save();
        //
        $document_type = new PersonDocumentType();
        $document_type->name = 'Tarjeta de Identidad';
        $document_type->slug = 'T.I';
        $document_type->save();
        //
        $document_type = new PersonDocumentType();
        $document_type->name = 'Pasaporte';
        $document_type->slug = 'P.P';
        $document_type->save();
    }
}
